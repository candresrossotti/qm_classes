﻿Imports System.Windows.Forms
Public Class AvisoGeneral2
    Private Sub AvisoGeneral2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Title.Text = avisoTitulo
        LblText.Text = avisoStr
        CenterObject(LblText)
        CenterObject(Title)
        CenterObject(BtnPanel)
    End Sub

    Private Sub BtnSI_Click(sender As Object, e As EventArgs) Handles BtnSI.Click
        DialogResult = DialogResult.Yes
    End Sub

    Private Sub BtnNO_Click(sender As Object, e As EventArgs) Handles BtnNO.Click
        DialogResult = DialogResult.No
    End Sub
End Class