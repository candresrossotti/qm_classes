﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AvisoGeneral2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Guna2BorderlessForm1 = New Guna.UI2.WinForms.Guna2BorderlessForm(Me.components)
        Me.TitlePanel = New Guna.UI2.WinForms.Guna2Panel()
        Me.Title = New System.Windows.Forms.Label()
        Me.Guna2ControlBox1 = New Guna.UI2.WinForms.Guna2ControlBox()
        Me.LblText = New System.Windows.Forms.Label()
        Me.Guna2DragControl1 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.Guna2DragControl2 = New Guna.UI2.WinForms.Guna2DragControl(Me.components)
        Me.BtnPanel = New System.Windows.Forms.Panel()
        Me.BtnNO = New Guna.UI2.WinForms.Guna2Button()
        Me.BtnSI = New Guna.UI2.WinForms.Guna2Button()
        Me.TitlePanel.SuspendLayout()
        Me.BtnPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'Guna2BorderlessForm1
        '
        Me.Guna2BorderlessForm1.AnimateWindow = True
        Me.Guna2BorderlessForm1.ContainerControl = Me
        Me.Guna2BorderlessForm1.ResizeForm = False
        '
        'TitlePanel
        '
        Me.TitlePanel.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.TitlePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(34, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.TitlePanel.Controls.Add(Me.Title)
        Me.TitlePanel.Controls.Add(Me.Guna2ControlBox1)
        Me.TitlePanel.Location = New System.Drawing.Point(0, 0)
        Me.TitlePanel.Name = "TitlePanel"
        Me.TitlePanel.ShadowDecoration.Parent = Me.TitlePanel
        Me.TitlePanel.Size = New System.Drawing.Size(500, 38)
        Me.TitlePanel.TabIndex = 54
        '
        'Title
        '
        Me.Title.AutoSize = True
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.Font = New System.Drawing.Font("Segoe UI", 16.0!)
        Me.Title.ForeColor = System.Drawing.Color.White
        Me.Title.Location = New System.Drawing.Point(232, 4)
        Me.Title.Name = "Title"
        Me.Title.Size = New System.Drawing.Size(67, 30)
        Me.Title.TabIndex = 2
        Me.Title.Text = "Titulo"
        '
        'Guna2ControlBox1
        '
        Me.Guna2ControlBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Guna2ControlBox1.BackColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.FillColor = System.Drawing.Color.Transparent
        Me.Guna2ControlBox1.HoverState.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.IconColor = System.Drawing.Color.White
        Me.Guna2ControlBox1.Location = New System.Drawing.Point(452, 3)
        Me.Guna2ControlBox1.Name = "Guna2ControlBox1"
        Me.Guna2ControlBox1.ShadowDecoration.Parent = Me.Guna2ControlBox1
        Me.Guna2ControlBox1.Size = New System.Drawing.Size(45, 29)
        Me.Guna2ControlBox1.TabIndex = 0
        Me.Guna2ControlBox1.UseTransparentBackground = True
        '
        'LblText
        '
        Me.LblText.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.LblText.ForeColor = System.Drawing.Color.White
        Me.LblText.Location = New System.Drawing.Point(12, 60)
        Me.LblText.Name = "LblText"
        Me.LblText.Size = New System.Drawing.Size(476, 70)
        Me.LblText.TabIndex = 55
        Me.LblText.Text = "Texto"
        Me.LblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Guna2DragControl1
        '
        Me.Guna2DragControl1.TargetControl = Me.Title
        '
        'Guna2DragControl2
        '
        Me.Guna2DragControl2.TargetControl = Me.TitlePanel
        '
        'BtnPanel
        '
        Me.BtnPanel.Controls.Add(Me.BtnNO)
        Me.BtnPanel.Controls.Add(Me.BtnSI)
        Me.BtnPanel.Location = New System.Drawing.Point(135, 148)
        Me.BtnPanel.Name = "BtnPanel"
        Me.BtnPanel.Size = New System.Drawing.Size(228, 40)
        Me.BtnPanel.TabIndex = 58
        '
        'BtnNO
        '
        Me.BtnNO.Animated = True
        Me.BtnNO.BorderColor = System.Drawing.Color.White
        Me.BtnNO.BorderRadius = 5
        Me.BtnNO.BorderThickness = 1
        Me.BtnNO.CheckedState.Parent = Me.BtnNO
        Me.BtnNO.CustomImages.Parent = Me.BtnNO
        Me.BtnNO.DisabledState.Parent = Me.BtnNO
        Me.BtnNO.FillColor = System.Drawing.Color.Transparent
        Me.BtnNO.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.BtnNO.ForeColor = System.Drawing.Color.White
        Me.BtnNO.HoverState.Parent = Me.BtnNO
        Me.BtnNO.Location = New System.Drawing.Point(117, 0)
        Me.BtnNO.Name = "BtnNO"
        Me.BtnNO.ShadowDecoration.Parent = Me.BtnNO
        Me.BtnNO.Size = New System.Drawing.Size(111, 40)
        Me.BtnNO.TabIndex = 59
        Me.BtnNO.Text = "No"
        '
        'BtnSI
        '
        Me.BtnSI.Animated = True
        Me.BtnSI.BorderColor = System.Drawing.Color.White
        Me.BtnSI.BorderRadius = 5
        Me.BtnSI.BorderThickness = 1
        Me.BtnSI.CheckedState.Parent = Me.BtnSI
        Me.BtnSI.CustomImages.Parent = Me.BtnSI
        Me.BtnSI.DisabledState.Parent = Me.BtnSI
        Me.BtnSI.FillColor = System.Drawing.Color.Transparent
        Me.BtnSI.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.BtnSI.ForeColor = System.Drawing.Color.White
        Me.BtnSI.HoverState.Parent = Me.BtnSI
        Me.BtnSI.Location = New System.Drawing.Point(0, 0)
        Me.BtnSI.Name = "BtnSI"
        Me.BtnSI.ShadowDecoration.Parent = Me.BtnSI
        Me.BtnSI.Size = New System.Drawing.Size(111, 40)
        Me.BtnSI.TabIndex = 58
        Me.BtnSI.Text = "Si"
        '
        'AvisoGeneral2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(49, Byte), Integer), CType(CType(66, Byte), Integer), CType(CType(82, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(500, 200)
        Me.Controls.Add(Me.BtnPanel)
        Me.Controls.Add(Me.TitlePanel)
        Me.Controls.Add(Me.LblText)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "AvisoGeneral2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AvisoGeneral2"
        Me.TitlePanel.ResumeLayout(False)
        Me.TitlePanel.PerformLayout()
        Me.BtnPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Guna2BorderlessForm1 As Guna.UI2.WinForms.Guna2BorderlessForm
    Friend WithEvents TitlePanel As Guna.UI2.WinForms.Guna2Panel
    Friend WithEvents Title As System.Windows.Forms.Label
    Friend WithEvents Guna2ControlBox1 As Guna.UI2.WinForms.Guna2ControlBox
    Friend WithEvents LblText As System.Windows.Forms.Label
    Friend WithEvents Guna2DragControl1 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents Guna2DragControl2 As Guna.UI2.WinForms.Guna2DragControl
    Friend WithEvents BtnPanel As System.Windows.Forms.Panel
    Friend WithEvents BtnNO As Guna.UI2.WinForms.Guna2Button
    Friend WithEvents BtnSI As Guna.UI2.WinForms.Guna2Button
End Class
