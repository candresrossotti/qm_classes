﻿Imports System.Windows.Forms

Public Class AvisoGeneral
    Private Sub AvisoGeneral_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Title.Text = avisoTitulo
        LblText.Text = avisoStr
        CenterObject(LblText)
        CenterObject(Title)
        CenterObject(BtnAceptar)
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles BtnAceptar.Click
        DialogResult = DialogResult.OK
    End Sub
End Class