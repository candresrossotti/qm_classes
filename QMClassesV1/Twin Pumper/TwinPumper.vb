﻿Imports System.Threading
Public Class TwinPumper
    Inherits TwinPumperConfig

    Private WithEvents TmrRead As New Forms.Timer()

    Private TPData As TPData

    ''Constructor
    Private Sub New(ByRef TP As TPData)
        TPData = TP
        TmrRead.Interval = 1000
    End Sub

    ''SINGLETON
    Private Shared ITwinPumper
    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton
    Public Shared Function GetInstancia(ByVal TP As TPData)
        If ITwinPumper Is Nothing Then
            ITwinPumper = New TwinPumper(TP)
        End If
        Return ITwinPumper
    End Function

#Region "Propiedades"
    Private Shared _valuesdict As New Dictionary(Of String, Double) From {
        {"VIATRAN1_SENSOR_IN", 0},
        {"VIATRAN1_SENSOR_IN_RAW", 0},
        {"VIATRAN1_ALARM_ACTIVE", 0},
        {"VIATRAN1_SCALED_ALARM_ACTIVE", 0},
        {"VIATRAN2_SENSOR_IN", 0},
        {"VIATRAN2_SENSOR_IN_RAW", 0},
        {"VIATRAN2_ALARM_ACTIVE", 0},
        {"VIATRAN2_SCALED_ALARM_ACTIVE", 0},
        {"TEMP_ACEITE_1_SENSOR_IN", 0},
        {"TEMP_ACEITE_1_SENSOR_IN_RAW", 0},
        {"TEMP_ACEITE_1_ALARM_ACTIVE", 0},
        {"TEMP_ACEITE_1_SCALED_ALARM_ACTIVE", 0},
        {"TEMP_ACEITE_2_SENSOR_IN", 0},
        {"TEMP_ACEITE_2_SENSOR_IN_RAW", 0},
        {"TEMP_ACEITE_2_ALARM_ACTIVE", 0},
        {"TEMP_ACEITE_2_SCALED_ALARM_ACTIVE", 0},
        {"SENSOR_NIVEL_1_%", 0},
        {"SENSOR_NIVEL_1_MM", 0},
        {"SENSOR_NIVEL_2_%", 0},
        {"SENSOR_NIVEL_2_MM", 0},
        {"RPM_SUC_1", 0},
        {"RPM_SUC_2", 0},
        {"RPM_TRANS_1", 0},
        {"RPM_TRANS_2", 0},
        {"PRES_SUC_1_IN", 0},
        {"PRES_SUC_1_SENSOR_IN_RAW", 0},
        {"PRES_SUC_1_SENSOR_ALARM_ACTIVE", 0},
        {"PRES_SUC_1_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"PRES_SUC_2_IN", 0},
        {"PRES_SUC_2_SENSOR_IN_RAW", 0},
        {"PRES_SUC_2_SENSOR_ALARM_ACTIVE", 0},
        {"PRES_SUC_2_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"PRES_ACEITE_1_IN", 0},
        {"PRES_ACEITE_1_SENSOR_IN_RAW", 0},
        {"PRES_ACEITE_1_SENSOR_ALARM_ACTIVE", 0},
        {"PRES_ACEITE_1_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"PRES_ACEITE_2_IN", 0},
        {"PRES_ACEITE_2_SENSOR_IN_RAW", 0},
        {"PRES_ACEITE_2_SENSOR_ALARM_ACTIVE", 0},
        {"PRES_ACEITE_2_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"LAS1_IN", 0},
        {"LAS1_SENSOR_IN_RAW", 0},
        {"LAS1_SENSOR_ALARM_ACTIVE", 0},
        {"LAS1_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"LAS2_IN", 0},
        {"LAS2_SENSOR_IN_RAW", 0},
        {"LAS2_SENSOR_ALARM_ACTIVE", 0},
        {"LAS2_SENSOR_SCALED_ALARM_ACTIVE", 0},
        {"LAS1_POTE_CABINA", 0},
        {"LAS2_POTE_CABINA", 0},
        {"SUC_1_POTE_CABINA", 0},
        {"SUC_2_POTE_CABINA", 0},
        {"MIXER_POTE_CABINA", 0},
        {"GraseraAutomatica_1_OUT", 0},
        {"GraseraAutomatica_2_OUT", 0},
        {"EstadoRPM_1", 0},
        {"EstadoRPM_2", 0},
        {"ParadaSobrePresion_1", 0},
        {"ParadaSobrePresion_2", 0},
        {"CAUDAL_1", 0},
        {"CAUDAL_2", 0},
        {"TRIP", 0}
    }
    Public Shared Property TwinPumperValues() As Dictionary(Of String, Double)
        Get
            Return _valuesdict
        End Get
        Set(value As Dictionary(Of String, Double))
            If value IsNot _valuesdict Then
                _valuesdict = value
            End If
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Se inicia la lectura de informacion.
    ''' </summary>
    Public Sub StartRead()
        TmrRead.Enabled = True
        TmrRead.Start()
    End Sub


    ''' <summary>
    ''' Se finaliza la conexion al fracturador
    ''' </summary>
    Public Sub StopRead()
        TmrRead.Stop()
        TmrRead.Enabled = False
    End Sub

    ''' <summary>
    ''' Timer elapsed.
    ''' </summary>
    Private Sub TmrReadTick(sender As Object, e As EventArgs) Handles TmrRead.Tick
        Dim ReadThread As New Task(AddressOf ReadThreadDo)
        ReadThread.Start()
    End Sub

    ''' <summary>
    ''' Sub para asignar los datos leidos.
    ''' </summary>
    Private Sub ReadThreadDo()
        Dim readResult As Integer() = MoxaRead(0, 62)
        If readResult.Length() = 62 Then
            TPData.TPStr.Item("VIATRAN1_SENSOR_IN") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN1_SENSOR_IN_StartAddress"))
            TPData.TPStr.Item("VIATRAN1_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN1_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("VIATRAN1_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN1_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("VIATRAN1_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN1_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("VIATRAN2_SENSOR_IN") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN2_SENSOR_IN_StartAddress"))
            TPData.TPStr.Item("VIATRAN2_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN2_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("VIATRAN2_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN2_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("VIATRAN2_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("VIATRAN2_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_1_SENSOR_IN") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_1_SENSOR_IN_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_1_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_1_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_1_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_1_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_1_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_1_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_2_SENSOR_IN") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_2_SENSOR_IN_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_2_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_2_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_2_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_2_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("TEMP_ACEITE_2_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("TEMP_ACEITE_2_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("SENSOR_NIVEL_1_%") = readResult(TPMoxaReceiveStartAddress.Item("SENSOR_NIVEL_1_%_StartAddress"))
            TPData.TPStr.Item("SENSOR_NIVEL_1_MM") = readResult(TPMoxaReceiveStartAddress.Item("SENSOR_NIVEL_1_MM_StartAddress"))
            TPData.TPStr.Item("SENSOR_NIVEL_2_%") = readResult(TPMoxaReceiveStartAddress.Item("SENSOR_NIVEL_2_%_StartAddress"))
            TPData.TPStr.Item("SENSOR_NIVEL_2_MM") = readResult(TPMoxaReceiveStartAddress.Item("SENSOR_NIVEL_2_MM_StartAddress"))
            TPData.TPStr.Item("RPM_SUC_1") = readResult(TPMoxaReceiveStartAddress.Item("RPM_SUC_1_StartAddress"))
            TPData.TPStr.Item("RPM_SUC_2") = readResult(TPMoxaReceiveStartAddress.Item("RPM_SUC_2_StartAddress"))
            TPData.TPStr.Item("RPM_TRANS_1") = readResult(TPMoxaReceiveStartAddress.Item("RPM_TRANS_1_StartAddress"))
            TPData.TPStr.Item("RPM_TRANS_2") = readResult(TPMoxaReceiveStartAddress.Item("RPM_TRANS_2_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_1_IN") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_1_IN_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_1_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_1_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_1_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_1_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_1_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_1_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_2_IN") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_2_IN_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_2_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_2_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_2_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_2_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_SUC_2_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_SUC_2_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_1_IN") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_1_IN_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_1_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_1_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_1_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_1_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_1_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_1_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_2_IN") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_2_IN_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_2_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_2_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_2_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_2_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("PRES_ACEITE_2_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("PRES_ACEITE_2_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("LAS1_IN") = readResult(TPMoxaReceiveStartAddress.Item("LAS1_IN_StartAddress"))
            TPData.TPStr.Item("LAS1_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("LAS1_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("LAS1_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("LAS1_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("LAS1_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("LAS1_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("LAS2_IN") = readResult(TPMoxaReceiveStartAddress.Item("LAS2_IN_StartAddress"))
            TPData.TPStr.Item("LAS2_SENSOR_IN_RAW") = readResult(TPMoxaReceiveStartAddress.Item("LAS2_SENSOR_IN_RAW_StartAddress"))
            TPData.TPStr.Item("LAS2_SENSOR_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("LAS2_SENSOR_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("LAS2_SENSOR_SCALED_ALARM_ACTIVE") = readResult(TPMoxaReceiveStartAddress.Item("LAS2_SENSOR_SCALED_ALARM_ACTIVE_StartAddress"))
            TPData.TPStr.Item("LAS1_POTE_CABINA") = readResult(TPMoxaReceiveStartAddress.Item("LAS1_POTE_CABINA_StartAddress"))
            TPData.TPStr.Item("LAS2_POTE_CABINA") = readResult(TPMoxaReceiveStartAddress.Item("LAS2_POTE_CABINA_StartAddress"))
            TPData.TPStr.Item("SUC_1_POTE_CABINA") = readResult(TPMoxaReceiveStartAddress.Item("SUC_1_POTE_CABINA_StartAddress"))
            TPData.TPStr.Item("SUC_2_POTE_CABINA") = readResult(TPMoxaReceiveStartAddress.Item("SUC_2_POTE_CABINA_StartAddress"))
            TPData.TPStr.Item("MIXER_POTE_CABINA") = readResult(TPMoxaReceiveStartAddress.Item("MIXER_POTE_CABINA_StartAddress"))
            TPData.TPStr.Item("GraseraAutomatica_1_OUT") = readResult(TPMoxaReceiveStartAddress.Item("GraseraAutomatica_1_OUT_StartAddress"))
            TPData.TPStr.Item("GraseraAutomatica_2_OUT") = readResult(TPMoxaReceiveStartAddress.Item("GraseraAutomatica_2_OUT_StartAddress"))
            TPData.TPStr.Item("EstadoRPM_1") = readResult(TPMoxaReceiveStartAddress.Item("EstadoRPM_1_StartAddress"))
            TPData.TPStr.Item("EstadoRPM_2") = readResult(TPMoxaReceiveStartAddress.Item("EstadoRPM_2_StartAddress"))
            TPData.TPStr.Item("ParadaSobrePresion_1") = readResult(TPMoxaReceiveStartAddress.Item("ParadaSobrePresion_1_StartAddress"))
            TPData.TPStr.Item("ParadaSobrePresion_2") = readResult(TPMoxaReceiveStartAddress.Item("ParadaSobrePresion_2_StartAddress"))
            TPData.TPStr.Item("CAUDAL_1") = readResult(TPMoxaReceiveStartAddress.Item("CAUDAL_1_StartAddress"))
            TPData.TPStr.Item("CAUDAL_2") = readResult(TPMoxaReceiveStartAddress.Item("CAUDAL_2_StartAddress"))
        End If
        Dim ReadResult1 As Integer() = MoxaRead(312, 1)
        If ReadResult1.Length = 1 Then
            TPData.TPStr.Item("TRIP") = ReadResult1(TPMoxaReceiveStartAddress.Item("TRIP_StartAddress") - 312)
        End If
    End Sub
End Class
