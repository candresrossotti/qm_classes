﻿Public Class TwinPumperConfig
    Inherits OneConfig

    Public Shared Event ConfigComplete()

    Public SettingsRM As New Dictionary(Of String, Integer)() From {
        {"VIATRAN1_SENSOR_SCALE_X0", 0},
        {"VIATRAN1_SENSOR_SCALE_X1", 0},
        {"VIATRAN1_SENSOR_SCALE_Y0", 0},
        {"VIATRAN1_SENSOR_SCALE_Y1", 0},
        {"VIATRAN1_SENSOR_SCALE_CAPMIN", 0},
        {"VIATRAN1_SENSOR_SCALE_CAPMAX", 0},
        {"VIATRAN1_SENSOR_SCALE_CAP", 0},
        {"VIATRAN1_SCALED_ALARM_SETPOINT", 0},
        {"VIATRAN1_SCALED_ALARM_ARMED", 0},
        {"VIATRAN1_FILTER_EN", 0},
        {"VIATRAN1_FILTER_TOL", 0},
        {"VIATRAN1_FILTER_TSMPL", 0},
        {"VIATRAN1_FILTER_NSMPL", 0},
        {"VIATRAN2_SENSOR_SCALE_X0", 0},
        {"VIATRAN2_SENSOR_SCALE_X1", 0},
        {"VIATRAN2_SENSOR_SCALE_Y0", 0},
        {"VIATRAN2_SENSOR_SCALE_Y1", 0},
        {"VIATRAN2_SENSOR_SCALE_CAPMIN", 0},
        {"VIATRAN2_SENSOR_SCALE_CAPMAX", 0},
        {"VIATRAN2_SENSOR_SCALE_CAP", 0},
        {"VIATRAN2_SCALED_ALARM_SETPOINT", 0},
        {"VIATRAN2_SCALED_ALARM_ARMED", 0},
        {"VIATRAN2_FILTER_EN", 0},
        {"VIATRAN2_FILTER_TOL", 0},
        {"VIATRAN2_FILTER_TSMPL", 0},
        {"VIATRAN2_FILTER_NSMPL", 0},
        {"LAS1_SENSOR_SCALE_X0", 0},
        {"LAS1_SENSOR_SCALE_X1", 0},
        {"LAS1_SENSOR_SCALE_Y0", 0},
        {"LAS1_SENSOR_SCALE_Y1", 0},
        {"LAS1_SENSOR_SCALE_CAPMIN", 0},
        {"LAS1_SENSOR_SCALE_CAPMAX", 0},
        {"LAS1_SENSOR_SCALE_CAP", 0},
        {"LAS1_SCALED_ALARM_SETPOINT", 0},
        {"LAS1_SCALED_ALARM_ARMED", 0},
        {"LAS1_FILTER_EN", 0},
        {"LAS1_FILTER_TOL", 0},
        {"LAS1_FILTER_TSMPL", 0},
        {"LAS1_FILTER_NSMPL", 0},
        {"LAS1_POTE_SCALE_X0", 0},
        {"LAS1_POTE_SCALE_X1", 0},
        {"LAS1_POTE_SCALE_Y0", 0},
        {"LAS1_POTE_SCALE_Y1", 0},
        {"LAS1_POTE_SCALE_CAPMIN", 0},
        {"LAS1_POTE_SCALE_CAPMAX", 0},
        {"LAS1_POTE_SCALE_CAP", 0},
        {"LAS2_SENSOR_SCALE_X0", 0},
        {"LAS2_SENSOR_SCALE_X1", 0},
        {"LAS2_SENSOR_SCALE_Y0", 0},
        {"LAS2_SENSOR_SCALE_Y1", 0},
        {"LAS2_SENSOR_SCALE_CAPMIN", 0},
        {"LAS2_SENSOR_SCALE_CAPMAX", 0},
        {"LAS2_SENSOR_SCALE_CAP", 0},
        {"LAS2_SCALED_ALARM_SETPOINT", 0},
        {"LAS2_SCALED_ALARM_ARMED", 0},
        {"LAS2_FILTER_EN", 0},
        {"LAS2_FILTER_TOL", 0},
        {"LAS2_FILTER_TSMPL", 0},
        {"LAS2_FILTER_NSMPL", 0},
        {"LAS2_POTE_SCALE_X0", 0},
        {"LAS2_POTE_SCALE_X1", 0},
        {"LAS2_POTE_SCALE_Y0", 0},
        {"LAS2_POTE_SCALE_Y1", 0},
        {"LAS2_POTE_SCALE_CAPMIN", 0},
        {"LAS2_POTE_SCALE_CAPMAX", 0},
        {"LAS2_POTE_SCALE_CAP", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_X0", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_X1", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_Y0", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_Y1", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_CAPMIN", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_CAPMAX", 0},
        {"SENSOR_NIVEL_1_SENSOR_SCALE_CAP", 0},
        {"SENSOR_NIVEL_1_FILTER_EN", 0},
        {"SENSOR_NIVEL_1_FILTER_TOL", 0},
        {"SENSOR_NIVEL_1_FILTER_TSMPL", 0},
        {"SENSOR_NIVEL_1_FILTER_NSMPL", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_X0", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_X1", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_Y0", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_Y1", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_CAPMIN", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_CAPMAX", 0},
        {"SENSOR_NIVEL_2_SENSOR_SCALE_CAP", 0},
        {"SENSOR_NIVEL_2_FILTER_EN", 0},
        {"SENSOR_NIVEL_2_FILTER_TOL", 0},
        {"SENSOR_NIVEL_2_FILTER_TSMPL", 0},
        {"SENSOR_NIVEL_2_FILTER_NSMPL", 0},
        {"MIXER_POTE_SCALE_X0", 0},
        {"MIXER_POTE_SCALE_X1", 0},
        {"MIXER_POTE_SCALE_Y0", 0},
        {"MIXER_POTE_SCALE_Y1", 0},
        {"MIXER_POTE_SCALE_CAPMIN", 0},
        {"MIXER_POTE_SCALE_CAPMAX", 0},
        {"MIXER_POTE_SCALE_CAP", 0},
        {"PRES_SUC_1_SENSOR_SCALE_X0", 0},
        {"PRES_SUC_1_SENSOR_SCALE_X1", 0},
        {"PRES_SUC_1_SENSOR_SCALE_Y0", 0},
        {"PRES_SUC_1_SENSOR_SCALE_Y1", 0},
        {"PRES_SUC_1_SENSOR_SCALE_CAPMIN", 0},
        {"PRES_SUC_1_SENSOR_SCALE_CAPMAX", 0},
        {"PRES_SUC_1_SENSOR_SCALE_CAP", 0},
        {"PRES_SUC_1_SCALED_ALARM_SETPOINT", 0},
        {"PRES_SUC_1_SCALED_ALARM_ARMED", 0},
        {"PRES_SUC_1_FILTER_EN", 0},
        {"PRES_SUC_1_FILTER_TOL", 0},
        {"PRES_SUC_1_FILTER_TSMPL", 0},
        {"PRES_SUC_1_FILTER_NSMPL", 0},
        {"PRES_SUC_2_SENSOR_SCALE_X0", 0},
        {"PRES_SUC_2_SENSOR_SCALE_X1", 0},
        {"PRES_SUC_2_SENSOR_SCALE_Y0", 0},
        {"PRES_SUC_2_SENSOR_SCALE_Y1", 0},
        {"PRES_SUC_2_SENSOR_SCALE_CAPMIN", 0},
        {"PRES_SUC_2_SENSOR_SCALE_CAPMAX", 0},
        {"PRES_SUC_2_SENSOR_SCALE_CAP", 0},
        {"PRES_SUC_2_SCALED_ALARM_SETPOINT", 0},
        {"PRES_SUC_2_SCALED_ALARM_ARMED", 0},
        {"PRES_SUC_2_FILTER_EN", 0},
        {"PRES_SUC_2_FILTER_TOL", 0},
        {"PRES_SUC_2_FILTER_TSMPL", 0},
        {"PRES_SUC_2_FILTER_NSMPL", 0},
        {"RPM_SUC_1_FILTER_EN", 0},
        {"RPM_SUC_1_FILTER_TOL", 0},
        {"RPM_SUC_1_FILTER_TSMPL", 0},
        {"RPM_SUC_1_FILTER_NSMPL", 0},
        {"RPM_SUC_2_FILTER_EN", 0},
        {"RPM_SUC_2_FILTER_TOL", 0},
        {"RPM_SUC_2_FILTER_TSMPL", 0},
        {"RPM_SUC_2_FILTER_NSMPL", 0},
        {"SUC_1_DIENTES", 0},
        {"SUC_2_DIENTES", 0},
        {"RPM_TRANS_1_FILTER_EN", 0},
        {"RPM_TRANS_1_FILTER_TOL", 0},
        {"RPM_TRANS_1_FILTER_TSMPL", 0},
        {"RPM_TRANS_1_FILTER_NSMPL", 0},
        {"RPM_TRANS_2_FILTER_EN", 0},
        {"RPM_TRANS_2_FILTER_TOL", 0},
        {"RPM_TRANS_2_FILTER_TSMPL", 0},
        {"RPM_TRANS_2_FILTER_NSMPL", 0},
        {"TRANS_1_DIENTES", 0},
        {"TRANS_2_DIENTES", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_X0", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_X1", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_Y0", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_Y1", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_CAPMIN", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_CAPMAX", 0},
        {"PRES_ACEITE_1_SENSOR_SCALE_CAP", 0},
        {"PRES_ACEITE_1_SCALED_ALARM_SETPOINT", 0},
        {"PRES_ACEITE_1_SCALED_ALARM_ARMED", 0},
        {"PRES_ACEITE_1_FILTER_EN", 0},
        {"PRES_ACEITE_1_FILTER_TOL", 0},
        {"PRES_ACEITE_1_FILTER_TSMPL", 0},
        {"PRES_ACEITE_1_FILTER_NSMPL", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_X0", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_X1", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_Y0", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_Y1", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_CAPMIN", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_CAPMAX", 0},
        {"PRES_ACEITE_2_SENSOR_SCALE_CAP", 0},
        {"PRES_ACEITE_2_SCALED_ALARM_SETPOINT", 0},
        {"PRES_ACEITE_2_SCALED_ALARM_ARMED", 0},
        {"PRES_ACEITE_2_FILTER_EN", 0},
        {"PRES_ACEITE_2_FILTER_TOL", 0},
        {"PRES_ACEITE_2_FILTER_TSMPL", 0},
        {"PRES_ACEITE_2_FILTER_NSMPL", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_X0", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_X1", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_Y0", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_Y1", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_CAPMIN", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_CAPMAX", 0},
        {"TEMP_ACEITE_1_SENSOR_SCALE_CAP", 0},
        {"TEMP_ACEITE_1_SCALED_ALARM_SETPOINT", 0},
        {"TEMP_ACEITE_1_SCALED_ALARM_ARMED", 0},
        {"TEMP_ACEITE_1_FILTER_EN", 0},
        {"TEMP_ACEITE_1_FILTER_TOL", 0},
        {"TEMP_ACEITE_1_FILTER_TSMPL", 0},
        {"TEMP_ACEITE_1_FILTER_NSMPL", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_X0", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_X1", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_Y0", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_Y1", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_CAPMIN", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_CAPMAX", 0},
        {"TEMP_ACEITE_2_SENSOR_SCALE_CAP", 0},
        {"TEMP_ACEITE_2_SCALED_ALARM_SETPOINT", 0},
        {"TEMP_ACEITE_2_SCALED_ALARM_ARMED", 0},
        {"TEMP_ACEITE_2_FILTER_EN", 0},
        {"TEMP_ACEITE_2_FILTER_TOL", 0},
        {"TEMP_ACEITE_2_FILTER_TSMPL", 0},
        {"TEMP_ACEITE_2_FILTER_NSMPL", 0},
        {"SUC_1_POTE_SCALE_X0", 0},
        {"SUC_1_POTE_SCALE_X1", 0},
        {"SUC_1_POTE_SCALE_Y0", 0},
        {"SUC_1_POTE_SCALE_Y1", 0},
        {"SUC_1_POTE_SCALE_CAPMIN", 0},
        {"SUC_1_POTE_SCALE_CAPMAX", 0},
        {"SUC_1_POTE_SCALE_CAP", 0},
        {"SUC_2_POTE_SCALE_X0", 0},
        {"SUC_2_POTE_SCALE_X1", 0},
        {"SUC_2_POTE_SCALE_Y0", 0},
        {"SUC_2_POTE_SCALE_Y1", 0},
        {"SUC_2_POTE_SCALE_CAPMIN", 0},
        {"SUC_2_POTE_SCALE_CAPMAX", 0},
        {"SUC_2_POTE_SCALE_CAP", 0},
        {"TRIP_1", 0},
        {"TRIP_2", 0},
        {"LAS1_AUTO_ACTIVE", 0},
        {"LAS1_AUTO_SETPOINT", 0},
        {"LAS2_AUTO_ACTIVE", 0},
        {"LAS2_AUTO_SETPOINT", 0}
    }

    ''' <summary>
    ''' Funcion para leer la configuracion del equipo.
    ''' </summary>
    Public Sub UpdateSettings()
        Dim UpdateTask As New Task(AddressOf Update)
        UpdateTask.Start()
    End Sub

    ''' <summary>
    ''' Funcion para actualizar la configuracion.
    ''' </summary>
    Private Async Sub Update()
        Try
            MoxaSend(TPMoxaSendStartAddress("GET_SETTINGS_StartAddress"), 1)
            Await Task.Delay(15000)
            MoxaSend(TPMoxaSendStartAddress("GET_SETTINGS_StartAddress"), 0)
            Dim Result As Integer() = MoxaRead(64, 120)
            If Result.Length = 120 Then

                ''VIATRAN1
                SettingsRM("VIATRAN1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("VIATRAN1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("VIATRAN1_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SCALED_ALARM_SETPOINT_StartAddress") - 64)
                SettingsRM("VIATRAN1_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("VIATRAN1_SCALED_ALARM_ARMED_StartAddress") - 64)
                SettingsRM("VIATRAN1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("VIATRAN1_FILTER_EN_StartAddress") - 64)
                SettingsRM("VIATRAN1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("VIATRAN1_FILTER_TOL_StartAddress") - 64)
                SettingsRM("VIATRAN1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("VIATRAN1_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("VIATRAN1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("VIATRAN1_FILTER_NSMPL_StartAddress") - 64)

                ''VIATRAN2
                SettingsRM("VIATRAN2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("VIATRAN2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("VIATRAN2_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SCALED_ALARM_SETPOINT_StartAddress") - 64)
                SettingsRM("VIATRAN2_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("VIATRAN2_SCALED_ALARM_ARMED_StartAddress") - 64)
                SettingsRM("VIATRAN2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("VIATRAN2_FILTER_EN_StartAddress") - 64)
                SettingsRM("VIATRAN2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("VIATRAN2_FILTER_TOL_StartAddress") - 64)
                SettingsRM("VIATRAN2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("VIATRAN2_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("VIATRAN2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("VIATRAN2_FILTER_NSMPL_StartAddress") - 64)

                ''LAS1
                SettingsRM("LAS1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("LAS1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("LAS1_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("LAS1_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("LAS1_SCALED_ALARM_SETPOINT_StartAddress") - 64)
                SettingsRM("LAS1_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("LAS1_SCALED_ALARM_ARMED_StartAddress") - 64)
                SettingsRM("LAS1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("LAS1_FILTER_EN_StartAddress") - 64)
                SettingsRM("LAS1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("LAS1_FILTER_TOL_StartAddress") - 64)
                SettingsRM("LAS1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("LAS1_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("LAS1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("LAS1_FILTER_NSMPL_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_X0") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_X0_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_X1") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_X1_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_Y0_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_Y1_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("LAS1_POTE_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("LAS1_POTE_SCALE_CAP_StartAddress") - 64)

                ''LAS2
                SettingsRM("LAS2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("LAS2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("LAS2_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("LAS2_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("LAS2_SCALED_ALARM_SETPOINT_StartAddress") - 64)
                SettingsRM("LAS2_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("LAS2_SCALED_ALARM_ARMED_StartAddress") - 64)
                SettingsRM("LAS2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("LAS2_FILTER_EN_StartAddress") - 64)
                SettingsRM("LAS2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("LAS2_FILTER_TOL_StartAddress") - 64)
                SettingsRM("LAS2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("LAS2_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("LAS2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("LAS2_FILTER_NSMPL_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_X0") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_X0_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_X1") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_X1_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_Y0_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_Y1_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("LAS2_POTE_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("LAS2_POTE_SCALE_CAP_StartAddress") - 64)

                ''SENSOR_NIVEL_1
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_FILTER_EN_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_FILTER_TOL_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_1_FILTER_NSMPL_StartAddress") - 64)

                ''SENSOR_NIVEL_2
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_SENSOR_SCALE_CAP_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_FILTER_EN_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_FILTER_TOL_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_FILTER_TSMPL_StartAddress") - 64)
                SettingsRM("SENSOR_NIVEL_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("SENSOR_NIVEL_2_FILTER_NSMPL_StartAddress") - 64)

                ''MIXER
                SettingsRM("MIXER_POTE_SCALE_X0") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_X0_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_X1") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_X1_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_Y0_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_Y1_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("MIXER_POTE_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("MIXER_POTE_SCALE_CAP_StartAddress") - 64)

                ''PRES_SUC_1
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_X0_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_X1_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_Y0_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_Y1_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_CAPMIN_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_CAPMAX_StartAddress") - 64)
                SettingsRM("PRES_SUC_1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SENSOR_SCALE_CAP_StartAddress") - 64)
            End If
            Dim Result1 As Integer() = MoxaRead(184, 120)
            If Result1.Length = 120 Then

                ''PRES_SUC_1
                SettingsRM("PRES_SUC_1_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("PRES_SUC_1_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("PRES_SUC_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_FILTER_EN_StartAddress") - 184)
                SettingsRM("PRES_SUC_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_FILTER_TOL_StartAddress") - 184)
                SettingsRM("PRES_SUC_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("PRES_SUC_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_1_FILTER_NSMPL_StartAddress") - 184)

                ''PRES_SUC_2
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_X0_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_X1_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_Y0_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_Y1_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SENSOR_SCALE_CAP_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_FILTER_EN_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_FILTER_TOL_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("PRES_SUC_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_FILTER_NSMPL_StartAddress") - 184)

                ''SUC_1
                SettingsRM("RPM_SUC_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("RPM_SUC_1_FILTER_EN_StartAddress") - 184)
                SettingsRM("RPM_SUC_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_1_FILTER_TOL_StartAddress") - 184)
                SettingsRM("RPM_SUC_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_1_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("RPM_SUC_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_1_FILTER_NSMPL_StartAddress") - 184)
                SettingsRM("RPM_SUC_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("RPM_SUC_2_FILTER_EN_StartAddress") - 184)
                SettingsRM("RPM_SUC_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_2_FILTER_TOL_StartAddress") - 184)
                SettingsRM("RPM_SUC_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_2_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("RPM_SUC_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("RPM_SUC_2_FILTER_NSMPL_StartAddress") - 184)
                SettingsRM("SUC_1_DIENTES") = Result(TPMoxaReceiveStartAddress("SUC_1_DIENTES_StartAddress") - 184)
                SettingsRM("SUC_2_DIENTES") = Result(TPMoxaReceiveStartAddress("PRES_SUC_2_FILTER_TOL_StartAddress") - 184)

                ''TRANS_1
                SettingsRM("RPM_TRANS_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_1_FILTER_EN_StartAddress") - 184)
                SettingsRM("RPM_TRANS_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_1_FILTER_TOL_StartAddress") - 184)
                SettingsRM("RPM_TRANS_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_1_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("RPM_TRANS_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_1_FILTER_NSMPL_StartAddress") - 184)
                SettingsRM("RPM_TRANS_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_2_FILTER_EN_StartAddress") - 184)
                SettingsRM("RPM_TRANS_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_2_FILTER_TOL_StartAddress") - 184)
                SettingsRM("RPM_TRANS_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_2_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("RPM_TRANS_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("RPM_TRANS_2_FILTER_NSMPL_StartAddress") - 184)
                SettingsRM("TRANS_1_DIENTES") = Result(TPMoxaReceiveStartAddress("TRANS_1_DIENTES_StartAddress") - 184)
                SettingsRM("TRANS_2_DIENTES") = Result(TPMoxaReceiveStartAddress("TRANS_2_DIENTES_StartAddress") - 184)

                ''PRES_ACEITE_1
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_X0_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_X1_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_Y0_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_Y1_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SENSOR_SCALE_CAP_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_FILTER_EN_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_FILTER_TOL_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_1_FILTER_NSMPL_StartAddress") - 184)

                ''PRES_ACEITE_2
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_X0_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_X1_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_Y0_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_Y1_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SENSOR_SCALE_CAP_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_FILTER_EN_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_FILTER_TOL_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("PRES_ACEITE_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("PRES_ACEITE_2_FILTER_NSMPL_StartAddress") - 184)

                ''TEMP_ACEITE_1
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_X0_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_X1_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_Y0_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_Y1_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SENSOR_SCALE_CAP_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_FILTER_EN") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_FILTER_EN_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_FILTER_TOL_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_1_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_1_FILTER_NSMPL_StartAddress") - 184)

                ''TEMP_ACEITE_2
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_X0") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_X0_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_X1") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_X1_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_Y0_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_Y1_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SENSOR_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SENSOR_SCALE_CAP_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SCALED_ALARM_SETPOINT") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SCALED_ALARM_SETPOINT_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_SCALED_ALARM_ARMED") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_SCALED_ALARM_ARMED_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_FILTER_EN") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_FILTER_EN_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_FILTER_TOL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_FILTER_TOL_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_FILTER_TSMPL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_FILTER_TSMPL_StartAddress") - 184)
                SettingsRM("TEMP_ACEITE_2_FILTER_NSMPL") = Result(TPMoxaReceiveStartAddress("TEMP_ACEITE_2_FILTER_NSMPL_StartAddress") - 184)

                ''SUC_1
                SettingsRM("SUC_1_POTE_SCALE_X0") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_X0_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_X1") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_X1_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_Y0_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_Y1_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_CAPMIN_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_CAPMAX_StartAddress") - 184)
                SettingsRM("SUC_1_POTE_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("SUC_1_POTE_SCALE_CAP_StartAddress") - 184)
            End If
            Dim Result2 As Integer() = MoxaRead(304, 16)
            If Result2.Length = 16 Then
                ''SUC_2
                SettingsRM("SUC_2_POTE_SCALE_X0") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_X0_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_X1") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_X1_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_Y0") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_Y0_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_Y1") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_Y1_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_CAPMIN") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_CAPMIN_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_CAPMAX") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_CAPMAX_StartAddress") - 304)
                SettingsRM("SUC_2_POTE_SCALE_CAP") = Result(TPMoxaReceiveStartAddress("SUC_2_POTE_SCALE_CAP_StartAddress") - 304)
            End If
            If Result1.Length + Result2.Length + Result.Length = 256 Then
                RaiseEvent ConfigComplete()
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
End Class
