﻿Imports System.Text.RegularExpressions
Imports System.IO
Imports System.Windows.Forms
Imports System.Runtime.CompilerServices
Imports System.Threading
Imports System.Net.Sockets

Public Module Funciones


    Public Function GetMax(ByVal Values As Object) As Double
        Dim MaxValue As Double
        MaxValue = Values(0)
        For SecondItem = 1 To Values.Length() - 1
            If Values(SecondItem) > MaxValue Then
                MaxValue = Values(SecondItem)
            End If
        Next
        Return MaxValue
    End Function

    <Extension()>
    Public Function GetMin(ByVal Values As Object) As Double
        Dim MinValue As Double
        MinValue = Double.MaxValue
        For FirstItem = 0 To Values.Length() - 1
            If Values(FirstItem) < MinValue Then
                MinValue = Values(FirstItem)
            End If
        Next
        Return MinValue
    End Function

    ''Funcion para limitar un valor entre un maximo y un minimo
    Public Function Clamp(ByVal LowerLimit As Integer, ByVal UpperLimit As Integer, ByVal Value As Integer) As Double
        Dim MiniMax As Double = Math.Min(UpperLimit, Value)
        Dim MaxiMin As Double = Math.Max(MiniMax, LowerLimit)
        Return MaxiMin
    End Function

    ''DEVUELVE NUMEROS EN UN STRING COMO ENTERO
    Public Function GetInteger(ByVal value As String) As Integer
        If Not String.IsNullOrEmpty(value) Then
            Dim returnVal As String = String.Empty
            Dim collection As MatchCollection = Regex.Matches(value, "[-\d+ +\d+]") '"\d+")
            For Each m As Match In collection
                returnVal += m.ToString()
            Next
            Dim Result As Integer
            Integer.TryParse(returnVal, Result)
            Return Result
        Else
            Return 0
        End If
    End Function


    ''VERIFICA SI UN STRING ES ENTERO
    Public Function IsInteger(ByVal value As String) As Boolean
        If String.IsNullOrEmpty(value) Then
            Return False
        Else
            Return Integer.TryParse(value, Nothing)
        End If
    End Function


    ''VERIFICA SI UN STRING ES DECIMAL
    Public Function IsDecimal(ByVal value As String) As Boolean
        If String.IsNullOrEmpty(value) Or InStr(value, " ") > 0 Then
            Return False
        Else
            Return Decimal.TryParse(value, Nothing)
        End If
    End Function

    ''VERIFICA SI UN STRING ES DOUBLE
    Public Function IsDouble(ByVal value As String) As Boolean
        If String.IsNullOrEmpty(value) Or InStr(value, " ") > 0 Then
            Return False
        Else
            Return Double.TryParse(value, Nothing)
        End If
    End Function



    ''DEVUELVE EL DECIMAL CON EL SEPARADOR CORRECTO SEGUN CADA SISTEMA
    Public Function GetDecimal(ByVal value As String) As Decimal
        If Not String.IsNullOrEmpty(value) Then
            Dim separator = Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator
            Select Case separator
                Case ","
                    If InStr(value, ".") <> 0 Then
                        Return Convert.ToDecimal(Replace(value, ".", ","))
                    Else
                        Return Convert.ToDecimal(value)
                    End If
                Case "."
                    If InStr(value, ",") <> 0 Then
                        Return Convert.ToDecimal(Replace(value, ",", "."))
                    Else
                        Return Convert.ToDecimal(value)
                    End If
            End Select
            Return Nothing
        End If
        Return Nothing
    End Function

    ''DEVUELVE EL DECIMAL CON EL SEPARADOR CORRECTO SEGUN CADA SISTEMA
    Public Function GetDouble(ByVal value As String) As Double
        If Not String.IsNullOrEmpty(value) Then
            Dim separator = Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator
            Select Case separator
                Case ","
                    If InStr(value, ".") <> 0 Then
                        Return Convert.ToDouble(Replace(value, ".", ","))
                    Else
                        Return Convert.ToDouble(value)
                    End If
                Case "."
                    If InStr(value, ",") <> 0 Then
                        Return Convert.ToDouble(Replace(value, ",", "."))
                    Else
                        Return Convert.ToDouble(value)
                    End If
            End Select
            Return Nothing
        End If
        Return Nothing
    End Function


    ''VERIFICA SI UN ARCHIVO EXISTE Y SE PUEDE ABRIR
    Public Function fileTest(ByRef sName As String) As Boolean
        Dim fs As FileStream
        Try
            fs = File.Open(sName, FileMode.Open, FileAccess.Read, FileShare.None)
            Return False
            fs.Close()
        Catch ex As Exception
            Return True
        End Try
    End Function


    ''CONVIERTE DE BGR A RGB - INVERSOR DE BYTES
    Public Function ColorFrom0BGR(ByVal bgrColor As Integer) As Integer
        Dim bytes As Byte() = BitConverter.GetBytes(bgrColor)

        Dim result As Byte()
        ReDim result(bytes.Length - 1)
        result(0) = bytes(2)
        result(1) = bytes(1)
        result(2) = bytes(0)

        Return BitConverter.ToInt32(result, 0)
    End Function


    ''CENTRA OBJECTO HORIZONTALMENTE
    Public Sub CenterObject(sender As Object, Optional ByVal SenderParent As Control = Nothing)
        If SenderParent IsNot Nothing Then
            sender.Left = (SenderParent.Width \ 2) - (sender.Width \ 2)
        ElseIf sender.Parent IsNot Nothing Then
            sender.Left = (sender.Parent.Width \ 2) - (sender.Width \ 2)
        End If
    End Sub

    Public Sub CenterAlign(sender As Object, ByVal Align As Object)
        sender.Left = Align.Left + ((Align.Width - sender.Width) / 2)
    End Sub

    Public Sub LeftAlign(ByVal sender As Object, ByVal Align As Object)
        sender.Left = Align.Left
    End Sub

    Public Sub LeftAlign(ByVal sender As Object, ByVal Z As Integer)
        sender.Left = Z - sender.Width
    End Sub

    Public Sub RightAlign(ByVal sender As Object, ByVal Z As Object)
        sender.Left = Z.Left + Z.Width + 6
    End Sub



    ''AGREGA CONTROL A PANEL
    Public Function AddControl(form As Form, control As Object)
        form.TopLevel = False
        form.Dock = DockStyle.Fill
        control.Controls.Add(form)
        Return True
    End Function



    ''FUNCION PARA CALCULAR EL VALOR MAS CERCANO A UN ENTERO
    Public Function GetClosestByValue(ByVal DataSet As Double(), ByVal SearchValue As Double) As Double
        Dim DataSetRange As Integer = DataSet.Length()
        Dim IndexValue As Double
        Dim MinimumDifference As Double = SearchValue
        For i = 0 To DataSetRange - 1
            If Math.Abs(DataSet(i) - SearchValue) < MinimumDifference Then
                MinimumDifference = Math.Abs(DataSet(i) - SearchValue)
                IndexValue = DataSet(i)
            End If
        Next
        Return IndexValue
    End Function

    Public Function GetClosestByValue(ByVal DataSet As Integer(), ByVal SearchValue As Integer) As Integer
        Dim DataSetRange As Integer = DataSet.Length()
        Dim IndexValue As Integer
        Dim MinimumDifference As Integer = SearchValue
        For i = 0 To DataSetRange - 1
            If Math.Abs(DataSet(i) - SearchValue) < MinimumDifference Then
                MinimumDifference = Math.Abs(DataSet(i) - SearchValue)
                IndexValue = DataSet(i)
            End If
        Next
        Return IndexValue
    End Function

    Public Function GetClosestByIndex(ByVal DataSet As Double(), ByVal SearchValue As Double) As Integer
        Dim DataSetRange As Integer = DataSet.Length()
        Dim IndexValue As Integer
        Dim MinimumDifference As Double = SearchValue
        For i = 0 To DataSetRange - 1
            If Math.Abs(DataSet(i) - SearchValue) < MinimumDifference Then
                MinimumDifference = Math.Abs(DataSet(i) - SearchValue)
                IndexValue = i
            End If
        Next
        Return IndexValue
    End Function

    Public Function GetClosestByIndex(ByVal DataSet As Integer(), ByVal SearchValue As Integer) As Integer
        Dim DataSetRange As Integer = DataSet.Length()
        Dim IndexValue As Integer
        Dim MinimumDifference As Integer = SearchValue
        For i = 0 To DataSetRange - 1
            If Math.Abs(DataSet(i) - SearchValue) < MinimumDifference Then
                MinimumDifference = Math.Abs(DataSet(i) - SearchValue)
                IndexValue = i
            End If
        Next
        Return IndexValue
    End Function


    ''FUNCION QUE DIVIDE UN ENTERO EN N PARTES ENTERAS
    Public Function GetEqualParts(number As Integer, nParts As Integer) As List(Of Integer)
        Dim parts As List(Of Integer) = New List(Of Integer)()
        Dim Division As Integer = Math.Floor(number / nParts)
        Dim Remainder As Integer = number Mod nParts
        For i = 0 To Remainder - 1
            parts.Add(Division + 1)
        Next
        For i = 0 To (nParts - Remainder) - 1
            parts.Add(Division)
        Next
        Return parts
    End Function

    ''Funcion para logear un error
    Public Sub LogError(ByVal Ex As Exception)
        Try
            Dim content(5) As String
            content(0) = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------"
            content(1) = "Source: " + Ex.Source
            content(2) = "TargetSite: " + Ex.TargetSite.ToString()
            content(3) = "Error: " + Ex.Message
            content(4) = "StackTrace: " + Ex.StackTrace
            content(5) = "Fecha: " + Now
            Directory.CreateDirectory("C:\QMSoftware\LogFiles\")
            File.AppendAllLines("C:\QMSoftware\LogFiles\LogData " + Today.Day.ToString() + "-" + Today.Month.ToString() + ".txt", content)
        Catch ex1 As Exception
        End Try
    End Sub
    Public Function TryPort(ByVal Host As String, ByVal Port As Integer) As Boolean
        Try
            Dim TCPClient As New TCPSingleClient(Host, Port)
            TCPClient.Client.Close()
            Return True
        Catch ex As Exception
            LogError(ex)
            Return False
        End Try


    End Function
End Module
