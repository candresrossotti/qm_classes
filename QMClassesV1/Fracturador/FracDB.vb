﻿Imports System.Data.SqlClient
Imports System.Data
Public Class FracDB

    Private Shared cn As SqlConnection
    Private Shared cmd As SqlCommand
    Private Shared da As SqlDataAdapter
    Private Shared dt As DataTable


    ''' <summary>
    ''' Funcion para conectarse a la base de datos.
    ''' </summary>
    Public Function DBConnect(ByVal Online As Boolean) As Boolean
        Try
            If Online Then
                cn = New SqlConnection("Data Source=DESKTOP-LT9N5J0\SQLEXPRESS;Initial Catalog=FracDB;Persist Security Info=True;User ID=RPC;Password=Qmiot123456")
            Else
                cn = New SqlConnection("Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\_db\LocalDB.mdf;Integrated Security=True")
            End If
            cn.Open()
            cn.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Funcion para desconectarse de la base de datos.
    ''' </summary>
    Public Sub DBDisconnect()
        cmd.Cancel()
        cmd.Dispose()
        da.Dispose()
        dt.Dispose()
        SqlConnection.ClearAllPools()
        cn.Dispose()
    End Sub

    ''' <summary>
    ''' Funcion que devuelve el nombre segun el FracID.
    ''' </summary>
    ''' <param name="ID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracName(ByVal ID As Integer) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracName FROM FracDB WHERE FracID = " + ID.ToString(), cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el nombre segun la IP.
    ''' </summary>
    ''' <param name="IP">IP del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracName(ByVal IP As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracName FROM FracDB WHERE FracIP = '" + IP + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function
    ''' <summary>
    ''' Funcion que devuelve si el fracturador tiene centrifuga.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracCentrifuge(ByVal FracID As String) As Boolean
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracCentrifuge FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el motor del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracEngine(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracEngine FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el motor del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFSEngineByFrac(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSEngine FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve la transmision del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracTransmission(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracTransmission FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el motor del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFSTransByFrac(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSTrans FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve la bomba del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracPump(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracPump FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el motor del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFSPumpByFrac(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSPump FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el FracID del fracturador segun su nombre.
    ''' </summary>
    ''' <param name="FracUnit">Nombre del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracID(ByVal FracUnit As String) As Integer
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracID FROM FracDB WHERE FracName = '" + FracUnit + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve la IP del fracturador segun su FracID.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetFracIP(ByVal FracID As Integer) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracIP FROM FracDB WHERE FracID = " + FracID.ToString(), cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que setea el FracCID. El CID es el ID de conexion.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    ''' <param name="Position">Position de conexion</param>
    '''  <remarks></remarks>
    Public Sub SetFracID(ByVal FracID As Integer, ByVal Position As Integer)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("INSERT INTO FracCID VALUES(" + FracID.ToString() + "," + Position.ToString() + ")", cn)
            cn.Open()
            da.InsertCommand = cmd
            da.InsertCommand.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
    End Sub

    ''' <summary>
    ''' Funcion que elimina la tabla de CID.
    ''' </summary>
    Public Sub DeleteCID()
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("DELETE FROM FracCID", cn)
            cn.Open()
            da.InsertCommand = cmd
            da.InsertCommand.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
    End Sub

    ''' <summary>
    ''' Funcion que devuelve el CID del fracturador segun su FracID.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetCID(ByVal FracID As Integer) As Integer
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT ConnectionID FROM FracCID WHERE FracID = " + FracID.ToString(), cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el FracID del fracturador segun su Id.
    ''' </summary>
    ''' <param name="CID">CID del fracturador</param>
    '''  <remarks></remarks>
    Public Function GetActiveFrac(ByVal CID As Integer) As Integer
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracID FROM FracCID WHERE ConnectionID = " + CID.ToString(), cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que setea el FracCID. El CID es el ID de conexion.
    ''' </summary>
    ''' <param name="FracID">FracID del fracturador</param>
    ''' <param name="Engine">Motor</param>
    '''  <remarks></remarks>
    Public Sub SetFracEngine(ByVal FracID As Integer, ByVal Engine As String, ByVal FSEngine As String)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("UPDATE FracDB SET FracEngine = '" + Engine + "' WHERE FracID = " + FracID.ToString(), cn)
            cn.Open()
            da.InsertCommand = cmd
            da.InsertCommand.ExecuteNonQuery()
            cmd = New SqlCommand("UPDATE FracDB SET FSEngine = '" + FSEngine + "' WHERE FracID = " + FracID.ToString(), cn)
            da.InsertCommand = cmd
            da.InsertCommand.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
    End Sub


    ''' <summary>
    ''' Funcion que devuelve el diccionario de un motor
    ''' </summary>
    ''' <param name="Engine">Motor</param>
    '''  <remarks></remarks>
    Public Function GetDBEngine(ByVal Engine As String) As Dictionary(Of String, Object)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT * FROM FracEngine WHERE EngineName = '" + Engine + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Dim REngine As New Dictionary(Of String, Object) From {
                {"Name", dt.Rows(0).Item(0)},
                {"FSName", dt.Rows(0).Item(1)},
                {"EfficientRPM", dt.Rows(0).Item(2)},
                {"EfficientHP", dt.Rows(0).Item(3)},
                {"MaxHP", dt.Rows(0).Item(4)},
                {"MinRPM", dt.Rows(0).Item(5)},
                {"MaxRPM", dt.Rows(0).Item(6)},
                {"LimitRPM", dt.Rows(0).Item(7)},
                {"RPM_LMin", dt.Rows(0).Item(8)},
                {"RPM_LMax", dt.Rows(0).Item(9)}
            }
            Return REngine
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el diccionario de una bomba
    ''' </summary>
    ''' <param name="Pump">Bomba</param>
    '''  <remarks></remarks>
    Public Function GetDBPump(ByVal Pump As String) As Dictionary(Of String, Object)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT * FROM FracPump WHERE PumpName = '" + Pump + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Dim RPump As New Dictionary(Of String, Object) From {
                {"Name", dt.Rows(0).Item(0)},
                {"FSName", dt.Rows(0).Item(1)},
                {"rTransBomba", dt.Rows(0).Item(2)},
                {"rGalRev", dt.Rows(0).Item(3)}
            }
            Return RPump
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el diccionario de una transmision
    ''' </summary>
    ''' <param name="Trans">Transmision</param>
    '''  <remarks></remarks>
    Public Function GetDBTrans(ByVal Trans As String) As Dictionary(Of String, Object)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT * FROM FracTrans WHERE TransName = '" + Trans + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Dim RTrans As New Dictionary(Of String, Object) From {
                {"Name", dt.Rows(0).Item(0)},
                {"FSName", dt.Rows(0).Item(1)},
                {"Gears", dt.Rows(0).Item(2)},
                {"RelacionGear1", dt.Rows(0).Item(3)},
                {"RelacionGear2", dt.Rows(0).Item(4)},
                {"RelacionGear3", dt.Rows(0).Item(5)},
                {"RelacionGear4", dt.Rows(0).Item(6)},
                {"RelacionGear5", dt.Rows(0).Item(7)},
                {"RelacionGear6", dt.Rows(0).Item(8)},
                {"RelacionGear7", dt.Rows(0).Item(9)},
                {"RelacionGear8", dt.Rows(0).Item(10)},
                {"RelacionGear9", dt.Rows(0).Item(11)},
                {"LimitGEAR", dt.Rows(0).Item(12)},
                {"Gear_LMin", dt.Rows(0).Item(13)},
                {"Gear_LMax", dt.Rows(0).Item(14)}
            }
            Return RTrans
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve todos los motores agregadores.
    ''' </summary>
    '''  <remarks></remarks>
    Public Function GetAllEngines() As List(Of String)
        Dim AllEngines As New List(Of String)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT EngineName FROM FracEngine", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            For CurrentRow = 0 To dt.Rows.Count - 1
                AllEngines.Add(dt.Rows(CurrentRow).Item(0))
            Next
            Return AllEngines
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
        Return AllEngines
    End Function

    ''' <summary>
    ''' Funcion que devuelve todas las bombas agregadas.
    ''' </summary>
    '''  <remarks></remarks>
    Public Function GetAllPumps() As List(Of String)
        Dim AllPumps As New List(Of String)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT PumpName FROM FracPump", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            For CurrentRow = 0 To dt.Rows.Count - 1
                AllPumps.Add(dt.Rows(CurrentRow).Item(0))
            Next
            Return AllPumps
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
        Return AllPumps
    End Function

    ''' <summary>
    ''' Funcion que devuelve todas las transmisiones agregadas.
    ''' </summary>
    '''  <remarks></remarks>
    Public Function GetAllTrans() As List(Of String)
        Dim AllTrans As New List(Of String)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT TransName FROM FracTrans", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            For CurrentRow = 0 To dt.Rows.Count - 1
                AllTrans.Add(dt.Rows(CurrentRow).Item(0))
            Next
            Return AllTrans
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
        Return AllTrans
    End Function

    ''' <summary>
    ''' Funcion que devuelve el nombre fs de un motor segun el motor.
    ''' </summary>
    ''' <param name="Engine">Motor</param>
    '''  <remarks></remarks>
    Public Function GetFSEngineByEngine(ByVal Engine As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSEngineName FROM FracEngine WHERE EngineName = '" + Engine + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el fs nombre de una trans segun la trans.
    ''' </summary>
    ''' <param name="Trans">Transmision</param>
    '''  <remarks></remarks>
    Public Function GetFSTransByTrans(ByVal Trans As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSTransName FROM FracTrans WHERE TransName = '" + Trans + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve el fs nombre de una bomba segun la bomba.
    ''' </summary>
    ''' <param name="Pump">Bomba</param>
    '''  <remarks></remarks>
    Public Function GetFSPumpByPump(ByVal Pump As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FSPumpName FROM FracPump WHERE PumpName = '" + Pump + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve un array con todos los fracturadores en la base de datos.
    ''' </summary>
    '''  <remarks></remarks>
    Public Function GetAllFracs() As List(Of String)
        Dim AllTrans As New List(Of String)
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracName FROM FracDB", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            For CurrentRow = 0 To dt.Rows.Count - 1
                AllTrans.Add(dt.Rows(CurrentRow).Item(0))
            Next
            Return AllTrans
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Funcion que devuelve True si el frac es baker, false si es QM.
    ''' </summary>
    ''' <param name="FracID">ID del Frac</param>
    '''  <remarks></remarks>
    Public Function IsFrackBaker(ByVal FracID As String) As String
        da = New SqlDataAdapter
        dt = New DataTable
        Try
            cmd = New SqlCommand("SELECT FracBaker FROM FracDB WHERE FracID = '" + FracID + "'", cn)
            cn.Open()
            da.SelectCommand = cmd
            da.Fill(dt)
            cn.Close()
            Return dt.Rows(0).Item(0)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            Return Nothing
        End Try
    End Function

End Class
