﻿Imports System.Resources
Imports System.Windows.Forms

Public Class LanguageSelect


    Private Shared Principal As Object
    Private Shared Secundaria As Object
    Private Shared Tercera As Object
    Private Shared FracSelect As Object
    Private Shared AjustesFracturador As Object
    Private Shared FracErrores As Object
    Private Shared SetFC As Object
    Private Shared SetGEAR As Object
    Private Shared SetRPM As Object
    Private Shared SetTRIP As Object
    Private Shared SetZERO As Object
    Private Shared AdminControl As Object
    Private Shared NewPassword As Object
    Private Shared FracMantenimiento As Object
    Private Shared FracDiagnostico As Object
    Private Shared TesteoLinea As Object
    Private Shared InfoShow As Object
    Private Shared FullAutomation As Object
    Private Shared ConfigFrac As Object
    Private Shared AddFrac As Object
    Private Shared EditFrac As Object

    Private Shared Container1 As Object
    Private Shared Container2 As Object
    Private Shared Container4 As Object
    Private Shared Container8 As Object
    Private Shared Container1_2 As Object
    Private Shared Container2_2 As Object
    Private Shared Container4_2 As Object
    Private Shared Container8_2 As Object
    Private Shared Container1_3 As Object
    Private Shared Container2_3 As Object
    Private Shared Container4_3 As Object
    Private Shared Container8_3 As Object

    Private Shared Ajustes As Object

    Private Shared SerialPortConfig As Object
    Private Shared ConfigWITS As Object

    Private Shared _dynamiclanguagerm As ResourceManager
    Public Shared Property DynamicLanguageRM As ResourceManager
        Get
            Return _dynamiclanguagerm
        End Get
        Set(value As ResourceManager)
            If value IsNot _dynamiclanguagerm Then
                _dynamiclanguagerm = value
            End If
        End Set
    End Property
    Private Shared _languagerm As ResourceManager
    Public Shared Property LanguageRM() As ResourceManager
        Get
            Return _languagerm
        End Get
        Set(value As ResourceManager)
            If value IsNot _languagerm Then
                _languagerm = value
            End If
        End Set
    End Property

    Private Shared _currentlanguage As String
    Public Shared Event LanguageChange()

    Public Shared Property CurrentLanguage() As String
        Get
            Return _currentlanguage
        End Get
        Set(value As String)
            If value <> _currentlanguage Then
                _currentlanguage = value
                RaiseEvent LanguageChange()
            End If
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByRef DataVanSystem As IDataVanSystem)

    End Sub

    Public Sub New(ByRef ISystem As IFracSystem)
        Principal = ISystem.GetObject("Principal")
        Secundaria = ISystem.GetObject("Secundaria")
        Tercera = ISystem.GetObject("Tercera")
        FracSelect = ISystem.GetObject("FracSelect")
        AjustesFracturador = ISystem.GetObject("AjustesFracturador")
        FracErrores = ISystem.GetObject("FracErrores")
        SetFC = ISystem.GetObject("SetFC")
        SetGEAR = ISystem.GetObject("SetGEAR")
        SetRPM = ISystem.GetObject("SetRPM")
        SetTRIP = ISystem.GetObject("SetTRIP")
        SetZERO = ISystem.GetObject("SetZERO")
        AdminControl = ISystem.GetObject("AdminControl")
        NewPassword = ISystem.GetObject("NewPassword")
        FracMantenimiento = ISystem.GetObject("FracMantenimiento")
        FracDiagnostico = ISystem.GetObject("FracDiagnostico")
        TesteoLinea = ISystem.GetObject("TesteoLinea")
        InfoShow = ISystem.GetObject("InfoShow")
        FullAutomation = ISystem.GetObject("FullAutomation")
        Ajustes = ISystem.GetObject("Ajustes")

        Container1 = ISystem.GetObject("Container1")
        Container2 = ISystem.GetObject("Container2")
        Container4 = ISystem.GetObject("Container4")
        Container8 = ISystem.GetObject("Container8")
        Container1_2 = ISystem.GetObject("Container1_2")
        Container2_2 = ISystem.GetObject("Container2_2")
        Container4_2 = ISystem.GetObject("Container4_2")
        Container8_2 = ISystem.GetObject("Container8_2")
        Container1_3 = ISystem.GetObject("Container1_3")
        Container2_3 = ISystem.GetObject("Container2_3")
        Container4_3 = ISystem.GetObject("Container4_3")
        Container8_3 = ISystem.GetObject("Container8_3")

        ConfigFrac = ISystem.GetObject("ConfigFrac")
        AddFrac = ISystem.GetObject("AddFrac")
        EditFrac = ISystem.GetObject("EditFrac")

        SerialPortConfig = ISystem.GetObject("SerialPortConfig")
        ConfigWITS = ISystem.GetObject("ConfigWITS")
    End Sub

    ''' <summary>
    ''' Actualizamos solo un form.
    ''' </summary>
    Public Sub UpdateForm(ByRef CurrentForm As Form)
        Select Case CurrentForm.Name
            Case "TesteoLinea"
                TesteoLinea = CurrentForm
                UpdateTesteoLineaInterface()
        End Select
    End Sub

    ''' <summary>
    ''' Actualizamos la clase LSelect con un nuevo IFracSystem.
    ''' </summary>
    Public Sub Update(ByRef ISystem As IFracSystem)
        Principal = ISystem.GetObject("Principal")
        Secundaria = ISystem.GetObject("Secundaria")
        Tercera = ISystem.GetObject("Tercera")
        FracSelect = ISystem.GetObject("FracSelect")
        AjustesFracturador = ISystem.GetObject("AjustesFracturador")
        FracErrores = ISystem.GetObject("FracErrores")
        SetFC = ISystem.GetObject("SetFC")
        SetGEAR = ISystem.GetObject("SetGEAR")
        SetRPM = ISystem.GetObject("SetRPM")
        SetTRIP = ISystem.GetObject("SetTRIP")
        SetZERO = ISystem.GetObject("SetZERO")
        AdminControl = ISystem.GetObject("AdminControl")
        NewPassword = ISystem.GetObject("NewPassword")
        FracMantenimiento = ISystem.GetObject("FracMantenimiento")
        FracDiagnostico = ISystem.GetObject("FracDiagnostico")
        TesteoLinea = ISystem.GetObject("TesteoLinea")
        InfoShow = ISystem.GetObject("InfoShow")
        FullAutomation = ISystem.GetObject("FullAutomation")
        Ajustes = ISystem.GetObject("Ajustes")

        Container1 = ISystem.GetObject("Container1")
        Container2 = ISystem.GetObject("Container2")
        Container4 = ISystem.GetObject("Container4")
        Container8 = ISystem.GetObject("Container8")
        Container1_2 = ISystem.GetObject("Container1_2")
        Container2_2 = ISystem.GetObject("Container2_2")
        Container4_2 = ISystem.GetObject("Container4_2")
        Container8_2 = ISystem.GetObject("Container8_2")
        Container1_3 = ISystem.GetObject("Container1_3")
        Container2_3 = ISystem.GetObject("Container2_3")
        Container4_3 = ISystem.GetObject("Container4_3")
        Container8_3 = ISystem.GetObject("Container8_3")

        ConfigFrac = ISystem.GetObject("ConfigFrac")
        AddFrac = ISystem.GetObject("AddFrac")
        EditFrac = ISystem.GetObject("EditFrac")
    End Sub

    ''' <summary>
    ''' Funcion para definir un idioma.
    ''' </summary>
    Public Sub SetLanguage(ByVal Language As String)
        CurrentLanguage = Language
    End Sub

    Private Sub LanguageSet() Handles MyClass.LanguageChange
        Select Case CurrentLanguage
            Case "Ingles"
                LanguageRM = My.Resources.English.ResourceManager
                DynamicLanguageRM = My.Resources.DynamicEnglish.ResourceManager
            Case "Español"
                LanguageRM = My.Resources.Español.ResourceManager
                DynamicLanguageRM = My.Resources.DynamicEspañol.ResourceManager
        End Select
    End Sub


    ''' <summary>
    ''' Funcion para actualizar interfaz de frac.
    ''' </summary>
    Public Sub UpdateFracInterface()

        ''Actualizamos FracSelect
        UpdateFracSelectInterface()

        ''Actualizamos Ajustes Fracturador
        UpdateAjustesFracturadorInterface()

        ''Actualizamos FracErrores
        UpdateFracErroresInterface()

        ''Actualizamos SetFC
        UpdateSetFCInterface()

        ''Actualizamos SetGEAR
        UpdateSetGEARInterface()

        ''Actualizamos SetRPM
        UpdateSetRPMInterface()

        ''Actualizamos SetTRIP
        UpdateSetTRIPInterface()

        ''Actualizamos SetZERO
        UpdateSetZeroInterface()

        ''Actualizamos AdminControl
        UpdateAdminControlInterface()

        ''Actualizamos NewPassword
        UpdateNewPasswordInterface()

        ''Actualizamos FracMantenimiento
        UpdateFracMantenimientoInterface()

        ''Actualizamos FracDiagnostico
        UpdateFracDiagnosticoInterface()

        ''Actualizamos TesteoLinea
        UpdateTesteoLineaInterface()

        ''Actualizamos InfoShow
        UpdateInfoShowInterface()

        ''Actualizamos FullAutomation
        UpdateFullAutomationInterface()

        ''Actualizamos la primera pantalla (Principal y contenedores)
        UpdateFirstScreenInterface()

        ''Actualizamos la segunda pantalla (Secundaria y contenedores)
        UpdateSecondScreenInterface()

        ''Actualizamos la tercera pantalla (Secundaria y contenedores)
        UpdateThirdScreenInterface()

        ''Actualizamos Ajustes
        UpdateAjustesInterface()

        ''Actualizamos Serial Port
        UpdateSerialPortInterface()

        ''Actualizamos ConfigWITS
        UpdateConfigWITSInterface()

        ''Actualizamos ConfigFrac
        UpdateConfigFracInterface()

        ''Actualizamos AddFrac
        UpdateAddFracInterface()

        ''Actualizamos EditFrac
        UpdateEditFracInterface()

    End Sub

#Region "Frac Screen Update"
    ''' <summary>
    ''' Funcion para setear idioma en EightFrac.
    ''' </summary>
    Public Sub EightFracLanguageSet(ByRef sender As Object)
        With sender
            .BtnErrores.Text = LanguageRM.GetString("EightFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("EightFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("EightFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("EightFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("EightFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("EightFrac.LblMarcha3")
            .LblPCorte.Text = LanguageRM.GetString("EightFrac.LblPCorte")
            .LblCaudal.Text = LanguageRM.GetString("EightFrac.LblCaudal")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en FourFrac.
    ''' </summary>
    Public Sub FourFracLanguageSet(ByRef sender As Object)
        With sender
            .LblErrorGrave.Text = LanguageRM.GetString("FourFrac.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("FourFrac.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("FourFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("FourFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("FourFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("FourFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("FourFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("FourFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("FourFrac.LblCaudal")
            .LblPCorte.Text = LanguageRM.GetString("FourFrac.LblPCorte")
            '.LblPCorte1.Text = LanguageRM.GetString("FourFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("FourFrac.LblPCorte3")
            '.LblOpciones.Text = LanguageRM.GetString("FourFrac.LblOpciones")
            '.BtnEmpezarTrabajo.Text = LanguageRM.GetString("FourFrac.BtnEmpezarTrabajo")
            '.BtnEncenderLuces.Text = LanguageRM.GetString("FourFrac.BtnEncenderLuces")
            '.BtnSemiAuto.Text = LanguageRM.GetString("FourFrac.BtnSemiAuto")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en TwoFrac.
    ''' </summary>
    Public Sub TwoFracLanguageSet(ByRef sender As Object)
        With sender
            .LblErrorGrave.Text = LanguageRM.GetString("TwoFrac.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("TwoFrac.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("TwoFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("TwoFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("TwoFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("TwoFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("TwoFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("TwoFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("TwoFrac.LblCaudal")
            If sender.Controls.Find("LblPCorte", True).Length() <> 0 Then .LblPCorte.Text = LanguageRM.GetString("TwoFrac.LblPCorte")
            .LblPCorte1.Text = LanguageRM.GetString("TwoFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("TwoFrac.LblPCorte3")
            .LblOpciones.Text = LanguageRM.GetString("TwoFrac.LblOpciones")
            .BtnEmpezarTrabajo.Text = LanguageRM.GetString("TwoFrac.BtnEmpezarTrabajo")
            .BtnEncenderLuces.Text = LanguageRM.GetString("TwoFrac.BtnEncenderLuces")
            .BtnSemiAuto.Text = LanguageRM.GetString("TwoFrac.BtnSemiAuto")
            .LblMotor.Text = LanguageRM.GetString("TwoFrac.LblMotor")
            .LblTempAgua.Text = LanguageRM.GetString("TwoFrac.LblTempAgua")
            .LblTempAceite.Text = LanguageRM.GetString("TwoFrac.LblTempAceite")
            .LblPresion.Text = LanguageRM.GetString("TwoFrac.LblPresion")
            .LblCombustible.Text = LanguageRM.GetString("TwoFrac.LblCombustible")
            .LblVoltaje.Text = LanguageRM.GetString("TwoFrac.LblVoltaje")
            .LblCombustible1.Text = LanguageRM.GetString("TwoFrac.LblCombustible1")
            .LblTransmision.Text = LanguageRM.GetString("TwoFrac.LblTransmision")
            .LblTrans1.Text = LanguageRM.GetString("TwoFrac.LblTrans1")
            .LblTrans3.Text = LanguageRM.GetString("TwoFrac.LblTrans3")
            .LblTrans5.Text = LanguageRM.GetString("TwoFrac.LblTrans5")
            .LblBomba.Text = LanguageRM.GetString("TwoFrac.LblBomba")
            .LblTempLub.Text = LanguageRM.GetString("TwoFrac.LblTempLub")
            .LblPresSuc.Text = LanguageRM.GetString("TwoFrac.LblPresSuc")
            .LblPresLub.Text = LanguageRM.GetString("TwoFrac.LblPresLub")
            .LblCargaMotor.Text = LanguageRM.GetString("TwoFrac.LblCargaMotor")
            .LblHM.Text = LanguageRM.GetString("TwoFrac.LblHM")

            .CenterEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en OneFrac.
    ''' </summary>
    Public Sub OneFracLanguageSet(ByRef sender As Object)
        With sender
            .BtnDiagnostico.Text = LanguageRM.GetString("OneFrac.BtnDiagnostico")
            .BtnErrores.Text = LanguageRM.GetString("OneFrac.BtnErrores")
            .LblRPM1.Text = LanguageRM.GetString("OneFrac.LblRPM1")
            .LblRPM3.Text = LanguageRM.GetString("OneFrac.LblRPM3")
            .LblMarcha.Text = LanguageRM.GetString("OneFrac.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("OneFrac.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("OneFrac.LblMarcha3")
            .LblCaudal.Text = LanguageRM.GetString("OneFrac.LblCaudal")
            .LblPCorte1.Text = LanguageRM.GetString("OneFrac.LblPCorte1")
            .LblPCorte3.Text = LanguageRM.GetString("OneFrac.LblPCorte3")
            .LblOpciones.Text = LanguageRM.GetString("OneFrac.LblOpciones")
            .BtnEmpezarTrabajo.Text = LanguageRM.GetString("OneFrac.BtnEmpezarTrabajo")
            .BtnEncenderLuces.Text = LanguageRM.GetString("OneFrac.BtnEncenderLuces")
            .BtnSemiAuto.Text = LanguageRM.GetString("OneFrac.BtnSemiAuto")
            .LblMotor.Text = LanguageRM.GetString("OneFrac.LblMotor")
            .LblTempAgua.Text = LanguageRM.GetString("OneFrac.LblTempAgua")
            .LblTempAceite.Text = LanguageRM.GetString("OneFrac.LblTempAceite")
            .LblPresion.Text = LanguageRM.GetString("OneFrac.LblPresion")
            .LblCombustible.Text = LanguageRM.GetString("OneFrac.LblCombustible")
            .LblVoltaje.Text = LanguageRM.GetString("OneFrac.LblVoltaje")
            .LblCombustible1.Text = LanguageRM.GetString("OneFrac.LblCombustible1")
            .LblTransmision.Text = LanguageRM.GetString("OneFrac.LblTransmision")
            .LblTrans1.Text = LanguageRM.GetString("OneFrac.LblTrans1")
            .LblTrans3.Text = LanguageRM.GetString("OneFrac.LblTrans3")
            .LblTrans5.Text = LanguageRM.GetString("OneFrac.LblTrans5")
            .LblBomba.Text = LanguageRM.GetString("OneFrac.LblBomba")
            .LblTempLub.Text = LanguageRM.GetString("OneFrac.LblTempLub")
            .LblPresSuc.Text = LanguageRM.GetString("OneFrac.LblPresSuc")
            .LblPresLub.Text = LanguageRM.GetString("OneFrac.LblPresLub")
            .LblCargaMotor.Text = LanguageRM.GetString("OneFrac.LblCargaMotor")
            .LblHM.Text = LanguageRM.GetString("OneFrac.LblHM")

            .centerEverything()
        End With
    End Sub

    ''' <summary>
    ''' Funcion para setear idioma en OneFracMaximize.
    ''' </summary>
    Public Sub MaximizeLanguageSet(ByRef sender As Object)
        ''OneFracMaximize

        With sender

            ''General
            .Motor.Text = LanguageRM.GetString("OneFracMaximize.Motor")
            .Transmision.Text = LanguageRM.GetString("OneFracMaximize.Transmision")
            .Bomba.Text = LanguageRM.GetString("OneFracMaximize.Bomba")
            .LblRPM3.Text = LanguageRM.GetString("OneFracMaximize.LblRPM3")
            .LblRPM1.Text = LanguageRM.GetString("OneFracMaximize.LblRPM1")
            .LblMotorGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorGeneral")
            .LblMotorTempAguaGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAguaGeneral")
            .LblMotorTempAceiteGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAceiteGeneral")
            .LblMotorPresionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorPresionGeneral")
            .LblMotorCombustible1General.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible1General")
            .LblMotorVoltajeGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblMotorVoltajeGeneral")
            .LblMotorCombustible2General.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible2General")
            .LblTransmisionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionGeneral")
            .LblTransTempGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransTempGeneral")
            .LblTransPresionGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransPresionGeneral")
            .LblTransRPMGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblTransRPMGeneral")
            .LblErrorGrave.Text = LanguageRM.GetString("OneFracMaximize.LblErrorGrave")
            .LblErrorAdmisible.Text = LanguageRM.GetString("OneFracMaximize.LblErrorAdmisible")
            .BtnErrores.Text = LanguageRM.GetString("OneFracMaximize.BtnErrores")
            .LblCaudal.Text = LanguageRM.GetString("OneFracMaximize.LblCaudal")
            .LblMarcha.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha")
            .LblMarcha1.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha1")
            .LblMarcha3.Text = LanguageRM.GetString("OneFracMaximize.LblMarcha3")
            .LblBombaGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaGeneral")
            .LblBombaTempGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaTempGeneral")
            .LblBombaPresGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresGeneral")
            .LblBombaPres1General.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPres1General")
            .LblBombaCargaMotorGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCargaMotorGeneral")
            .LblBombaHMGeneral.Text = LanguageRM.GetString("OneFracMaximize.LblBombaHMGeneral")
            .LblPCorte3.Text = LanguageRM.GetString("OneFracMaximize.LblPCorte3")
            .LblPCorte1.Text = LanguageRM.GetString("OneFracMaximize.LblPCorte1")
            .BtnDiagnostico.Text = LanguageRM.GetString("OneFracMaximize.BtnDiagnostico")

            ''Motor
            .Title1.Text = LanguageRM.GetString("OneFracMaximize.Title1")
            .LblMotorTempAgua.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAgua")
            .LblMotorTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblMotorTempAceite")
            .LblBombaHorasMotorMotor.Text = LanguageRM.GetString("OneFracMaximize.LblBombaHorasMotorMotor")
            .LblMotorCombustible1Motor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustible1Motor")
            .LblRPM1M.Text = LanguageRM.GetString("OneFracMaximize.LblRPM1M")
            .LblRPM2.Text = LanguageRM.GetString("OneFracMaximize.LblRPM2")
            .LblMotorCombustibleMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCombustibleMotor")
            .LblMotorCargaMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorCargaMotor")
            .LblMotorVoltajeMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorVoltajeMotor")
            .LblMotorPresionMotor.Text = LanguageRM.GetString("OneFracMaximize.LblMotorPresionMotor")
            .LblErroresMotor.Text = LanguageRM.GetString("OneFracMaximize.LblErroresMotor")
            .LblInfoUnidadM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoUnidadM")
            .LblInfoActivoM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoActivoM")
            .LblInfoErrTotM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoErrTotM")
            .LblInfoDescripM.Text = LanguageRM.GetString("OneFracMaximize.LblInfoDescripM")
            .LblDiagnosticoMotor.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoMotor")
            .LblMCombustible.Text = LanguageRM.GetString("OneFracMaximize.LblMCombustible")
            .LblMTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblMTempAceite")
            .LblMHoras.Text = LanguageRM.GetString("OneFracMaximize.LblMHoras")
            .LblMTempAgua.Text = LanguageRM.GetString("OneFracMaximize.LblMTempAgua")

            ''Transmision
            .Title2.Text = LanguageRM.GetString("OneFracMaximize.Title2")
            .LblTransmisionMarchaActual.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionMarchaActual")
            .LblTransmisionMarchaDeseada.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionMarchaDeseada")
            .LblTransmisionPresion.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionPresion")
            .LblTransmisionRPMOut.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionRPMOut")
            .LblTransmisionTemp.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionTemp")
            .LblErroresTrans.Text = LanguageRM.GetString("OneFracMaximize.LblErroresTrans")
            .LblInfoUnidadT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoUnidadT")
            .LblInfoActivoT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoActivoT")
            .LblInfoErrTotT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoErrTotT")
            .LblInfoDescripT.Text = LanguageRM.GetString("OneFracMaximize.LblInfoDescripT")
            .LblDiagnosticoTrans.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoTrans")
            .LblTTempAceite.Text = LanguageRM.GetString("OneFracMaximize.LblTTempAceite")
            .LblTPresAceite.Text = LanguageRM.GetString("OneFracMaximize.LblTPresAceite")
            .LblTransmisionDiag1.Text = LanguageRM.GetString("OneFracMaximize.LblTransmisionDiag1")

            ''Bomba
            .Title3.Text = LanguageRM.GetString("OneFracMaximize.Title3")
            .LblBombaPresLub.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresLub")
            .LblBombaCargaMotor.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCargaMotor")
            .LblBombaPresionActual.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionActual")
            .LblBombaPresionCorte.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionCorte")
            .LblBombaCaudal.Text = LanguageRM.GetString("OneFracMaximize.LblBombaCaudal")
            .LblBombaPresionSuccion.Text = LanguageRM.GetString("OneFracMaximize.LblBombaPresionSuccion")
            .LblBombaTempLub.Text = LanguageRM.GetString("OneFracMaximize.LblBombaTempLub")
            .LblDiagnosticoBomba.Text = LanguageRM.GetString("OneFracMaximize.LblDiagnosticoBomba")
            .LblSPresDescarga.Text = LanguageRM.GetString("OneFracMaximize.LblSPresDescarga")
            .LblSPresSuccion.Text = LanguageRM.GetString("OneFracMaximize.LblSPresSuccion")
            .LblSPresLub.Text = LanguageRM.GetString("OneFracMaximize.LblSPresLub")
            .LblSTempLub.Text = LanguageRM.GetString("OneFracMaximize.LblSTempLub")

            .CenterEverything()
        End With
    End Sub

#End Region

#Region "Single Interface Update"
    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateEditFracInterface()
        ''Pantalla de EditFrac
        EditFrac.CbCentrifuga.Text = LanguageRM.GetString("AE.CbCentrifuga")
        EditFrac.LblBomba.Text = LanguageRM.GetString("AE.LblBomba")
        EditFrac.LblMotor.Text = LanguageRM.GetString("AE.LblMotor")
        EditFrac.LblNombre.Text = LanguageRM.GetString("AE.LblNombre")
        EditFrac.LblTransmision.Text = LanguageRM.GetString("AE.LblTransmision")
        EditFrac.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        EditFrac.Title.Text = LanguageRM.GetString("EF.Title")
        EditFrac.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateAddFracInterface()
        ''Pantalla de AddFrac
        AddFrac.CbCentrifuga.Text = LanguageRM.GetString("AE.CbCentrifuga")
        AddFrac.LblBomba.Text = LanguageRM.GetString("AE.LblBomba")
        AddFrac.LblMotor.Text = LanguageRM.GetString("AE.LblMotor")
        AddFrac.LblNombre.Text = LanguageRM.GetString("AE.LblNombre")
        AddFrac.LblTransmision.Text = LanguageRM.GetString("AE.LblTransmision")
        AddFrac.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        AddFrac.Title.Text = LanguageRM.GetString("AF.Title")
        AddFrac.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateConfigFracInterface()
        ''Pantalla de ConfigFrac(Manager de SQL)
        ConfigFrac.Title.Text = LanguageRM.GetString("ConfigFrac.Title")
        ConfigFrac.BtnAgregar.Text = LanguageRM.GetString("ConfigFrac.BtnAgregar")
        ConfigFrac.BtnEditar.Text = LanguageRM.GetString("ConfigFrac.BtnEditar")
        ConfigFrac.BtnEliminar.Text = LanguageRM.GetString("ConfigFrac.BtnEliminar")
        CenterObject(ConfigFrac.Title)
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateConfigWITSInterface()
        ''Pantalla de WITS
        ConfigWITS.LblDato1.Text = LanguageRM.GetString("SerialPortConfig.LblDato1")
        ConfigWITS.LblDatos.Text = LanguageRM.GetString("SerialPortConfig.LblDatos")
        ConfigWITS.LblPuertoSerie.Text = LanguageRM.GetString("SerialPortConfig.LblPuertoSerie")
        ConfigWITS.LblWITS1.Text = LanguageRM.GetString("SerialPortConfig.LblSP1")
        ConfigWITS.LblWITS2.Text = LanguageRM.GetString("SerialPortConfig.LblSP2")
        ConfigWITS.LblWITS3.Text = LanguageRM.GetString("SerialPortConfig.LblSP3")
        ConfigWITS.LblWITS4.Text = LanguageRM.GetString("SerialPortConfig.LblSP4")
        ConfigWITS.LblWITS5.Text = LanguageRM.GetString("SerialPortConfig.LblSP5")
        ConfigWITS.LblWITS7.Text = LanguageRM.GetString("SerialPortConfig.LblSP8")
        ConfigWITS.LblWITS8.Text = DynamicLanguageRM.GetString("Dato")
        ConfigWITS.Title.Text = LanguageRM.GetString("SerialPortConfig.Title")
        ConfigWITS.BtnAgregarDato.Text = LanguageRM.GetString("AgregarDato")
        ConfigWITS.BtnDelDato.Text = LanguageRM.GetString("DelDato")
        ConfigWITS.BtnGuardar.Text = LanguageRM.GetString("Guardar")
        ConfigWITS.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSerialPortInterface()
        ''Pantalla de Serial Port
        SerialPortConfig.LblDato1.Text = LanguageRM.GetString("SerialPortConfig.LblDato1")
        SerialPortConfig.LblDatos.Text = LanguageRM.GetString("SerialPortConfig.LblDatos")
        SerialPortConfig.LblPuertoSerie.Text = LanguageRM.GetString("SerialPortConfig.LblPuertoSerie")
        SerialPortConfig.LblSP1.Text = LanguageRM.GetString("SerialPortConfig.LblSP1")
        SerialPortConfig.LblSP2.Text = LanguageRM.GetString("SerialPortConfig.LblSP2")
        SerialPortConfig.LblSP3.Text = LanguageRM.GetString("SerialPortConfig.LblSP3")
        SerialPortConfig.LblSP4.Text = LanguageRM.GetString("SerialPortConfig.LblSP4")
        SerialPortConfig.LblSP5.Text = LanguageRM.GetString("SerialPortConfig.LblSP5")
        SerialPortConfig.LblSP6.Text = LanguageRM.GetString("SerialPortConfig.LblSP6")
        SerialPortConfig.LblSP7.Text = LanguageRM.GetString("SerialPortConfig.LblSP7")
        SerialPortConfig.LblSP8.Text = LanguageRM.GetString("SerialPortConfig.LblSP8")
        SerialPortConfig.Title.Text = LanguageRM.GetString("SerialPortConfig.Title")
        SerialPortConfig.BtnAgregarDato.Text = LanguageRM.GetString("AgregarDato")
        SerialPortConfig.BtnDelDato.Text = LanguageRM.GetString("DelDato")
        SerialPortConfig.BtnGuardar.Text = LanguageRM.GetString("Guardar")
        SerialPortConfig.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateAjustesInterface()
        ''Pantalla de ajustes
        Ajustes.Title.Text = LanguageRM.GetString("Ajustes.Title")
        Ajustes.LblGeneral.Text = LanguageRM.GetString("Ajustes.LblGeneral")
        Ajustes.LblFracturadores.Text = LanguageRM.GetString("Ajustes.LblFracturadores")
        Ajustes.LblResolucion.Text = LanguageRM.GetString("Ajustes.LblResolucion")
        Ajustes.LblIdioma.Text = LanguageRM.GetString("Ajustes.LblIdioma")
        Ajustes.BtnComSerie.Text = LanguageRM.GetString("Ajustes.BtnComSerie")
        Ajustes.BtnWITS.Text = LanguageRM.GetString("Ajustes.BtnWITS")
        Ajustes.LblFracturador.Text = LanguageRM.GetString("Ajustes.LblFracturador")
        Ajustes.LblMotor.Text = LanguageRM.GetString("Ajustes.LblMotor")
        Ajustes.LblTransmision.Text = LanguageRM.GetString("Ajustes.LblTransmision")
        Ajustes.LblBomba.Text = LanguageRM.GetString("Ajustes.LblBomba")
        Ajustes.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        Ajustes.LblPantallaSimple.Text = LanguageRM.GetString("Ajustes.LblPantallaSimple")
        Ajustes.CenterEverything()
    End Sub


    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSecondScreenInterface()

        Secundaria.Title.Text = LanguageRM.GetString("Principal.Title")
        Secundaria.LblTOp.Text = LanguageRM.GetString("OneFrac.LblTOp")
        ''Container 2
        Container1_2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2_2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2_2.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4_2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container4_2.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4_2.Title3.Text = LanguageRM.GetString("Container.Title")
        Container4_2.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title2.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title3.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title5.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title6.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title7.Text = LanguageRM.GetString("Container.Title")
        Container8_2.Title8.Text = LanguageRM.GetString("Container.Title")
        Container1_2.CenterEverything()
        Container2_2.CenterEverything()
        Container4_2.CenterEverything()
        Container8_2.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateThirdScreenInterface()

        Tercera.Title.Text = LanguageRM.GetString("Principal.Title")
        Tercera.LblTOp.Text = LanguageRM.GetString("OneFrac.LblTOp")
        ''Container 2
        Container1_3.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2_3.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2_3.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4_3.Title1.Text = LanguageRM.GetString("Container.Title")
        Container4_3.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4_3.Title3.Text = LanguageRM.GetString("Container.Title")
        Container4_3.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title1.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title2.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title3.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title5.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title6.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title7.Text = LanguageRM.GetString("Container.Title")
        Container8_3.Title8.Text = LanguageRM.GetString("Container.Title")
        Container1_3.CenterEverything()
        Container2_3.CenterEverything()
        Container4_3.CenterEverything()
        Container8_3.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFirstScreenInterface()
        Principal.Title.Text = LanguageRM.GetString("Principal.Title")
        Principal.LblTotalizador.Text = LanguageRM.GetString("Container.LblTotalizador")
        Principal.LblCombustibleTotal.Text = LanguageRM.GetString("Container.LblCombustibleTotal")
        Principal.LblCaudalTotal.Text = LanguageRM.GetString("Container.LblCaudalTotal")
        Principal.LblPresionMaxima.Text = LanguageRM.GetString("Container.LblPresionMaxima")

        ''Container 1
        Container1.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2.Title1.Text = LanguageRM.GetString("Container.Title")
        Container2.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4.Title1.Text = LanguageRM.GetString("Container.Title")
        Container4.Title2.Text = LanguageRM.GetString("Container.Title")
        Container4.Title3.Text = LanguageRM.GetString("Container.Title")
        Container4.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8.Title1.Text = LanguageRM.GetString("Container.Title")
        Container8.Title2.Text = LanguageRM.GetString("Container.Title")
        Container8.Title3.Text = LanguageRM.GetString("Container.Title")
        Container8.Title4.Text = LanguageRM.GetString("Container.Title")
        Container8.Title5.Text = LanguageRM.GetString("Container.Title")
        Container8.Title6.Text = LanguageRM.GetString("Container.Title")
        Container8.Title7.Text = LanguageRM.GetString("Container.Title")
        Container8.Title8.Text = LanguageRM.GetString("Container.Title")
        Container1.CenterEverything()
        Container2.CenterEverything()
        Container4.CenterEverything()
        Container8.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFullAutomationInterface()
        ''FullAutomation
        FullAutomation.Title.Text = LanguageRM.GetString("FullAutomation.Title")
        ' FullAutomation.LblPaso.Text = LanguageRM.GetString("FullAutomation.LblPaso")
        FullAutomation.LblFA2.Text = LanguageRM.GetString("FullAutomation.LblFA2")
        FullAutomation.LblFA3.Text = LanguageRM.GetString("FullAutomation.LblFA3")
        FullAutomation.LblFA4.Text = LanguageRM.GetString("FullAutomation.LblFA4")
        ' FullAutomation.BtnAnterior.Text = LanguageRM.GetString("FullAutomation.BtnAnterior")
        FullAutomation.BtnStart.Text = LanguageRM.GetString("FullAutomation.BtnStart")
        '  FullAutomation.BtnSiguiente.Text = LanguageRM.GetString("FullAutomation.BtnSiguiente")
        FullAutomation.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateInfoShowInterface()
        ''InfoShow
        InfoShow.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateTesteoLineaInterface()
        ''TesteoLinea
        TesteoLinea.Title.Text = LanguageRM.GetString("TesteoLinea.Title")
        TesteoLinea.LblAjustes.Text = LanguageRM.GetString("TesteoLinea.LblAjustes")
        TesteoLinea.LblTL1.Text = LanguageRM.GetString("TesteoLinea.LblTL1")
        TesteoLinea.LblTL2.Text = LanguageRM.GetString("TesteoLinea.LblTL2")
        TesteoLinea.LblTL3.Text = LanguageRM.GetString("TesteoLinea.LblTL3")
        TesteoLinea.LblTL4.Text = LanguageRM.GetString("TesteoLinea.LblTL4")
        TesteoLinea.LblTL5.Text = LanguageRM.GetString("TesteoLinea.LblTL5")
        TesteoLinea.LblTL6.Text = LanguageRM.GetString("TesteoLinea.LblTL6")
        TesteoLinea.LblTL7.Text = LanguageRM.GetString("TesteoLinea.LblTL7")
        TesteoLinea.BtnComenzar.Text = LanguageRM.GetString("TesteoLinea.BtnComenzar")
        TesteoLinea.LblGrafico.Text = LanguageRM.GetString("TesteoLinea.LblGrafico")
        TesteoLinea.CenterEverything()
    End Sub


    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFracDiagnosticoInterface()
        ''FracDiagnostico
        FracDiagnostico.LblMantD.Text = LanguageRM.GetString("FracDiagnostico.LblMantD")
        FracDiagnostico.LblMCombustible.Text = LanguageRM.GetString("FracDiagnostico.LblMCombustible")
        FracDiagnostico.LblMHoras.Text = LanguageRM.GetString("FracDiagnostico.LblMHoras")
        FracDiagnostico.LblMPresAceite.Text = LanguageRM.GetString("FracDiagnostico.LblMPresAceite")
        FracDiagnostico.LblMTempAceite.Text = LanguageRM.GetString("FracDiagnostico.LblMTempAceite")
        FracDiagnostico.LblMTempAgua.Text = LanguageRM.GetString("FracDiagnostico.LblMTempAgua")
        FracDiagnostico.LblSPresDescarga.Text = LanguageRM.GetString("FracDiagnostico.LblSPresDescarga")
        FracDiagnostico.LblSPresLub.Text = LanguageRM.GetString("FracDiagnostico.LblSPresLub")
        FracDiagnostico.LblSPresSuccion.Text = LanguageRM.GetString("FracDiagnostico.LblSPresSuccion")
        FracDiagnostico.LblSTempLub.Text = LanguageRM.GetString("FracDiagnostico.LblSTempLub")
        FracDiagnostico.LblTareas1.Text = LanguageRM.GetString("FracDiagnostico.LblTareas1")
        FracDiagnostico.LblTareas10.Text = LanguageRM.GetString("FracDiagnostico.LblTareas10")
        FracDiagnostico.LblTareas11.Text = LanguageRM.GetString("FracDiagnostico.LblTareas11")
        FracDiagnostico.LblTareas2.Text = LanguageRM.GetString("FracDiagnostico.LblTareas2")
        FracDiagnostico.LblTareas3.Text = LanguageRM.GetString("FracDiagnostico.LblTareas3")
        FracDiagnostico.LblTareas4.Text = LanguageRM.GetString("FracDiagnostico.LblTareas4")
        FracDiagnostico.LblTareas5.Text = LanguageRM.GetString("FracDiagnostico.LblTareas5")
        FracDiagnostico.LblTareas6.Text = LanguageRM.GetString("FracDiagnostico.LblTareas6")
        FracDiagnostico.LblTareas7.Text = LanguageRM.GetString("FracDiagnostico.LblTareas7")
        FracDiagnostico.LblTareas8.Text = LanguageRM.GetString("FracDiagnostico.LblTareas8")
        FracDiagnostico.LblTareas9.Text = LanguageRM.GetString("FracDiagnostico.LblTareas9")
        FracDiagnostico.LblTPresAceite.Text = LanguageRM.GetString("FracDiagnostico.LblTPresAceite")
        FracDiagnostico.LblTTempAceite.Text = LanguageRM.GetString("FracDiagnostico.LblTTempAceite")
        FracDiagnostico.LblUnidadD.Text = LanguageRM.GetString("FracDiagnostico.LblUnidadD")
        FracDiagnostico.Title.Text = LanguageRM.GetString("FracDiagnostico.Title")
        FracDiagnostico.LblSensores.Text = LanguageRM.GetString("FracDiagnostico.LblSensores")
        FracDiagnostico.LblMotor.Text = LanguageRM.GetString("FracDiagnostico.LblMotor")
        FracDiagnostico.LblTransmision.Text = LanguageRM.GetString("FracDiagnostico.LblTransmision")
        FracDiagnostico.LblTarea.Text = LanguageRM.GetString("FracDiagnostico.LblTarea")
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFracMantenimientoInterface()
        ''FracMantenimiento
        FracMantenimiento.Title.Text = LanguageRM.GetString("FracMantenimiento.Title")
        FracMantenimiento.LblTB.Text = LanguageRM.GetString("FracMantenimiento.LblTB")
        FracMantenimiento.CbPassMem.Text = LanguageRM.GetString("FracMantenimiento.CbPassMem")
        FracMantenimiento.CbMant.Text = LanguageRM.GetString("FracMantenimiento.CbMant")
        FracMantenimiento.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        FracMantenimiento.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateNewPasswordInterface()
        ''New Password
        NewPassword.Title.Text = LanguageRM.GetString("NewPassword.Title")
        NewPassword.LblTB.Text = LanguageRM.GetString("NewPassword.LblTB")
        NewPassword.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        NewPassword.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateAdminControlInterface()
        ''Admin Control
        AdminControl.Title.Text = LanguageRM.GetString("AdminControl.Title")
        AdminControl.LblTB.Text = LanguageRM.GetString("AdminControl.LblTB")
        AdminControl.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        AdminControl.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSetZeroInterface()
        ''SetZERO
        SetZERO.Title.Text = LanguageRM.GetString("SetZERO.Title")
        SetZERO.LblTB.Text = LanguageRM.GetString("SetZERO.LblTB")
        SetZERO.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        SetZERO.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSetTRIPInterface()
        ''SetTRIP
        SetTRIP.Title.Text = LanguageRM.GetString("SetTRIP.Title")
        SetTRIP.LblTB.Text = LanguageRM.GetString("SetTRIP.LblTB")
        SetTRIP.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        SetTRIP.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSetRPMInterface()
        ''SetRPM
        SetRPM.Title.Text = LanguageRM.GetString("SetRPM.Title")
        SetRPM.LblTB.Text = LanguageRM.GetString("SetRPM.LblTB")
        SetRPM.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        SetRPM.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSetGEARInterface()
        ''SetGEAR
        SetGEAR.Title.Text = LanguageRM.GetString("SetGEAR.Title")
        SetGEAR.LblTB.Text = LanguageRM.GetString("SetGEAR.LblTB")
        SetGEAR.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        SetGEAR.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateSetFCInterface()
        ''SetFC
        SetFC.Title.Text = LanguageRM.GetString("SetFC.Title")
        SetFC.LblTB.Text = LanguageRM.GetString("SetFC.LblTB")
        SetFC.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        SetFC.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFracErroresInterface()
        ''Frac Errores
        FracErrores.Title.Text = LanguageRM.GetString("FracErrores.Title")
        FracErrores.LblInfo.Text = LanguageRM.GetString("FracErrores.LblInfo")
        FracErrores.LblErroresTotales.Text = LanguageRM.GetString("FracErrores.LblErroresTotales")
        FracErrores.LblProcedenciaD.Text = LanguageRM.GetString("FracErrores.LblProcedenciaD")
        FracErrores.LblActivoD.Text = LanguageRM.GetString("FracErrores.LblActivoD")
        FracErrores.LblUnidadD.Text = LanguageRM.GetString("FracErrores.LblUnidadD")
        FracErrores.LblDescripcionD.Text = LanguageRM.GetString("FracErrores.LblDescripcionD")
        FracErrores.BtnAnterior.Text = LanguageRM.GetString("FracErrores.BtnAnterior")
        FracErrores.BtnSiguiente.Text = LanguageRM.GetString("FracErrores.BtnSiguiente")
        FracErrores.BtnBorrarActual.Text = LanguageRM.GetString("FracErrores.BtnBorrarActual")
        FracErrores.BtnBorrarTodo.Text = LanguageRM.GetString("FracErrores.BtnBorrarTodo")
        FracErrores.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateAjustesFracturadorInterface()
        ''Ajustes Fracturador
        AjustesFracturador.Title.Text = LanguageRM.GetString("AjustesFracturador.Title")
        AjustesFracturador.CbCentrifuga.Text = LanguageRM.GetString("AjustesFracturador.CbCentrifuga")
        AjustesFracturador.LblCentrifuga.Text = LanguageRM.GetString("AjustesFracturador.LblCentrifuga")
        AjustesFracturador.LblCentrifugaD.Text = LanguageRM.GetString("AjustesFracturador.LblCentrifugaD")
        'AjustesFracturador.LblMaximoEntradaD.Text = LanguageRM.GetString("AjustesFracturador.LblMaximoEntradaD")
        'AjustesFracturador.LblMaximoSalidaD.Text = LanguageRM.GetString("AjustesFracturador.LblMaximoSalidaD")
        'AjustesFracturador.LblMinimoEntradaD.Text = LanguageRM.GetString("AjustesFracturador.LblMinimoEntradaD")
        'AjustesFracturador.LblMinimoSalidaD.Text = LanguageRM.GetString("AjustesFracturador.LblMinimoSalidaD")
        'AjustesFracturador.LblSensores.Text = LanguageRM.GetString("AjustesFracturador.LblSensores")
        'AjustesFracturador.LblSensoresD.Text = LanguageRM.GetString("AjustesFracturador.LblSensoresD")
        AjustesFracturador.BtnAceptar.Text = LanguageRM.GetString("BtnAceptar")
        'AjustesFracturador.BtnAdmin.Text = LanguageRM.GetString("AjustesFracturador.BtnAdmin")

        'AjustesFracturador.CbSensores.Items.Clear()
        'AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[0]"))
        'AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[1]"))
        'AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[2]"))
        'AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[3]"))
        'AjustesFracturador.CbSensores.Items.Add(LanguageRM.GetString("AjustesFracturador.CbSensores[4]"))

        AjustesFracturador.LblComunicacion.Text = LanguageRM.GetString("AjustesFracturador.LblComunicacion")
        AjustesFracturador.LblSP.Text = LanguageRM.GetString("AjustesFracturador.LblSP")

        AjustesFracturador.CenterEverything()
    End Sub

    ''' <summary>
    ''' Sub para editar interfaz.
    ''' </summary>
    Private Sub UpdateFracSelectInterface()
        ''Frac Select
        FracSelect.Title.Text = LanguageRM.GetString("FracSelect.Title")
    End Sub
#End Region



End Class
