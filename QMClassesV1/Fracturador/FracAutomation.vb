﻿Imports System.Threading
Imports System.Windows.Forms

Public Class FracAutomation

    ''Timer y stopwatch para el control de la fractura
    Private WithEvents TmrNStart As New Timers.Timer(1000)
    Private ReadOnly NStopWatch As New Stopwatch

    ''Eventos en cambio de etapa y comienzo de operacion
    Public Shared Event EtapaChange()
    Public Shared Event AutomationStart()

    ''Variables de objetos para el calculo de la automatizacion
    Private Shared FracSystem As IFracSystem
    Private Shared FracData As FData

    Public Shared FracAutomationSettings As New Dictionary(Of Integer, Dictionary(Of String, Double))

    ''Lista de fracturadores a usar y un fracturador de backup
    Public Shared FracList As New List(Of Integer)
    Public Shared LostFracs As New List(Of Integer)
    Private Shared ReadOnly BackupFrac As Integer

    ''Eficiencia mayor al 70
    Public Shared EfficiencyFlag As Boolean

    ''Variables a usar cuando falla un frac
    Public Shared FracFails As Integer
    Public Shared LostFlowRate As Double
    Private Shared ReadOnly THold As Boolean = False
    Private Shared ReadOnly DeltaHold As Integer = 0

    ''Eventos
    Private Shared Event TRIPChanged()

    ''HHP
    Private Shared HHP As Integer
    Private Shared AvailableHP As Integer
    Private Shared AverageUtilization As Double

#Region "Properties 1"
    Private Shared _currentpressure As Integer
    Public Shared Property CurrentPressure() As Integer
        Get
            Return _currentpressure
        End Get
        Set(value As Integer)
            If value <> _currentpressure Then
                _currentpressure = value
                If value > TRIP Then
                    RaiseEvent TRIPChanged()
                End If
            End If
        End Set
    End Property
    Private Shared _safeflowrate As Double
    Public Shared Property SafeFlowRate() As Double
        Get
            Return _safeflowrate
        End Get
        Set(value As Double)
            If value <> _safeflowrate Then
                _safeflowrate = Math.Min(Math.Abs(value), 1)
            End If
        End Set
    End Property
    Private Shared _trip As Integer
    Public Shared Property TRIP() As Integer
        Get
            Return _trip
        End Get
        Set(value As Integer)
            If value <> _trip Then
                _trip = value
            End If
        End Set
    End Property
    Private Shared _nfrac As Integer
    Public Shared Property NFrac() As Integer
        Get
            Return _nfrac
        End Get
        Set(value As Integer)
            If value <> _nfrac Then
                _nfrac = value
            End If
        End Set
    End Property
    Private Shared _activenfrac As Integer
    Public Shared Property ActiveNFrac() As Integer
        Get
            Return _activenfrac
        End Get
        Set(value As Integer)
            If value <> _activenfrac Then
                _activenfrac = value
            End If
        End Set
    End Property
    Private Shared _caudaltotal As Double
    Public Shared Property CaudalTotal() As Double
        Get
            Return _caudaltotal
        End Get
        Set(value As Double)
            If value <> _caudaltotal Then
                _caudaltotal = value
            End If
        End Set
    End Property
    Private Shared _barriles As Double
    Public Shared Property Barriles() As Double
        Get
            Return _barriles
        End Get
        Set(value As Double)
            If value <> _barriles Then
                _barriles = value
            End If
        End Set
    End Property
    Private Shared _duracionetapa As Double
    Public Shared Property DuracionEtapa() As Double
        Get
            Return _duracionetapa
        End Get
        Set(value As Double)
            If value <> _duracionetapa Then
                _duracionetapa = value
            End If
        End Set
    End Property
    Private Shared _flowrate As New Dictionary(Of Integer, Double)
    Public Shared Property FlowRate() As Dictionary(Of Integer, Double)
        Get
            Return _flowrate
        End Get
        Set(value As Dictionary(Of Integer, Double))
            If value IsNot _flowrate Then
                _flowrate = value
            End If
        End Set
    End Property
    Private Shared _autostart As Boolean = False
    Public Shared Property AutoStart() As Boolean
        Get
            Return _autostart
        End Get
        Set(ByVal value As Boolean)
            If value <> _autostart Then
                _autostart = value
                RaiseEvent AutomationStart()
            End If
        End Set
    End Property
#End Region


    ''' <summary>
    ''' Funcion para actualizar las variables de la clase.
    ''' </summary>
    Public Sub Update(ByVal ISystem As IFracSystem, ByVal DataSet As FData)
        FracSystem = ISystem
        FracData = DataSet
    End Sub

    ''' <summary>
    ''' Funcion para actualizar los parametros iniciales para el calculo automatico de la fractura.
    ''' </summary>
    Public Function SetParameters(ByVal sNFrac As Integer, ByVal sTRIP As Integer, ByVal sCaudal As Double, ByVal sBarriles As Double, ByVal sID As List(Of Integer)) As Boolean

        FracList = sID                ''ID's de los frac a usar
        TRIP = sTRIP                ''TRIP -> IGUAL PARA TODOS LOS FRAC
        NFrac = sNFrac              ''Cantidad de fracturadores a usar
        ActiveNFrac = sNFrac        ''Cantidad de fracturadores activos (inicializamos la variable)
        CaudalTotal = sCaudal       ''Caudal TOTAL (es unico, por etapa)
        Barriles = sBarriles        ''Barriles totales por etapa

        InitFracData()            ''Iniciamos el diccionario para todos los datos necesarios

        If CheckStatus(True) = True Then
            GetEachHP()               ''Obtenemos los HP para cada frac
            GetNCaudal()              ''Obtenemos el caudal para cada frac
            GetRPMRgi()               ''Sacamos Rgi para cada Frac
            GetAvailableGears()              ''Sacamos maxima y minima marcha para cada frac
            GetDuracion()             ''Sacamos la duracion de cada etapa
            GetNearestSettings()      ''Sacamos la configuracion final para cada frac
            Return True
        Else
            Return False
        End If
    End Function


    ''' <summary>
    ''' Funcion para iniciar los diccionarios con los datos de la automatizacion
    ''' </summary>
    Private Sub InitFracData()
        For ActualFrac = 0 To NFrac - 1
            FracAutomationSettings.Add(FracList(ActualFrac), New Dictionary(Of String, Double))
        Next
        HHP = (CaudalTotal * TRIP) / 40.8
        GetNHP()
        AverageUtilization = HHP / AvailableHP
    End Sub

    ''' <summary>
    ''' Funcion para obtener los HP disponibles.
    ''' </summary>
    Private Sub GetNHP()
        For ActualFrac = 0 To NFrac - 1
            AvailableHP += FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString()).Item("MaxHP")
        Next
    End Sub

    ''' <summary>
    ''' Funcion para obtener los caudales segun cada fracturador.
    ''' </summary>
    Private Sub GetEachHP()
        For ActualFrac = 0 To NFrac - 1
            Dim UsedHP As Integer = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("MaxHP") * AverageUtilization
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("UsedHP", UsedHP)
        Next
    End Sub

    ''' <summary>
    ''' Funcion para obtener los caudales segun cada fracturador.
    ''' </summary>
    Private Sub GetNCaudal()
        Dim AllHP As Object
        ReDim AllHP(NFrac - 1)
        For ActualFrac = 0 To NFrac - 1
            AllHP(ActualFrac) = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("MaxHP")
        Next

        Dim MaxHP = GetMax(AllHP)
        Dim NormalFactorSUM As Double = 0
        For ActualFrac = 0 To NFrac - 1
            Dim ActualFracHP As Integer = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("MaxHP")
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("NormalFactor", ActualFracHP / MaxHP)
            NormalFactorSUM += ActualFracHP / MaxHP
        Next

        For ActualFrac = 0 To NFrac - 1
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("Caudal", CaudalTotal * FracAutomationSettings.Item(FracList(ActualFrac)).Item("NormalFactor") / NormalFactorSUM)
        Next
    End Sub


    Private Function CheckStatus(ByVal ShowStat As Boolean) As Boolean

        If HHP > AvailableHP Then
            If ShowStat = True Then
                avisoStr = "La potencia requerida para la fractura es de " + HHP.ToString() + " HHP, pero solo se cuenta con " + (AvailableHP).ToString() + " HHP."
                avisoTitulo = "Error"
                Dim AG As New AvisoGeneral
                AG.ShowDialog()
            End If
            Return False
        ElseIf AverageUtilization > 0.7 Then
            EfficiencyFlag = True
            Return True
        Else
            EfficiencyFlag = False
            Return True
        End If
    End Function

    ''' <summary>
    ''' Funcion para calcular duracion de cada etapa.
    ''' </summary>
    Private Sub GetDuracion()
        DuracionEtapa = (Barriles / CaudalTotal) * 60
    End Sub

    ''' <summary>
    ''' Funcion para calcular RPM/Rgi.
    ''' </summary>
    Private Sub GetRPMRgi()
        For ActualFrac = 0 To NFrac - 1
            Dim rGalRev As Double = FracData.SelectedPumps("SelectedPump_" + FracList(ActualFrac).ToString())("rGalRev")
            Dim rTransBomba As Double = FracData.SelectedPumps("SelectedPump_" + FracList(ActualFrac).ToString())("rTransBomba")
            Dim Caudal As Double = FracAutomationSettings.Item(FracList(ActualFrac)).Item("Caudal")
            Dim CurrentRr As Double = (42 * rTransBomba * Caudal) / (rGalRev)
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("RPMrgi", CurrentRr)
        Next
    End Sub




    ''' <summary>
    ''' Obtenemos las marchas minimas y maximas disponibles
    ''' </summary>
    Private Sub GetAvailableGears()
        For ActualFrac = 0 To NFrac - 1
            Dim MinGear As Integer = GetMinGear(ActualFrac)
            Dim MaxGear As Integer = GetMaxGear(ActualFrac)
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("MinGear", MinGear)
            FracAutomationSettings.Item(FracList(ActualFrac)).Add("MaxGear", MaxGear)
        Next
    End Sub

    Private Function GetMinGear(ByVal ActualFrac As Integer) As Integer
        Dim MaxRgi As Double = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("MaxRPM") / FracAutomationSettings.Item(FracList(ActualFrac))("RPMrgi")
        For i = 1 To 7
            If FracData.SelectedTransmissions("SelectedTransmission_" + FracList(ActualFrac).ToString()).Item("RelacionGear" + i.ToString()) < MaxRgi Then
                Return i
                Exit Function
            End If
        Next
        Return 0
    End Function

    Private Function GetMaxGear(ByVal ActualFrac As Integer) As Integer
        Dim MinRgi As Double = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("MinRPM") / FracAutomationSettings.Item(FracList(ActualFrac))("RPMrgi")
        For i = 7 To 1 Step -1
            If FracData.SelectedTransmissions("SelectedTransmission_" + FracList(ActualFrac).ToString()).Item("RelacionGear" + i.ToString()) > MinRgi Then
                Return i
            End If
        Next
        Return 0
    End Function


    ''' <summary>
    ''' Obtenemos los RPM para la marcha actual.
    ''' </summary>
    Private Sub GetNearestSettings()
        For ActualFrac = 0 To NFrac - 1
            Dim MinGear As Integer = FracAutomationSettings.Item(FracList(ActualFrac))("MinGear")
            Dim MaxGear As Integer = FracAutomationSettings.Item(FracList(ActualFrac))("MaxGear")
            Dim rGalRev As Double = FracData.SelectedPumps("SelectedPump_" + FracList(ActualFrac).ToString())("rGalRev")
            Dim rTransBomba As Double = FracData.SelectedPumps("SelectedPump_" + FracList(ActualFrac).ToString())("rTransBomba")
            Dim Caudal As Double = FracAutomationSettings.Item(FracList(ActualFrac))("Caudal")
            Dim EfficientRPM As Integer = FracData.SelectedEngines("SelectedEngine_" + FracList(ActualFrac).ToString())("EfficientRPM")
            Dim GearRange As Integer = MaxGear - MinGear

            Dim RPMRange(GearRange) As Integer
            For ThisGear = 0 To GearRange
                RPMRange(ThisGear) = (Caudal * 42 * rTransBomba * FracData.SelectedTransmissions("SelectedTransmission_" + FracList(ActualFrac).ToString()).Item("RelacionGear" + (ThisGear + MinGear).ToString())) / rGalRev
            Next
            FracAutomationSettings(FracList(ActualFrac)).Add("TargetRPM", GetClosestByValue(RPMRange, EfficientRPM))
            FracAutomationSettings(FracList(ActualFrac)).Add("TargetGear", GetClosestByIndex(RPMRange, EfficientRPM) + MinGear)
        Next
    End Sub



    ''' <summary>
    ''' Funcion para actualizar presion maxima actual.
    ''' </summary>
    Public Sub UpdatePressure(ByVal Pressure As Integer)
        CurrentPressure = Pressure
    End Sub

    ''' <summary>
    ''' Funcion para actualizar el caudal actual.
    ''' </summary>
    Public Sub UpdateFlowRate(ByVal sFlowRate As Double, ByVal CurrentFrac As Integer)
        If FlowRate.ContainsKey(CurrentFrac) Then
            FlowRate.Item(CurrentFrac) = sFlowRate
        Else
            FlowRate.Add(CurrentFrac, sFlowRate)
        End If
    End Sub

    ''' <summary>
    ''' Funcion para iniciar operacion.
    ''' </summary>
    Public Sub StartOp()
        Dim StartTask As New Task(AddressOf StartOpTask)
        StartTask.Start()
    End Sub

    Private Async Sub StartOpTask()

        AutoStart = True

        For i = 0 To ActiveNFrac - 1
            FracSystem.MoxaSend(FracMoxaSendStartAddress("TRIPStartAddress"), TRIP, FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("ResetRPMStartAddress"), 1, FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("BrakeStartAddress"), FracGeneralValues("BrakeOFF"), FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("RPMStartAddress"), FracAutomationSettings(FracList(i))("TargetRPM"), FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("GearStartAddress"), 125 + FracAutomationSettings(FracList(i))("TargetGear"), FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("ResetRPMStartAddress"), 0, FracList.Item(i))
        Next
        NStopWatch.Start()
        TmrNStart.Start()
    End Sub

    ''' <summary>
    ''' Funcion para detener operacion.
    ''' </summary>
    Public Sub StopOp()
        Dim StopTask As New Task(AddressOf StopOpTask)
        StopTask.Start()
    End Sub

    Private Async Sub StopOpTask()
        TmrNStart.Stop()
        NStopWatch.Reset()
        AutoStart = False
        For i = 0 To ActiveNFrac - 1
            FracSystem.MoxaSend(FracMoxaSendStartAddress("BrakeStartAddress"), FracGeneralValues("BrakeOFF"), FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("RPMStartAddress"), FracData.SelectedEngines("SelectedEngine_" + FracList.Item(i).ToString())("MinRPM"), FracList.Item(i))
            Await Task.Delay(50)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("GearStartAddress"), 125 + FracGeneralValues("MarchaN"), FracList.Item(i))
        Next
    End Sub

    ''' <summary>
    ''' Tick timer.
    ''' </summary>
    Private Sub TmrTick() Handles TmrNStart.Elapsed
        Dim StartTh As New Task(AddressOf FracDo)
        StartTh.Start()
    End Sub


    ''' <summary>
    ''' Thread de timer.
    ''' </summary>
    Private Async Sub FracDo()

        Dim ThisTime As Decimal = NStopWatch.ElapsedTicks / Stopwatch.Frequency

        If ThisTime >= DuracionEtapa Then
            StopOp()


        Else
            'If THold = True And DeltaHold < 20 Then
            '    DeltaHold += 1
            'Else
            '    THold = False
            '    DeltaHold = 0
            '    CheckFlowRate()
            'End If
        End If
        Await Task.Delay(1)
    End Sub





    'Private Sub CheckFlowRate()
    '    TmrNStart.Stop()
    '    FracFails = 0
    '    LostFlowRate = 0
    '    Dim FracsRemove As New List(Of Integer)

    '    For i = 0 To ActiveNFrac - 1
    '        If FlowRate.ElementAt(i).Value < SafeFlowRate * OneCaudal Then
    '            LostFracs.Add(FracList(i - FracFails))
    '            FracList.RemoveAt(i - FracFails)
    '            FracsRemove.Add(i)
    '            FracFails += 1
    '            LostFlowRate += OneCaudal - FlowRate.ElementAt(i).Value
    '        End If
    '    Next

    '    For Fail = 0 To FracFails - 1
    '        FlowRate.Remove(FlowRate.ElementAt(FracsRemove(Fail) - Fail).Key)
    '    Next

    '    If FracFails > 0 Then
    '        THold = True
    '        ActiveNFrac -= FracFails
    '        OneCaudal = CaudalTotal / ActiveNFrac
    '        If CheckStatus(False) = True Then
    '            GetRPMRgi()
    '            GetMinGear()
    '            GetMaxGear()
    '            GetNearestSettings()
    '        Else
    '            If BackupFrac <> 0 Then
    '                ActiveNFrac += 1
    '                If CheckStatus(False) = True Then
    '                    avisoStr = "Desea agregar el backup para seguir?"
    '                    avisoTitulo = "Aviso"
    '                    Dim AG2 As New AvisoGeneral2
    '                    If AG2.ShowDialog() = DialogResult.Yes Then
    '                        FracList.Add(BackupFrac)
    '                        GetRPMRgi()
    '                        GetMinGear()
    '                        GetMaxGear()
    '                        GetNearestSettings()
    '                        For i = 0 To ActiveNFrac - 1
    '                            FracSystem.MoxaSend(FracMoxaSendStartAddress("BrakeStartAddress"), FracGeneralValues("BrakeOFF"), FracList.Item(i))
    '                            FracSystem.MoxaSend(FracMoxaSendStartAddress("RPMStartAddress"), TargetRPM, FracList.Item(i))
    '                            FracSystem.MoxaSend(FracMoxaSendStartAddress("GearStartAddress"), 125 + TargetGEAR, FracList.Item(i))
    '                        Next
    '                        RaiseEvent AutomationStart()
    '                    Else
    '                        ActiveNFrac -= 1
    '                    End If
    '                Else
    '                    ActiveNFrac = 0
    '                    StopOp()
    '                End If

    '            End If
    '        End If
    '    End If
    'End Sub



End Class
