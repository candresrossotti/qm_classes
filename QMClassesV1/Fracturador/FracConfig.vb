﻿Imports System.Threading
Public Class FracConfig

    Public Shared Event ConfigComplete()

    Public SettingsRM As New Dictionary(Of String, Integer)() From {
        {"VIATRAN_SENSOR_SCALE_X0", 0},
        {"VIATRAN_SENSOR_SCALE_X1", 0},
        {"VIATRAN_SENSOR_SCALE_Y0", 0},
        {"VIATRAN_SENSOR_SCALE_Y1", 0},
        {"VIATRAN_SENSOR_SCALE_CAPMIN", 0},
        {"VIATRAN_SENSOR_SCALE_CAPMAX", 0},
        {"VIATRAN_SENSOR_SCALE_CAP", 0},
        {"VIATRAN_SCALED_ALARM_SETPOINT", 0},
        {"VIATRAN_SCALED_ALARM_ARMED", 0},
        {"VIATRAN_FILTER_EN", 0},
        {"VIATRAN_FILTER_TOL", 0},
        {"VIATRAN_FILTER_TSMPL", 0},
        {"VIATRAN_FILTER_NSMPL", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_X0", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_X1", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_Y0", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_Y1", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_CAPMIN", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_CAPMAX", 0},
        {"TEMPLUB_LP_SENSOR_SCALE_CAP", 0},
        {"TEMPLUB_LP_SCALED_ALARM_SETPOINT", 0},
        {"TEMPLUB_LP_SCALED_ALARM_ARMED", 0},
        {"TEMPLUB_LP_FILTER_EN", 0},
        {"TEMPLUB_LP_FILTER_TOL", 0},
        {"TEMPLUB_LP_FILTER_TSMPL", 0},
        {"TEMPLUB_LP_FILTER_NSMPL", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_X0", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_X1", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_Y0", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_Y1", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_CAPMIN", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_CAPMAX", 0},
        {"TEMPLUB_HP_SENSOR_SCALE_CAP", 0},
        {"TEMPLUB_HP_SCALED_ALARM_SETPOINT", 0},
        {"TEMPLUB_HP_SCALED_ALARM_ARMED", 0},
        {"TEMPLUB_HP_FILTER_EN", 0},
        {"TEMPLUB_HP_FILTER_TOL", 0},
        {"TEMPLUB_HP_FILTER_TSMPL", 0},
        {"TEMPLUB_HP_FILTER_NSMPL", 0},
        {"PRESLUB_LP_SENSOR_SCALE_X0", 0},
        {"PRESLUB_LP_SENSOR_SCALE_X1", 0},
        {"PRESLUB_LP_SENSOR_SCALE_Y0", 0},
        {"PRESLUB_LP_SENSOR_SCALE_Y1", 0},
        {"PRESLUB_LP_SENSOR_SCALE_CAPMIN", 0},
        {"PRESLUB_LP_SENSOR_SCALE_CAPMAX", 0},
        {"PRESLUB_LP_SENSOR_SCALE_CAP", 0},
        {"PRESLUB_LP_SCALED_ALARM_SETPOINT", 0},
        {"PRESLUB_LP_SCALED_ALARM_ARMED", 0},
        {"PRESLUB_LP_FILTER_EN", 0},
        {"PRESLUB_LP_FILTER_TOL", 0},
        {"PRESLUB_LP_FILTER_TSMPL", 0},
        {"PRESLUB_LP_FILTER_NSMPL", 0},
        {"PRESLUB_HP_SENSOR_SCALE_X0", 0},
        {"PRESLUB_HP_SENSOR_SCALE_X1", 0},
        {"PRESLUB_HP_SENSOR_SCALE_Y0", 0},
        {"PRESLUB_HP_SENSOR_SCALE_Y1", 0},
        {"PRESLUB_HP_SENSOR_SCALE_CAPMIN", 0},
        {"PRESLUB_HP_SENSOR_SCALE_CAPMAX", 0},
        {"PRESLUB_HP_SENSOR_SCALE_CAP", 0},
        {"PRESLUB_HP_SCALED_ALARM_SETPOINT", 0},
        {"PRESLUB_HP_SCALED_ALARM_ARMED", 0},
        {"PRESLUB_HP_FILTER_EN", 0},
        {"PRESLUB_HP_FILTER_TOL", 0},
        {"PRESLUB_HP_FILTER_TSMPL", 0},
        {"PRESLUB_HP_FILTER_NSMPL", 0},
        {"PRESSUC_SENSOR_SCALE_X0", 0},
        {"PRESSUC_SENSOR_SCALE_X1", 0},
        {"PRESSUC_SENSOR_SCALE_Y0", 0},
        {"PRESSUC_SENSOR_SCALE_Y1", 0},
        {"PRESSUC_SENSOR_SCALE_CAPMIN", 0},
        {"PRESSUC_SENSOR_SCALE_CAPMAX", 0},
        {"PRESSUC_SENSOR_SCALE_CAP", 0},
        {"PRESSUC_SCALED_ALARM_SETPOINT", 0},
        {"PRESSUC_SCALED_ALARM_ARMED", 0},
        {"PRESSUC_FILTER_EN", 0},
        {"PRESSUC_FILTER_TOL", 0},
        {"PRESSUC_FILTER_TSMPL", 0},
        {"PRESSUC_FILTER_NSMPL", 0},
        {"PRESTRANS_SENSOR_SCALE_X0", 0},
        {"PRESTRANS_SENSOR_SCALE_X1", 0},
        {"PRESTRANS_SENSOR_SCALE_Y0", 0},
        {"PRESTRANS_SENSOR_SCALE_Y1", 0},
        {"PRESTRANS_SENSOR_SCALE_CAPMIN", 0},
        {"PRESTRANS_SENSOR_SCALE_CAPMAX", 0},
        {"PRESTRANS_SENSOR_SCALE_CAP", 0},
        {"PRESTRANS_SCALED_ALARM_SETPOINT", 0},
        {"PRESTRANS_SCALED_ALARM_ARMED", 0},
        {"PRESTRANS_FILTER_EN", 0},
        {"PRESTRANS_FILTER_TOL", 0},
        {"PRESTRANS_FILTER_TSMPL", 0},
        {"PRESTRANS_FILTER_NSMPL", 0},
        {"CAUDAL_FILTER_EN", 0},
        {"CAUDAL_FILTER_TOL", 0},
        {"CAUDAL_FILTER_TSMPL", 0},
        {"CAUDAL_FILTER_NSMPL", 0},
        {"CAUDAL_SCALED_ALARM_SETPOINT", 0},
        {"CAUDAL_SCALED_ALARM_ARMED", 0},
        {"UseSource", 0},
        {"MotorSource", 0},
        {"TransmisionSource", 0},
        {"rGalRev", 0},
        {"rTransBomba", 0},
        {"VB_CTRL", 0},
        {"CurrentMotor", 0},
        {"CurrentTrans", 0},
        {"CurrentPump", 0}
    }


    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      PROPIEDADES PARA TODA LA CLASE     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''
    Private Shared _isystem As IFracSystem
    Private Shared Property FracSystem() As IFracSystem
        Get
            Return _isystem
        End Get
        Set(value As IFracSystem)
            If value IsNot _isystem Then
                _isystem = value
            End If
        End Set
    End Property


    ''' <summary>
    ''' Funcion para ajustar una sola configuracion.
    ''' </summary>
    ''' <param name="UpdateDirectionAddress">Address del write para escribir en memoria</param>
    ''' <param name="UpdateDirection">Address del data enable.</param>
    ''' <param name="DataAddress">Address del dato</param>
    ''' <param name="Data">Dato a escribir</param>
    ''' <param name="socket">socket del frac</param>
    Public Sub SetSingleConfig(ByVal UpdateDirectionAddress As Integer, ByVal UpdateDirection As Integer, ByVal DataAddress As Integer, ByVal Data As Integer, ByVal socket As Integer)
        Dim SingleConfigTask As New Task(Sub() SingleConfig(UpdateDirectionAddress, UpdateDirection, DataAddress, Data, socket))
        SingleConfigTask.Start()
    End Sub

    ''' <summary>
    ''' Funcion para ajustar una sola configuracion.
    ''' </summary>
    Private Async Sub SingleConfig(ByVal UpdateDirectionAddress As Integer, ByVal UpdateDirection As Integer, ByVal DataAddress As Integer, ByVal Data As Integer, ByVal socket As Integer)
        FracSystem.MoxaSend(UpdateDirectionAddress, UpdateDirection, socket)
        Await Task.Delay(200)
        FracSystem.MoxaSend(DataAddress, Data, socket)
    End Sub

    ''' <summary>
    ''' Funcion para actualizar FracSystem
    ''' </summary>
    Public Sub UpdateISystem(ByRef ISystem As IFracSystem)
        FracSystem = ISystem
    End Sub

    ''' <summary>
    ''' Funcion para calibrar
    ''' </summary>
    Public Sub AutoCalibrate(ByVal Sensor As String, ByVal socket As Integer)
        Dim NewACTask As New Task(Sub() ACTask(Sensor, socket))
        NewACTask.Start()
    End Sub

    Private Async Sub ACTask(ByVal Sensor As String, ByVal socket As Integer)
        FracSystem.MoxaSend(FracMoxaSendStartAddress(Sensor + "_StartAddress"), 1, socket)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress(Sensor + "_StartAddress"), 0, socket)
    End Sub

    ''' <summary>
    ''' Funcion para leer la configuracion del equipo.
    ''' </summary>
    Public Sub UpdateSettings(ByVal socket As Integer)
        Dim UpdateTask As New Task(Sub() Update(socket))
        UpdateTask.Start()
    End Sub

    ''' <summary>
    ''' Funcion para actualizar la configuracion.
    ''' </summary>
    Private Async Sub Update(ByVal socket As Integer)
        Try
            FracSystem.MoxaSend(FracMoxaSendStartAddress("GET_SETTINGS_StartAddress"), 1, socket)
            Await Task.Delay(15000)
            FracSystem.MoxaSend(FracMoxaSendStartAddress("GET_SETTINGS_StartAddress"), 0, socket)
            Dim Result As Integer() = FracSystem.MoxaRead(76, 120, socket)
            If Result.Length() = 120 Then

                ''VIATRAN
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("VIATRAN_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("VIATRAN_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("VIATRAN_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_FILTER_EN") = Result(FracMoxaReceiveStartAddress("VIATRAN_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("VIATRAN_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("VIATRAN_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("VIATRAN_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("VIATRAN_FILTER_NSMPL_StartAddress") - 76)

                ''TEMP LUB LP
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_FILTER_EN") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_LP_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_LP_FILTER_NSMPL_StartAddress") - 76)

                ''TEMP LUB HP
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_FILTER_EN") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("TEMPLUB_HP_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("TEMPLUB_HP_FILTER_NSMPL_StartAddress") - 76)

                ''PRES LUB LP
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_FILTER_EN") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_LP_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("PRESLUB_LP_FILTER_NSMPL_StartAddress") - 76)

                ''PRES LUB HP
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_FILTER_EN") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("PRESLUB_HP_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("PRESLUB_HP_FILTER_NSMPL_StartAddress") - 76)

                ''PRES SUC
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("PRESSUC_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("PRESSUC_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("PRESSUC_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_FILTER_EN") = Result(FracMoxaReceiveStartAddress("PRESSUC_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("PRESSUC_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("PRESSUC_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("PRESSUC_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("PRESSUC_FILTER_NSMPL_StartAddress") - 76)

                ''PRES TRANS
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_X0") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_X0_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_X1") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_X1_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_Y0") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_Y0_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_Y1") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_Y1_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_CAPMIN") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_CAPMIN_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_CAPMAX") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_CAPMAX_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SENSOR_SCALE_CAP") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SENSOR_SCALE_CAP_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("PRESTRANS_SCALED_ALARM_ARMED_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_FILTER_EN") = Result(FracMoxaReceiveStartAddress("PRESTRANS_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("PRESTRANS_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("PRESTRANS_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("PRESTRANS_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("PRESTRANS_FILTER_NSMPL_StartAddress") - 76)

                ''CAUDAL
                SettingsRM.Item("CAUDAL_FILTER_EN") = Result(FracMoxaReceiveStartAddress("CAUDAL_FILTER_EN_StartAddress") - 76)
                SettingsRM.Item("CAUDAL_FILTER_TOL") = Result(FracMoxaReceiveStartAddress("CAUDAL_FILTER_TOL_StartAddress") - 76)
                SettingsRM.Item("CAUDAL_FILTER_TSMPL") = Result(FracMoxaReceiveStartAddress("CAUDAL_FILTER_TSMPL_StartAddress") - 76)
                SettingsRM.Item("CAUDAL_FILTER_NSMPL") = Result(FracMoxaReceiveStartAddress("CAUDAL_FILTER_NSMPL_StartAddress") - 76)
                SettingsRM.Item("CAUDAL_SCALED_ALARM_SETPOINT") = Result(FracMoxaReceiveStartAddress("CAUDAL_SCALED_ALARM_SETPOINT_StartAddress") - 76)
                SettingsRM.Item("CAUDAL_SCALED_ALARM_ARMED") = Result(FracMoxaReceiveStartAddress("CAUDAL_SCALED_ALARM_ARMED_StartAddress") - 76)

            End If
            Dim Result1 As Integer() = FracSystem.MoxaRead(196, 12, socket)
            If Result1.Length() = 12 Then

                ''EXTRA
                SettingsRM.Item("UseSource") = Result1(FracMoxaReceiveStartAddress("UseSource_StartAddress") - 196)
                SettingsRM.Item("MotorSource") = Result1(FracMoxaReceiveStartAddress("MotorSource_StartAddress") - 196)
                SettingsRM.Item("TransmisionSource") = Result1(FracMoxaReceiveStartAddress("TransmisionSource_StartAddress") - 196)
                SettingsRM.Item("rGalRev") = Result1(FracMoxaReceiveStartAddress("rGalRev_StartAddress") - 196)
                SettingsRM.Item("rTransBomba") = Result1(FracMoxaReceiveStartAddress("rTransBomba_StartAddress") - 196)
                SettingsRM.Item("VB_CTRL") = Result1(FracMoxaReceiveStartAddress("VB_CTRL_StartAddress") - 196)
                SettingsRM.Item("CurrentMotor") = Result1(FracMoxaReceiveStartAddress("CurrentMotor_StartAddress") - 196)
                SettingsRM.Item("CurrentTrans") = Result1(FracMoxaReceiveStartAddress("CurrentTrans_StartAddress") - 196)
                SettingsRM.Item("CurrentPump") = Result1(FracMoxaReceiveStartAddress("CurrentPump_StartAddress") - 196)

            End If
            If Result.Length + Result1.Length = 132 Then
                RaiseEvent ConfigComplete()
            End If
        Catch ex As Exception

        End Try

    End Sub



End Class
