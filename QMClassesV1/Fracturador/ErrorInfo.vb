﻿Imports System.Threading
Public Class ErrorInfo

    ''--DICCIONARIO EN INGLES DE LOS ERRORES--''
    Private Shared ReadOnly ErrorDictionary_English As New Dictionary(Of IEnumerable(Of Integer), String)() From {
        {{516428, 31}, "Air pressure sensors - Plausibility error"},
        {{108, 2}, "Ambient pressure sensor - Plausibility error"},
        {{108, 31}, "Ambient pressure sensor - Supply voltage out of range"},
        {{1176, 1}, "Boost pressure - Pressure too high"},
        {{1176, 0}, "Boost pressure - Pressure too low"},
        {{973, 2}, "Brakes - System Controller - Communication error"},
        {{636, 2}, "Camshaft position - Plausibility error"},
        {{636, 14}, "Camshaft position sensor - Ground error"},
        {{636, 5}, "Camshaft position sensor - Open circuit"},
        {{636, 3}, "Camshaft position sensor - Short circuit to battery or open circuit"},
        {{636, 4}, "Camshaft position sensor - Short circuit to ground"},
        {{636, 31}, "Camshaft position sensor - Supply voltage out of range"},
        {{636, 8}, "Camshaft speed sensor voltage - Plausibility error"},
        {{157, 0}, "Common rail - Critical overpressure"},
        {{1239, 2}, "Common rail - Leakage"},
        {{157, 15}, "Common rail - Overpressure warning"},
        {{516117, 11}, "Common rail - PCV opened due to overpressure"},
        {{1075, 2}, "Common rail - Plausibility error"},
        {{516118, 11}, "Common rail - Plausibility error of VCV current"},
        {{516121, 11}, "Common rail - Pressure remains above the set point"},
        {{516122, 11}, "Common rail - Pressure remains below the set point"},
        {{516123, 11}, "Common rail - Start pressure too low"},
        {{516131, 11}, "Common rail pressure sensor - No signal variation"},
        {{157, 3}, "Common rail pressure sensor - Short circuit to battery or open circuit"},
        {{157, 4}, "Common rail pressure sensor - Short circuit to ground"},
        {{157, 31}, "Common rail pressure sensor - Supply voltage out of range"},
        {{110, 5}, "Coolant temperature sensor - Open circuit"},
        {{110, 2}, "Coolant temperature sensor - Plausibility error"},
        {{110, 3}, "Coolant temperature sensor - Short circuit to battery"},
        {{110, 4}, "Coolant temperature sensor - Short circuit to ground"},
        {{110, 31}, "Coolant temperature sensor - Supply voltage out of range"},
        {{190, 2}, "Crankshaft position - Plausibility error"},
        {{190, 14}, "Crankshaft position sensor - Ground error"},
        {{190, 5}, "Crankshaft position sensor - Open circuit"},
        {{190, 3}, "Crankshaft position sensor - Short circuit to battery or open circuit"},
        {{190, 4}, "Crankshaft position sensor - Short circuit to ground"},
        {{190, 31}, "Crankshaft position sensor - Supply voltage out of range"},
        {{190, 8}, "Crankshaft speed sensor voltage - Plausibility error"},
        {{516184, 2}, "Dashboard - Communication error"},
        {{516564, 2}, "Device Passenger-Operator Climate Control - Communication error"},
        {{516542, 2}, "Device Primary Cab Controller - Communication error"},
        {{1042, 2}, "Device Tractor-Trailer Bridge - Communication error"},
        {{516576, 31}, "ECU - Clamp 30 Off before ECU shutdown"},
        {{1136, 0}, "ECU - Critical overtemperature"},
        {{516543, 31}, "ECU - Data recorder was triggered"},
        {{1136, 15}, "ECU - Overtemperature warning"},
        {{516495, 31}, "ECU - Runtime reached the maximal time without reset"},
        {{1136, 31}, "ECU temperature sensor 1 - Supply voltage out of range"},
        {{516140, 31}, "ECU temperature sensor 2 - Supply voltage out of range"},
        {{516141, 31}, "ECU temperature sensor 3 - Supply voltage out of range"},
        {{190, 0}, "Engine - Critical overspeed"},
        {{190, 15}, "Engine - Overspeed warning"},
        {{516544, 31}, "Engine - Starter activated but no engine rotation detected"},
        {{516356, 31}, "Engine control unit - Communication error to secondary injection control module"},
        {{516494, 31}, "Engine Control Unit CAN - CAN1 message received after end of post drive"},
        {{639, 5}, "Engine Control Unit CAN 1 - Open circuit"},
        {{639, 11}, "Engine Control Unit CAN 1 - Short circuit"},
        {{1668, 5}, "Engine Control Unit CAN 4 - Open circuit"},
        {{1668, 11}, "Engine Control Unit CAN 4 - Short circuit"},
        {{110, 0}, "Engine coolant - Critical overtemperature"},
        {{110, 15}, "Engine coolant - Overtemperature warning"},
        {{516532, 15}, "Engine monitoring - Engine mean torque during engine lifetime above threshold"},
        {{516530, 15}, "Engine monitoring - Engine mean torque during last day above threshold"},
        {{516531, 15}, "Engine monitoring - Engine mean torque during last hour above threshold"},
        {{516566, 0}, "Engine oil - Critical dilution rate"},
        {{100, 1}, "Engine oil - Critical underpressure"},
        {{516566, 15}, "Engine oil - Dilution warning"},
        {{100, 2}, "Engine oil - Plausibility error"},
        {{100, 17}, "Engine oil - Underpressure warning"},
        {{516283, 15}, "Engine protection power reduction - Boost pressure too high"},
        {{516284, 17}, "Engine protection power reduction - Boost pressure too low"},
        {{516150, 15}, "Engine protection power reduction - Intake manifold temperature too high"},
        {{516354, 0}, "Engine protection power reduction - Turbocharger protection"},
        {{4413, 0}, "Exhaust gas temperature (upstream SCR) 2 - Critical overtemperature"},
        {{4413, 15}, "Exhaust gas temperature (upstream SCR) 2 - Overtemperature warning"},
        {{1639, 3}, "Fan speed input 1 - Short circuit to battery or open circuit"},
        {{1639, 4}, "Fan speed input 1 - Short circuit to ground"},
        {{94, 0}, "Fuel supply - Critical overpressure"},
        {{174, 0}, "Fuel supply - Critical overtemperature"},
        {{94, 1}, "Fuel supply - Critical underpressure"},
        {{94, 15}, "Fuel supply - Overpressure warning"},
        {{174, 15}, "Fuel supply - Overtemperature warning"},
        {{94, 17}, "Fuel supply - Underpressure warning"},
        {{94, 3}, "Fuel supply pressure sensor - Short circuit to battery"},
        {{94, 4}, "Fuel supply pressure sensor - Short circuit to ground or open circuit"},
        {{94, 31}, "Fuel supply pressure sensor - Supply voltage out of range"},
        {{174, 5}, "Fuel temperature sensor - Open circuit"},
        {{174, 2}, "Fuel temperature sensor - Plausibility error"},
        {{174, 3}, "Fuel temperature sensor - Short circuit to battery"},
        {{174, 4}, "Fuel temperature sensor - Short circuit to ground"},
        {{174, 31}, "Fuel temperature sensor - Supply voltage out of range"},
        {{516195, 2}, "Generator Set Controller - Communication error"},
        {{729, 6}, "Heating flange 1 - Critical high output current"},
        {{729, 11}, "Heating flange 1 - Electrical error"},
        {{729, 5}, "Heating flange 1 - Open circuit"},
        {{729, 3}, "Heating flange 1 - Short circuit to battery"},
        {{729, 4}, "Heating flange 1 - Short circuit to ground"},
        {{516346, 5}, "Heating flange status input 1 - Open circuit"},
        {{516346, 3}, "Heating flange status input 1 - Short circuit to battery"},
        {{516346, 4}, "Heating flange status input 1 - Short circuit to ground"},
        {{516577, 31}, "High pressure pump 1 - Installation error"},
        {{516578, 31}, "High pressure pump 2 - Installation error"},
        {{651, 0}, "Injector 1 - Current Rise time too long"},
        {{651, 8}, "Injector 1 - No current rise time measured"},
        {{651, 5}, "Injector 1 - Open circuit"},
        {{651, 3}, "Injector 1 - Short circuit to battery"},
        {{651, 4}, "Injector 1 - Short circuit to ground"},
        {{516401, 31}, "Injector 1 - Voltage Based Small quantity injection correction failed"},
        {{660, 0}, "Injector 10 - Current Rise time too long"},
        {{660, 8}, "Injector 10 - No current rise time measured"},
        {{660, 5}, "Injector 10 - Open circuit"},
        {{660, 3}, "Injector 10 - Short circuit to battery"},
        {{660, 4}, "Injector 10 - Short circuit to ground"},
        {{516410, 31}, "Injector 10 - Voltage Based Small quantity injection correction failed"},
        {{661, 0}, "Injector 11 - Current Rise time too long"},
        {{661, 8}, "Injector 11 - No current rise time measured"},
        {{661, 5}, "Injector 11 - Open circuit"},
        {{661, 3}, "Injector 11 - Short circuit to battery"},
        {{661, 4}, "Injector 11 - Short circuit to ground"},
        {{516411, 31}, "Injector 11 - Voltage Based Small quantity injection correction failed"},
        {{662, 0}, "Injector 12 - Current Rise time too long"},
        {{662, 8}, "Injector 12 - No current rise time measured"},
        {{662, 5}, "Injector 12 - Open circuit"},
        {{662, 3}, "Injector 12 - Short circuit to battery"},
        {{662, 4}, "Injector 12 - Short circuit to ground"},
        {{516412, 31}, "Injector 12 - Voltage Based Small quantity injection correction failed"},
        {{663, 0}, "Injector 13 - Current Rise time too long"},
        {{663, 8}, "Injector 13 - No current rise time measured"},
        {{663, 5}, "Injector 13 - Open circuit"},
        {{663, 3}, "Injector 13 - Short circuit to battery"},
        {{663, 4}, "Injector 13 - Short circuit to ground"},
        {{516413, 31}, "Injector 13 - Voltage Based Small quantity injection correction failed"},
        {{664, 0}, "Injector 14 - Current Rise time too long"},
        {{664, 8}, "Injector 14 - No current rise time measured"},
        {{664, 5}, "Injector 14 - Open circuit"},
        {{664, 3}, "Injector 14 - Short circuit to battery"},
        {{664, 4}, "Injector 14 - Short circuit to ground"},
        {{516414, 31}, "Injector 14 - Voltage Based Small quantity injection correction failed"},
        {{665, 0}, "Injector 15 - Current Rise time too long"},
        {{665, 8}, "Injector 15 - No current rise time measured"},
        {{665, 5}, "Injector 15 - Open circuit"},
        {{665, 3}, "Injector 15 - Short circuit to battery"},
        {{665, 4}, "Injector 15 - Short circuit to ground"},
        {{516415, 31}, "Injector 15 - Voltage Based Small quantity injection correction failed"},
        {{666, 0}, "Injector 16 - Current Rise time too long"},
        {{666, 8}, "Injector 16 - No current rise time measured"},
        {{666, 5}, "Injector 16 - Open circuit"},
        {{666, 3}, "Injector 16 - Short circuit to battery"},
        {{666, 4}, "Injector 16 - Short circuit to ground"},
        {{516416, 31}, "Injector 16 - Voltage Based Small quantity injection correction failed"},
        {{667, 0}, "Injector 17 - Current Rise time too long"},
        {{667, 8}, "Injector 17 - No current rise time measured"},
        {{667, 5}, "Injector 17 - Open circuit"},
        {{667, 3}, "Injector 17 - Short circuit to battery"},
        {{667, 4}, "Injector 17 - Short circuit to ground"},
        {{516417, 31}, "Injector 17 - Voltage Based Small quantity injection correction failed"},
        {{668, 0}, "Injector 18 - Current Rise time too long"},
        {{668, 8}, "Injector 18 - No current rise time measured"},
        {{668, 5}, "Injector 18 - Open circuit"},
        {{668, 3}, "Injector 18 - Short circuit to battery"},
        {{668, 4}, "Injector 18 - Short circuit to ground"},
        {{516418, 31}, "Injector 18 - Voltage Based Small quantity injection correction failed"},
        {{669, 0}, "Injector 19 - Current Rise time too long"},
        {{669, 8}, "Injector 19 - No current rise time measured"},
        {{669, 5}, "Injector 19 - Open circuit"},
        {{669, 3}, "Injector 19 - Short circuit to battery"},
        {{669, 4}, "Injector 19 - Short circuit to ground"},
        {{516419, 31}, "Injector 19 - Voltage Based Small quantity injection correction failed"},
        {{652, 0}, "Injector 2 - Current Rise time too long"},
        {{652, 8}, "Injector 2 - No current rise time measured"},
        {{652, 5}, "Injector 2 - Open circuit"},
        {{652, 3}, "Injector 2 - Short circuit to battery"},
        {{652, 4}, "Injector 2 - Short circuit to ground"},
        {{516402, 31}, "Injector 2 - Voltage Based Small quantity injection correction failed"},
        {{670, 0}, "Injector 20 - Current Rise time too long"},
        {{670, 8}, "Injector 20 - No current rise time measured"},
        {{670, 5}, "Injector 20 - Open circuit"},
        {{670, 3}, "Injector 20 - Short circuit to battery"},
        {{670, 4}, "Injector 20 - Short circuit to ground"},
        {{516420, 31}, "Injector 20 - Voltage Based Small quantity injection correction failed"},
        {{653, 0}, "Injector 3 - Current Rise time too long"},
        {{653, 8}, "Injector 3 - No current rise time measured"},
        {{653, 5}, "Injector 3 - Open circuit"},
        {{653, 3}, "Injector 3 - Short circuit to battery"},
        {{653, 4}, "Injector 3 - Short circuit to ground"},
        {{516403, 31}, "Injector 3 - Voltage Based Small quantity injection correction failed"},
        {{654, 0}, "Injector 4 - Current Rise time too long"},
        {{654, 8}, "Injector 4 - No current rise time measured"},
        {{654, 5}, "Injector 4 - Open circuit"},
        {{654, 3}, "Injector 4 - Short circuit to battery"},
        {{654, 4}, "Injector 4 - Short circuit to ground"},
        {{516404, 31}, "Injector 4 - Voltage Based Small quantity injection correction failed"},
        {{655, 0}, "Injector 5 - Current Rise time too long"},
        {{655, 8}, "Injector 5 - No current rise time measured"},
        {{655, 5}, "Injector 5 - Open circuit"},
        {{655, 3}, "Injector 5 - Short circuit to battery"},
        {{655, 4}, "Injector 5 - Short circuit to ground"},
        {{516405, 31}, "Injector 5 - Voltage Based Small quantity injection correction failed"},
        {{656, 0}, "Injector 6 - Current Rise time too long"},
        {{656, 8}, "Injector 6 - No current rise time measured"},
        {{656, 5}, "Injector 6 - Open circuit"},
        {{656, 3}, "Injector 6 - Short circuit to battery"},
        {{656, 4}, "Injector 6 - Short circuit to ground"},
        {{516406, 31}, "Injector 6 - Voltage Based Small quantity injection correction failed"},
        {{657, 0}, "Injector 7 - Current Rise time too long"},
        {{657, 8}, "Injector 7 - No current rise time measured"},
        {{657, 5}, "Injector 7 - Open circuit"},
        {{657, 3}, "Injector 7 - Short circuit to battery"},
        {{657, 4}, "Injector 7 - Short circuit to ground"},
        {{516407, 31}, "Injector 7 - Voltage Based Small quantity injection correction failed"},
        {{658, 0}, "Injector 8 - Current Rise time too long"},
        {{658, 8}, "Injector 8 - No current rise time measured"},
        {{658, 5}, "Injector 8 - Open circuit"},
        {{658, 3}, "Injector 8 - Short circuit to battery"},
        {{658, 4}, "Injector 8 - Short circuit to ground"},
        {{516408, 31}, "Injector 8 - Voltage Based Small quantity injection correction failed"},
        {{659, 0}, "Injector 9 - Current Rise time too long"},
        {{659, 8}, "Injector 9 - No current rise time measured"},
        {{659, 5}, "Injector 9 - Open circuit"},
        {{659, 3}, "Injector 9 - Short circuit to battery"},
        {{659, 4}, "Injector 9 - Short circuit to ground"},
        {{516409, 31}, "Injector 9 - Voltage Based Small quantity injection correction failed"},
        {{516191, 17}, "Injector supply boost converter - Undervoltage warning"},
        {{102, 0}, "Intake manifold - Critical overpressure"},
        {{105, 0}, "Intake manifold - Critical overtemperature"},
        {{102, 1}, "Intake manifold - Critical underpressure"},
        {{102, 15}, "Intake manifold - Overpressure warning"},
        {{105, 15}, "Intake manifold - Overtemperature warning"},
        {{102, 17}, "Intake manifold - Underpressure warning"},
        {{102, 2}, "Intake manifold pressure sensor - Plausibility error"},
        {{102, 3}, "Intake manifold pressure sensor - Short circuit to battery"},
        {{102, 4}, "Intake manifold pressure sensor - Short circuit to ground or open circuit"},
        {{102, 31}, "Intake manifold pressure sensor - Supply voltage out of range"},
        {{105, 5}, "Intake manifold temperature sensor - Open circuit"},
        {{105, 2}, "Intake manifold temperature sensor - Plausibility error"},
        {{105, 3}, "Intake manifold temperature sensor - Short circuit to battery"},
        {{105, 4}, "Intake manifold temperature sensor - Short circuit to ground"},
        {{105, 31}, "Intake manifold temperature sensor - Supply voltage out of range"},
        {{695, 11}, "J1939 (CTL) - Communication error"},
        {{516196, 2}, "J1939 (Prop3) - Communication error"},
        {{695, 2}, "Management Computer - Communication error"},
        {{516486, 5}, "Manual speed potentiometer activation - Open circuit"},
        {{516486, 3}, "Manual speed potentiometer activation - Short circuit to battery or open circuit"},
        {{516486, 4}, "Manual speed potentiometer activation - Short circuit to ground or open circuit"},
        {{516486, 1}, "Manual speed potentiometer activation - Value out of range"},
        {{516282, 11}, "Monitoring System - Injector plausibility error"},
        {{516227, 11}, "Monitoring System - Starter ECU internal safety error"},
        {{516223, 11}, "Monitoring System - Terminal 15 ECU internal safety error"},
        {{99, 0}, "Oil filter pressure 1 - Critical overpressure"},
        {{99, 15}, "Oil filter pressure 1 - Overpressure warning"},
        {{100, 3}, "Oil pressure sensor - Short circuit to battery"},
        {{100, 4}, "Oil pressure sensor - Short circuit to ground or open circuit"},
        {{100, 31}, "Oil pressure sensor - Supply voltage out of range"},
        {{3597, 1}, "Power supply - Critical undervoltage"},
        {{3597, 15}, "Power supply - Overvoltage warning"},
        {{3597, 17}, "Power supply - Undervoltage warning"},
        {{5571, 6}, "Pressure control valve - Critical high output current"},
        {{5571, 0}, "Pressure control valve - Critical overcurrent"},
        {{5571, 1}, "Pressure control valve - Critical undercurrent"},
        {{516209, 15}, "Pressure control valve - Current remains above the set point"},
        {{516209, 17}, "Pressure control valve - Current remains below the set point"},
        {{5571, 11}, "Pressure control valve - Electrical error"},
        {{516422, 11}, "Pressure control valve - Load is short circuited"},
        {{5571, 5}, "Pressure control valve - Open circuit"},
        {{516211, 11}, "Pressure control valve - PWM signal - High limit reached"},
        {{516212, 11}, "Pressure control valve - PWM signal - Plausibility error"},
        {{5571, 3}, "Pressure control valve - Short circuit to battery (high side)"},
        {{516331, 3}, "Pressure control valve - Short circuit to battery (low side)"},
        {{5571, 4}, "Pressure control valve - Short circuit to ground (high side)"},
        {{516331, 4}, "Pressure control valve - Short circuit to ground (low side)"},
        {{900, 2}, "Retarder - Driveline - Communication error"},
        {{516239, 5}, "Secondary ECU mode selection pin - Open circuit"},
        {{516239, 3}, "Secondary ECU mode selection pin - Short circuit to battery or open circuit"},
        {{516239, 4}, "Secondary ECU mode selection pin - Short circuit to ground or open circuit"},
        {{516239, 11}, "Secondary ECU mode selection pin - Value out of range"},
        {{516197, 2}, "Secondary injection control unit - Communication error"},
        {{516510, 31}, "Speed sensors - Sensors position inversion error"},
        {{677, 6}, "Starter - Critical high output current"},
        {{677, 11}, "Starter - Electrical error"},
        {{677, 31}, "Starter - Locked due to overtemperature"},
        {{677, 5}, "Starter - Open circuit"},
        {{677, 3}, "Starter - Short circuit to battery"},
        {{677, 4}, "Starter - Short circuit to ground"},
        {{516424, 11}, "Starter digital input - Starter inhibition due to short circuit"},
        {{516511, 31}, "Starters - Not connected in the output 1  to ECU"},
        {{1620, 2}, "Tachograph - Communication error"},
        {{898, 2}, "Transmission control module - Communication error"},
        {{1076, 6}, "Volume control valve - Critical high output current"},
        {{1076, 0}, "Volume control valve - Critical overcurrent"},
        {{1076, 1}, "Volume control valve - Critical undercurrent"},
        {{516251, 0}, "Volume control valve - Current remains above the set point"},
        {{516251, 1}, "Volume control valve - Current remains below the set point"},
        {{516330, 11}, "Volume control valve - Electrical error"},
        {{1076, 11}, "Volume control valve - Load is short circuited"},
        {{1076, 5}, "Volume control valve - Open circuit"},
        {{516253, 0}, "Volume control valve - PWM signal - High limit reached"},
        {{516253, 31}, "Volume control valve - PWM signal - Plausibility error"},
        {{1076, 3}, "Volume control valve - Short circuit to battery (high side)"},
        {{516328, 3}, "Volume control valve - Short circuit to battery (low side)"},
        {{1076, 4}, "Volume control valve - Short circuit to ground (high side)"},
        {{516328, 4}, "Volume control valve - Short circuit to ground (low side)"},
        {{5386, 6}, "Wastegate valve - Critical high output current"},
        {{5386, 11}, "Wastegate valve - Electrical error"},
        {{516318, 31}, "Wastegate valve - Load is short circuited"},
        {{5386, 5}, "Wastegate valve - Open circuit"},
        {{5386, 3}, "Wastegate valve - Short circuit to battery (high side)"},
        {{516266, 3}, "Wastegate valve - Short circuit to battery (low side)"},
        {{5386, 4}, "Wastegate valve - Short circuit to ground (high side)"},
        {{516266, 4}, "Wastegate valve - Short circuit to ground (low side)"},
        {{5386, 31}, "Wastegate valve - Supply voltage out of range"},
        {{97, 5}, "Water in fuel sensor - Open circuit"},
        {{97, 3}, "Water in fuel sensor - Short circuit to battery"},
        {{97, 4}, "Water in fuel sensor - Short circuit to ground"},
        {{97, 31}, "Water in fuel sensor - Supply voltage out of range"},
        {{97, 11}, "Water in fuel sensor - Value out of range"},
        {{97, 0}, "Water in fuel sensor - Water in fuel detected"},
        {{516262, 11}, "Water pump - Unable to reach desired speed"},
        {{5201, 3}, "Pressure regulator A (DR_A) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5202, 3}, "Pressure regulator B (DR_B) has a short circuit on the positive supply pin (VPS) of the proportionaln valves"},
        {{5203, 3}, "Pressure regulator D (DR_D) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5204, 3}, "Pressure regulator E (DR_E) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5205, 3}, "Pressure regulator F (DR_F) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5206, 3}, "Pressure regulator C (DR_C) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5207, 3}, "Main pressure regulator (DR_HD) has a short circuit on the positive supply pin (VPS) of the proportional valves"},
        {{5211, 11}, "During the take off process either the speed has droped too much or the number of allowed tries for take off has reched"},
        {{5212, 11}, "The pressure filter has too much differential pressure, check the Filter"},
        {{5221, 4}, "Pressure regulator A (DR_A) has a short circuit to ground"},
        {{5222, 4}, "Pressure regulator B (DR_B) has a short circuit to ground"},
        {{5223, 4}, "Pressure regulator D (DR_D) has a short circuit to ground"},
        {{5224, 4}, "Pressure regulator E (DR_E) has a short circuit to ground"},
        {{5225, 4}, "Pressure regulator F (DR_F) has a short circuit to ground"},
        {{5226, 4}, "Pressure regulator C (DR_C) has a short circuit to ground"},
        {{5227, 4}, "Main pressure regulator (DR_HD) has a short circuit to ground"},
        {{5231, 5}, "Circuit of pressure regulator A (DR_A) is interrupted"},
        {{5232, 5}, "Circuit of pressure regulator B (DR_B) is interrupted"},
        {{5233, 5}, "Circuit of pressure regulator D (DR_D) is interrupted"},
        {{5234, 5}, "Circuit of pressure regulator E (DR_E) is interrupted"},
        {{5235, 5}, "Circuit of pressure regulator F (DR_F) is interrupted"},
        {{5236, 5}, "Circuit of pressure regulator C (DR_C) is interrupted"},
        {{5237, 5}, "Circuit of main pressure regulator (DR_HD) is interrupted"},
        {{5241, 14}, "Pressure regulator A (DR_A) has a short circuit on another output (AIM)"},
        {{5242, 14}, "Pressure regulator B (DR_B) has a short circuit on another output (AIM)"},
        {{5243, 14}, "Pressure regulator D (DR_D) has a short circuit on another output (AIM)"},
        {{5244, 14}, "Pressure regulator E (DR_E) has a short circuit on another output (AIM)"},
        {{5245, 14}, "Pressure regulator F (DR_F) has a short circuit on another output (AIM)"},
        {{5246, 14}, "Pressure regulator C (DR_C) has a short circuit on another output (AIM)"},
        {{5247, 14}, "Main pressure regulator (DR_HD) has a short circuit on another output (AIM)"},
        {{5252, 4}, "Positive supply pins of all proportional valves (VPS) have a short circuit to ground"},
        {{5253, 14}, "Electrical fault on turbine speed sensor"},
        {{5254, 14}, "Malfunction of hardware input for turbine speed sensor"},
        {{5255, 5}, "Unrealistic change in turbine speed on EF0"},
        {{5256, 14}, "Electrical fault on output speed sensor"},
        {{5257, 14}, "Malfunction of hardware input for output speed sensor"},
        {{5258, 5}, "Unrealistic change in output speed on EF1"},
        {{5260, 14}, "Active drive program configuration invalid"},
        {{5262, 14}, "The central cut-off unit (ZAE) no longer cuts off"},
        {{5264, 1}, "TCU power supply (KL30) undervoltage"},
        {{5265, 0}, "TCU power supply (KL30) overvoltage"},
        {{5266, 14}, "Key and driving direction signal from the speed range selector message are not plausible"},
        {{5273, 1}, "Temperature sensor sump (ER1) failure low -> corresponds to high temperature"},
        {{5274, 0}, "Temperature sensor sump (ER1) failure high -> corresponds to low temperature"},
        {{5280, 4}, "Power supply turbine speed sensor (AU1) has short circuit to ground"},
        {{5281, 3}, "Power supply turbine speed sensor (AU1) has short circuit to positive supply (VPS)"},
        {{5283, 4}, "Power supply turbine speed sensor (AU2) has short circuit to ground"},
        {{5284, 3}, "Power supply turbine speed sensor (AU2) has short circuit to positive supply (VPS)"},
        {{5292, 14}, "CAN_A or CAN_B has an error during initialization"},
        {{5293, 14}, "CAN_A - BUSOFF (vehicle CAN)"},
        {{5295, 14}, "CAN_A - error passive (vehicle CAN)"},
        {{5296, 14}, "CAN_A - error warning (vehicle CAN)"},
        {{5310, 14}, "Failure of CAN message EEC1 - electronic engine controller 1"},
        {{5311, 14}, "Failure of CAN message EEC2 - electronic engine controller 2"},
        {{5313, 14}, "Failure of CAN message EC - engine configuration"},
        {{5316, 14}, "Failure of CAN message TC1 - transmission control 1 (from other)"},
        {{5327, 14}, "Current engine torque is not available (SAE)"},
        {{5328, 14}, "Engine speed is not available (SAE)"},
        {{5330, 14}, "Accelerator pedal position is not available (SAE)"},
        {{5331, 14}, "The ratio of the current engine torque to the maximum possible engine torque is not available at the current speed (SAE)"},
        {{5332, 14}, "Friction torque is not available (SAE)"},
        {{5333, 14}, "Engine reference torque is not available or outside of specification window (SAE)"},
        {{5343, 14}, "CAN_A sending error"},
        {{5355, 14}, "Checksum EEPROM incorrect"},
        {{5356, 14}, "Composite error for TCU hardware (internal)"},
        {{5357, 14}, "Composite error for TCU software (internal)"},
        {{5363, 14}, "Failure of turbine speed (n_Tu) and output speed (n_Ab)"},
        {{5364, 14}, "Failure of turbine speed (n_Tu) and engine speed (n_Mot)"},
        {{5365, 14}, "Failure of engine speed (n_Mot) and output speed (n_Ab)"},
        {{5388, 14}, "Slip in fixed gear"},
        {{5432, 14}, "Failure of engine speed (n_Mot), turbine speed (n_Tu), and output speed (n_Ab)"},
        {{5435, 14}, "Warning increased sump temperature"},
        {{5436, 14}, "Sump temperature overheating"},
        {{5460, 14}, "Desk mode is active"},
        {{5572, 14}, "TCU hardware (internal)"},
        {{5585, 14}, "Analog Digital Converter Error"},
        {{5590, 14}, "Electric-Current-Calibration Values are not plausible"},
        {{5591, 14}, "EEprom values are not plausible"}
    }

    ''--DICCIONARIO EN ESPAÑOL DE LOS ERRORES--''
    Private Shared ReadOnly ErrorDictionary_Español As New Dictionary(Of IEnumerable(Of Integer), String)() From {
        {{516428, 31}, "Sensores de presión de aire - Error de plausibilidad"},
        {{108, 2}, "Sensor de presión ambiental - Error de plausibilidad"},
        {{108, 31}, "Sensor de presión ambiental - Tensión de alimentación fuera del alcance"},
        {{1176, 1}, "Aumentar la presión - Presión demasiado alta"},
        {{1176, 0}, "Aumentar la presión - Presión demasiado baja"},
        {{973, 2}, "Frenos - Controlador de sistema - Error de comunicación"},
        {{636, 2}, "Posición del árbol de levas - Error de plausibilidad"},
        {{636, 14}, "Sensor de posición del árbol de levas - Error de tierra"},
        {{636, 5}, "Sensor de posición de árbol de levas - Circuito abierto"},
        {{636, 3}, "Sensor de posición del árbol de levas - Cortocircuito a la batería o circuito abierto"},
        {{636, 4}, "Sensor de posición de árbol de levas - Cortocircuito a tierra"},
        {{636, 31}, "Sensor de posición del árbol de levas - Tensión de alimentación fuera del alcance"},
        {{636, 8}, "Voltaje del sensor de velocidad del árbol de levas - Error de verosimilitud"},
        {{157, 0}, "Common Rail - Sobrepresión crítica"},
        {{1239, 2}, "Common Rail - Leakage"},
        {{157, 15}, "Common Rail - Advertencia de sobrepresión"},
        {{516117, 11}, "Common Rail - PCV abierto debido a la sobrepresión"},
        {{1075, 2}, "Common Rail - Error de plausibilidad"},
        {{516118, 11}, "Common Rail - Error de plausibilidad de la corriente VCV"},
        {{516121, 11}, "Common Rail - La presión permanece por encima del punto de ajuste"},
        {{516122, 11}, "Common Rail - La presión permanece por debajo del punto de ajuste"},
        {{516123, 11}, "Common Rail - Presión de arranque demasiado baja"},
        {{516131, 11}, "Sensor de presión Common Rail - Sin variación de señal"},
        {{157, 3}, "Sensor de presión de Common Rail - Cortocircuito a batería o circuito abierto"},
        {{157, 4}, "Sensor de presión Common Rail - Cortocircuito a tierra"},
        {{157, 31}, "Sensor de presión Common Rail - Tensión de alimentación fuera de rango"},
        {{110, 5}, "Sensor de temperatura del refrigerante - Circuito abierto"},
        {{110, 2}, "Sensor de temperatura del refrigerante - Error de plausibilidad"},
        {{110, 3}, "Sensor de temperatura del refrigerante - Cortocircuito a la batería"},
        {{110, 4}, "Sensor de temperatura del refrigerante - Cortocircuito a tierra"},
        {{110, 31}, "Sensor de temperatura del refrigerante - Tensión de alimentación fuera del alcance"},
        {{190, 2}, "Posición del cigüeñal - Error de plausibilidad"},
        {{190, 14}, "Sensor de posición del cigüeñal - Error de tierra"},
        {{190, 5}, "Sensor de posición del cigüeñal - Circuito abierto"},
        {{190, 3}, "Sensor de posición del cigüeñal - Cortocircuito a la batería o circuito abierto"},
        {{190, 4}, "Sensor de posición del cigüeñal - Cortocircuito a tierra"},
        {{190, 31}, "Sensor de posición del cigüeñal - Tensión de alimentación fuera del alcance"},
        {{190, 8}, "Tensión del sensor de velocidad del cigüeñal - Error de plausibilidad"},
        {{516184, 2}, "Panel de control - Error de comunicación"},
        {{516564, 2}, "Control climático entre pasajeros y operadores de dispositivos - Error de comunicación"},
        {{516542, 2}, "Controlador de cabina principal del dispositivo - Error de comunicación"},
        {{1042, 2}, "Puente tractor-remolque del dispositivo - Error de comunicación"},
        {{516576, 31}, "ECU - Abrazadera 30 Desactivada antes del apagado de la ECU"},
        {{1136, 0}, "ECU - Sobretemperatura crítica"},
        {{516543, 31}, "ECU - Se activó la grabadora de datos"},
        {{1136, 15}, "ECU - Advertencia de sobretemperatura"},
        {{516495, 31}, "ECU - El tiempo de ejecución alcanzó el tiempo máximo sin restablecer"},
        {{1136, 31}, "Sensor de temperatura ecu 1 - Tensión de alimentación fuera del alcance"},
        {{516140, 31}, "Sensor de temperatura ecu 2 - Tensión de alimentación fuera del alcance"},
        {{516141, 31}, "Sensor de temperatura ecu 3 - Tensión de alimentación fuera del alcance"},
        {{190, 0}, "Motor - Exceso de velocidad crítico"},
        {{190, 15}, "Motor - Advertencia de exceso de velocidad"},
        {{516544, 31}, "Motor - Arrancador activado pero sin rotación del motor detectada"},
        {{516356, 31}, "Unidad de control del motor - Error de comunicación al módulo de control de inyección secundario"},
        {{516494, 31}, "Unidad de control del motor CAN - mensaje CAN1 recibido después del final de la unidad de post"},
        {{639, 5}, "Unidad de control del motor CAN 1 - Circuito abierto"},
        {{639, 11}, "Unidad de control del motor CAN 1 - Cortocircuito"},
        {{1668, 5}, "Unidad de control del motor CAN 4 - Circuito abierto"},
        {{1668, 11}, "Unidad de control del motor CAN 4 - Cortocircuito"},
        {{110, 0}, "Refrigerante del motor - Sobretemperatura crítica"},
        {{110, 15}, "Refrigerante del motor - Advertencia de sobretemperatura"},
        {{516532, 15}, "Monitorización del motor - Par medio del motor durante la vida útil del motor por encima del umbral"},
        {{516530, 15}, "Monitorización del motor - Par medio del motor durante el último día por encima del umbral"},
        {{516531, 15}, "Monitorización del motor - Par medio del motor durante la última hora por encima del umbral"},
        {{516566, 0}, "Aceite del motor - Tasa crítica de dilución"},
        {{100, 1}, "Aceite del motor - Subpresión crítica"},
        {{516566, 15}, "Aceite del motor - Advertencia de dilución"},
        {{100, 2}, "Aceite del motor - Error de plausibilidad"},
        {{100, 17}, "Aceite del motor - Advertencia de subpresión"},
        {{516283, 15}, "Reducción de potencia de protección del motor - La presión de aumento es demasiado alta"},
        {{516284, 17}, "Reducción de potencia de protección del motor - La presión de aumento es demasiado baja"},
        {{516150, 15}, "Reducción de potencia de protección del motor - Temperatura múltiple de admisión demasiado alta"},
        {{516354, 0}, "Reducción de potencia de protección del motor - Protección del turbocompresor"},
        {{4413, 0}, "Temperatura del gas de escape (SCR aguas arriba) 2 - Sobretemperatura crítica"},
        {{4413, 15}, "Temperatura del gas de escape (SCR ascendente) 2 - Advertencia de sobretemperatura"},
        {{1639, 3}, "Entrada de velocidad del ventilador 1 - Cortocircuito a la batería o circuito abierto"},
        {{1639, 4}, "Entrada de velocidad del ventilador 1 - Cortocircuito a tierra"},
        {{94, 0}, "Suministro de combustible - Sobrepresión crítica"},
        {{174, 0}, "Suministro de combustible - Sobretemperatura crítica"},
        {{94, 1}, "Suministro de combustible - Subpresión crítica"},
        {{94, 15}, "Suministro de combustible - Advertencia de sobrepresión"},
        {{174, 15}, "Suministro de combustible - Advertencia de sobretemperatura"},
        {{94, 17}, "Suministro de combustible - Advertencia de subpresión"},
        {{94, 3}, "Sensor de presión de suministro de combustible - Cortocircuito a la batería"},
        {{94, 4}, "Sensor de presión de suministro de combustible - Cortocircuito a tierra o circuito abierto"},
        {{94, 31}, "Sensor de presión de suministro de combustible - Tensión de alimentación fuera del alcance"},
        {{174, 5}, "Sensor de temperatura del combustible - Circuito abierto"},
        {{174, 2}, "Sensor de temperatura del combustible - Error de plausibilidad"},
        {{174, 3}, "Sensor de temperatura del combustible - Cortocircuito a la batería"},
        {{174, 4}, "Sensor de temperatura del combustible - Cortocircuito a tierra"},
        {{174, 31}, "Sensor de temperatura del combustible - Tensión de alimentación fuera del alcance"},
        {{516195, 2}, "Controlador de conjunto de generadores - Error de comunicación"},
        {{729, 6}, "Brida de calentamiento 1 - Corriente crítica de alta salida"},
        {{729, 11}, "Brida de calefacción 1 - Error eléctrico"},
        {{729, 5}, "Brida de calefacción 1 - Circuito abierto"},
        {{729, 3}, "Brida de calefacción 1 - Cortocircuito a la batería"},
        {{729, 4}, "Brida de calefacción 1 - Cortocircuito a tierra"},
        {{516346, 5}, "Entrada de estado de la brida de calefacción 1 - Circuito abierto"},
        {{516346, 3}, "Entrada de estado de la brida de calefacción 1 - Cortocircuito a la batería"},
        {{516346, 4}, "Entrada de estado de la brida de calefacción 1 - Cortocircuito a tierra"},
        {{516577, 31}, "Bomba de alta presión 1 - Error de instalación"},
        {{516578, 31}, "Bomba de alta presión 2 - Error de instalación"},
        {{651, 0}, "Inyector 1 - Tiempo de aumento actual demasiado largo"},
        {{651, 8}, "Inyector 1 - No se mide el tiempo de aumento actual"},
        {{651, 5}, "Inyector 1 - Circuito Abierto"},
        {{651, 3}, "Inyector 1 - Cortocircuito a la batería"},
        {{651, 4}, "Inyector 1 - Cortocircuito a tierra"},
        {{516401, 31}, "Inyector 1 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{660, 0}, "Inyector 10 - Tiempo de aumento de corriente demasiado largo"},
        {{660, 8}, "Inyector 10 - No se mide el tiempo de aumento actual"},
        {{660, 5}, "Inyector 10 - Circuito Abierto"},
        {{660, 3}, "Inyector 10 - Cortocircuito a la batería"},
        {{660, 4}, "Inyector 10 - Cortocircuito a tierra"},
        {{516410, 31}, "Inyector 10 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{661, 0}, "Inyector 11 - Tiempo de aumento de corriente demasiado largo"},
        {{661, 8}, "Inyector 11 - No se mide el tiempo de aumento actual"},
        {{661, 5}, "Inyector 11 - Circuito Abierto"},
        {{661, 3}, "Inyector 11 - Cortocircuito a la batería"},
        {{661, 4}, "Inyector 11 - Cortocircuito a tierra"},
        {{516411, 31}, "Inyector 11 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{662, 0}, "Inyector 12 - Tiempo de aumento actual demasiado largo"},
        {{662, 8}, "Inyector 12 - No se mide el tiempo de aumento actual"},
        {{662, 5}, "Inyector 12 - Circuito Abierto"},
        {{662, 3}, "Inyector 12 - Cortocircuito a la batería"},
        {{662, 4}, "Inyector 12 - Cortocircuito a tierra"},
        {{516412, 31}, "Inyector 12 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{663, 0}, "Inyector 13 - Tiempo de aumento de corriente demasiado largo"},
        {{663, 8}, "Inyector 13 - No se mide el tiempo de aumento actual"},
        {{663, 5}, "Inyector 13 - Circuito Abierto"},
        {{663, 3}, "Inyector 13 - Cortocircuito a la batería"},
        {{663, 4}, "Inyector 13 - Cortocircuito a tierra"},
        {{516413, 31}, "Inyector 13 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{664, 0}, "Inyector 14 - Tiempo de aumento actual demasiado largo"},
        {{664, 8}, "Inyector 14 - No se mide el tiempo de aumento actual"},
        {{664, 5}, "Inyector 14 - Circuito Abierto"},
        {{664, 3}, "Inyector 14 - Cortocircuito a la batería"},
        {{664, 4}, "Inyector 14 - Cortocircuito a tierra"},
        {{516414, 31}, "Inyector 14 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{665, 0}, "Inyector 15 - Tiempo de aumento actual demasiado largo"},
        {{665, 8}, "Inyector 15 - No se mide el tiempo de aumento actual"},
        {{665, 5}, "Inyector 15 - Circuito Abierto"},
        {{665, 3}, "Inyector 15 - Cortocircuito a la batería"},
        {{665, 4}, "Inyector 15 - Cortocircuito a tierra"},
        {{516415, 31}, "Inyector 15 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{666, 0}, "Inyector 16 - Tiempo de aumento de corriente demasiado largo"},
        {{666, 8}, "Inyector 16 - No se mide el tiempo de aumento actual"},
        {{666, 5}, "Inyector 16 - Circuito Abierto"},
        {{666, 3}, "Inyector 16 - Cortocircuito a la batería"},
        {{666, 4}, "Inyector 16 - Cortocircuito a tierra"},
        {{516416, 31}, "Inyector 16 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{667, 0}, "Inyector 17 - Tiempo de aumento actual demasiado largo"},
        {{667, 8}, "Inyector 17 - No se mide el tiempo de aumento actual"},
        {{667, 5}, "Inyector 17 - Circuito Abierto"},
        {{667, 3}, "Inyector 17 - Cortocircuito a la batería"},
        {{667, 4}, "Inyector 17 - Cortocircuito a tierra"},
        {{516417, 31}, "Inyector 17 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{668, 0}, "Inyector 18 - Tiempo de aumento actual demasiado largo"},
        {{668, 8}, "Inyector 18 - No se mide el tiempo de ascenso actual"},
        {{668, 5}, "Inyector 18 - Circuito Abierto"},
        {{668, 3}, "Inyector 18 - Cortocircuito a la batería"},
        {{668, 4}, "Inyector 18 - Cortocircuito a tierra"},
        {{516418, 31}, "Inyector 18 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{669, 0}, "Inyector 19 - Tiempo de aumento actual demasiado largo"},
        {{669, 8}, "Inyector 19 - No se mide el tiempo de aumento actual"},
        {{669, 5}, "Inyector 19 - Circuito Abierto"},
        {{669, 3}, "Inyector 19 - Cortocircuito a la batería"},
        {{669, 4}, "Inyector 19 - Cortocircuito a tierra"},
        {{516419, 31}, "Inyector 19 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{652, 0}, "Inyector 2 - Tiempo de aumento de corriente demasiado largo"},
        {{652, 8}, "Inyector 2 - No se mide el tiempo de aumento actual"},
        {{652, 5}, "Inyector 2 - Circuito Abierto"},
        {{652, 3}, "Inyector 2 - Cortocircuito a la batería"},
        {{652, 4}, "Inyector 2 - Cortocircuito a tierra"},
        {{516402, 31}, "Inyector 2 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{670, 0}, "Inyector 20 - Tiempo de aumento de corriente demasiado largo"},
        {{670, 8}, "Inyector 20 - No se mide el tiempo de aumento actual"},
        {{670, 5}, "Inyector 20 - Circuito Abierto"},
        {{670, 3}, "Inyector 20 - Cortocircuito a la batería"},
        {{670, 4}, "Inyector 20 - Cortocircuito a tierra"},
        {{516420, 31}, "Inyector 20 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{653, 0}, "Inyector 3 - Tiempo de aumento de corriente demasiado largo"},
        {{653, 8}, "Inyector 3 - No se mide el tiempo de aumento actual"},
        {{653, 5}, "Inyector 3 - Circuito Abierto"},
        {{653, 3}, "Inyector 3 - Cortocircuito a la batería"},
        {{653, 4}, "Inyector 3 - Cortocircuito a tierra"},
        {{516403, 31}, "Inyector 3 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{654, 0}, "Inyector 4 - Tiempo de aumento de corriente demasiado largo"},
        {{654, 8}, "Inyector 4 - No se mide el tiempo de aumento actual"},
        {{654, 5}, "Inyector 4 - Circuito Abierto"},
        {{654, 3}, "Inyector 4 - Cortocircuito a la batería"},
        {{654, 4}, "Inyector 4 - Cortocircuito a tierra"},
        {{516404, 31}, "Inyector 4 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{655, 0}, "Inyector 5 - Tiempo de aumento de corriente demasiado largo"},
        {{655, 8}, "Inyector 5 - No se mide el tiempo de aumento actual"},
        {{655, 5}, "Inyector 5 - Circuito Abierto"},
        {{655, 3}, "Inyector 5 - Cortocircuito a la batería"},
        {{655, 4}, "Inyector 5 - Cortocircuito a tierra"},
        {{516405, 31}, "Inyector 5 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{656, 0}, "Inyector 6 - Tiempo de aumento de corriente demasiado largo"},
        {{656, 8}, "Inyector 6 - No se mide el tiempo de aumento actual"},
        {{656, 5}, "Inyector 6 - Circuito Abierto"},
        {{656, 3}, "Inyector 6 - Cortocircuito a la batería"},
        {{656, 4}, "Inyector 6 - Cortocircuito a tierra"},
        {{516406, 31}, "Inyector 6 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{657, 0}, "Inyector 7 - Tiempo de aumento de corriente demasiado largo"},
        {{657, 8}, "Inyector 7 - No se mide el tiempo de aumento actual"},
        {{657, 5}, "Inyector 7 - Circuito Abierto"},
        {{657, 3}, "Inyector 7 - Cortocircuito a la batería"},
        {{657, 4}, "Inyector 7 - Cortocircuito a tierra"},
        {{516407, 31}, "Inyector 7 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{658, 0}, "Inyector 8 - Tiempo de aumento de corriente demasiado largo"},
        {{658, 8}, "Inyector 8 - No se mide el tiempo de ascenso actual"},
        {{658, 5}, "Inyector 8 - Circuito Abierto"},
        {{658, 3}, "Inyector 8 - Cortocircuito a la batería"},
        {{658, 4}, "Inyector 8 - Cortocircuito a tierra"},
        {{516408, 31}, "Inyector 8 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{659, 0}, "Inyector 9 - Tiempo de aumento actual demasiado largo"},
        {{659, 8}, "Inyector 9 - No se mide el tiempo de ascenso actual"},
        {{659, 5}, "Inyector 9 - Circuito Abierto"},
        {{659, 3}, "Inyector 9 - Cortocircuito a la batería"},
        {{659, 4}, "Inyector 9 - Cortocircuito a la batería"},
        {{516409, 31}, "Inyector 9 - Error en la corrección de inyección de cantidad pequeña basada en voltaje"},
        {{516191, 17}, "Convertidor de impulso de suministro de inyector - Advertencia de subtensión"},
        {{102, 0}, "Colector de admisión - Sobrepresión crítica"},
        {{105, 0}, "Colector de admisión - Sobretemperatura crítica"},
        {{102, 1}, "Colector de admisión - Subpresión crítica"},
        {{102, 15}, "Colector de admisión - Advertencia de sobrepresión"},
        {{105, 15}, "Colector de admisión - Advertencia de sobretemperatura"},
        {{102, 17}, "Colector de admisión - Advertencia de subpresión"},
        {{102, 2}, "Sensor de presión múltiple de admisión - Error de plausibilidad"},
        {{102, 3}, "Sensor de presión del colector de admisión - Cortocircuito a la batería"},
        {{102, 4}, "Sensor de presión múltiple de admisión - Cortocircuito a tierra o circuito abierto"},
        {{102, 31}, "Sensor de presión del colector de admisión - Tensión de alimentación fuera del alcance"},
        {{105, 5}, "Sensor de temperatura múltiple de admisión - Circuito abierto"},
        {{105, 2}, "Sensor de temperatura múltiple de admisión - Error de plausibilidad"},
        {{105, 3}, "Sensor de temperatura múltiple de admisión - Cortocircuito a la batería"},
        {{105, 4}, "Sensor de temperatura múltiple de admisión - Cortocircuito a tierra"},
        {{105, 31}, "Sensor de temperatura múltiple de admisión - Tensión de alimentación fuera del alcance"},
        {{695, 11}, "J1939 (CTL) - Error de comunicación"},
        {{516196, 2}, "J1939 (Prop3) - Error de comunicación"},
        {{695, 2}, "Equipo de gestión - Error de comunicación"},
        {{516486, 5}, "Activación manual del potenciómetro de velocidad - Circuito abierto"},
        {{516486, 3}, "Activación manual del potenciómetro de velocidad - Cortocircuito a batería o circuito abierto"},
        {{516486, 4}, "Activación manual del potenciómetro de velocidad - Cortocircuito a tierra o circuito abierto"},
        {{516486, 1}, "Activación manual del potenciómetro de velocidad - Valor fuera del alcance"},
        {{516282, 11}, "Sistema de monitoreo - Error de plausibilidad del inyector"},
        {{516227, 11}, "Sistema de monitoreo - Error de seguridad interna de la ECU inicial"},
        {{516223, 11}, "Sistema de monitoreo - Terminal 15 Ecu error de seguridad interna"},
        {{99, 0}, "Presión del filtro de aceite 1 - Sobrepresión crítica"},
        {{99, 15}, "Presión del filtro de aceite 1 - Advertencia de sobrepresión"},
        {{100, 3}, "Sensor de presión de aceite - Cortocircuito a la batería"},
        {{100, 4}, "Sensor de presión de aceite - Cortocircuito a tierra o circuito abierto"},
        {{100, 31}, "Sensor de presión de aceite - Tensión de alimentación fuera del alcance"},
        {{3597, 1}, "Fuente de alimentación - Subtensión crítica"},
        {{3597, 15}, "Fuente de alimentación - Advertencia de sobretensión"},
        {{3597, 17}, "Fuente de alimentación - Advertencia de subtensión"},
        {{5571, 6}, "Válvula de control de presión - Corriente crítica de alta salida"},
        {{5571, 0}, "Válvula de control de presión - Sobrecorriente crítica"},
        {{5571, 1}, "Válvula de control de presión - Corriente subyacente crítica"},
        {{516209, 15}, "Válvula de control de presión - La corriente permanece por encima del punto de ajuste"},
        {{516209, 17}, "Válvula de control de presión - La corriente permanece por debajo del punto de ajuste"},
        {{5571, 11}, "Válvula de control de presión - Error eléctrico"},
        {{516422, 11}, "Válvula de control de presión - La carga es cortocircuitada"},
        {{5571, 5}, "Válvula de control de presión - Circuito abierto"},
        {{516211, 11}, "Válvula de control de presión - Señal PWM - Límite alto alcanzado"},
        {{516212, 11}, "Válvula de control de presión - Señal PWM - Error de plausibilidad"},
        {{5571, 3}, "Válvula de control de presión - Cortocircuito a batería (lado alto)"},
        {{516331, 3}, "Válvula de control de presión - Cortocircuito a la batería (lado bajo)"},
        {{5571, 4}, "Válvula de control de presión - Cortocircuito a tierra (lado alto)"},
        {{516331, 4}, "Válvula de control de presión - Cortocircuito a tierra (lado bajo)"},
        {{900, 2}, "Retardador - Línea motrante - Error de comunicación"},
        {{516239, 5}, "Pasador de selección de modo ECU secundario - Circuito abierto"},
        {{516239, 3}, "Pasador de selección de modo ECU secundario - Cortocircuito a batería o circuito abierto"},
        {{516239, 4}, "Pasador de selección de modo ECU secundario - Cortocircuito a tierra o circuito abierto"},
        {{516239, 11}, "Pin de selección de modo ECU secundario - Valor fuera del rango"},
        {{516197, 2}, "Unidad de control de inyección secundaria - Error de comunicación"},
        {{516510, 31}, "Sensores de velocidad - Error de inversión de posición de los sensores"},
        {{677, 6}, "Arrancador - Corriente crítica de alta salida"},
        {{677, 11}, "Arrancador - Error Eléctrico"},
        {{677, 31}, "Arrancador - Bloqueado debido a la sobretemperatura"},
        {{677, 5}, "Arrancador - Circuito Abierto"},
        {{677, 3}, "Arrancador - Cortocircuito a la batería"},
        {{677, 4}, "Arrancador - Cortocircuito a tierra"},
        {{516424, 11}, "Entrada digital de arranque - Inhibición del arranque debido al cortocircuito"},
        {{516511, 31}, "Arrancadores - No conectados en la salida 1 a ecu"},
        {{1620, 2}, "Tacógrafo - Error de comunicación"},
        {{898, 2}, "Módulo de control de transmisión - Error de comunicación"},
        {{1076, 6}, "Válvula de control de volumen - Corriente crítica de alta salida"},
        {{1076, 0}, "Válvula de control de volumen - Sobrecorriente crítica"},
        {{1076, 1}, "Válvula de control de volumen - Corriente subyacente crítica"},
        {{516251, 0}, "Válvula de control de volumen - La corriente permanece por encima del punto de ajuste"},
        {{516251, 1}, "Válvula de control de volumen - La corriente permanece por debajo del punto de ajuste"},
        {{516330, 11}, "Válvula de control de volumen - Error eléctrico"},
        {{1076, 11}, "Válvula de control de volumen - La carga es cortocircuitada"},
        {{1076, 5}, "Válvula de control de volumen - Circuito abierto"},
        {{516253, 0}, "Válvula de control de volumen - Señal PWM - Límite alto alcanzado"},
        {{516253, 31}, "Válvula de control de volumen - Señal PWM - Error de plausibilidad"},
        {{1076, 3}, "Válvula de control de volumen - Cortocircuito a la batería (lado alto)"},
        {{516328, 3}, "Válvula de control de volumen - Cortocircuito a la batería (lado bajo)"},
        {{1076, 4}, "Válvula de control de volumen - Cortocircuito a tierra (lado alto)"},
        {{516328, 4}, "Válvula de control de volumen - Cortocircuito a tierra (lado bajo)"},
        {{5386, 6}, "Válvula de descarga - Corriente crítica de alta salida"},
        {{5386, 11}, "Válvula de descarga - Error eléctrico"},
        {{516318, 31}, "Válvula de descarga - La carga es cortocircuitada"},
        {{5386, 5}, "Válvula de descarga - Circuito abierto"},
        {{5386, 3}, "Válvula de descarga - Cortocircuito a batería (lado alto)"},
        {{516266, 3}, "Válvula de descarga - Cortocircuito a batería (lado bajo)"},
        {{5386, 4}, "Válvula de descarga - Cortocircuito a tierra (lado alto)"},
        {{516266, 4}, "Válvula de descarga - Cortocircuito a tierra (lado bajo)"},
        {{5386, 31}, "Válvula de descarga - Tensión de alimentación fuera del alcance"},
        {{97, 5}, "Agua en el sensor de combustible - Circuito abierto"},
        {{97, 3}, "Agua en el sensor de combustible - Cortocircuito a la batería"},
        {{97, 4}, "Agua en el sensor de combustible - Cortocircuito a tierra"},
        {{97, 31}, "Agua en el sensor de combustible - Tensión de suministro fuera de rango"},
        {{97, 11}, "Agua en el sensor de combustible - Valor fuera de alcance"},
        {{97, 0}, "Agua en el sensor de combustible - Agua en el combustible detectado"},
        {{516262, 11}, "Bomba de agua - Incapaz de alcanzar la velocidad deseada"},
        {{5201, 3}, "El regulador de presión A (DR_A) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5202, 3}, "El regulador de presión B (DR_B) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5203, 3}, "El regulador de presión D (DR_D) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5204, 3}, "El regulador de presión E (DR_E) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5205, 3}, "El regulador de presión F (DR_F) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5206, 3}, "El regulador de presión C (DR_C) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5207, 3}, "El regulador de presión principal (DR_HD) tiene un cortocircuito en el pasador de alimentación positiva (VPS) de las válvulas proporcionales"},
        {{5211, 11}, "Durante el proceso de despegue, ya sea la velocidad ha descendido demasiado o el número de intentos permitidos para despegar ha"},
        {{5212, 11}, "El filtro de presión tiene demasiada presión diferencial, compruebe el filtro"},
        {{5221, 4}, "El regulador de presión A (DR_A) tiene un cortocircuito"},
        {{5222, 4}, "El regulador de presión B (DR_B) tiene un cortocircuito"},
        {{5223, 4}, "El regulador de presión D (DR_D) tiene un cortocircuito"},
        {{5224, 4}, "El regulador de presión E (DR_E) tiene un cortocircuito"},
        {{5225, 4}, "El regulador de presión F (DR_F) tiene un cortocircuito"},
        {{5226, 4}, "El regulador de presión C (DR_C) tiene un cortocircuito"},
        {{5227, 4}, "El regulador de presión principal (DR_HD) tiene un cortocircuito"},
        {{5231, 5}, "Circuito del regulador de presión A (DR_A) se interrumpió"},
        {{5232, 5}, "Circuito del regulador de presión B (DR_B) se interrumpió"},
        {{5233, 5}, "Circuito del regulador de presión D (DR_D) se interrumpió"},
        {{5234, 5}, "Circuito del regulador de presión E (DR_E) se interrumpió"},
        {{5235, 5}, "Circuito del regulador de presión F (DR_F) se interrumpió"},
        {{5236, 5}, "Circuito del regulador de presión C (DR_C) se interrumpió"},
        {{5237, 5}, "Circuito del regulador de presión principal (DR_HD) se interrumpió"},
        {{5241, 14}, "El regulador de presión A (DR_A) tiene un cortocircuito en otra salida (AIM)"},
        {{5242, 14}, "El regulador de presión B (DR_B) tiene un cortocircuito en otra salida (AIM)."},
        {{5243, 14}, "El regulador de presión D (DR_D) tiene un cortocircuito en otra salida (AIM)"},
        {{5244, 14}, "El regulador de presión E (DR_E) tiene un cortocircuito en otra salida (AIM)."},
        {{5245, 14}, "El regulador de presión F (DR_F) tiene un cortocircuito en otra salida (AIM)"},
        {{5246, 14}, "El regulador de presión C (DR_C) tiene un cortocircuito en otra salida (AIM)"},
        {{5247, 14}, "El regulador de presión principal (DR_HD) tiene un cortocircuito en otra salida (AIM)"},
        {{5252, 4}, "Los pasadores de alimentación positivos de todas las válvulas proporcionales (VPS) tienen un cortocircuito"},
        {{5253, 14}, "Fallo eléctrico en el sensor de velocidad de la turbina"},
        {{5254, 14}, "Mal funcionamiento de la entrada de hardware para el sensor de velocidad de la turbina"},
        {{5255, 5}, "Cambio poco realista en la velocidad de la turbina en EF0"},
        {{5256, 14}, "Fallo eléctrico en el sensor de velocidad de salida"},
        {{5257, 14}, "Mal funcionamiento de la entrada de hardware para el sensor de velocidad de salida"},
        {{5258, 5}, "Cambio poco realista en la velocidad de salida en EF1"},
        {{5260, 14}, "Configuración del programa de unidades activas no válida"},
        {{5262, 14}, "La unidad central de corte (ZAE) ya no se corta."},
        {{5264, 1}, "Fuente de alimentación TCU (KL30) subvoltage"},
        {{5265, 0}, "Fuente de alimentación TCU (KL30) sobretensión"},
        {{5266, 14}, "La señal de dirección de la tecla y la conducción del mensaje selector de rango de velocidad no son plausibles"},
        {{5273, 1}, "Fallo del sumidero del sensor de temperatura (ER1) bajo -> corresponde a alta temperatura"},
        {{5274, 0}, "Fallo de sumidero del sensor de temperatura (ER1) alto -> corresponde a baja temperatura"},
        {{5280, 4}, "El sensor de velocidad de la turbina de alimentación (AU1) tiene cortocircuito a tierra"},
        {{5281, 3}, "El sensor de velocidad de la turbina de alimentación (AU1) tiene cortocircuito para una fuente positiva (VPS)"},
        {{5283, 4}, "El sensor de velocidad de la turbina de alimentación (AU2) tiene cortocircuito a tierra"},
        {{5284, 3}, "El sensor de velocidad de la turbina de alimentación (AU2) tiene cortocircuito para una fuente positiva (VPS)"},
        {{5292, 14}, "CAN_A o CAN_B tiene un error durante la inicialización"},
        {{5293, 14}, "CAN_A - BUSOFF (vehículo CAN)"},
        {{5295, 14}, "CAN_A - error pasivo (vehículo CAN)"},
        {{5296, 14}, "CAN_A - advertencia de error (vehículo CAN)"},
        {{5310, 14}, "Fallo del mensaje CAN EEC1 - controlador de motor electrónico 1"},
        {{5311, 14}, "Fallo del mensaje CAN EEC2 - controlador de motor electrónico 2"},
        {{5313, 14}, "Fallo del mensaje CAN EC - configuración del motor"},
        {{5316, 14}, "Error del mensaje CAN TC1 - control de transmisión 1 (de otro)"},
        {{5327, 14}, "El par del motor actual no está disponible (SAE)"},
        {{5328, 14}, "La velocidad del motor no está disponible (SAE)"},
        {{5330, 14}, "La posición del pedal del acelerador no está disponible (SAE)"},
        {{5331, 14}, "La relación entre el par actual del motor y el par máximo posible del motor no está disponible a la velocidad actual (SAE)"},
        {{5332, 14}, "El par de fricción no está disponible (SAE)"},
        {{5333, 14}, "El par de referencia del motor no está disponible ni fuera de la ventana de especificación (SAE)"},
        {{5343, 14}, "CAN_A error de envío"},
        {{5355, 14}, "Checksum EEPROM incorrecto"},
        {{5356, 14}, "Error compuesto para hardware TCU (interno)"},
        {{5357, 14}, "Error compuesto para el software TCU (interno)"},
        {{5363, 14}, "Fallo de la velocidad de la turbina (n_Tu) y la velocidad de salida (n_Ab)"},
        {{5364, 14}, "Fallo de la velocidad de la turbina (n_Tu) y la velocidad del motor (n_Mot)"},
        {{5365, 14}, "Fallo de la velocidad del motor (n_Mot) y la velocidad de salida (n_Ab)"},
        {{5388, 14}, "Deslizamiento en piñón fijo"},
        {{5432, 14}, "Fallo de la velocidad del motor (n_Mot), la velocidad de la turbina (n_Tu) y la velocidad de salida (n_Ab)"},
        {{5435, 14}, "Advertencia aumento de la temperatura del sumidero"},
        {{5436, 14}, "Sobrecalentamiento de la temperatura del sumidero"},
        {{5460, 14}, "El modo de escritorio está activo"},
        {{5572, 14}, "TCU hardware (interno)"},
        {{5585, 14}, "Error del convertidor digital analógico"},
        {{5590, 14}, "Los valores de calibración de corriente eléctrica no son plausibles"},
        {{5591, 14}, "Los valores de EEprom no son plausibles"}
    }

#Region "Properties"
    Private Shared _errordictionary As New Dictionary(Of IEnumerable(Of Integer), String)() From {}
    Public Shared Property ErrorDictionary() As Dictionary(Of IEnumerable(Of Integer), String)
        Get
            Return _errordictionary
        End Get
        Set(value As Dictionary(Of IEnumerable(Of Integer), String))
            If value IsNot _errordictionary Then
                _errordictionary = value
            End If
        End Set
    End Property
    Private Shared Event SourceChanged()
    Private Shared _source As String
    Private Shared Property Source() As String
        Get
            Return _source
        End Get
        Set(value As String)
            If value <> _source Then
                _source = value
                RaiseEvent SourceChanged()
            End If
        End Set
    End Property
    Private Shared Event UseSourceChanged()
    Private Shared _usesource As Boolean
    Private Shared Property UseSource() As Boolean
        Get
            Return _usesource
        End Get
        Set(value As Boolean)
            If value <> _usesource Then
                _usesource = value
                RaiseEvent UseSourceChanged()
            End If
        End Set
    End Property
    Private Shared _currentfrac As Integer
    Private Shared Property CurrentFrac() As Integer
        Get
            Return _currentfrac
        End Get
        Set(value As Integer)
            If value <> _currentfrac Then
                _currentfrac = value
            End If
        End Set
    End Property
    Private Shared Event MotorSourceChange()
    Private Shared _motorsource As Integer
    Private Shared Property MotorSource() As Integer
        Get
            Return _motorsource
        End Get
        Set(value As Integer)
            If value <> _motorsource Then
                _motorsource = value
                RaiseEvent MotorSourceChange()
            End If
        End Set
    End Property
    Private Shared Event TransSourceChange()
    Private Shared _transsource As Integer
    Private Shared Property TransSource() As Integer
        Get
            Return _transsource
        End Get
        Set(value As Integer)
            If value <> _transsource Then
                _transsource = value
                RaiseEvent TransSourceChange()
            End If
        End Set
    End Property
    Private Shared _fracsystem As IFracSystem
    Private Shared Property FracSystem() As IFracSystem
        Get
            Return _fracsystem
        End Get
        Set(value As IFracSystem)
            If value IsNot _fracsystem Then
                _fracsystem = value
            End If
        End Set
    End Property
    Private Shared DblVFrac As New Dictionary(Of String, Double)() From {}
#End Region

    ''--CONSTRUCTOR DE LA CLASE--''
    Public Sub New(ByVal DblVFracSel As Dictionary(Of String, Double), ByVal ISystem As IFracSystem)
        DblVFrac = DblVFracSel
        FracSystem = ISystem
        Update()
        AddHandler LanguageSelect.LanguageChange, AddressOf Update
    End Sub

    ''' <summary>
    ''' Funcion para actualizar el diccionario segun lenguaje.
    ''' </summary>
    Public Sub Update()
        Select Case LanguageSelect.CurrentLanguage
            Case "Español"
                ErrorDictionary = ErrorDictionary_Español
            Case "Ingles"
                ErrorDictionary = ErrorDictionary_English
        End Select
    End Sub


    ''' <summary>
    ''' Funcion para buscar un error por SPN y FMI.
    ''' </summary>
    ''' <param name="SPN">Parametro SPN para errores en ECU.</param>
    ''' <param name="FMI">Parametro FMI para errores en ECU.</param>
    ''' <remarks></remarks>
    Public Function Search(ByVal SPN As Integer, ByVal FMI As Integer)
        For Each currentKey As IEnumerable(Of Integer) In ErrorDictionary.Keys
            If currentKey(0) = SPN And currentKey(1) = FMI Then
                Return ErrorDictionary(currentKey)
            End If
        Next
        Return "Error desconocido"
    End Function

    ''' <summary>
    ''' Funcion para saber si existe un error segun SPN y FMI.
    ''' </summary>
    ''' <param name="SPN">Parametro SPN para errores en ECU.</param>
    ''' <param name="FMI">Parametro FMI para errores en ECU.</param> 
    ''' <remarks></remarks>
    Public Function ContainsKey(ByVal SPN As Integer, ByVal FMI As Integer)
        For Each currentKey As IEnumerable(Of Integer) In ErrorDictionary.Keys
            If currentKey(0) = SPN And currentKey(1) = FMI Then
                Return True
            End If
        Next
        Return False
    End Function


    ''' <summary>
    ''' Funcion para configurar los parametros para la busqueda de errores.
    ''' </summary>
    ''' <param name="sSource">Nombre de la fuente, "Motor" o "Transmision".</param>
    ''' <param name="sUseSource">Variable para uso de fuente, si es True se puede optar por usar fuentes especificas.</param>
    ''' <param name="sCurrentFrac">ID del Fracturador actual.</param>
    ''' <param name="sMotorSource">Source del Motor.</param>
    ''' <param name="sTransSource">Source de la transmision.</param>
    '''  <remarks></remarks>
    Public Sub Settings(ByVal sSource As String, ByVal sUseSource As Boolean, ByVal sCurrentFrac As Integer, Optional ByVal sMotorSource As Integer = 5, Optional ByVal sTransSource As Integer = 3)

        CurrentFrac = sCurrentFrac
        Source = sSource
        UseSource = sUseSource
        MotorSource = sMotorSource
        TransSource = sTransSource

    End Sub

    ''' <summary>
    ''' Funcion para buscar el siguiente error.
    ''' </summary>
    Public Sub FindNextError()
        Dim NextError As New Task(AddressOf NextErrorDo)
        NextError.Start()
    End Sub
    Private Async Sub NextErrorDo()
        FracSystem.MoxaSend(FracMoxaSendStartAddress("PrevError_StartAddress"), 0, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("NextError_StartAddress"), 1, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("NextError_StartAddress"), 0, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para buscar el error previo.
    ''' </summary>
    Public Sub FindPrevError()
        Dim PrevError As New Task(AddressOf PrevErrorDo)
        PrevError.Start()
    End Sub
    Private Async Sub PrevErrorDo()
        FracSystem.MoxaSend(FracMoxaSendStartAddress("NextError_StartAddress"), 0, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("PrevError_StartAddress"), 1, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("PrevError_StartAddress"), 0, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para borrar el error actual.
    ''' </summary>
    Public Sub DeleteCurrentError()
        Dim DelError As New Task(AddressOf DelErrorDo)
        DelError.Start()
    End Sub
    Private Async Sub DelErrorDo()
        FracSystem.MoxaSend(FracMoxaSendStartAddress("DelError_StartAddress"), 1, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("DelError_StartAddress"), 0, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para borrar todos los errores.
    ''' </summary>
    Public Sub DeleteAllErrors()
        Dim DelAllError As New Task(AddressOf DelAllErrorDo)
        DelAllError.Start()
    End Sub
    Private Async Sub DelAllErrorDo()
        FracSystem.MoxaSend(FracMoxaSendStartAddress("DelAllError_StartAddress"), 1, CurrentFrac)
        Await Task.Delay(100)
        FracSystem.MoxaSend(FracMoxaSendStartAddress("DelAllError_StartAddress"), 0, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para leer la descripcion del error.
    ''' </summary>
    Public Function GetError() As String
        If DblVFrac("TotalErr_" + CurrentFrac.ToString()) <> 0 Then
            Return Search(DblVFrac("SPN_" + CurrentFrac.ToString()), DblVFrac("FMI_" + CurrentFrac.ToString()))
        Else
            Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.NoError")
        End If
    End Function

    ''' <summary>
    ''' Funcion para obtener la fuente del error.
    ''' </summary>
    Public Function GetSource() As String
        Select Case DblVFrac("Direccion_" + CurrentFrac.ToString())
            Case 3
                Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.Transmision")
            Case 0
                Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.Motor")
        End Select
        Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.Procedencia.Ninguna")
    End Function


    ''' <summary>
    ''' Funcion para saber si un error esta activo.
    ''' </summary>
    Public Function GetActive() As String
        If DblVFrac("TotalErr_" + CurrentFrac.ToString()) <> 0 Then
            Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.Activo.Si")
        Else
            Return LanguageSelect.DynamicLanguageRM.GetString("FracErrores.Activo.No")
        End If
    End Function

    ''' <summary>
    ''' Funcion para setear el use source.
    ''' </summary>
    Private Sub SetUseSource() Handles MyClass.UseSourceChanged
        FracSystem.MoxaSend(FracMoxaSendStartAddress("UseSource_StartAddress"), UseSource, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para setear source del motor.
    ''' </summary>
    Private Sub SetMotorSource() Handles MyClass.MotorSourceChange
        FracSystem.MoxaSend(FracMoxaSendStartAddress("MotorSource_StartAddress"), MotorSource, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para setear source de la transmision.
    ''' </summary>
    Private Sub SetTransSource() Handles MyClass.TransSourceChange
        FracSystem.MoxaSend(FracMoxaSendStartAddress("TransmissionSource_StartAddress"), UseSource, CurrentFrac)
    End Sub

    ''' <summary>
    ''' Funcion para setear vbctrl.
    ''' </summary>
    Private Sub SetVBCTRL() Handles MyClass.SourceChanged
        Select Case Source
            Case "Motor"
                FracSystem.MoxaSend(FracMoxaSendStartAddress("VB_CTRL_StartAddress"), 0, CurrentFrac)
            Case "Transmision"
                FracSystem.MoxaSend(FracMoxaSendStartAddress("VB_CTRL_StartAddress"), 3, CurrentFrac)
            Case Else
                FracSystem.MoxaSend(FracMoxaSendStartAddress("VB_CTRL_StartAddress"), 1, CurrentFrac)
        End Select
    End Sub


End Class