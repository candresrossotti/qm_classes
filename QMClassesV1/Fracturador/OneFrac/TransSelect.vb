﻿Public Class TransSelect

    Private Shared _transmission As String
    Public Shared Event TransmissionChange()
    Public Shared SelectedTransmisison As Dictionary(Of String, Object)

    ''---Singleton---
    Private Shared TransSelect
    Private Sub New()
    End Sub

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton 
    Public Shared Function GetInstancia()
        If TransSelect Is Nothing Then
            TransSelect = New TransSelect()
        End If
        Return TransSelect
    End Function

    'Public Shared TwinDiscTA918501_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Twin Disc TA91-8501"},
    '    {"FSTransName", "TA91-8501"},
    '    {"Gears", 9},
    '    {"RelacionGear1", 4.47},
    '    {"RelacionGear2", 3.57},
    '    {"RelacionGear3", 2.85},
    '    {"RelacionGear4", 2.41},
    '    {"RelacionGear5", 1.92},
    '    {"RelacionGear6", 1.54},
    '    {"RelacionGear7", 1.25},
    '    {"RelacionGear8", 1},
    '    {"RelacionGear9", 0.8},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 9}
    '}

    'Public Shared TwinDiscTA907500_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Twin Disc TA90-7500"},
    '    {"FSTransName", "TA90-7500"},
    '    {"Gears", 9},
    '    {"RelacionGear1", 2.95},
    '    {"RelacionGear2", 2.55},
    '    {"RelacionGear3", 2.17},
    '    {"RelacionGear4", 1.82},
    '    {"RelacionGear5", 1.57},
    '    {"RelacionGear6", 1.33},
    '    {"RelacionGear7", 1.16},
    '    {"RelacionGear8", 1},
    '    {"RelacionGear9", 0.85},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 9}
    '}

    'Public Shared Allison4700OFS_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Allison 4700OFS"},
    '    {"FSTransName", "A-4700OFS"},
    '    {"Gears", 7},
    '    {"RelacionGear1", 7.63},
    '    {"RelacionGear2", 3.51},
    '    {"RelacionGear3", 1.91},
    '    {"RelacionGear4", 1.43},
    '    {"RelacionGear5", 1},
    '    {"RelacionGear6", 0.74},
    '    {"RelacionGear7", 0.64},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 7}
    '}

    'Public Shared Allison4750OFS_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Allison 4750OFS"},
    '    {"FSTransName", "A-4750OFS"},
    '    {"Gears", 7},
    '    {"RelacionGear1", 7.63},
    '    {"RelacionGear2", 3.51},
    '    {"RelacionGear3", 1.91},
    '    {"RelacionGear4", 1.43},
    '    {"RelacionGear5", 1},
    '    {"RelacionGear6", 0.74},
    '    {"RelacionGear7", 0.64},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 7}
    '}

    'Public Shared Allison9800_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Allison 9800"},
    '    {"FSTransName", "A-9800"},
    '    {"Gears", 8},
    '    {"RelacionGear1", 3.75},
    '    {"RelacionGear2", 2.69},
    '    {"RelacionGear3", 2.2},
    '    {"RelacionGear4", 1.77},
    '    {"RelacionGear5", 1.58},
    '    {"RelacionGear6", 1.27},
    '    {"RelacionGear7", 1},
    '    {"RelacionGear8", 0.72},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 8}
    '}

    'Public Shared AllisonS8610_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Allison S8640"},
    '    {"FSTransName", "A-S8640"},
    '    {"Gears", 6},
    '    {"RelacionGear1", 4.24},
    '    {"RelacionGear2", 2.32},
    '    {"RelacionGear3", 1.69},
    '    {"RelacionGear4", 1.31},
    '    {"RelacionGear5", 1},
    '    {"RelacionGear6", 0.73},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 6}
    '}

    'Public Shared Allison9600_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "Allison 9600"},
    '    {"FSTransName", "A-9600"},
    '    {"Gears", 6},
    '    {"RelacionGear1", 4.24},
    '    {"RelacionGear2", 3.05},
    '    {"RelacionGear3", 2.32},
    '    {"RelacionGear4", 1.67},
    '    {"RelacionGear5", 1},
    '    {"RelacionGear6", 0.72},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 6}
    '}

    'Public Shared TH55E90_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "CAT TH55-E90"},
    '    {"FSTransName", "TH55-E90"},
    '    {"Gears", 9},
    '    {"RelacionGear1", 4.67},
    '    {"RelacionGear2", 3.43},
    '    {"RelacionGear3", 3.03},
    '    {"RelacionGear4", 2.53},
    '    {"RelacionGear5", 2.22},
    '    {"RelacionGear6", 1.85},
    '    {"RelacionGear7", 1.64},
    '    {"RelacionGear8", 1.36},
    '    {"RelacionGear9", 1},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 9}
    '}

    'Public Shared TH55E70_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "CAT TH55-E70"},
    '    {"FSTransName", "TH55-E70"},
    '    {"Gears", 7},
    '    {"RelacionGear1", 6.25},
    '    {"RelacionGear2", 4.59},
    '    {"RelacionGear3", 3.38},
    '    {"RelacionGear4", 2.48},
    '    {"RelacionGear5", 1.83},
    '    {"RelacionGear6", 1.36},
    '    {"RelacionGear7", 1},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 7}
    '}

    'Public Shared TH48E80_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "CAT TH48-E80"},
    '    {"FSTransName", "TH48-E80"},
    '    {"Gears", 8},
    '    {"RelacionGear1", 3.339},
    '    {"RelacionGear2", 2.453},
    '    {"RelacionGear3", 2.2},
    '    {"RelacionGear4", 1.808},
    '    {"RelacionGear5", 1.616},
    '    {"RelacionGear6", 1.36},
    '    {"RelacionGear7", 1.191},
    '    {"RelacionGear8", 1},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 8}
    '}

    'Public Shared TH48E70_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "CAT TH48-E70"},
    '    {"FSTransName", "TH48-E70"},
    '    {"Gears", 7},
    '    {"RelacionGear1", 6.16},
    '    {"RelacionGear2", 4.52},
    '    {"RelacionGear3", 3.33},
    '    {"RelacionGear4", 2.47},
    '    {"RelacionGear5", 1.82},
    '    {"RelacionGear6", 1.36},
    '    {"RelacionGear7", 1},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 7}
    '}

    'Public Shared ZF8TX_TRANSMISSION As New Dictionary(Of String, Object) From {
    '    {"TransName", "ZF-8 TX"},
    '    {"FSTransName", "ZF-8"},
    '    {"Gears", 7},
    '    {"RelacionGear1", 3.674},
    '    {"RelacionGear2", 2.667},
    '    {"RelacionGear3", 2.156},
    '    {"RelacionGear4", 1.708},
    '    {"RelacionGear5", 1.378},
    '    {"RelacionGear6", 1.217},
    '    {"RelacionGear7", 1.005},
    '    {"LimitGEAR", False},
    '    {"Gear_LMin", 1},
    '    {"Gear_LMax", 7}
    '}



    Public Shared Property Transmission() As String
        Get
            Return _transmission
        End Get
        Set(ByVal value As String)
            If value <> _transmission Then
                _transmission = value
                RaiseEvent TransmissionChange()
            End If
        End Set
    End Property

    Private Shared _fracsystem As IFracSystem
    Private Shared Property FracSystem() As IFracSystem
        Get
            Return _fracsystem
        End Get
        Set(value As IFracSystem)
            If value IsNot _fracsystem Then
                _fracsystem = value
            End If
        End Set
    End Property


    Public Sub Update(ByRef ISystem As IFracSystem)
        FracSystem = ISystem
    End Sub


    'Public Sub New()
    '    If String.IsNullOrEmpty(My.Settings.TwinDiscTA918501) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TwinDiscTA918501_TRANSMISSION)
    '        My.Settings.TwinDiscTA918501 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TwinDiscTA918501_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TwinDiscTA918501, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.TwinDiscTA907500) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TwinDiscTA907500_TRANSMISSION)
    '        My.Settings.TwinDiscTA907500 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TwinDiscTA907500_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TwinDiscTA907500, GetType(Dictionary(Of String, Object)))
    '    End If


    '    If String.IsNullOrEmpty(My.Settings.Allison4700OFS) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(Allison4700OFS_TRANSMISSION)
    '        My.Settings.Allison4700OFS = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        Allison4700OFS_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.Allison4700OFS, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.Allison4750OFS) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(Allison4750OFS_TRANSMISSION)
    '        My.Settings.Allison4750OFS = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        Allison4750OFS_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.Allison4750OFS, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.Allison9800) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(Allison9800_TRANSMISSION)
    '        My.Settings.Allison9800 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        Allison9800_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.Allison9800, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.AllisonS8610) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(AllisonS8610_TRANSMISSION)
    '        My.Settings.AllisonS8610 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        AllisonS8610_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.AllisonS8610, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.Allison9600) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(Allison9600_TRANSMISSION)
    '        My.Settings.Allison9600 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        Allison9600_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.Allison9600, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.TH55E90) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TH55E90_TRANSMISSION)
    '        My.Settings.TH55E90 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TH55E90_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TH55E90, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.TH55E70) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TH55E70_TRANSMISSION)
    '        My.Settings.TH55E70 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TH55E70_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TH55E70, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.TH48E80) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TH48E80_TRANSMISSION)
    '        My.Settings.TH48E80 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TH48E80_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TH48E80, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.TH48E70) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(TH48E70_TRANSMISSION)
    '        My.Settings.TH48E70 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        TH48E70_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.TH48E70, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.ZF8TX) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(ZF8TX_TRANSMISSION)
    '        My.Settings.ZF8TX = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        ZF8TX_TRANSMISSION = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.ZF8TX, GetType(Dictionary(Of String, Object)))
    '    End If
    'End Sub

    'Public Sub ResetTransmissions()
    '    My.Settings.TwinDiscTA918501 = Nothing
    '    My.Settings.TwinDiscTA907500 = Nothing
    '    My.Settings.Allison4700OFS = Nothing
    '    My.Settings.Allison4750OFS = Nothing
    '    My.Settings.Allison9800 = Nothing
    '    My.Settings.AllisonS8610 = Nothing
    '    My.Settings.Allison9600 = Nothing
    '    My.Settings.TH55E90 = Nothing
    '    My.Settings.TH55E70 = Nothing
    '    My.Settings.TH48E80 = Nothing
    '    My.Settings.TH48E70 = Nothing
    '    My.Settings.ZF8TX = Nothing

    '    My.Settings.Save()
    'End Sub

    Private Sub TransChange() Handles MyClass.TransmissionChange

        SelectedTransmisison = FracSystem.GetTransmission(Transmission)

        'Select Case Transmission
        '    Case "ZF-8 TX"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(ZF8TX_TRANSMISSION)
        '    Case "CAT TH48-E70"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TH48E70_TRANSMISSION)
        '    Case "CAT TH48-E80"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TH48E80_TRANSMISSION)
        '    Case "CAT TH55-E70"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TH55E70_TRANSMISSION)
        '    Case "CAT TH55-E90"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TH55E90_TRANSMISSION)
        '    Case "Allison 9600"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(Allison9600_TRANSMISSION)
        '    Case "Allison S8640"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(AllisonS8610_TRANSMISSION)
        '    Case "Allison 9800"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(Allison9800_TRANSMISSION)
        '    Case "Twin Disc TA90-7500"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TwinDiscTA907500_TRANSMISSION)
        '    Case "Twin Disc TA91-8501"
        '        SelectedTransmisison = New Dictionary(Of String, Object)(TwinDiscTA918501_TRANSMISSION)
        '    Case Else
        '        SelectedTransmisison = Nothing
        'End Select
    End Sub



    ''' <summary>
    ''' Funcion para saber la transmision del fracturador.
    ''' </summary>
    Public Function GetTrans(ByVal Frac As Integer) As String
        Return FracSystem.GetFracTransmission(Frac.ToString())
        'Dim CurrentTrans As Integer() = FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentTransStartAddress"), 1, Frac)
        'Select Case Frac'CurrentTrans(0)
        '    Case 0
        '        Return "ZF-8 TX"
        '    Case 1
        '        Return "CAT TH48-E70"
        '    Case 2
        '        Return "CAT TH48-E80"
        '    Case 3
        '        Return "CAT TH55-E70"
        '    Case 4
        '        Return "CAT TH55-E90"
        '    Case 5
        '        Return "Allison 9600"
        '    Case 6
        '        Return "Allison S8640"
        '    Case 7
        '        Return "Allison 9800"
        '    Case 8
        '        Return "Twin Disc TA90-7500"
        '    Case 9
        '        Return "Twin Disc TA91-8501"
        '    Case Else
        '        Return "NA"
        'End Select
    End Function

    Public Function GetTransFromFrac(ByVal Frac As Integer) As String
        Select Case FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentPumpStartAddress"), 1, Frac)(0)
            Case 1
                Return "ZF-8 TX"
            Case 2
                Return "CAT TH48-E70"
            Case 3
                Return "CAT TH48-E80"
            Case 4
                Return "CAT TH55-E70"
            Case 5
                Return "CAT TH55-E90"
            Case 6
                Return "Allison 9600"
            Case 7
                Return "Allison S8640"
            Case 8
                Return "Allison 9800"
            Case 9
                Return "Twin Disc TA90-7500"
            Case 10
                Return "Twin Disc TA91-8501"
            Case Else
                Return ""
        End Select
    End Function
End Class

