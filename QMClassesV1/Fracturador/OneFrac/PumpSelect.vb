﻿Public Class PumpSelect

    ''--DICCIONARIO QUE CONTIENE TODAS LAS VARIABLES DEPENDIENDO DEL MOTOR--''
    Public Shared SelectedPump As Dictionary(Of String, Object)

    ''---Singleton---
    Private Shared PumpSelect
    Private Sub New()
    End Sub

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton 
    Public Shared Function GetInstancia()
        If PumpSelect Is Nothing Then
            PumpSelect = New PumpSelect()
        End If
        Return PumpSelect
    End Function

    'Public Shared SPMQEM3000_3_75_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QEM-3000"},
    '    {"FSPumpName", "QEM 3000"},
    '    {"rTransBomba", 6.963},
    '    {"rGalRev", 1.91}
    '}

    'Public Shared SPMQEM3000_4_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QEM-3000"},
    '    {"FSPumpName", "QEM 3000"},
    '    {"rTransBomba", 6.963},
    '    {"rGalRev", 2.18}
    '}

    'Public Shared SPMQEM3000_4_5_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QEM-3000"},
    '    {"FSPumpName", "QEM 3000"},
    '    {"rTransBomba", 6.963},
    '    {"rGalRev", 2.75}
    '}

    'Public Shared SPMQEM3000_5_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QEM-3000"},
    '    {"FSPumpName", "QEM 3000"},
    '    {"rTransBomba", 6.963},
    '    {"rGalRev", 3.4}
    '}

    'Public Shared SPMQWS2500_4_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QWS-2500"},
    '    {"FSPumpName", "QWS 2500"},
    '    {"rTransBomba", 6.353},
    '    {"rGalRev", 2.18}
    '}

    'Public Shared SPMQWS2500_4_5_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QWS-2500"},
    '    {"FSPumpName", "QWS 2500"},
    '    {"rTransBomba", 6.353},
    '    {"rGalRev", 2.75}
    '}

    'Public Shared SPMQWS2500_5_PUMP As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QWS-2500"},
    '    {"FSPumpName", "QWS 2500"},
    '    {"rTransBomba", 6.353},
    '    {"rGalRev", 3.4}
    '}

    'Public Shared SPMQEM5000 As New Dictionary(Of String, Object) From {
    '    {"PumpName", "SPM QEM-5000"},
    '    {"FSPumpName", "QEM 5000"},
    '    {"rTransBomba", 10.05},
    '    {"rGalRev", 3.4}
    '}

    Public Shared Event PumpChange()
    Private Shared _pump As String
    Public Shared Property Pump() As String
        Get
            Return _pump
        End Get
        Set(ByVal value As String)
            If value <> _pump Then
                _pump = value
                RaiseEvent PumpChange()
            End If
        End Set
    End Property

    Private Shared _fracsystem As IFracSystem
    Private Shared Property FracSystem() As IFracSystem
        Get
            Return _fracsystem
        End Get
        Set(value As IFracSystem)
            If value IsNot _fracsystem Then
                _fracsystem = value
            End If
        End Set
    End Property


    'Public Sub New()
    '    If String.IsNullOrEmpty(My.Settings.SPMQEM3000_3_75) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQEM3000_3_75_PUMP)
    '        My.Settings.SPMQEM3000_3_75 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQEM3000_3_75_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQEM3000_3_75, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQEM3000_4) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQEM3000_4_PUMP)
    '        My.Settings.SPMQEM3000_4 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQEM3000_4_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQEM3000_4, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQEM3000_4_5) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQEM3000_4_5_PUMP)
    '        My.Settings.SPMQEM3000_4_5 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQEM3000_4_5_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQEM3000_4_5, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQEM3000_5) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQEM3000_5_PUMP)
    '        My.Settings.SPMQEM3000_5 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQEM3000_5_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQEM3000_5, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQWS2500_4) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQWS2500_4_PUMP)
    '        My.Settings.SPMQWS2500_4 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQWS2500_4_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQWS2500_4, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQWS2500_4_5) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQWS2500_4_5_PUMP)
    '        My.Settings.SPMQWS2500_4_5 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQWS2500_4_5_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQWS2500_4_5, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.SPMQWS2500_5) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(SPMQWS2500_5_PUMP)
    '        My.Settings.SPMQWS2500_5 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        SPMQWS2500_5_PUMP = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.SPMQWS2500_5, GetType(Dictionary(Of String, Object)))
    '    End If

    'End Sub

    Public Sub Update(ByRef ISystem As IFracSystem)
        FracSystem = ISystem
    End Sub

    'Public Sub ResetPumps()
    '    My.Settings.SPMQEM3000_3_75 = Nothing
    '    My.Settings.SPMQEM3000_4 = Nothing
    '    My.Settings.SPMQEM3000_4_5 = Nothing
    '    My.Settings.SPMQEM3000_5 = Nothing
    '    My.Settings.SPMQWS2500_4 = Nothing
    '    My.Settings.SPMQWS2500_4_5 = Nothing
    '    My.Settings.SPMQWS2500_5 = Nothing

    '    My.Settings.Save()
    'End Sub

    Private Sub InitPump() Handles Me.PumpChange

        SelectedPump = FracSystem.GetPump(Pump)

        'Select Case Pump
        '    Case "SPM QWS 2500 - 4"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQWS2500_4_PUMP)
        '    Case "SPM QWS 2500 - 4.5"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQWS2500_4_5_PUMP)
        '    Case "SPM QWS 2500 - 5"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQWS2500_5_PUMP)
        '    Case "SPM QEM-3000 - 3.75"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQEM3000_3_75_PUMP)
        '    Case "SPM QEM-3000 - 4"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQEM3000_4_PUMP)
        '    Case "SPM QEM-3000 - 4.5"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQEM3000_4_5_PUMP)
        '    Case "SPM QEM-3000 - 5"
        '        SelectedPump = New Dictionary(Of String, Object)(SPMQEM3000_5_PUMP)
        '    Case Else
        '        SelectedPump = Nothing
        'End Select
    End Sub

    ''' <summary>
    ''' Funcion para saber la bomba del fracturador.
    ''' </summary>
    Public Function GetPump(ByVal Frac As Integer) As String

        Return FracSystem.GetFracPump(Frac.ToString())

        'Dim CurrentPump As Integer() = FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentPumpStartAddress"), 1, Frac)
        'Select Case Frac'CurrentPump(0)
        '    Case 3
        '        Return "SPM QWS 2500 - 4"
        '    Case 1
        '        Return "SPM QWS 2500 - 4.5"
        '    Case 7
        '        Return "SPM QWS 2500 - 5"
        '    Case 3
        '        Return "SPM QEM-3000 - 3.75"
        '    Case 4
        '        Return "SPM QEM-3000 - 4"
        '    Case 8
        '        Return "SPM QEM-3000 - 4.5"
        '    Case 6
        '        Return "SPM QEM-3000 - 5"
        '    Case Else
        '        Return "NA"
        'End Select
    End Function

    Public Function GetPumpFromFrac(ByVal Frac As Integer) As String
        Select Case FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentPumpStartAddress"), 1, Frac)(0)
            Case 1
                Return "SPM QWS 2500 - 4"
            Case 2
                Return "SPM QWS 2500 - 4.5"
            Case 3
                Return "SPM QWS 2500 - 5"
            Case 4
                Return "SPM QEM-3000 - 3.75"
            Case 5
                Return "SPM QEM-3000 - 4"
            Case 6
                Return "SPM QEM-3000 - 4.5"
            Case 7
                Return "SPM QEM-3000 - 5"
            Case Else
                Return ""
        End Select
    End Function

End Class
