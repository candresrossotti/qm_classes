﻿Public Class OneFracConfig
    Inherits FracDB

    Private ReadOnly TS As TransSelect = TransSelect.GetInstancia
    Private ReadOnly ES As EngineSelect = EngineSelect.GetInstancia
    Private ReadOnly PS As PumpSelect = PumpSelect.GetInstancia

    Public Sub Update(ByRef FracSystem As IFracSystem)
        ES.Update(FracSystem)
        TS.Update(FracSystem)
        PS.Update(FracSystem)
    End Sub

    Public Sub InitFrac(ByVal Frac As Integer)
        InitPump(Frac)
        InitTrans(Frac)
        InitEngine(Frac)
    End Sub

    Public Function GetPump(ByVal Pump As String) As Dictionary(Of String, Object)
        Return GetDBPump(Pump)
    End Function
    Public Function GetTransmission(ByVal Trans As String) As Dictionary(Of String, Object)
        Return GetDBTrans(Trans)
    End Function

    Public Function GetEngine(ByVal Engine As String) As Dictionary(Of String, Object)
        Return GetDBEngine(Engine)
    End Function

    Private Sub InitPump(ByVal Frac As Integer)
        PumpSelect.Pump = PS.GetPump(Frac)
    End Sub

    Private Sub InitTrans(ByVal Frac As Integer)
        TransSelect.Transmission = TS.GetTrans(Frac)
    End Sub

    Private Sub InitEngine(ByVal Frac As Integer)
        EngineSelect.Engine = ES.GetEngine(Frac)
    End Sub

    'Public Sub ResetEngines()
    '    ES.ResetEngines()
    'End Sub

    'Public Sub ResetPumps()
    '    PS.ResetPumps()
    'End Sub

    'Public Sub ResetTransmissions()
    '    TS.ResetTransmissions()
    'End Sub

End Class
