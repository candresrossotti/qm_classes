﻿Public Class EngineSelect

    ''--EVENTO QUE SE TRIGGEREA CUANDO SE CAMBIA EL MOTOR DE UN FRAC--''
    Private Shared Event EngineChange()

    ''--DICCIONARIO QUE CONTIENE TODAS LAS VARIABLES DEPENDIENDO DEL MOTOR--''
    Public Shared SelectedEngine As Dictionary(Of String, Object)



    ''---Singleton---
    Private Shared EngineSelect
    Private Sub New()
    End Sub

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton 
    Public Shared Function GetInstancia()
        If EngineSelect Is Nothing Then
            EngineSelect = New EngineSelect()
        End If
        Return EngineSelect
    End Function

    'Public Shared CAT3512E_ENGINE As New Dictionary(Of String, Object) From {
    '    {"EngineName", "CAT3512E"},
    '    {"FSEngineName", "CAT3512E"},
    '    {"EfficientRPM", 1900},
    '    {"EfficientHP", 2250 * 0.7},
    '    {"MaxHP", 2250},
    '    {"MinRPM", 700},
    '    {"MaxRPM", 1950},
    '    {"LimitRPM", False},
    '    {"RPM_LMin", 700},
    '    {"RPM_LMax", 1950}
    '}

    'Public Shared CAT3512CDM8864_ENGINE As New Dictionary(Of String, Object) From {
    '    {"EngineName", "CAT3512C-DM8864"},
    '    {"FSEngineName", "CAT3512C"},
    '    {"EfficientRPM", 1900},
    '    {"EfficientHP", 2250 * 0.7},
    '    {"MaxHP", 2250},
    '    {"MinRPM", 650},
    '    {"MaxRPM", 1900},
    '    {"LimitRPM", False},
    '    {"RPM_LMin", 650},
    '    {"RPM_LMax", 1900}
    '}


    'Public Shared CAT3512CDM8865_ENGINE As New Dictionary(Of String, Object) From {
    '    {"EngineName", "CAT3512C-DM8865"},
    '    {"FSEngineName", "CAT3512C"},
    '    {"EfficientRPM", 1900},
    '    {"EfficientHP", 2500 * 0.7},
    '    {"MaxHP", 2500},
    '    {"MinRPM", 650},
    '    {"MaxRPM", 1900},
    '    {"LimitRPM", False},
    '    {"RPM_LMin", 650},
    '    {"RPM_LMax", 1900}
    '}

    'Public Shared LiebherrD9620A7_ENGINE As New Dictionary(Of String, Object) From {
    '    {"EngineName", "LiebherrD9620A7"},
    '    {"FSEngineName", "D9620A7"},
    '    {"EfficientRPM", 1800},
    '    {"EfficientHP", 2250 * 0.7},
    '    {"MaxHP", 2250},
    '    {"MinRPM", 800},
    '    {"MaxRPM", 2100},
    '    {"LimitRPM", False},
    '    {"RPM_LMin", 800},
    '    {"RPM_LMax", 2100}
    '}



    Private Shared _engine As String
    Public Shared Property Engine() As String
        Get
            Return _engine
        End Get
        Set(ByVal value As String)
            If value <> _engine Then
                _engine = value
                RaiseEvent EngineChange()
            End If
        End Set
    End Property
    Private Shared _fracsystem As IFracSystem
    Private Shared Property FracSystem() As IFracSystem
        Get
            Return _fracsystem
        End Get
        Set(value As IFracSystem)
            If value IsNot _fracsystem Then
                _fracsystem = value
            End If
        End Set
    End Property

    'Public Sub New()

    '    If String.IsNullOrEmpty(My.Settings.CAT3512E) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(CAT3512E_ENGINE)
    '        My.Settings.CAT3512E = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        CAT3512E_ENGINE = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.CAT3512E, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.CAT3512CDM8864) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(CAT3512CDM8864_ENGINE)
    '        My.Settings.CAT3512CDM8864 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        CAT3512CDM8864_ENGINE = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.CAT3512CDM8864, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.CAT3512CDM8865) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(CAT3512CDM8865_ENGINE)
    '        My.Settings.CAT3512CDM8865 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        CAT3512CDM8865_ENGINE = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.CAT3512CDM8865, GetType(Dictionary(Of String, Object)))
    '    End If

    '    If String.IsNullOrEmpty(My.Settings.LiebherrD9620A7) Then
    '        Dim SerializedSettings = Newtonsoft.Json.JsonConvert.SerializeObject(LiebherrD9620A7_ENGINE)
    '        My.Settings.LiebherrD9620A7 = SerializedSettings
    '        My.Settings.Save()
    '    Else
    '        LiebherrD9620A7_ENGINE = Newtonsoft.Json.JsonConvert.DeserializeObject(My.Settings.LiebherrD9620A7, GetType(Dictionary(Of String, Object)))
    '    End If

    'End Sub

    'Public Sub ResetEngines()
    '    My.Settings.CAT3512E = Nothing
    '    My.Settings.CAT3512CDM8864 = Nothing
    '    My.Settings.CAT3512CDM8865 = Nothing
    '    My.Settings.LiebherrD9620A7 = Nothing

    '    My.Settings.Save()
    'End Sub

    Public Sub Update(ByRef ISystem As IFracSystem)
        FracSystem = ISystem
    End Sub

    ''' <summary>
    ''' Funcion para inicializar el motor.
    ''' </summary>
    Private Sub InitEngine() Handles Me.EngineChange

        SelectedEngine = FracSystem.GetEngine(Engine)
        'Select Case Engine
        '    Case "Liebherr D9620A7"
        '        'SelectedEngine = New Dictionary(Of String, Object)(LiebherrD9620A7_ENGINE)

        '    Case "CAT3512C-DM8864"
        '        'SelectedEngine = New Dictionary(Of String, Object)(CAT3512CDM8864_ENGINE)
        '    Case "CAT3512C-DM8865"
        '        'SelectedEngine = New Dictionary(Of String, Object)(CAT3512CDM8865_ENGINE)
        '    Case "CAT3512E"
        '        'SelectedEngine = New Dictionary(Of String, Object)(CAT3512E_ENGINE)
        '    Case Else
        '        SelectedEngine = Nothing
        'End Select

        For i = 1 To 7
            SelectedEngine.Add("PresionMax" + (i).ToString(), 40.8 * SelectedEngine("EfficientHP") * 1.2 / (1300 * PumpSelect.SelectedPump("rGalRev") / (42 * TransSelect.SelectedTransmisison("RelacionGear" + i.ToString()) * PumpSelect.SelectedPump("rTransBomba"))))
            ''VER SI HAY QUE CAMBIAR EN LA LINEA DE ACA ARRIBA DE 1300 POR MIN RPM
        Next
    End Sub

    ''' <summary>
    ''' Funcion para saber el motor del fracturador.
    ''' </summary>
    Public Function GetEngine(ByVal Frac As Integer) As String

        Return FracSystem.GetFracEngine(Frac.ToString()) 'FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentMotorStartAddress"), 1, Frac)
        'Select Case CurrentMotor'CurrentMotor(0)                                                ''ESTO TIENE Q VOLVER A LO DE ANTES, ES PARA PROBAR NOMA
        '    Case 3
        '        Return "Liebherr D9620A7"
        '    Case 7
        '        Return "CAT3512C-DM8864"
        '    Case 8
        '        Return "CAT3512C-DM8865"
        '    Case 4
        '        Return "CAT3512E"
        '    Case Else
        '        Return "CAT3512E"
        'End Select
    End Function

    Public Function GetEngineFromFrac(ByVal Frac As Integer) As String
        Select Case FracSystem.MoxaRead(FracMoxaReceiveStartAddress("CurrentMotorStartAddress"), 1, Frac)(0)
            Case 1
                Return "Liebherr D9620A7"
            Case 2
                Return "CAT3512C-DM8864"
            Case 3
                Return "CAT3512C-DM8865"
            Case 4
                Return "CAT3512E"
            Case Else
                Return ""
        End Select
    End Function

End Class
