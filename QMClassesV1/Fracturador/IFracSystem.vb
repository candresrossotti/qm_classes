﻿Imports System.Net.NetworkInformation

Public Class IFracSystem
    Inherits OneFracConfig

    Private Shared ReadOnly devicePORT As Integer = 502
    Private Shared deviceIP As String

    Public Shared Event InitializationFinished()
    Public Shared Event OperationStart()
    Public Shared Event ScanStep()

#Region "Forms"
    Private Shared Inicio As Object
    Private Shared Principal As Form
    Private Shared Secundaria As Form
    Private Shared Tercera As Form
    Private Shared FracSelect As Form
    Private Shared AjustesFracturador As Form
    Private Shared FracErrores As Form
    Private Shared SetFC As Form
    Private Shared SetGEAR As Form
    Private Shared SetRPM As Form
    Private Shared SetTRIP As Form
    Private Shared SetZERO As Form
    Private Shared AdminControl As Form
    Private Shared NewPassword As Form
    Private Shared FracMantenimiento As Form
    Private Shared FracDiagnostico As Form
    Private Shared TesteoLinea As Form
    Private Shared InfoShow As Form
    Private Shared FullAutomation As Form
    Private Shared ConfigFrac As Form
    Private Shared AddFrac As Form
    Private Shared EditFrac As Form

    Private Shared Container1 As Object
    Private Shared Container2 As Object
    Private Shared Container4 As Object
    Private Shared Container8 As Object
    Private Shared Container1_2 As Object
    Private Shared Container2_2 As Object
    Private Shared Container4_2 As Object
    Private Shared Container8_2 As Object
    Private Shared Container1_3 As Object
    Private Shared Container2_3 As Object
    Private Shared Container4_3 As Object
    Private Shared Container8_3 As Object

    Private Shared Ajustes As Object
    Private Shared SerialPortConfig As Object
    Private Shared ConfigWITS As Object
#End Region

    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      PROPIEDADES PARA TODA LA CLASE     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

#Region "Properties"
    Private Shared _activeop As Boolean
    Public Shared Property ActiveOP() As Boolean
        Get
            Return _activeop
        End Get
        Set(value As Boolean)
            If value <> _activeop Then
                _activeop = value
                RaiseEvent OperationStart()
            End If
        End Set
    End Property
    Private Shared _totalfracs As Integer = 24
    Private Shared Property TotalFracs() As Integer
        Get
            Return _totalfracs
        End Get
        Set(value As Integer)
            If value <> _totalfracs Then
                _totalfracs = value
            End If
        End Set
    End Property
    Private Shared _msocket As ModbusClient() = Nothing
    Private Shared Property MSocket() As ModbusClient()
        Get
            Return _msocket
        End Get
        Set(value As ModbusClient())
            If value IsNot _msocket Then
                _msocket = value
            End If
        End Set
    End Property
    Private Shared _ipscanstartaddress As Integer = 101
    Public Shared Property IPScanStartAddress As Integer
        Get
            Return _ipscanstartaddress
        End Get
        Set(value As Integer)
            If value <> _ipscanstartaddress Then
                _ipscanstartaddress = value
            End If
        End Set
    End Property
    Private Shared _ipscanendaddress As Integer = _ipscanstartaddress + TotalFracs - 1
    Public Shared Property IPScanEndAddress As Integer
        Get
            Return _ipscanendaddress
        End Get
        Set(value As Integer)
            If value <> _ipscanendaddress Then
                _ipscanendaddress = value
            End If
        End Set
    End Property
    Private Shared _devices As Integer = 0
    Public Property Devices() As Integer
        Get
            Return _devices
        End Get
        Set(value As Integer)
            If value <> _devices Then
                _devices = value
            End If
        End Set
    End Property
#End Region

#Region "Constructor"
    ''---Singleton---
    Private Shared IFracSystem

    ''Devuelve una instancia unica de IFracSystem - Funciona como Singleton 
    Public Shared Function GetInstancia()
        If IFracSystem Is Nothing Then
            IFracSystem = New IFracSystem()
        End If
        Return IFracSystem
    End Function
    Private Sub New()
        ReDim MSocket(TotalFracs - 1)
    End Sub
    Public Shared Function GetInstancia(ByRef InicioF As Form, ByRef AjustesF As Form, ByRef AjustesFracturadorF As Form, ByRef AdminControlF As Form,
                   ByRef NewPasswordF As Form, ByRef FracDiagnosticoF As Form, ByRef FracMantenimientoF As Form, ByRef FracErroresF As Form,
                   ByRef FullAutomationF As Form, ByRef InfoShowF As Form, ByRef TesteoLineaF As Form, ByRef SetFCF As Form, ByRef SetTRIPF As Form,
                   ByRef SetZEROF As Form, ByRef FracSelectF As Form, ByRef Container1F As Form, ByRef SetGEARF As Form, ByRef SetRPMF As Form,
                    ByRef Container2F As Form, ByRef Container4F As Form, ByRef Container8F As Form,
                   ByRef PrincipalF As Form, ByRef SecundariaF As Form, ByRef TerceraF As Form, ByRef ConfigWITSF As Form,
                   ByRef SerialPortConfigF As Form, ByRef ConfigFracF As Form, ByRef AddFracF As Form, ByRef EditFracF As Form)

        Inicio = InicioF
        Principal = PrincipalF
        Secundaria = SecundariaF
        Tercera = TerceraF
        FracSelect = FracSelectF
        AjustesFracturador = AjustesFracturadorF
        FracErrores = FracErroresF
        SetFC = SetFCF
        SetGEAR = SetGEARF
        SetRPM = SetRPMF
        SetTRIP = SetTRIPF
        SetZERO = SetZEROF
        AdminControl = AdminControlF
        NewPassword = NewPasswordF
        FracMantenimiento = FracMantenimientoF
        FracDiagnostico = FracDiagnosticoF
        TesteoLinea = TesteoLineaF
        InfoShow = InfoShowF
        FullAutomation = FullAutomationF
        ConfigFrac = ConfigFracF
        AddFrac = AddFracF
        EditFrac = EditFracF

        Container1 = Container1F
        Container2 = Container2F
        Container4 = Container4F
        Container8 = Container8F



        Ajustes = AjustesF

        SerialPortConfig = SerialPortConfigF
        ConfigWITS = ConfigWITSF


        If IFracSystem Is Nothing Then
            IFracSystem = New IFracSystem()
        End If
        Return IFracSystem
    End Function
#End Region


    ''' <summary>
    ''' Devuelve el objecto seleccionado.
    ''' </summary>
    Public Function GetObject(ByVal Obj As String) As Object
        Select Case Obj
            Case "Inicio"
                Return Inicio
            Case "Principal"
                Return Principal
            Case "Secundaria"
                Return Secundaria
            Case "Tercera"
                Return Tercera
            Case "FracSelect"
                Return FracSelect
            Case "AjustesFracturador"
                Return AjustesFracturador
            Case "FracErrores"
                Return FracErrores
            Case "SetFC"
                Return SetFC
            Case "SetGEAR"
                Return SetGEAR
            Case "SetRPM"
                Return SetRPM
            Case "SetTRIP"
                Return SetTRIP
            Case "SetZERO"
                Return SetZERO
            Case "AdminControl"
                Return AdminControl
            Case "NewPassword"
                Return NewPassword
            Case "FracMantenimiento"
                Return FracMantenimiento
            Case "FracDiagnostico"
                Return FracDiagnostico
            Case "TesteoLinea"
                Return TesteoLinea
            Case "InfoShow"
                Return InfoShow
            Case "FullAutomation"
                Return FullAutomation
            Case "Container1"
                Return Container1
            Case "Container2"
                Return Container2
            Case "Container4"
                Return Container4
            Case "Container8"
                Return Container8
            Case "Container1_2"
                Return Container1_2
            Case "Container2_2"
                Return Container2_2
            Case "Container4_2"
                Return Container4_2
            Case "Container8_2"
                Return Container8_2
            Case "Container1_3"
                Return Container1_3
            Case "Container2_3"
                Return Container2_3
            Case "Container4_3"
                Return Container4_3
            Case "Container8_3"
                Return Container8_3
            Case "Ajustes"
                Return Ajustes
            Case "SerialPortConfig"
                Return SerialPortConfig
            Case "ConfigWITS"
                Return ConfigWITS
            Case "ConfigFrac"
                Return ConfigFrac
            Case "AddFrac"
                Return AddFrac
            Case "EditFrac"
                Return EditFrac
            Case Else
                Return Nothing
        End Select
    End Function

    ''' <summary>
    ''' Devuelve un clon del objeto IFracSystem.
    ''' </summary>
    Public Function GetClone() As Object
        Return MemberwiseClone()
    End Function


    ''' <summary>
    ''' Actualizar referencia al form.
    ''' </summary>
    Public Sub UpdateISystem(ByRef CurrentForm As Object, Optional CurrentScreen As Integer = 0)
        If CurrentScreen = 0 Then
            Select Case CurrentForm.Name
                Case "Inicio"
                    Inicio = CurrentForm
                Case "Principal_1280x840"
                    Principal = CurrentForm
                Case "Principal_1920x1080"
                    Principal = CurrentForm
                Case "Secundaria_1280x840"
                    Secundaria = CurrentForm
                Case "Secundaria_1920x1080"
                    Secundaria = CurrentForm
                Case "Tercera_1280x840"
                    Tercera = CurrentForm
                Case "Tercera_1920x1080"
                    Tercera = CurrentForm
                Case "FracSelect"
                    FracSelect = CurrentForm
                Case "AjustesFracturador"
                    AjustesFracturador = CurrentForm
                Case "FracErrores"
                    FracErrores = CurrentForm
                Case "SetFC"
                    SetFC = CurrentForm
                Case "SetGEAR"
                    SetGEAR = CurrentForm
                Case "SetRPM"
                    SetRPM = CurrentForm
                Case "SetTRIP"
                    SetTRIP = CurrentForm
                Case "SetZERO"
                    SetZERO = CurrentForm
                Case "AdminControl"
                    AdminControl = CurrentForm
                Case "NewPassword"
                    NewPassword = CurrentForm
                Case "FracMantenimiento"
                    FracMantenimiento = CurrentForm
                Case "FracDiagnostico"
                    FracDiagnostico = CurrentForm
                Case "TesteoLinea"
                    TesteoLinea = CurrentForm
                Case "InfoShow"
                    InfoShow = CurrentForm
                Case "FullAutomation"
                    FullAutomation = CurrentForm
                Case "Container1_1280x840"
                    Container1 = CurrentForm
                Case "Container2_1280x840"
                    Container2 = CurrentForm
                Case "Container4_1280x840"
                    Container4 = CurrentForm
                Case "Container8_1280x840"
                    Container8 = CurrentForm
                Case "Container1_1920x1080"
                    Container1 = CurrentForm
                Case "Container2_1920x1080"
                    Container2 = CurrentForm
                Case "Container4_1920x1080"
                    Container4 = CurrentForm
                Case "Container8_1920x1080"
                    Container8 = CurrentForm
                Case "SerialPortConfig"
                    SerialPortConfig = CurrentForm
                Case "ConfigWITS"
                    ConfigWITS = CurrentForm
            End Select

        ElseIf CurrentScreen = 1 Then

            Select Case CurrentForm.Name
                Case "Container1_1280x840"
                    Container1_2 = CurrentForm
                Case "Container2_1280x840"
                    Container2_2 = CurrentForm
                Case "Container4_1280x840"
                    Container4_2 = CurrentForm
                Case "Container8_1280x840"
                    Container8_2 = CurrentForm
                Case "Container1_1920x1080"
                    Container1_2 = CurrentForm
                Case "Container2_1920x1080"
                    Container2_2 = CurrentForm
                Case "Container4_1920x1080"
                    Container4_2 = CurrentForm
                Case "Container8_1920x1080"
                    Container8_2 = CurrentForm
            End Select

        ElseIf CurrentScreen = 2 Then
            Select Case CurrentForm.Name
                Case "Container1_1280x840"
                    Container1_3 = CurrentForm
                Case "Container2_1280x840"
                    Container2_3 = CurrentForm
                Case "Container4_1280x840"
                    Container4_3 = CurrentForm
                Case "Container8_1280x840"
                    Container8_3 = CurrentForm
                Case "Container1_1920x1080"
                    Container1_3 = CurrentForm
                Case "Container2_1920x1080"
                    Container2_3 = CurrentForm
                Case "Container4_1920x1080"
                    Container4_3 = CurrentForm
                Case "Container8_1920x1080"
                    Container8_3 = CurrentForm
            End Select
        End If
    End Sub


    ''----------------------------------------------------------------------------------------------------------''
    ''-------------------------------      FUNCIONES DE CONEXION/DESCONEXION     -------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para escanear fracturadores en un rango de IPs
    ''' </summary>
    Public Function AutoScan(ByVal Online As Boolean) As Boolean
        Try
            If DBConnect(Online) Then
                Devices = 0

                For i = IPScanStartAddress To IPScanEndAddress      ''17 a 24  ACA VA IPSCANENDADDRESS, loCAMBIO PARA TENARIS
                    deviceIP = "192.168.2." + i.ToString()
                    Dim PingS As New Ping
                    If PingS.Send(deviceIP, 100).Status = IPStatus.Success Then
                        For Each Pn As Panel In FracSelect.Controls.Find("MainPanel", True)(0).Controls.OfType(Of Panel)
                            If Pn.Tag = Devices.ToString() Then
                                Pn.Show()
                                Pn.Controls.Find("BtnFrac" + (Pn.Tag + 1).ToString(), True)(0).Text = GetFracName(deviceIP)
                                Pn.Controls.Find("LblFrac" + (Pn.Tag + 1).ToString() + "Engine", True)(0).Text = GetFSEngineByFrac(GetFracID(GetFracName(deviceIP)))
                                Pn.Controls.Find("LblFrac" + (Pn.Tag + 1).ToString() + "Trans", True)(0).Text = GetFSTransByFrac(GetFracID(GetFracName(deviceIP)))
                                Pn.Controls.Find("LblFrac" + (Pn.Tag + 1).ToString() + "Pump", True)(0).Text = GetFSPumpByFrac(GetFracID(GetFracName(deviceIP)))
                                SetFracID(GetFracID(GetFracName(deviceIP)), Devices)
                            End If
                        Next
                        Devices += 1
                    End If
                    RaiseEvent ScanStep()
                Next

                If Devices > 10 Then
                    FracSelect.Width = 810
                    With TryCast(FracSelect.Controls.Find("MainPanel", True)(0), Guna.UI2.WinForms.Guna2Panel)
                        .Width = 798
                        .AutoScroll = False
                        .VerticalScroll.Maximum = (Math.Ceiling(Devices / 2) * 99.4)
                        .VerticalScroll.Minimum = 0
                        .VerticalScroll.Enabled = True
                        .VerticalScroll.Visible = True
                        .AutoScroll = True
                    End With
                    With FracSelect.Controls.Find("TitlePanel", True)(0)
                        .Width = 810
                        .Left = 0
                        .Top = 0
                    End With
                ElseIf Devices = 0 Then
                    Return False
                End If

                RaiseEvent InitializationFinished()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogError(ex)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Funcion para checkear conexion a un fracturador.
    ''' </summary>
    Public Function CheckConnection(ByVal Frac As Integer) As Boolean
        Try
            Return MSocket(Frac).Available(1000)
        Catch ex As Exception
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Funcion para conectarse a un fracturador.
    ''' </summary>
    ''' <param name="FracIP">IP del Fracturador.</param>
    ''' <param name="FracID">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function ManualConnect(ByVal FracIP As String, ByVal FracID As Integer) As Boolean
        Dim state As Boolean

        Try
            MSocket(FracID) = New ModbusClient With {
                .ConnectionTimeout = 3000,
                .IPAddress = FracIP,
                .Port = devicePORT,
                .NumberOfRetries = 3
            }
            MSocket(FracID).Connect()
            If MSocket(FracID).Connected Then
                MoxaSend(FracMoxaSendStartAddress("ActiveFrac_StartAddress"), 1, FracID)
            End If
            Return MSocket(FracID).Connected
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = "Se produjo un error al intentar conectarse al fracturador."         ''PASAR ESTO A LSELECT
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            state = False
            Return state
        End Try
    End Function

    ''' <summary>
    ''' Funcion para conectarse a un fracturador.
    ''' </summary>
    ''' <param name="Frac">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function Connect(ByVal Frac As Integer) As Boolean
        Dim state As Boolean

        deviceIP = GetFracIP(Frac)

        Try
            MSocket(Frac) = New ModbusClient With {
                .ConnectionTimeout = 3000,
                .IPAddress = deviceIP,
                .Port = devicePORT,
                .NumberOfRetries = 3
            }
            MSocket(Frac).Connect()
            If MSocket(Frac).Connected Then
                MoxaSend(FracMoxaSendStartAddress("ActiveFrac_StartAddress"), 1, Frac)
            End If
            Return MSocket(Frac).Connected
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = "Se produjo un error al intentar conectarse al fracturador."         ''PASAR ESTO A LSELECT
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            state = False
            Return state
        End Try
    End Function



    ''' <summary>
    ''' Funcion para desconectarse de un fracturador.
    ''' </summary>
    Public Sub Disconnect(ByVal Frac As Integer)
        Try
            If (MSocket(Frac).Connected) Then
                MoxaSend(FracMoxaSendStartAddress("ActiveFrac_StartAddress"), 0, Frac)
                MSocket(Frac).Disconnect()                                              ''ESTE DISCONNECT PUEDE GENERAR PROBLEMAS
            End If
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error al desconectarse. Intente en un momento"
            Dim AG As New AvisoGeneral
            AG.ShowDialog()
        End Try
    End Sub

    ''' <summary>
    ''' Funcion para desconectarse de todos los fracturadores.
    ''' </summary>
    Public Sub DisconnectAll()
        Try
            For i = 0 To TotalFracs - 1
                If MSocket(i) IsNot Nothing Then
                    If (MSocket(i).Connected) Then
                        MSocket(i).Disconnect()                             ''ESTE DISCONNECT PUEDE GENERAR PROBLEMAS
                    End If
                End If
            Next
            DeleteCID()
            DBDisconnect()
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error al desconectarse. Intente en un momento"
            Dim AG As New AvisoGeneral
            AG.ShowDialog()
        End Try
    End Sub
    ''----------------------------------------------------------------------------------------------------------''
    ''---------------------------------      FUNCIONES DE ESCRITURA/LECTURA     --------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Funcion para enviar datos al moxa.
    ''' </summary>
    ''' <param name="startAddressTX">Address inicial donde se van a escribir los datos.</param>
    ''' <param name="data">Datos a escribir.</param>
    ''' <param name="socket">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Sub MoxaSend(startAddressTX As Integer, data As Integer, socket As Integer)
        Try
            MSocket(socket).WriteSingleRegister(startAddressTX, data)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
    End Sub

    ''' <summary>
    ''' Funcion para leer datos del moxa.
    ''' </summary>
    ''' <param name="startAddressRX">Address inicial donde se van a leer los datos.</param>
    ''' <param name="numberOfPoints">Cantidad de datos a leer.</param>
    ''' <param name="socket">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Function MoxaRead(startAddressRX As Integer, numberOfPoints As Integer, socket As Integer) As Integer()
        Try
            Return MSocket(socket).ReadHoldingRegisters(startAddressRX, numberOfPoints)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
        Return {0}
    End Function

    ''' <summary>
    ''' Funcion para escribir multiples datos al moxa.
    ''' </summary>
    ''' <param name="startAddressTX">Address inicial donde se van a escribir los datos.</param>
    ''' <param name="data">Datos a escribir.</param>
    ''' <param name="socket">ID del Fracturador.</param>
    '''  <remarks></remarks>
    Public Sub MoxaSendMultiple(startAddressTX As Integer, data As Integer(), socket As Integer)
        Try
            MSocket(socket).WriteMultipleRegisters(startAddressTX, data)
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
        End Try
    End Sub



    ''----------------------------------------------------------------------------------------------------------''
    ''--------------------------------      FUNCIONES PARA EL INICIO DEL FRAC     ------------------------------''
    ''----------------------------------------------------------------------------------------------------------''

    ''' <summary>
    ''' Cuando se realizo el auto scaneo configuro el resto de cosas.
    ''' </summary>
    Private Sub InitializationFlag() Handles Me.InitializationFinished
        Update(Me)
    End Sub


    ''' <summary>
    ''' Funcion para inicializar el fracturador.
    ''' </summary>
    Public Sub InitializeSystem(ByVal CurrentFrac As Integer)
        InitFrac(CurrentFrac)
    End Sub

End Class
