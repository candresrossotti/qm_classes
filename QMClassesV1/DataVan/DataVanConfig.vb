﻿Imports System.Threading
Public Class DataVanConfig
    Inherits OneConfig

    Public Shared Event ConfigComplete()

    Public SettingsRM As New Dictionary(Of String, Integer)() From {
        {"SensorA1_X0", 0},
        {"SensorA1_X1", 0},
        {"SensorA1_Y0", 0},
        {"SensorA1_Y1", 0},
        {"SensorA1_SET", 0},
        {"SensorA1_CAP", 0},
        {"SensorA1_CAPMIN", 0},
        {"SensorA1_CAPMAX", 0},
        {"SensorA1Filter_EN", 0},
        {"SensorA1Filter_TOL", 0},
        {"SensorA1Filter_TSMPL", 0},
        {"SensorA1Filter_NSMPL", 0},
        {"SensorA2_X0", 0},
        {"SensorA2_X1", 0},
        {"SensorA2_Y0", 0},
        {"SensorA2_Y1", 0},
        {"SensorA2_SET", 0},
        {"SensorA2_CAP", 0},
        {"SensorA2_CAPMIN", 0},
        {"SensorA2_CAPMAX", 0},
        {"SensorA2Filter_EN", 0},
        {"SensorA2Filter_TOL", 0},
        {"SensorA2Filter_TSMPL", 0},
        {"SensorA2Filter_NSMPL", 0},
        {"SensorA3_X0", 0},
        {"SensorA3_X1", 0},
        {"SensorA3_Y0", 0},
        {"SensorA3_Y1", 0},
        {"SensorA3_SET", 0},
        {"SensorA3_CAP", 0},
        {"SensorA3_CAPMIN", 0},
        {"SensorA3_CAPMAX", 0},
        {"SensorA3Filter_EN", 0},
        {"SensorA3Filter_TOL", 0},
        {"SensorA3Filter_TSMPL", 0},
        {"SensorA3Filter_NSMPL", 0},
        {"SensorA4_X0", 0},
        {"SensorA4_X1", 0},
        {"SensorA4_Y0", 0},
        {"SensorA4_Y1", 0},
        {"SensorA4_SET", 0},
        {"SensorA4_CAP", 0},
        {"SensorA4_CAPMIN", 0},
        {"SensorA4_CAPMAX", 0},
        {"SensorA4Filter_EN", 0},
        {"SensorA4Filter_TOL", 0},
        {"SensorA4Filter_TSMPL", 0},
        {"SensorA4Filter_NSMPL", 0},
        {"SensorA5_X0", 0},
        {"SensorA5_X1", 0},
        {"SensorA5_Y0", 0},
        {"SensorA5_Y1", 0},
        {"SensorA5_SET", 0},
        {"SensorA5_CAP", 0},
        {"SensorA5_CAPMIN", 0},
        {"SensorA5_CAPMAX", 0},
        {"SensorA5Filter_EN", 0},
        {"SensorA5Filter_TOL", 0},
        {"SensorA5Filter_TSMPL", 0},
        {"SensorA5Filter_NSMPL", 0},
        {"SensorF1_DIENTES", 0},
        {"SensorF1_OFFSET", 0},
        {"SensorF1_CAP", 0},
        {"SensorF1_CAPMIN", 0},
        {"SensorF1_CAPMAX", 0},
        {"SensorF1Filter_EN", 0},
        {"SensorF1Filter_TOL", 0},
        {"SensorF1Filter_TSMPL", 0},
        {"SensorF1Filter_NSMPL", 0},
        {"SensorF2_DIENTES", 0},
        {"SensorF2_OFFSET", 0},
        {"SensorF2_CAP", 0},
        {"SensorF2_CAPMIN", 0},
        {"SensorF2_CAPMAX", 0},
        {"SensorF2Filter_EN", 0},
        {"SensorF2Filter_TOL", 0},
        {"SensorF2Filter_TSMPL", 0},
        {"SensorF2Filter_NSMPL", 0},
        {"SensorF3_DIENTES", 0},
        {"SensorF3_OFFSET", 0},
        {"SensorF3_CAP", 0},
        {"SensorF3_CAPMIN", 0},
        {"SensorF3_CAPMAX", 0},
        {"SensorF3Filter_EN", 0},
        {"SensorF3Filter_TOL", 0},
        {"SensorF3Filter_TSMPL", 0},
        {"SensorF3Filter_NSMPL", 0},
        {"SensorF4_DIENTES", 0},
        {"SensorF4_OFFSET", 0},
        {"SensorF4_CAP", 0},
        {"SensorF4_CAPMIN", 0},
        {"SensorF4_CAPMAX", 0},
        {"SensorF4Filter_EN", 0},
        {"SensorF4Filter_TOL", 0},
        {"SensorF4Filter_TSMPL", 0},
        {"SensorF4Filter_NSMPL", 0},
        {"SensorF5_DIENTES", 0},
        {"SensorF5_OFFSET", 0},
        {"SensorF5_CAP", 0},
        {"SensorF5_CAPMIN", 0},
        {"SensorF5_CAPMAX", 0},
        {"SensorF5Filter_EN", 0},
        {"SensorF5Filter_TOL", 0},
        {"SensorF5Filter_TSMPL", 0},
        {"SensorF5Filter_NSMPL", 0},
        {"SensorDL1_X0", 0},
        {"SensorDL1_X1", 0},
        {"SensorDL1_Y0", 0},
        {"SensorDL1_Y1", 0},
        {"SensorDL1_SET", 0},
        {"SensorDL1_CAP", 0},
        {"SensorDL1_CAPMIN", 0},
        {"SensorDL1_CAPMAX", 0},
        {"SensorDL1Filter_EN", 0},
        {"SensorDL1Filter_TOL", 0},
        {"SensorDL1Filter_TSMPL", 0},
        {"SensorDL1Filter_NSMPL", 0},
        {"SensorDL2_X0", 0},
        {"SensorDL2_X1", 0},
        {"SensorDL2_Y0", 0},
        {"SensorDL2_Y1", 0},
        {"SensorDL2_SET", 0},
        {"SensorDL2_CAP", 0},
        {"SensorDL2_CAPMIN", 0},
        {"SensorDL2_CAPMAX", 0},
        {"SensorDL2Filter_EN", 0},
        {"SensorDL2Filter_TOL", 0},
        {"SensorDL2Filter_TSMPL", 0},
        {"SensorDL2Filter_NSMPL", 0},
        {"Multiplicador_SensorA1", 0},
        {"Multiplicador_SensorA2", 0},
        {"Multiplicador_SensorA3", 0},
        {"Multiplicador_SensorA4", 0},
        {"Multiplicador_SensorA5", 0},
        {"Multiplicador_SensorF1", 0},
        {"Multiplicador_SensorF2", 0},
        {"Multiplicador_SensorF3", 0},
        {"Multiplicador_SensorF4", 0},
        {"Multiplicador_SensorF5", 0},
        {"Multiplicador_SensorDL1", 0},
        {"Multiplicador_SensorDL2", 0}
    }

    ''' <summary>
    ''' Funcion para leer la configuracion del equipo.
    ''' </summary>
    Public Sub UpdateSettings()
        Dim UpdateTask As New Task(AddressOf Update)
        UpdateTask.Start()
    End Sub

    ''' <summary>
    ''' Funcion para actualizar la configuracion.
    ''' </summary>
    Private Async Sub Update()
        Try
            MoxaSend(DVMoxaSendStartAddress("GetSettings_StartAddress"), 1)
            Await Task.Delay(15000)
            MoxaSend(DVMoxaSendStartAddress("GetSettings_StartAddress"), 0)
            Dim Result As Integer() = MoxaRead(12, 120)
            If Result.Length() = 120 Then

                SettingsRM("SensorA1_X0") = Result(DVMoxaReceiveStartAddress("SensorA1_X0_StartAddress") - 12)
                SettingsRM("SensorA1_X1") = Result(DVMoxaReceiveStartAddress("SensorA1_X1_StartAddress") - 12)
                SettingsRM("SensorA1_Y0") = Result(DVMoxaReceiveStartAddress("SensorA1_Y0_StartAddress") - 12)
                SettingsRM("SensorA1_Y1") = Result(DVMoxaReceiveStartAddress("SensorA1_Y1_StartAddress") - 12)
                SettingsRM("SensorA1_SET") = Result(DVMoxaReceiveStartAddress("SensorA1_SET_StartAddress") - 12)
                SettingsRM("SensorA1_CAP") = Result(DVMoxaReceiveStartAddress("SensorA1_CAP_StartAddress") - 12)
                SettingsRM("SensorA1_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorA1_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorA1_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorA1_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorA1Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorA1Filter_EN_StartAddress") - 12)
                SettingsRM("SensorA1Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorA1Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorA1Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorA1Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorA1Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorA1Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorA2_X0") = Result(DVMoxaReceiveStartAddress("SensorA2_X0_StartAddress") - 12)
                SettingsRM("SensorA2_X1") = Result(DVMoxaReceiveStartAddress("SensorA2_X1_StartAddress") - 12)
                SettingsRM("SensorA2_Y0") = Result(DVMoxaReceiveStartAddress("SensorA2_Y0_StartAddress") - 12)
                SettingsRM("SensorA2_Y1") = Result(DVMoxaReceiveStartAddress("SensorA2_Y1_StartAddress") - 12)
                SettingsRM("SensorA2_SET") = Result(DVMoxaReceiveStartAddress("SensorA2_SET_StartAddress") - 12)
                SettingsRM("SensorA2_CAP") = Result(DVMoxaReceiveStartAddress("SensorA2_CAP_StartAddress") - 12)
                SettingsRM("SensorA2_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorA2_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorA2_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorA2_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorA2Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorA2Filter_EN_StartAddress") - 12)
                SettingsRM("SensorA2Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorA2Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorA2Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorA2Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorA2Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorA2Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorA3_X0") = Result(DVMoxaReceiveStartAddress("SensorA3_X0_StartAddress") - 12)
                SettingsRM("SensorA3_X1") = Result(DVMoxaReceiveStartAddress("SensorA3_X1_StartAddress") - 12)
                SettingsRM("SensorA3_Y0") = Result(DVMoxaReceiveStartAddress("SensorA3_Y0_StartAddress") - 12)
                SettingsRM("SensorA3_Y1") = Result(DVMoxaReceiveStartAddress("SensorA3_Y1_StartAddress") - 12)
                SettingsRM("SensorA3_SET") = Result(DVMoxaReceiveStartAddress("SensorA3_SET_StartAddress") - 12)
                SettingsRM("SensorA3_CAP") = Result(DVMoxaReceiveStartAddress("SensorA3_CAP_StartAddress") - 12)
                SettingsRM("SensorA3_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorA3_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorA3_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorA3_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorA3Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorA3Filter_EN_StartAddress") - 12)
                SettingsRM("SensorA3Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorA3Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorA3Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorA3Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorA3Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorA3Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorA4_X0") = Result(DVMoxaReceiveStartAddress("SensorA4_X0_StartAddress") - 12)
                SettingsRM("SensorA4_X1") = Result(DVMoxaReceiveStartAddress("SensorA4_X1_StartAddress") - 12)
                SettingsRM("SensorA4_Y0") = Result(DVMoxaReceiveStartAddress("SensorA4_Y0_StartAddress") - 12)
                SettingsRM("SensorA4_Y1") = Result(DVMoxaReceiveStartAddress("SensorA4_Y1_StartAddress") - 12)
                SettingsRM("SensorA4_SET") = Result(DVMoxaReceiveStartAddress("SensorA4_SET_StartAddress") - 12)
                SettingsRM("SensorA4_CAP") = Result(DVMoxaReceiveStartAddress("SensorA4_CAP_StartAddress") - 12)
                SettingsRM("SensorA4_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorA4_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorA4_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorA4_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorA4Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorA4Filter_EN_StartAddress") - 12)
                SettingsRM("SensorA4Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorA4Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorA4Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorA4Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorA4Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorA4Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorA5_X0") = Result(DVMoxaReceiveStartAddress("SensorA5_X0_StartAddress") - 12)
                SettingsRM("SensorA5_X1") = Result(DVMoxaReceiveStartAddress("SensorA5_X1_StartAddress") - 12)
                SettingsRM("SensorA5_Y0") = Result(DVMoxaReceiveStartAddress("SensorA5_Y0_StartAddress") - 12)
                SettingsRM("SensorA5_Y1") = Result(DVMoxaReceiveStartAddress("SensorA5_Y1_StartAddress") - 12)
                SettingsRM("SensorA5_SET") = Result(DVMoxaReceiveStartAddress("SensorA5_SET_StartAddress") - 12)
                SettingsRM("SensorA5_CAP") = Result(DVMoxaReceiveStartAddress("SensorA5_CAP_StartAddress") - 12)
                SettingsRM("SensorA5_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorA5_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorA5_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorA5_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorA5Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorA5Filter_EN_StartAddress") - 12)
                SettingsRM("SensorA5Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorA5Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorA5Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorA5Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorA5Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorA5Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorF1_DIENTES") = Result(DVMoxaReceiveStartAddress("SensorF1_DIENTES_StartAddress") - 12)
                SettingsRM("SensorF1_OFFSET") = Result(DVMoxaReceiveStartAddress("SensorF1_OFFSET_StartAddress") - 12)
                SettingsRM("SensorF1_CAP") = Result(DVMoxaReceiveStartAddress("SensorF1_CAP_StartAddress") - 12)
                SettingsRM("SensorF1_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorF1_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorF1_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorF1_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorF1Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorF1Filter_EN_StartAddress") - 12)
                SettingsRM("SensorF1Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorF1Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorF1Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorF1Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorF1Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorF1Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorF2_DIENTES") = Result(DVMoxaReceiveStartAddress("SensorF2_DIENTES_StartAddress") - 12)
                SettingsRM("SensorF2_OFFSET") = Result(DVMoxaReceiveStartAddress("SensorF2_OFFSET_StartAddress") - 12)
                SettingsRM("SensorF2_CAP") = Result(DVMoxaReceiveStartAddress("SensorF2_CAP_StartAddress") - 12)
                SettingsRM("SensorF2_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorF2_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorF2_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorF2_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorF2Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorF2Filter_EN_StartAddress") - 12)
                SettingsRM("SensorF2Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorF2Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorF2Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorF2Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorF2Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorF2Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorF3_DIENTES") = Result(DVMoxaReceiveStartAddress("SensorF3_DIENTES_StartAddress") - 12)
                SettingsRM("SensorF3_OFFSET") = Result(DVMoxaReceiveStartAddress("SensorF3_OFFSET_StartAddress") - 12)
                SettingsRM("SensorF3_CAP") = Result(DVMoxaReceiveStartAddress("SensorF3_CAP_StartAddress") - 12)
                SettingsRM("SensorF3_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorF3_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorF3_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorF3_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorF3Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorF3Filter_EN_StartAddress") - 12)
                SettingsRM("SensorF3Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorF3Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorF3Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorF3Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorF3Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorF3Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorF4_DIENTES") = Result(DVMoxaReceiveStartAddress("SensorF4_DIENTES_StartAddress") - 12)
                SettingsRM("SensorF4_OFFSET") = Result(DVMoxaReceiveStartAddress("SensorF4_OFFSET_StartAddress") - 12)
                SettingsRM("SensorF4_CAP") = Result(DVMoxaReceiveStartAddress("SensorF4_CAP_StartAddress") - 12)
                SettingsRM("SensorF4_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorF4_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorF4_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorF4_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorF4Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorF4Filter_EN_StartAddress") - 12)
                SettingsRM("SensorF4Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorF4Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorF4Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorF4Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorF4Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorF4Filter_NSMPL_StartAddress") - 12)

                SettingsRM("SensorF5_DIENTES") = Result(DVMoxaReceiveStartAddress("SensorF5_DIENTES_StartAddress") - 12)
                SettingsRM("SensorF5_OFFSET") = Result(DVMoxaReceiveStartAddress("SensorF5_OFFSET_StartAddress") - 12)
                SettingsRM("SensorF5_CAP") = Result(DVMoxaReceiveStartAddress("SensorF5_CAP_StartAddress") - 12)
                SettingsRM("SensorF5_CAPMIN") = Result(DVMoxaReceiveStartAddress("SensorF5_CAPMIN_StartAddress") - 12)
                SettingsRM("SensorF5_CAPMAX") = Result(DVMoxaReceiveStartAddress("SensorF5_CAPMAX_StartAddress") - 12)
                SettingsRM("SensorF5Filter_EN") = Result(DVMoxaReceiveStartAddress("SensorF5Filter_EN_StartAddress") - 12)
                SettingsRM("SensorF5Filter_TOL") = Result(DVMoxaReceiveStartAddress("SensorF5Filter_TOL_StartAddress") - 12)
                SettingsRM("SensorF5Filter_TSMPL") = Result(DVMoxaReceiveStartAddress("SensorF5Filter_TSMPL_StartAddress") - 12)
                SettingsRM("SensorF5Filter_NSMPL") = Result(DVMoxaReceiveStartAddress("SensorF5Filter_NSMPL_StartAddress") - 12)


            End If
            Await Task.Delay(2000)
            Dim Result1 As Integer() = MoxaRead(132, 36)
            If Result1.Length() = 36 Then

                SettingsRM("SensorDL1_X0") = Result1(DVMoxaReceiveStartAddress("SensorDL1_X0_StartAddress") - 132)
                SettingsRM("SensorDL1_X1") = Result1(DVMoxaReceiveStartAddress("SensorDL1_X1_StartAddress") - 132)
                SettingsRM("SensorDL1_Y0") = Result1(DVMoxaReceiveStartAddress("SensorDL1_Y0_StartAddress") - 132)
                SettingsRM("SensorDL1_Y1") = Result1(DVMoxaReceiveStartAddress("SensorDL1_Y1_StartAddress") - 132)
                SettingsRM("SensorDL1_SET") = Result1(DVMoxaReceiveStartAddress("SensorDL1_SET_StartAddress") - 132)
                SettingsRM("SensorDL1_CAP") = Result1(DVMoxaReceiveStartAddress("SensorDL1_CAP_StartAddress") - 132)
                SettingsRM("SensorDL1_CAPMIN") = Result1(DVMoxaReceiveStartAddress("SensorDL1_CAPMIN_StartAddress") - 132)
                SettingsRM("SensorDL1_CAPMAX") = Result1(DVMoxaReceiveStartAddress("SensorDL1_CAPMAX_StartAddress") - 132)
                SettingsRM("SensorDL1Filter_EN") = Result1(DVMoxaReceiveStartAddress("SensorDL1Filter_EN_StartAddress") - 132)
                SettingsRM("SensorDL1Filter_TOL") = Result1(DVMoxaReceiveStartAddress("SensorDL1Filter_TOL_StartAddress") - 132)
                SettingsRM("SensorDL1Filter_TSMPL") = Result1(DVMoxaReceiveStartAddress("SensorDL1Filter_TSMPL_StartAddress") - 132)
                SettingsRM("SensorDL1Filter_NSMPL") = Result1(DVMoxaReceiveStartAddress("SensorDL1Filter_NSMPL_StartAddress") - 132)

                SettingsRM("SensorDL2_X0") = Result1(DVMoxaReceiveStartAddress("SensorDL2_X0_StartAddress") - 132)
                SettingsRM("SensorDL2_X1") = Result1(DVMoxaReceiveStartAddress("SensorDL2_X1_StartAddress") - 132)
                SettingsRM("SensorDL2_Y0") = Result1(DVMoxaReceiveStartAddress("SensorDL2_Y0_StartAddress") - 132)
                SettingsRM("SensorDL2_Y1") = Result1(DVMoxaReceiveStartAddress("SensorDL2_Y1_StartAddress") - 132)
                SettingsRM("SensorDL2_SET") = Result1(DVMoxaReceiveStartAddress("SensorDL2_SET_StartAddress") - 132)
                SettingsRM("SensorDL2_CAP") = Result1(DVMoxaReceiveStartAddress("SensorDL2_CAP_StartAddress") - 132)
                SettingsRM("SensorDL2_CAPMIN") = Result1(DVMoxaReceiveStartAddress("SensorDL2_CAPMIN_StartAddress") - 132)
                SettingsRM("SensorDL2_CAPMAX") = Result1(DVMoxaReceiveStartAddress("SensorDL2_CAPMAX_StartAddress") - 132)
                SettingsRM("SensorDL2Filter_EN") = Result1(DVMoxaReceiveStartAddress("SensorDL2Filter_EN_StartAddress") - 132)
                SettingsRM("SensorDL2Filter_TOL") = Result1(DVMoxaReceiveStartAddress("SensorDL2Filter_TOL_StartAddress") - 132)
                SettingsRM("SensorDL2Filter_TSMPL") = Result1(DVMoxaReceiveStartAddress("SensorDL2Filter_TSMPL_StartAddress") - 132)
                SettingsRM("SensorDL2Filter_NSMPL") = Result1(DVMoxaReceiveStartAddress("SensorDL2Filter_NSMPL_StartAddress") - 132)

                SettingsRM("Multiplicador_SensorA1") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorA1_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorA2") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorA2_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorA3") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorA3_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorA4") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorA4_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorA5") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorA5_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorF1") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorF1_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorF2") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorF2_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorF3") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorF3_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorF4") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorF4_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorF5") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorF5_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorDL1") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorDL1_StartAddress") - 132)
                SettingsRM("Multiplicador_SensorDL2") = Result1(DVMoxaReceiveStartAddress("Multiplicador_SensorDL2_StartAddress") - 132)
            End If
            RaiseEvent ConfigComplete()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
End Class
