﻿
Imports System.Threading
Public Class IDataVanSystem
    Inherits DataVanConfig

    Private WithEvents TmrRead As New System.Windows.Forms.Timer

#Region "Propiedades"
    Private Shared _valuesdict As New Dictionary(Of String, Double) From {
        {"SensorA1", 0},
        {"SensorA2", 0},
        {"SensorA3", 0},
        {"SensorA4", 0},
        {"SensorA5", 0},
        {"SensorF1", 0},
        {"SensorF2", 0},
        {"SensorF3", 0},
        {"SensorF4", 0},
        {"SensorF5", 0},
        {"SensorDL1", 0},
        {"SensorDL2", 0}
    }
    Public Shared Property DataVanValues() As Dictionary(Of String, Double)
        Get
            Return _valuesdict
        End Get
        Set(value As Dictionary(Of String, Double))
            If value IsNot _valuesdict Then
                _valuesdict = value
            End If
        End Set
    End Property
#End Region


    Public Sub New()

    End Sub
    ''' <summary>
    ''' Se inicia la lectura de informacion.
    ''' </summary>
    Public Sub StartRead()
        TmrRead.Enabled = True
        TmrRead.Start()
    End Sub


    ''' <summary>
    ''' Se finaliza la conexion al fracturador
    ''' </summary>
    Public Sub StopRead()
        TmrRead.Stop()
        TmrRead.Enabled = False
    End Sub

    ''' <summary>
    ''' Timer elapsed.
    ''' </summary>
    Private Sub TmrReadTick(sender As Object, e As EventArgs) Handles TmrRead.Tick

        Dim ReadThread As New Thread(AddressOf ReadThreadDo)
        ReadThread.Start()

    End Sub


    ''' <summary>
    ''' Sub para asignar los datos leidos.
    ''' </summary>
    Private Sub ReadThreadDo()
        Dim readResult As Integer() = MoxaRead(0, 12)
        If readResult.Length() = 12 Then
            DataVanValues.Item("SensorA1") = readResult(DVMoxaReceiveStartAddress("SensorA1_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorA2") = readResult(DVMoxaReceiveStartAddress("SensorA2_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorA3") = readResult(DVMoxaReceiveStartAddress("SensorA3_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorA4") = readResult(DVMoxaReceiveStartAddress("SensorA4_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorA5") = readResult(DVMoxaReceiveStartAddress("SensorA5_CAN_OUT_StartAddress"))

            DataVanValues.Item("SensorF1") = readResult(DVMoxaReceiveStartAddress("SensorF1_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorF2") = readResult(DVMoxaReceiveStartAddress("SensorF2_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorF3") = readResult(DVMoxaReceiveStartAddress("SensorF3_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorF4") = readResult(DVMoxaReceiveStartAddress("SensorF4_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorF5") = readResult(DVMoxaReceiveStartAddress("SensorF5_CAN_OUT_StartAddress"))

            DataVanValues.Item("SensorDL1") = readResult(DVMoxaReceiveStartAddress("SensorDL1_CAN_OUT_StartAddress"))
            DataVanValues.Item("SensorDL2") = readResult(DVMoxaReceiveStartAddress("SensorDL2_CAN_OUT_StartAddress"))

        ElseIf readResult.Length() = 0 Then
            TmrRead.Stop()
            TmrRead.Enabled = False
            Console.WriteLine("listo")
        End If
    End Sub

End Class
