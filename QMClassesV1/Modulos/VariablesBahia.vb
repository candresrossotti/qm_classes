﻿Public Module VariablesBahia
    Public Class BhData
        Public DblBahia As New Dictionary(Of String, Double) From {
            {"SuctionPress", 0},
            {"PumpFlowRate", 0},
            {"DischargePress", 0},
            {"TransmissionRPM", 0},
            {"EngineRPM", 0},
            {"HHP", 0},
            {"TransmissionGear", 0},
            {"TankTemp", 0},
            {"EngineOilPress", 0},
            {"EngineOilTemp", 0},
            {"EngineCoolTemp", 0},
            {"EngineFuelRate", 0},
            {"TransmissionOilTemp", 0},
            {"TransmissionOilPress", 0}
        }
    End Class
End Module
