﻿Imports MathNet.Filtering

Public Class DataFilter

    Public DataSensor_Filtered As New List(Of Double) From {0, 0, 0, 0, 0}

    Public Shared GraphConnector As RGraphConnect

    Private ReadOnly MedianFilters As New List(Of Median.OnlineMedianFilter) From {New Median.OnlineMedianFilter(10), New Median.OnlineMedianFilter(10), New Median.OnlineMedianFilter(10),
                                                                            New Median.OnlineMedianFilter(10), New Median.OnlineMedianFilter(10)}

    Public TmrFilter As New List(Of Timer) From {New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}}                  ''Timer de lectura de SP

    ''---Singleton---
    Private Shared DataFilter

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton 
    Public Shared Function GetInstancia()
        If DataFilter Is Nothing Then
            DataFilter = New DataFilter()
        End If
        Return DataFilter
    End Function

    ''Sub para configurar filtro
    Public Sub ConfigureFilter(ByVal AddDataIndex As Integer, ByVal FilterWindow As Integer)
        MedianFilters(AddDataIndex) = New Median.OnlineMedianFilter(FilterWindow)
    End Sub

    ''Delegado para poder utilizar multiples puertos serie y un mismo SUB
    Public Delegate Sub MyDelegate(AddDataIndex As Integer, EntryIndex As Integer, DataIndex As Integer)
    Public Sub StartFiltering(ByVal AddDataIndex As Integer, ByVal EntryIndex As Integer, ByVal DataIndex As Integer)
        CreateSPDelegate(AddDataIndex, EntryIndex, DataIndex, AddressOf FilterData)
        DataSensor_Filtered.Item(AddDataIndex) = MedianFilters.Item(AddDataIndex).ProcessSample(GraphConnector.UsedValues.Item(EntryIndex).Item(DataIndex))
    End Sub

    ''Sub para dejar de filtrar
    Public Sub StopFiltering(ByVal AddDataIndex As Integer)
        TmrFilter.Item(AddDataIndex).Stop()
    End Sub

    ''Creamos el sub DataReceived para el puerto serie en DataIndex
    Private Sub CreateSPDelegate(AddDataIndex As Integer, ByVal EntryIndex As Integer, ByVal DataIndex As Integer, del As MyDelegate)
        AddHandler TmrFilter.Item(AddDataIndex).Tick, Sub() del(AddDataIndex, EntryIndex, DataIndex)
        TmrFilter.Item(AddDataIndex).Start()
    End Sub

    ''Sub para filtrar los datos
    Private Sub FilterData(ByVal AddDataIndex As Integer, ByVal EntryIndex As Integer, ByVal DataIndex As Integer)
        Task.Run(Sub() FilterDataTask(AddDataIndex, EntryIndex, DataIndex))
    End Sub
    ''Task de filtro
    Private Sub FilterDataTask(ByVal AddDataIndex As Integer, ByVal EntryIndex As Integer, ByVal DataIndex As Integer)
        DataSensor_Filtered(AddDataIndex) = MedianFilters.Item(AddDataIndex).ProcessSample(GraphConnector.UsedValues.Item(EntryIndex).Item(DataIndex))
    End Sub
End Class
