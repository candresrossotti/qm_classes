﻿Imports System.IO
Imports System.Windows.Forms
Imports System.Text

Public Class FractureCreate

#Region "Properties"
    Private Shared _nfrac As Integer
    Public Shared Property NFrac() As Integer
        Get
            Return _nfrac
        End Get
        Set(value As Integer)
            If value <> _nfrac Then
                _nfrac = value
            End If
        End Set
    End Property
    Private Shared _las1 As Double()
    Public Shared Property LAS1() As Double()
        Get
            Return _las1
        End Get
        Set(value As Double())
            If value IsNot _las1 Then
                _las1 = value
            End If
        End Set
    End Property
    Private Shared _las2 As Double()
    Public Shared Property LAS2() As Double()
        Get
            Return _las2
        End Get
        Set(value As Double())
            If value IsNot _las2 Then
                _las2 = value
            End If
        End Set
    End Property
    Private Shared _las3 As Double()
    Public Shared Property LAS3() As Double()
        Get
            Return _las3
        End Get
        Set(value As Double())
            If value IsNot _las3 Then
                _las3 = value
            End If
        End Set
    End Property
    Private Shared _las4 As Double()
    Public Shared Property LAS4() As Double()
        Get
            Return _las4
        End Get
        Set(value As Double())
            If value IsNot _las4 Then
                _las4 = value
            End If
        End Set
    End Property
    Private Shared _das1 As Double()
    Public Shared Property DAS1() As Double()
        Get
            Return _das1
        End Get
        Set(value As Double())
            If value IsNot _das1 Then
                _das1 = value
            End If
        End Set
    End Property
    Private Shared _das2 As Double()
    Public Shared Property DAS2() As Double()
        Get
            Return _das2
        End Get
        Set(value As Double())
            If value IsNot _das2 Then
                _das2 = value
            End If
        End Set
    End Property
    Private Shared _presiondeseada As Integer
    Public Shared Property PresionDeseada() As Integer
        Get
            Return _presiondeseada
        End Get
        Private Set(value As Integer)
            If value <> _presiondeseada Then
                _presiondeseada = value
            End If
        End Set
    End Property
    Private Shared _caudaldeseado As Double
    Public Shared Property CaudalDeseado() As Double
        Get
            Return _caudaldeseado
        End Get
        Private Set(value As Double)
            If value <> _caudaldeseado Then
                _caudaldeseado = value
            End If
        End Set
    End Property
    Private Shared _barritestotales As Double()
    Public Shared Property BarrilesTotales() As Double()
        Get
            Return _barritestotales
        End Get
        Private Set(value As Double())
            If value IsNot _barritestotales Then
                _barritestotales = value
            End If
        End Set
    End Property
    Private Shared _tub As Integer
    Public Shared Property TUB() As Integer
        Get
            Return _tub
        End Get
        Set(value As Integer)
            If value <> _tub Then
                _tub = value
            End If
        End Set
    End Property
    Private Shared _psainicial As Double()
    Public Shared Property PSAInicial() As Double()
        Get
            Return _psainicial
        End Get
        Set(value As Double())
            If value IsNot _psainicial Then
                _psainicial = value
            End If
        End Set
    End Property
    Private Shared _tornillos As Boolean()
    Public Shared Property Tornillos() As Boolean()
        Get
            Return _tornillos
        End Get
        Set(value As Boolean())
            If value IsNot _tornillos Then
                _tornillos = value
            End If
        End Set
    End Property
    Private Shared _psafinal As Double()
    Public Shared Property PSAFinal() As Double()
        Get
            Return _psafinal
        End Get
        Set(value As Double())
            If value IsNot _psafinal Then
                _psafinal = value
            End If
        End Set
    End Property
    Private Shared _densidadfluidobase As Double
    Public Shared Property DensidadFluidoBase() As Double
        Get
            Return _densidadfluidobase
        End Get
        Set(value As Double)
            If value <> _densidadfluidobase Then
                _densidadfluidobase = value
            End If
        End Set
    End Property
    Private Shared _densidadarena As Double
    Public Shared Property DensidadArena() As Double
        Get
            Return _densidadarena
        End Get
        Set(value As Double)
            If value <> _densidadarena Then
                _densidadarena = value
            End If
        End Set
    End Property
    Private Shared _volumenpozo As Double
    Public Shared Property VolumenPozo() As Double
        Get
            Return _volumenpozo
        End Get
        Set(value As Double)
            If value <> _volumenpozo Then
                _volumenpozo = value
            End If
        End Set
    End Property
    Private Shared _etapas As Integer
    Public Shared Property Etapas() As Integer
        Get
            Return _etapas
        End Get
        Private Set(value As Integer)
            If value <> _etapas Then
                _etapas = value
            End If
        End Set
    End Property
#End Region


    ''' <summary>
    ''' Funcion para ajustar la configuracion de la fractura.
    ''' </summary>
    Public Sub SetProperties(ByVal nPresionDeseada As Integer, ByVal nCaudalDeseado As Double, ByVal nBarrilesTotales As Double(),
                             ByVal nLAS1 As Double(), ByVal nLAS2 As Double(), ByVal nLAS3 As Double(), ByVal nLAS4 As Double(), ByVal nDAS1 As Double(),
                             ByVal nDAS2 As Double(), ByVal nTUB As Integer, ByVal nPSAInicial As Double(), ByVal nPSAFinal As Double(),
                             ByVal nDensidadFluidoBase As Double, ByVal nDensidadArena As Double, ByVal nVolumenPozo As Double, ByVal nEtapas As Integer,
                             ByVal bTornillo1 As Boolean, ByVal bTornillo2 As Boolean, ByVal sNFrac As Integer)

        ReDim Tornillos(1)
        PresionDeseada = nPresionDeseada
        CaudalDeseado = nCaudalDeseado
        BarrilesTotales = nBarrilesTotales
        LAS1 = nLAS1
        LAS2 = nLAS2
        LAS3 = nLAS3
        LAS4 = nLAS4
        DAS1 = nDAS1
        DAS2 = nDAS2
        TUB = nTUB
        PSAInicial = nPSAInicial
        PSAFinal = nPSAFinal
        DensidadFluidoBase = nDensidadFluidoBase
        DensidadArena = nDensidadArena
        VolumenPozo = nVolumenPozo
        Etapas = nEtapas
        Tornillos(0) = bTornillo1
        Tornillos(1) = bTornillo2
        NFrac = sNFrac
    End Sub

    ''' <summary>
    ''' Funcion para generar los archivos de fractura.
    ''' </summary>
    Public Sub Write()
        Dim SaveDlg As New SaveFileDialog With {
            .Filter = "(*.fcf)|*.fcf",
            .FileName = "FracSession"
        }
        If SaveDlg.ShowDialog() = DialogResult.OK Then
            FracWrite(SaveDlg.FileName)
            BlenderWrite(SaveDlg.FileName)
        End If
    End Sub

    '' <summary>
    '' Funcion para leer archivo del blender.
    '' </summary>
    Public Function BlenderRead() As Boolean
        Dim OpenDlg As New OpenFileDialog With {
            .Filter = "(*.fcf)|*.fcf",
            .FileName = "FracSession_Blender"
        }
        If OpenDlg.ShowDialog() = DialogResult.OK Then

            Dim fileReader = File.ReadAllLines(OpenDlg.FileName)

            If Decode(fileReader(0)) = "BLENDER" Then
                Etapas = Decode(fileReader(1))
                RedimAll(Etapas)
                CaudalDeseado = Decode(fileReader(2))
                TUB = Decode(fileReader(3))
                Tornillos(0) = Decode(fileReader(4))
                Tornillos(1) = Decode(fileReader(5))
                DensidadFluidoBase = Decode(fileReader(6))
                DensidadArena = Decode(fileReader(7))
                VolumenPozo = Decode(fileReader(8))

                Dim BarrilesFileReader = Decode(fileReader(9))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = BarrilesFileReader.IndexOf(";")
                    BarrilesFileReader = Replace(BarrilesFileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = BarrilesFileReader.IndexOf(";")
                    BarrilesTotales(i) = BarrilesFileReader.Substring(0, NextOne)
                Next

                Dim PSAInicialFileReader = Decode(fileReader(10))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = PSAInicialFileReader.IndexOf(";")
                    PSAInicialFileReader = Replace(PSAInicialFileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = PSAInicialFileReader.IndexOf(";")
                    PSAInicial(i) = PSAInicialFileReader.Substring(0, NextOne)
                Next

                Dim PSAFinalFileReader = Decode(fileReader(11))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = PSAFinalFileReader.IndexOf(";")
                    PSAFinalFileReader = Replace(PSAFinalFileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = PSAFinalFileReader.IndexOf(";")
                    PSAFinal(i) = PSAFinalFileReader.Substring(0, NextOne)
                Next

                Dim LAS1FileReader = Decode(fileReader(12))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = LAS1FileReader.IndexOf(";")
                    LAS1FileReader = Replace(LAS1FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = LAS1FileReader.IndexOf(";")
                    LAS1(i) = LAS1FileReader.Substring(0, NextOne)
                Next

                Dim LAS2FileReader = Decode(fileReader(13))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = LAS2FileReader.IndexOf(";")
                    LAS2FileReader = Replace(LAS2FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = LAS2FileReader.IndexOf(";")
                    LAS2(i) = LAS2FileReader.Substring(0, NextOne)
                Next

                Dim LAS3FileReader = Decode(fileReader(14))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = LAS3FileReader.IndexOf(";")
                    LAS3FileReader = Replace(LAS3FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = LAS3FileReader.IndexOf(";")
                    LAS3(i) = LAS3FileReader.Substring(0, NextOne)
                Next

                Dim LAS4FileReader = Decode(fileReader(15))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = LAS4FileReader.IndexOf(";")
                    LAS4FileReader = Replace(LAS4FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = LAS4FileReader.IndexOf(";")
                    LAS4(i) = LAS4FileReader.Substring(0, NextOne)
                Next

                Dim DAS1FileReader = Decode(fileReader(16))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = DAS1FileReader.IndexOf(";")
                    DAS1FileReader = Replace(DAS1FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = DAS1FileReader.IndexOf(";")
                    DAS1(i) = DAS1FileReader.Substring(0, NextOne)
                Next

                Dim DAS2FileReader = Decode(fileReader(17))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = DAS2FileReader.IndexOf(";")
                    DAS2FileReader = Replace(DAS2FileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = DAS2FileReader.IndexOf(";")
                    DAS2(i) = DAS2FileReader.Substring(0, NextOne)
                Next
                Return True
            Else
                avisoStr = "Archivo incorrecto"
                avisoTitulo = "Error"
                Dim AG As New AvisoGeneral
                AG.ShowDialog()
                Return False
            End If
        End If
        Return False
    End Function

    Private Sub RedimAll(ByVal Etapas As Integer)
        ReDim Tornillos(1)
        ReDim BarrilesTotales(Etapas - 1)
        ReDim PSAInicial(Etapas - 1)
        ReDim PSAFinal(Etapas - 1)
        ReDim LAS1(Etapas - 1)
        ReDim LAS2(Etapas - 1)
        ReDim LAS3(Etapas - 1)
        ReDim LAS4(Etapas - 1)
        ReDim DAS1(Etapas - 1)
        ReDim DAS2(Etapas - 1)
    End Sub

    Private Sub BlenderWrite(ByVal FileName As String)


        Dim Obj As New List(Of String) From {
                Encode("BLENDER"),
                Encode(Etapas.ToString()),
                Encode(CaudalDeseado.ToString()),
                Encode(TUB.ToString()),
                Encode(Tornillos(0).ToString()),
                Encode(Tornillos(1).ToString()),
                Encode(DensidadFluidoBase.ToString()),
                Encode(DensidadArena.ToString()),
                Encode(VolumenPozo.ToString())
            }
        Dim StrBarriles As String = ";"
        For k = 0 To Etapas - 1
            StrBarriles += (BarrilesTotales(k).ToString() + ";")
        Next
        Obj.Add(Encode(StrBarriles))

        Dim StrPSAInicial As String = ";"
        For k = 0 To Etapas - 1
            StrPSAInicial += PSAInicial(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrPSAInicial))

        Dim StrPSAFinal As String = ";"
        For k = 0 To Etapas - 1
            StrPSAFinal += PSAFinal(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrPSAFinal))

        Dim StrLAS1 As String = ";"
        For k = 0 To Etapas - 1
            StrLAS1 += LAS1(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrLAS1))

        Dim StrLAS2 As String = ";"
        For k = 0 To Etapas - 1
            StrLAS2 += LAS2(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrLAS2))

        Dim StrLAS3 As String = ";"
        For k = 0 To Etapas - 1
            StrLAS3 += LAS3(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrLAS3))

        Dim StrLAS4 As String = ";"
        For k = 0 To Etapas - 1
            StrLAS4 += LAS4(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrLAS4))

        Dim StrDAS1 As String = ";"
        For k = 0 To Etapas - 1
            StrDAS1 += DAS1(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrDAS1))

        Dim StrDAS2 As String = ";"
        For k = 0 To Etapas - 1
            StrDAS2 += DAS2(k).ToString() + ";"
        Next
        Obj.Add(Encode(StrDAS2))

        File.WriteAllLines(Replace(FileName, ".fcf", "_Blender.fcf"), Obj)
    End Sub

    Private Sub FracWrite(ByVal FileName As String)
        Dim Obj As New List(Of String) From {
                Encode("FRAC"),
                Encode(NFrac),
                Encode(Etapas.ToString()),
                Encode(PresionDeseada.ToString()),
                Encode(CaudalDeseado.ToString())
            }
        Dim StrBarriles As String = ";"
        For k = 0 To Etapas - 1
            StrBarriles += (BarrilesTotales(k).ToString() + ";")
        Next
        Obj.Add(Encode(StrBarriles))
        File.WriteAllLines(Replace(FileName, ".fcf", "_Frac.fcf"), Obj)
    End Sub

    '' <summary>
    '' Funcion para leer archivo del fracturador.
    '' </summary>
    Public Function FracRead() As Boolean
        Dim OpenDlg As New OpenFileDialog With {
            .Filter = "(*.fcf)|*.fcf",
            .FileName = "FracSession_Frac"
        }
        If OpenDlg.ShowDialog() = DialogResult.OK Then

            Dim fileReader = File.ReadAllLines(OpenDlg.FileName)


            If Decode(fileReader(0)) = "FRAC" Then
                NFrac = Decode(fileReader(1))
                Etapas = Decode(fileReader(2))
                ReDim BarrilesTotales(Etapas - 1)
                PresionDeseada = Decode(fileReader(3))
                CaudalDeseado = Decode(fileReader(4))

                Dim BarrilesTotalesFileReader = Decode(fileReader(5))
                For i = 0 To Etapas - 1
                    Dim FirstOne As Integer = BarrilesTotalesFileReader.IndexOf(";")
                    BarrilesTotalesFileReader = Replace(BarrilesTotalesFileReader, ";", Nothing, FirstOne + 1, 1)
                    Dim NextOne As Integer = BarrilesTotalesFileReader.IndexOf(";")
                    BarrilesTotales(i) = BarrilesTotalesFileReader.Substring(0, NextOne)
                Next
                Return True
            Else
                avisoStr = "Archivo incorrecto"
                avisoTitulo = "Error"
                Dim AG As New AvisoGeneral
                AG.ShowDialog()
                Return False
            End If
        End If
        Return False
    End Function

    Private Function Encode(ByVal Sender As String) As String
        Dim Enc As Encoding = Encoding.Unicode
        Dim ByteArray = Enc.GetBytes(Sender)
        Return String.Join("*", ByteArray)
    End Function

    Private Function Decode(ByVal Sender As String) As String
        Dim Enc As Encoding = Encoding.Unicode
        Dim StringArray = Sender.Split("*")
        Dim ByteArray As Byte()
        ReDim ByteArray(StringArray.Length - 1)
        For i = 0 To StringArray.Length - 1
            ByteArray(i) = Convert.ToByte(StringArray(i))
        Next
        Return Enc.GetString(ByteArray)
    End Function


End Class
