﻿Imports System.Threading
Public Class OneConfig
    Inherits MoxaHandle


    ''' <summary>
    ''' Funcion para ajustar una sola configuracion.
    ''' </summary>
    ''' <param name="UpdateDirectionAddress">Address del write para escribir en memoria</param>
    ''' <param name="UpdateDirection">Address del data enable.</param>
    ''' <param name="DataAddress">Address del dato</param>
    ''' <param name="Data">Dato a escribir</param>
    Public Sub SetSingleConfig(ByVal UpdateDirectionAddress As Integer, ByVal UpdateDirection As Integer, ByVal DataAddress As Integer, ByVal Data As Integer)
        Dim SingleConfigTask As New Task(Sub() SingleConfig(UpdateDirectionAddress, UpdateDirection, DataAddress, Data))
        SingleConfigTask.Start()
    End Sub

    ''' <summary>
    ''' Funcion para ajustar una sola configuracion.
    ''' </summary>
    Private Async Sub SingleConfig(ByVal UpdateDirectionAddress As Integer, ByVal UpdateDirection As Integer, ByVal DataAddress As Integer, ByVal Data As Integer)
        MoxaSend(UpdateDirectionAddress, UpdateDirection)
        Await Task.Delay(200)
        MoxaSend(DataAddress, Data)
    End Sub

End Class
