﻿Imports System.Configuration
Imports System.ComponentModel
Imports System.Globalization
Imports Newtonsoft.Json
Imports System.Drawing

<TypeConverter(GetType(ProfileConverter))>
<SettingsSerializeAs(SettingsSerializeAs.String)>
Public Class GraphProfile

    Public Sub New()
        FormSettings = EmptyFormSettings
        ChartSettings = EmptyChartSettings
        DataSettings = EmptyDataSettings
        AddDataSettings = EmptyAddDataSettings
        FloatingScreenSettings = EmptyFloatingScreenSettings
    End Sub

    ''Variables para el manejo de pantallas flotantes
    Public FloatingScreenSettings As New Dictionary(Of Object, Object)
    Public ReadOnly EmptyFloatingScreenSettings As New Dictionary(Of Object, Object) From {
        {"TotalScreens", 1},
        {"ColumnCount", 1},
        {"RowCount", 1},
        {"ColumnSpan_1", 1},
        {"RowSpan_1", 1},
        {"BackColor_1", -8355712},
        {"Font_1", "Montserrat, 12"}
    }

    ''Variables para el manejo de ajustes del form
    Public FormSettings As New Dictionary(Of Object, Object)
    Public ReadOnly EmptyFormSettings As New Dictionary(Of Object, Object) From {
        {"Titulo1", "Graficador1"},
        {"Titulo2", "Graficador2"},
        {"Titulo3", "Graficador3"},
        {"Resolucion1", 8},
        {"Resolucion2", 8},
        {"Resolucion3", 8}
    }

    ''Variables para el manejo de ajustes del grafico (3 GRAFICOS)
    Public ChartSettings As New Dictionary(Of Object, Object)
    Public ReadOnly EmptyChartSettings As New Dictionary(Of Object, Object) From {
        {"XAxisWidth1", 1},
        {"XAxisWidth2", 1},
        {"XAxisWidth3", 1},
        {"XAxisMin1", 0},
        {"XAxisMin2", 0},
        {"XAxisMin3", 0},
        {"XAxisMax1", 0},
        {"XAxisMax2", 0},
        {"XAxisMax3", 0},
        {"AxisWidth1_1", 1},
        {"AxisWidth2_1", 1},
        {"AxisWidth3_1", 1},
        {"AxisWidth4_1", 1},
        {"AxisWidth1_2", 1},
        {"AxisWidth2_2", 1},
        {"AxisWidth3_2", 1},
        {"AxisWidth4_2", 1},
        {"AxisWidth1_3", 1},
        {"AxisWidth2_3", 1},
        {"AxisWidth3_3", 1},
        {"AxisWidth4_3", 1},
        {"AxisTitle1_1", "Eje Y1"},
        {"AxisTitle2_1", "Eje Y2"},
        {"AxisTitle3_1", "Eje Y3"},
        {"AxisTitle4_1", "Eje Y4"},
        {"AxisTitle1_2", "Eje Y1"},
        {"AxisTitle2_2", "Eje Y2"},
        {"AxisTitle3_2", "Eje Y3"},
        {"AxisTitle4_2", "Eje Y4"},
        {"AxisTitle1_3", "Eje Y1"},
        {"AxisTitle2_3", "Eje Y2"},
        {"AxisTitle3_3", "Eje Y3"},
        {"AxisTitle4_3", "Eje Y4"},
        {"AxisMin1_1", 0},
        {"AxisMin2_1", 0},
        {"AxisMin3_1", 0},
        {"AxisMin4_1", 0},
        {"AxisMin1_2", 0},
        {"AxisMin2_2", 0},
        {"AxisMin3_2", 0},
        {"AxisMin4_2", 0},
        {"AxisMin1_3", 0},
        {"AxisMin2_3", 0},
        {"AxisMin3_3", 0},
        {"AxisMin4_3", 0},
        {"AxisMax1_1", 1000},
        {"AxisMax2_1", 1000},
        {"AxisMax3_1", 1000},
        {"AxisMax4_1", 1000},
        {"AxisMax1_2", 1000},
        {"AxisMax2_2", 1000},
        {"AxisMax3_2", 1000},
        {"AxisMax4_2", 1000},
        {"AxisMax1_3", 1000},
        {"AxisMax2_3", 1000},
        {"AxisMax3_3", 1000},
        {"AxisMax4_3", 1000},
        {"LinearAxisAdjust1_1", False},
        {"LinearAxisAdjust2_1", False},
        {"LinearAxisAdjust3_1", False},
        {"LinearAxisAdjust4_1", False},
        {"LinearAxisAdjust1_2", False},
        {"LinearAxisAdjust2_2", False},
        {"LinearAxisAdjust3_2", False},
        {"LinearAxisAdjust4_2", False},
        {"LinearAxisAdjust1_3", False},
        {"LinearAxisAdjust2_3", False},
        {"LinearAxisAdjust3_3", False},
        {"LinearAxisAdjust4_3", False},
        {"AxisVisible1_1", True},
        {"AxisVisible2_1", True},
        {"AxisVisible3_1", True},
        {"AxisVisible4_1", True},
        {"AxisVisible1_2", True},
        {"AxisVisible2_2", True},
        {"AxisVisible3_2", True},
        {"AxisVisible4_2", True},
        {"AxisVisible1_3", True},
        {"AxisVisible2_3", True},
        {"AxisVisible3_3", True},
        {"AxisVisible4_3", True},
        {"ChartStartX_1", 160},
        {"ChartStartX_2", 160},
        {"ChartStartX_3", 160},
        {"ChartEndX_1", 320},
        {"ChartEndX_2", 320},
        {"ChartEndX_3", 320},
        {"Leyenda1", 1},
        {"Leyenda2", 1},
        {"Leyenda3", 1}
    }

    ''Variables para el manejo de ajustes de datos (30 DATOS EN TOTAL)
    Public DataSettings As New Dictionary(Of Object, Object)
    Public ReadOnly EmptyDataSettings As New Dictionary(Of Object, Object) From {
        {"LayerName1", "Dato1"},
        {"LayerName2", "Dato2"},
        {"LayerName3", "Dato3"},
        {"LayerName4", "Dato4"},
        {"LayerName5", "Dato5"},
        {"LayerName6", "Dato6"},
        {"LayerName7", "Dato7"},
        {"LayerName8", "Dato8"},
        {"LayerName9", "Dato9"},
        {"LayerName10", "Dato10"},
        {"LayerName11", "Dato11"},
        {"LayerName12", "Dato12"},
        {"LayerName13", "Dato13"},
        {"LayerName14", "Dato14"},
        {"LayerName15", "Dato15"},
        {"LayerName16", "Dato16"},
        {"LayerName17", "Dato17"},
        {"LayerName18", "Dato18"},
        {"LayerName19", "Dato19"},
        {"LayerName20", "Dato20"},
        {"LayerName21", "Dato21"},
        {"LayerName22", "Dato22"},
        {"LayerName23", "Dato23"},
        {"LayerName24", "Dato24"},
        {"LayerName25", "Dato25"},
        {"LayerName26", "Dato26"},
        {"LayerName27", "Dato27"},
        {"LayerName28", "Dato28"},
        {"LayerName29", "Dato29"},
        {"LayerName30", "Dato30"},
        {"LayerColor1", &HFFFFFF},
        {"LayerColor2", &HFFFFFF},
        {"LayerColor3", &HFFFFFF},
        {"LayerColor4", &HFFFFFF},
        {"LayerColor5", &HFFFFFF},
        {"LayerColor6", &HFFFFFF},
        {"LayerColor7", &HFFFFFF},
        {"LayerColor8", &HFFFFFF},
        {"LayerColor9", &HFFFFFF},
        {"LayerColor10", &HFFFFFF},
        {"LayerColor11", &HFFFFFF},
        {"LayerColor12", &HFFFFFF},
        {"LayerColor13", &HFFFFFF},
        {"LayerColor14", &HFFFFFF},
        {"LayerColor15", &HFFFFFF},
        {"LayerColor16", &HFFFFFF},
        {"LayerColor17", &HFFFFFF},
        {"LayerColor18", &HFFFFFF},
        {"LayerColor19", &HFFFFFF},
        {"LayerColor20", &HFFFFFF},
        {"LayerColor21", &HFFFFFF},
        {"LayerColor22", &HFFFFFF},
        {"LayerColor23", &HFFFFFF},
        {"LayerColor24", &HFFFFFF},
        {"LayerColor25", &HFFFFFF},
        {"LayerColor26", &HFFFFFF},
        {"LayerColor27", &HFFFFFF},
        {"LayerColor28", &HFFFFFF},
        {"LayerColor29", &HFFFFFF},
        {"LayerColor30", &HFFFFFF},
        {"LayerWidth1", 1},
        {"LayerWidth2", 1},
        {"LayerWidth3", 1},
        {"LayerWidth4", 1},
        {"LayerWidth5", 1},
        {"LayerWidth6", 1},
        {"LayerWidth7", 1},
        {"LayerWidth8", 1},
        {"LayerWidth9", 1},
        {"LayerWidth10", 1},
        {"LayerWidth11", 1},
        {"LayerWidth12", 1},
        {"LayerWidth13", 1},
        {"LayerWidth14", 1},
        {"LayerWidth15", 1},
        {"LayerWidth16", 1},
        {"LayerWidth17", 1},
        {"LayerWidth18", 1},
        {"LayerWidth19", 1},
        {"LayerWidth20", 1},
        {"LayerWidth21", 1},
        {"LayerWidth22", 1},
        {"LayerWidth23", 1},
        {"LayerWidth24", 1},
        {"LayerWidth25", 1},
        {"LayerWidth26", 1},
        {"LayerWidth27", 1},
        {"LayerWidth28", 1},
        {"LayerWidth29", 1},
        {"LayerWidth30", 1},
        {"LayerBindingAxis1_1", 1},
        {"LayerBindingAxis2_1", 2},
        {"LayerBindingAxis3_1", 3},
        {"LayerBindingAxis4_1", 4},
        {"LayerBindingAxis5_1", 1},
        {"LayerBindingAxis6_1", 2},
        {"LayerBindingAxis7_1", 3},
        {"LayerBindingAxis8_1", 4},
        {"LayerBindingAxis9_1", 1},
        {"LayerBindingAxis10_1", 2},
        {"LayerBindingAxis11_1", 3},
        {"LayerBindingAxis12_1", 4},
        {"LayerBindingAxis13_1", 1},
        {"LayerBindingAxis14_1", 2},
        {"LayerBindingAxis15_1", 3},
        {"LayerBindingAxis16_1", 4},
        {"LayerBindingAxis17_1", 1},
        {"LayerBindingAxis18_1", 2},
        {"LayerBindingAxis19_1", 3},
        {"LayerBindingAxis20_1", 4},
        {"LayerBindingAxis21_1", 1},
        {"LayerBindingAxis22_1", 2},
        {"LayerBindingAxis23_1", 3},
        {"LayerBindingAxis24_1", 4},
        {"LayerBindingAxis25_1", 1},
        {"LayerBindingAxis26_1", 2},
        {"LayerBindingAxis27_1", 3},
        {"LayerBindingAxis28_1", 4},
        {"LayerBindingAxis29_1", 1},
        {"LayerBindingAxis30_1", 2},
        {"LayerBindingAxis1_2", 3},
        {"LayerBindingAxis2_2", 4},
        {"LayerBindingAxis3_2", 1},
        {"LayerBindingAxis4_2", 1},
        {"LayerBindingAxis5_2", 1},
        {"LayerBindingAxis6_2", 1},
        {"LayerBindingAxis7_2", 1},
        {"LayerBindingAxis8_2", 1},
        {"LayerBindingAxis9_2", 1},
        {"LayerBindingAxis10_2", 1},
        {"LayerBindingAxis11_2", 1},
        {"LayerBindingAxis12_2", 1},
        {"LayerBindingAxis13_2", 1},
        {"LayerBindingAxis14_2", 1},
        {"LayerBindingAxis15_2", 1},
        {"LayerBindingAxis16_2", 1},
        {"LayerBindingAxis17_2", 1},
        {"LayerBindingAxis18_2", 1},
        {"LayerBindingAxis19_2", 1},
        {"LayerBindingAxis20_2", 1},
        {"LayerBindingAxis21_2", 1},
        {"LayerBindingAxis22_2", 1},
        {"LayerBindingAxis23_2", 1},
        {"LayerBindingAxis24_2", 1},
        {"LayerBindingAxis25_2", 1},
        {"LayerBindingAxis26_2", 1},
        {"LayerBindingAxis27_2", 1},
        {"LayerBindingAxis28_2", 1},
        {"LayerBindingAxis29_2", 1},
        {"LayerBindingAxis30_2", 1},
        {"LayerBindingAxis1_3", 1},
        {"LayerBindingAxis2_3", 1},
        {"LayerBindingAxis3_3", 1},
        {"LayerBindingAxis4_3", 1},
        {"LayerBindingAxis5_3", 1},
        {"LayerBindingAxis6_3", 1},
        {"LayerBindingAxis7_3", 1},
        {"LayerBindingAxis8_3", 1},
        {"LayerBindingAxis9_3", 1},
        {"LayerBindingAxis10_3", 1},
        {"LayerBindingAxis11_3", 1},
        {"LayerBindingAxis12_3", 1},
        {"LayerBindingAxis13_3", 1},
        {"LayerBindingAxis14_3", 1},
        {"LayerBindingAxis15_3", 1},
        {"LayerBindingAxis16_3", 1},
        {"LayerBindingAxis17_3", 1},
        {"LayerBindingAxis18_3", 1},
        {"LayerBindingAxis19_3", 1},
        {"LayerBindingAxis20_3", 1},
        {"LayerBindingAxis21_3", 1},
        {"LayerBindingAxis22_3", 1},
        {"LayerBindingAxis23_3", 1},
        {"LayerBindingAxis24_3", 1},
        {"LayerBindingAxis25_3", 1},
        {"LayerBindingAxis26_3", 1},
        {"LayerBindingAxis27_3", 1},
        {"LayerBindingAxis28_3", 1},
        {"LayerBindingAxis29_3", 1},
        {"LayerBindingAxis30_3", 1},
        {"LayerVisible1_1", False},
        {"LayerVisible2_1", False},
        {"LayerVisible3_1", False},
        {"LayerVisible4_1", False},
        {"LayerVisible5_1", False},
        {"LayerVisible6_1", False},
        {"LayerVisible7_1", False},
        {"LayerVisible8_1", False},
        {"LayerVisible9_1", False},
        {"LayerVisible10_1", False},
        {"LayerVisible11_1", False},
        {"LayerVisible12_1", False},
        {"LayerVisible13_1", False},
        {"LayerVisible14_1", False},
        {"LayerVisible15_1", False},
        {"LayerVisible16_1", False},
        {"LayerVisible17_1", False},
        {"LayerVisible18_1", False},
        {"LayerVisible19_1", False},
        {"LayerVisible20_1", False},
        {"LayerVisible21_1", False},
        {"LayerVisible22_1", False},
        {"LayerVisible23_1", False},
        {"LayerVisible24_1", False},
        {"LayerVisible25_1", False},
        {"LayerVisible26_1", False},
        {"LayerVisible27_1", False},
        {"LayerVisible28_1", False},
        {"LayerVisible29_1", False},
        {"LayerVisible30_1", False},
        {"LayerVisible1_2", False},
        {"LayerVisible2_2", False},
        {"LayerVisible3_2", False},
        {"LayerVisible4_2", False},
        {"LayerVisible5_2", False},
        {"LayerVisible6_2", False},
        {"LayerVisible7_2", False},
        {"LayerVisible8_2", False},
        {"LayerVisible9_2", False},
        {"LayerVisible10_2", False},
        {"LayerVisible11_2", False},
        {"LayerVisible12_2", False},
        {"LayerVisible13_2", False},
        {"LayerVisible14_2", False},
        {"LayerVisible15_2", False},
        {"LayerVisible16_2", False},
        {"LayerVisible17_2", False},
        {"LayerVisible18_2", False},
        {"LayerVisible19_2", False},
        {"LayerVisible20_2", False},
        {"LayerVisible21_2", False},
        {"LayerVisible22_2", False},
        {"LayerVisible23_2", False},
        {"LayerVisible24_2", False},
        {"LayerVisible25_2", False},
        {"LayerVisible26_2", False},
        {"LayerVisible27_2", False},
        {"LayerVisible28_2", False},
        {"LayerVisible29_2", False},
        {"LayerVisible30_2", False},
        {"LayerVisible1_3", False},
        {"LayerVisible2_3", False},
        {"LayerVisible3_3", False},
        {"LayerVisible4_3", False},
        {"LayerVisible5_3", False},
        {"LayerVisible6_3", False},
        {"LayerVisible7_3", False},
        {"LayerVisible8_3", False},
        {"LayerVisible9_3", False},
        {"LayerVisible10_3", False},
        {"LayerVisible11_3", False},
        {"LayerVisible12_3", False},
        {"LayerVisible13_3", False},
        {"LayerVisible14_3", False},
        {"LayerVisible15_3", False},
        {"LayerVisible16_3", False},
        {"LayerVisible17_3", False},
        {"LayerVisible18_3", False},
        {"LayerVisible19_3", False},
        {"LayerVisible20_3", False},
        {"LayerVisible21_3", False},
        {"LayerVisible22_3", False},
        {"LayerVisible23_3", False},
        {"LayerVisible24_3", False},
        {"LayerVisible25_3", False},
        {"LayerVisible26_3", False},
        {"LayerVisible27_3", False},
        {"LayerVisible28_3", False},
        {"LayerVisible29_3", False},
        {"LayerVisible30_3", False}
    }

    ''Variables para el manejo de ajustes de datos agregados (5 DATOS AGREGADOS EN TOTAL)
    Public AddDataSettings As New Dictionary(Of Object, Object)
    Public ReadOnly EmptyAddDataSettings As New Dictionary(Of Object, Object) From {
        {"AddLayerName1", ""},
        {"AddLayerName2", ""},
        {"AddLayerName3", ""},
        {"AddLayerName4", ""},
        {"AddLayerName5", ""},
        {"AddLayerColor1", &HFFFFFF},
        {"AddLayerColor2", &HFFFFFF},
        {"AddLayerColor3", &HFFFFFF},
        {"AddLayerColor4", &HFFFFFF},
        {"AddLayerColor5", &HFFFFFF},
        {"AddLayerWidth1", 1},
        {"AddLayerWidth2", 1},
        {"AddLayerWidth3", 1},
        {"AddLayerWidth4", 1},
        {"AddLayerWidth5", 1},
        {"AddLayerBindingAxis1_1", 1},
        {"AddLayerBindingAxis2_1", 1},
        {"AddLayerBindingAxis3_1", 1},
        {"AddLayerBindingAxis4_1", 1},
        {"AddLayerBindingAxis5_1", 1},
        {"AddLayerBindingAxis1_2", 1},
        {"AddLayerBindingAxis2_2", 1},
        {"AddLayerBindingAxis3_2", 1},
        {"AddLayerBindingAxis4_2", 1},
        {"AddLayerBindingAxis5_2", 1},
        {"AddLayerBindingAxis1_3", 1},
        {"AddLayerBindingAxis2_3", 1},
        {"AddLayerBindingAxis3_3", 1},
        {"AddLayerBindingAxis4_3", 1},
        {"AddLayerBindingAxis5_3", 1},
        {"AddLayerVisible1_1", False},
        {"AddLayerVisible2_1", False},
        {"AddLayerVisible3_1", False},
        {"AddLayerVisible4_1", False},
        {"AddLayerVisible5_1", False},
        {"AddLayerVisible1_2", False},
        {"AddLayerVisible2_2", False},
        {"AddLayerVisible3_2", False},
        {"AddLayerVisible4_2", False},
        {"AddLayerVisible5_2", False},
        {"AddLayerVisible1_3", False},
        {"AddLayerVisible2_3", False},
        {"AddLayerVisible3_3", False},
        {"AddLayerVisible4_3", False},
        {"AddLayerVisible5_3", False},
        {"ActiveLayer1", False},
        {"ActiveLayer2", False},
        {"ActiveLayer3", False},
        {"ActiveLayer4", False},
        {"ActiveLayer5", False},
        {"OperationsAddLayer1", ""},
        {"OperandsAddLayer1", ""},
        {"TotalizeLayer1", False},
        {"OperationsAddLayer2", ""},
        {"OperandsAddLayer2", ""},
        {"TotalizeLayer2", False},
        {"OperationsAddLayer3", ""},
        {"OperandsAddLayer3", ""},
        {"TotalizeLayer3", False},
        {"OperationsAddLayer4", ""},
        {"OperandsAddLayer4", ""},
        {"TotalizeLayer4", False},
        {"OperationsAddLayer5", ""},
        {"OperandsAddLayer5", ""},
        {"TotalizeLayer5", False}
    }

    ''Get FormSettings
    Public Function GetFormSettings() As Dictionary(Of Object, Object)
        Return FormSettings
    End Function

    ''Get ChartSettings
    Public Function GetChartSettings() As Dictionary(Of Object, Object)
        Return ChartSettings
    End Function

    ''Get DataSettings
    Public Function GetDataSettings() As Dictionary(Of Object, Object)
        Return DataSettings
    End Function

    ''Get AddDataSettings
    Public Function GetAddDataSettings() As Dictionary(Of Object, Object)
        Return AddDataSettings
    End Function

    ''Get FloatingScreenSettings
    Public Function GetFloatingScreenSettings() As Dictionary(Of Object, Object)
        Return FloatingScreenSettings
    End Function

    Public Sub ClearFloatingScreenSettings()
        FloatingScreenSettings = EmptyFloatingScreenSettings
    End Sub

    ''Clear FormSettings
    Public Sub ClearFormSettings()
        FormSettings = EmptyFormSettings
    End Sub
    ''Clear ChartSettings
    Public Sub ClearChartSettings()
        ChartSettings = EmptyChartSettings
    End Sub
    ''Clear DataSettings
    Public Sub ClearDataSettings()
        DataSettings = EmptyDataSettings
    End Sub
    ''Clear AddDataSettings
    Public Sub ClearAddDataSettings()
        AddDataSettings = EmptyAddDataSettings
    End Sub

    ''Set FormSettings
    Public Sub SetFormSettings(ByVal NewFormSettings As Dictionary(Of Object, Object))
        FormSettings = NewFormSettings
    End Sub

    ''Set FormSettings
    Public Sub SetChartSettings(ByVal NewChartSettings As Dictionary(Of Object, Object))
        ChartSettings = NewChartSettings
    End Sub

    ''Set FormSettings
    Public Sub SetDataSettings(ByVal NewDataSettings As Dictionary(Of Object, Object))
        DataSettings = NewDataSettings
    End Sub

    ''Set FormSettings
    Public Sub SetAddDataSettings(ByVal NewAddDataSettings As Dictionary(Of Object, Object))
        AddDataSettings = NewAddDataSettings
    End Sub

    Public Sub SetFloatingScreenSettings(ByVal NewFloatingScreenSettings As Dictionary(Of Object, Object))
        FloatingScreenSettings = NewFloatingScreenSettings
    End Sub
End Class

Public Class ProfileConverter
    Inherits TypeConverter

    Public Overrides Function CanConvertFrom(context As ITypeDescriptorContext, sourceType As Type) As Boolean
        Return sourceType = GetType(String)
    End Function

    Public Overrides Function ConvertFrom(context As ITypeDescriptorContext, culture As CultureInfo, value As Object) As Object
        If TypeOf value Is String Then
            Dim Parts As String() = Split(value, "#*")
            Dim GP As New GraphProfile
            GP.SetFormSettings(JsonConvert.DeserializeObject(Parts(0), GetType(Dictionary(Of Object, Object))))
            GP.SetChartSettings(JsonConvert.DeserializeObject(Parts(1), GetType(Dictionary(Of Object, Object))))
            GP.SetDataSettings(JsonConvert.DeserializeObject(Parts(2), GetType(Dictionary(Of Object, Object))))
            GP.SetAddDataSettings(JsonConvert.DeserializeObject(Parts(3), GetType(Dictionary(Of Object, Object))))
            GP.SetFloatingScreenSettings(JsonConvert.DeserializeObject(Parts(4), GetType(Dictionary(Of Object, Object))))
            Return GP
        End If
        Return MyBase.ConvertFrom(context, culture, value)
    End Function

    Public Overrides Function ConvertTo(context As ITypeDescriptorContext, culture As CultureInfo, value As Object, destinationType As Type) As Object
        If GetType(String) = destinationType Then
            Dim GP As GraphProfile = value
            Dim FormSettings As String = JsonConvert.SerializeObject(GP.GetFormSettings)
            Dim ChartSettings As String = JsonConvert.SerializeObject(GP.GetChartSettings)
            Dim DataSettings As String = JsonConvert.SerializeObject(GP.GetDataSettings)
            Dim AddDataSettings As String = JsonConvert.SerializeObject(GP.GetAddDataSettings)
            Dim FloatingScreenSettings As String = JsonConvert.SerializeObject(GP.GetFloatingScreenSettings)
            Return String.Format("{0}#*{1}#*{2}#*{3}#*{4}", FormSettings, ChartSettings, DataSettings, AddDataSettings, FloatingScreenSettings)
        End If
        Return MyBase.ConvertTo(context, culture, value, destinationType)
    End Function
End Class