﻿Imports GodSharp.SerialPort
Imports System.IO
Public Class RGraphConnect

    Public Shared Event GraphStart()

#Region "Variables Generales"
    Public StaticData As Integer = 30
    Public DetectedValue As New List(Of List(Of Double)) From {New List(Of Double), New List(Of Double), New List(Of Double), New List(Of Double), New List(Of Double)}       ''Valores de los datos detectados
    Public DetectedNames As New List(Of List(Of String)) From {New List(Of String), New List(Of String), New List(Of String), New List(Of String), New List(Of String)}       ''Nombres de los datos (se asignan por el usuario)
    Public UsedValues As New List(Of List(Of Double)) From {New List(Of Double), New List(Of Double), New List(Of Double), New List(Of Double), New List(Of Double)}          ''Valores que se utilizan realmente
    Public UsedNames As New List(Of List(Of String)) From {New List(Of String), New List(Of String), New List(Of String), New List(Of String), New List(Of String)}           ''Nombres que se utilizan realmente (se asignan por el usuario)

    Public NameList As New List(Of String) From {"", "", "", "", ""}
    Public ProtocolList As New List(Of String) From {"", "", "", "", ""}
    Public ActiveList As New List(Of Boolean) From {False, False, False, False, False}
    Public OrganizedValues As New List(Of Double) With {.Capacity = 30}

    Public Shared IsReading As Boolean = False
#End Region

    ''---------------------TCP/IP----------------------''

#Region "TCP/IP"
#Region "TCP Properties/Events"
    Private WithEvents TCPServer As TCPServer
    Public Event NewConnection(ByVal ID As Integer)
    Public Event MessageReceived()
    Public Host As String = ""
    Public Port As Integer
    Private Structure Frame
        Public DeviceID As String
        Public ActionID As String
        Public Data As String
    End Structure

    Private TCPDataIndex As Integer = 0
    Public DataName As New Dictionary(Of Integer, String())
    Public DataValues As New Dictionary(Of Integer, Double())
    Public IDRecord As New List(Of Integer)
#End Region
    ''Sub para crear server TCP
    Public Sub TCPConnect(DataIndex As Integer)
        TCPDataIndex = DataIndex
        TCPServer = New TCPServer(Host, Port)
    End Sub

    ''Sub para desconectar server
    Public Sub TCPDisconnect()
        If TCPServer IsNot Nothing Then TCPServer.Close()
    End Sub

    ''Handles de nuevo cliente
    Private Sub ClientConnected(ByVal sender As Integer) Handles TCPServer.NewConnection
        RaiseEvent NewConnection(sender)
    End Sub
    ''Se recibio un nuevo mensaje
    Private Sub NewMessage(ByVal Data As String) Handles TCPServer.MessageReceived
        Dim NewMessageTask As New Task(Sub() NewMessageT(Data))
        NewMessageTask.Start()
    End Sub
    ''Nuevo mensaje
    Private Sub NewMessageT(data As String)
        Try
            Dim ReceivedFrame As New Frame
            Dim FrameData As String() = Split(data, ";")
            Dim StrData(FrameData.Length - 3) As String
            ReceivedFrame.DeviceID = FrameData(0)
            ReceivedFrame.ActionID = FrameData(1)
            For FramePosition = 2 To FrameData.Length - 1
                StrData(FramePosition - 2) = FrameData(FramePosition)
            Next

            Select Case ReceivedFrame.ActionID
                Case 0
                    Dim DoubleArray As Double() = Array.ConvertAll(StrData, New Converter(Of String, Double)(AddressOf DoubleConverter))
                    If DataValues.ContainsKey(ReceivedFrame.DeviceID) Then
                        DataValues.Item(ReceivedFrame.DeviceID) = DoubleArray
                    Else
                        DataValues.Add(ReceivedFrame.DeviceID, DoubleArray)
                    End If
                    AssignData()
                Case 1
                    If DataName.ContainsKey(ReceivedFrame.DeviceID) Then
                        DataName.Item(ReceivedFrame.DeviceID) = StrData
                    Else
                        DataName.Add(ReceivedFrame.DeviceID, StrData)
                        IDRecord.Add(ReceivedFrame.DeviceID)
                    End If
                Case 2
                    If DataName.ContainsKey(ReceivedFrame.DeviceID) Then DataName.Remove(ReceivedFrame.DeviceID)
                    If DataValues.ContainsKey(ReceivedFrame.DeviceID) Then DataValues.Remove(ReceivedFrame.DeviceID)
                    IDRecord.Remove(ReceivedFrame.DeviceID)
            End Select
            RaiseEvent MessageReceived()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Sub AssignData()
        Dim ActualData As Integer = 0
        For CurrentItem = 0 To DetectedNames.Item(TCPDataIndex).Count - 1
            If UsedNames.Item(TCPDataIndex).Contains(DetectedNames.Item(TCPDataIndex).Item(CurrentItem)) Then
                Dim FullString As String() = Split(UsedNames.Item(TCPDataIndex).Item(ActualData), "-")
                Dim DeviceID As Integer = FullString(0)
                Dim DataPosition As Integer = FullString(1)
                UsedValues.Item(TCPDataIndex).Item(ActualData) = DataValues.Item(DeviceID)(DataPosition)
                ActualData += 1
            End If
        Next
    End Sub

    ''Sub para detectar los datos recibidos por nombre
    Public Function TCPDataDetect() As String()
        Try
            DetectedNames.Item(TCPDataIndex).Clear()
            For DataNameItem = 0 To DataName.Count - 1
                For SubDataNameItem = 0 To DataName.Item(IDRecord(DataNameItem)).Count - 1
                    DetectedNames.Item(TCPDataIndex).Add(IDRecord(DataNameItem).ToString() + " - " + SubDataNameItem.ToString() + " - " + DataName.Item(IDRecord(DataNameItem))(SubDataNameItem))
                Next
            Next
            Return DetectedNames.Item(TCPDataIndex).ToArray()
        Catch ex As Exception
            LogError(ex)
            Return {""}
        End Try
    End Function

    ''Se perdio un cliente
    Private Sub LostClient(ByVal sender As Integer) Handles TCPServer.ClientDisconnected
        DataName.Remove(IDRecord.Item(sender))
        DataValues.Remove(IDRecord.Item(sender))
        IDRecord.Remove(sender)
    End Sub

    ''Convertir a double
    Private Function DoubleConverter(ByVal text As String) As Double
        Dim value As Double = 0
        Double.TryParse(text, value)
        Return value
    End Function
#End Region

    ''----------------PROTOCOLO INTERNO----------------''

#Region "PC"

    Private WithEvents FsWatcher As FileSystemWatcher
    Private DetectedData As Integer                         ''Cantidad de datos detectados
    Private PCDataIndex As Integer

    Public Sub InitializePC(DataIndex As Integer)
        PCDataIndex = DataIndex
        FsWatcher = New FileSystemWatcher("C:\QMSoftware\") With {
            .EnableRaisingEvents = True,
            .Filter = "GraphData.graph",
            .NotifyFilter = IO.NotifyFilters.LastWrite
        }
    End Sub

    ''Handler que se triggerea cuando el archivo cambio
    Private Sub PCFileChanged(sender As Object, e As FileSystemEventArgs) Handles FsWatcher.Changed
        If ActiveList.Item(PCDataIndex) And IsReading Then
            Dim PCFCT As New Task(AddressOf FCTask)
            PCFCT.Start()
        End If
    End Sub

    ''Task que se llama para modificar datos
    Private Async Sub FCTask()
        Try
            Await Task.Delay(50)
            Dim ActualData As Integer = 0
            Dim fileReader = File.ReadAllLines("C:\QMSoftware\GraphData.graph")
            DetectedData = fileReader.Length()
            For CurrentData = 0 To DetectedData - 1
                Dim FRCD As String() = fileReader(CurrentData).Split(";")
                DetectedValue.Item(PCDataIndex).Item(CurrentData) = FRCD(1)
                If UsedNames.Item(PCDataIndex).Contains(FRCD(0)) Then
                    UsedValues.Item(PCDataIndex).Item(ActualData) = FRCD(1)
                    ActualData += 1
                End If
            Next
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Sub identico al que detecta los cambios, pero leemos una sola vez todo el archivo y listo
    Public Function PCDataDetect() As String()
        Try
            Dim fileReader = IO.File.ReadAllLines("C:\QMSoftware\GraphData.graph")
            DetectedData = fileReader.Length()
            DetectedNames.Item(PCDataIndex).Clear()
            DetectedValue.Item(PCDataIndex).Clear()
            For CurrentData = 0 To DetectedData - 1
                Dim FRCD As String() = fileReader(CurrentData).Split(";")
                Dim CurrentName As String = FRCD(0)
                Dim CurrentValue As Double = FRCD(1)
                DetectedNames.Item(PCDataIndex).Add(CurrentName)
                DetectedValue.Item(PCDataIndex).Add(CurrentValue)
            Next
            Return DetectedNames.Item(PCDataIndex).ToArray
        Catch ex As Exception
            LogError(ex)
            Return {""}
        End Try
    End Function

    ''Sub para detener lectura
    Public Sub StopPCWatcher()
        FsWatcher = Nothing
    End Sub

#End Region

    ''----------------SERIAL PORT/WITS-----------------''

#Region "SERIAL PORT/WITS"
#Region "SP Variables"
    Private ReadOnly SPList As New List(Of GodSerialPort) From {New GodSerialPort("COM2", 9600, 0, 8, 1, 0), New GodSerialPort("COM2", 9600, 0, 8, 1, 0), New GodSerialPort("COM2", 9600, 0, 8, 1, 0),
                                                                New GodSerialPort("COM2", 9600, 0, 8, 1, 0), New GodSerialPort("COM2", 9600, 0, 8, 1, 0)}
    Public SerialPortName As New List(Of String) From {"", "", "", "", ""}                                                  ''Nombre del puerto serie
    Public BaudRate As New List(Of Integer) From {9600, 9600, 9600, 9600, 9600}                                             ''BaudRate del puerto serie
    Public Parity As New List(Of Parity) From {0, 0, 0, 0, 0}                                                               ''Paridad del puerto serie
    Public DataBits As New List(Of Integer) From {8, 8, 8, 8, 8}                                                            ''DataBits del puerto serie
    Public StopBits As New List(Of StopBits) From {1, 1, 1, 1, 1}                                                           ''StopBits del puerto serie
    Public SPSeparator As New List(Of String) From {";", ";", ";", ";", ";"}                                                ''Separador usado
    Public SPData As New List(Of String) From {"", "", "", "", ""}                                                          ''DataStream leido
    Public IDList As New List(Of List(Of Integer)) From {New List(Of Integer), New List(Of Integer), New List(Of Integer),  ''Lista de ID por si se usa WITS
                                                            New List(Of Integer), New List(Of Integer)}

    Private SPLength As New List(Of Integer) From {0, 0, 0, 0, 0}

    Public BytesToReadList As New List(Of Integer) From {0, 0, 0, 0, 0}

    Public UsedWITSValues As New List(Of List(Of String)) From {New List(Of String), New List(Of String), New List(Of String), New List(Of String), New List(Of String)}

    Public GetFirstData As New List(Of Boolean) From {False, False, False, False, False}                                    ''Priorizamos el primer byte recibido

    Public Event NewData(DataIndex As Integer)                                                                              ''Evento de nuevo dato recibido

    Public SPTmrRead As New List(Of Timer) From {New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}}                  ''Timer de lectura de SP

    Private WithEvents TmrUpdateUsedValues As New Timer() With {.Interval = 1000, .Enabled = False}
#End Region

    ''Delegado para poder utilizar multiples puertos serie y un mismo SUB
    Public Delegate Sub MyDelegate(DataIndex As Integer)

    ''Iniciamos el puerto serie con todos los parametros pasados por el usuario
    Public Sub InitializeSP(ByVal DataIndex As Integer)
        Try
            If Not SPList.Item(DataIndex).IsOpen Then
                SPList.Item(DataIndex) = New GodSerialPort(SerialPortName.Item(DataIndex), BaudRate.Item(DataIndex), Parity.Item(DataIndex), DataBits.Item(DataIndex), StopBits.Item(DataIndex), Handshake.RequestToSendXOnXOff) With {
                    .DtrEnable = True,
                    .RtsEnable = True,
                    .ReadTimeout = 100
                }
                SPList.Item(DataIndex).Open()
                SPList.Item(DataIndex).DiscardInBuffer()

                If ProtocolList.Item(DataIndex) = "Puerto Serie" Then
                    CreateSPDelegate(DataIndex, AddressOf DataReceivedEventSP)
                ElseIf ProtocolList.Item(DataIndex) = "WITS" Then
                    CreateSPDelegate(DataIndex, AddressOf DataReceivedEventWITS)
                End If

                SPTmrRead.Item(DataIndex).Start()
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Creamos el sub DataReceived para el puerto serie en DataIndex
    Private Sub CreateSPDelegate(DataIndex As Integer, del As MyDelegate)
        AddHandler SPTmrRead.Item(DataIndex).Tick, Sub() del(DataIndex)
    End Sub

    ''Desconectamos el puerto serie
    Public Sub SPDisconnect(DataIndex As Integer)
        If SPList.Item(DataIndex) IsNot Nothing Then SPList.Item(DataIndex).Close()
    End Sub

    ''Evento de recepcion de datos
    Private Sub DataReceivedEventSP(ByVal DataIndex As Integer)
        Dim DRTask As New Task(Sub() DataReceivedEventTaskSP(DataIndex))
        DRTask.Start()
    End Sub

    ''Evento de recepcion de datos
    Private Sub DataReceivedEventWITS(ByVal DataIndex As Integer)
        Dim DRTask As New Task(Sub() DataReceivedEventTaskWITS(DataIndex))
        DRTask.Start()
    End Sub

    ''Task DataReceived SP
    Private Sub DataReceivedEventTaskSP(ByVal DataIndex As Integer)
        Try
            If SPList.Item(DataIndex).IsOpen Then
                SPData.Item(DataIndex) = SPList.Item(DataIndex).ReadExisting        ''READLINE
                RaiseEvent NewData(DataIndex)
                SPHandle(SPData.Item(DataIndex), DataIndex)
                If IsReading Then AssignSPData(DataIndex)
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Task DataReceived
    Private Sub DataReceivedEventTaskWITS(ByVal DataIndex As Integer)
        Try
            If SPList.Item(DataIndex).IsOpen Then
                SPData.Item(DataIndex) = SPList.Item(DataIndex).ReadExisting
                RaiseEvent NewData(DataIndex)
                WITSHandle(SPData.Item(DataIndex), DataIndex)
                If IsReading Then AssignSPData(DataIndex)
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Leemos todos los datos correspondientes y los metemos en la lista de valores detectados
    Private Sub SPHandle(ByVal CurrentDataStr As String, ByVal DataIndex As Integer)
        Try
            Dim InData() As String = CurrentDataStr.Split(SPSeparator.Item(DataIndex))
            SPLength.Item(DataIndex) = InData.Length
            If SPLength.Item(DataIndex) = DetectedNames.Item(DataIndex).Count Then
                Dim DoubleList As New List(Of Double)
                For CurrentData = 0 To SPLength.Item(DataIndex) - 1
                    If IsDouble(InData(CurrentData)) Then
                        DoubleList.Add(GetDouble(InData(CurrentData)))
                    Else
                        Dim LastValue As Double
                        If DetectedValue.Item(DataIndex).Count = 0 Then LastValue = 0 Else LastValue = DetectedValue.Item(DataIndex).Item(CurrentData)
                        DoubleList.Add(LastValue)
                    End If
                Next
                DetectedValue.Item(DataIndex) = DoubleList
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Leemos todos los datos correspondientes y los metemos en la lista de valores detectados
    Private Sub WITSHandle(ByVal CurrentDataStr As String, ByVal DataIndex As Integer)
        Try
            Dim InData() As String = CurrentDataStr.Split(vbNewLine)
            If InData(0) = "&&" Then
                InData = InData.Select(Function(s) s.Replace(vbLf, "").Replace(vbCr, "")).ToArray()
                Dim DoubleList As New List(Of Double)
                Dim UsedWITSValuesList As New List(Of String)
                If InData.Length - 3 = DetectedNames.Item(DataIndex).Count Then
                    For CurrentData = 0 To DetectedNames.Item(DataIndex).Count - 1
                        If InData(CurrentData + 1).StartsWith(IDList.Item(DataIndex).Item(CurrentData).ToString()) Then
                            UsedWITSValuesList.Add(InData(CurrentData + 1))
                            DoubleList.Add(Replace(InData(CurrentData + 1), IDList.Item(DataIndex).Item(CurrentData).ToString(), "", 1, 1))
                        End If
                    Next
                    UsedWITSValues.Item(DataIndex) = UsedWITSValuesList
                    DetectedValue.Item(DataIndex) = DoubleList
                End If
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Segun los nombres que se elije usar, asignamos los datos
    Private Sub AssignSPData(ByVal DataIndex As Integer)
        Try
            If UsedValues.Item(DataIndex).Count = UsedNames.Item(DataIndex).Count Then
                Dim ActualData As Integer = 0
                For CurrentData = 0 To UsedNames.Item(DataIndex).Count - 1
                    If DetectedNames.Item(DataIndex).Contains(UsedNames.Item(DataIndex).Item(CurrentData)) Then
                        UsedValues.Item(DataIndex).Item(ActualData) = DetectedValue.Item(DataIndex).Item(DetectedNames.Item(DataIndex).IndexOf(UsedNames.Item(DataIndex).Item(CurrentData)))
                        ActualData += 1
                    End If
                Next
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    ''Sub para saber si la estructura que se recibio es la correcta
    Public Function SPDataDetect(ByVal DataIndex As Integer) As String()
        Try
            If DetectedNames.Item(DataIndex).Count = DetectedValue.Item(DataIndex).Count Then
                Return DetectedNames.Item(DataIndex).ToArray
            ElseIf Not SPList.Item(DataIndex).IsOpen Then
                Return {"Configurar puerto serie."}
            Else
                Return {"Estructura incorrecta", "Se detectaron " + (SPLength.Item(DataIndex)).ToString() + " datos."}
            End If
        Catch ex As Exception
            LogError(ex)
            Return {""}
        End Try
    End Function
#End Region

    ''---------------------GENERAL---------------------''

#Region "GENERAL"
    ''Constructor
    Private Sub New()
        Dim ZeroData(StaticData - 1) As Double
        OrganizedValues.AddRange(ZeroData)
    End Sub

    Private Shared NewRGraphConnect

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton
    Public Shared Function GetInstancia()
        If NewRGraphConnect Is Nothing Then
            NewRGraphConnect = New RGraphConnect()
        End If
        Return NewRGraphConnect
    End Function

    ''Sub para iniciar vectores
    Public Sub InitVectors(DataIndex As Integer)
        ClearVectors(DataIndex)
        Dim ZeroData(UsedNames.Item(DataIndex).Count - 1) As Double
        UsedValues.Item(DataIndex).AddRange(ZeroData)
        ActiveList.Item(DataIndex) = True
    End Sub

    ''Sub para limpiar vectores
    Public Sub ClearVectors(DataIndex As Integer)
        UsedValues.Item(DataIndex).Clear()
        ActiveList.Item(DataIndex) = False
    End Sub

    ''Empieza la lectura
    Public Sub StartRead()
        Dim ZeroData(StaticData - 1) As Double
        OrganizedValues.AddRange(ZeroData)
        For Each SP As GodSerialPort In SPList
            If Not SP.IsOpen Then
                SP.Open()
            End If
        Next
        IsReading = True
        TmrUpdateUsedValues.Start()
        RaiseEvent GraphStart()
    End Sub

    ''Termina la lectura
    Public Sub StopRead()
        IsReading = False
        StopPCWatcher()
        'For Each SP As GodSerialPort In SPList
        '    SP.Close()
        'Next
        TmrUpdateUsedValues.Stop()
        RaiseEvent GraphStart()
    End Sub

    ''Sub para actualizar los valores usados
    Public Sub UpdateUsedValues() Handles TmrUpdateUsedValues.Tick
        Dim UsedValueList As New List(Of Double)
        For ThisObject = 0 To 4
            If IsReading And ActiveList.Item(ThisObject) Then
                UsedValueList.AddRange(UsedValues.Item(ThisObject))
            End If
        Next
        OrganizedValues = UsedValueList
    End Sub


    ''Devuelve una lista con todos los nombres
    Public Function GetUsedNames() As List(Of String)
        Dim UsedNamesList As New List(Of String)
        For ThisEntry = 0 To 4
            If ActiveList.Item(ThisEntry) Then
                UsedNamesList.AddRange(UsedNames.Item(ThisEntry))
            End If
        Next
        Return UsedNamesList
    End Function
#End Region
End Class