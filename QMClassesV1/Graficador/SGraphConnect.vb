﻿Imports System.IO
Public Class SGraphConnect

    Private WithEvents PCTmrWrite As New Timer() With {.Interval = 1000, .Enabled = False}
    Private WithEvents TCPTmrWrite As New Timer() With {.Interval = 1000, .Enabled = False}
    Public Shared Event GraphStart()

#Region "Properties"
    Private Shared _protocol As String = ""
    Public Shared Property Protocol() As String
        Get
            Return _protocol
        End Get
        Set(value As String)
            If value <> _protocol Then
                _protocol = value
            End If
        End Set
    End Property
    Private Shared _datanombre As String()
    Public Shared Property DataNombre() As String()
        Get
            Return _datanombre
        End Get
        Set(value As String())
            If value IsNot _datanombre Then
                _datanombre = value
            End If
        End Set
    End Property
    Private Shared _active As Boolean
    Public Shared Property Active() As Boolean
        Get
            Return _active
        End Get
        Set(value As Boolean)
            If value <> _active Then
                _active = value
            End If
        End Set
    End Property
    Private Shared _values As String()
    Public Shared Property Values() As String()
        Get
            Return _values
        End Get
        Set(value As String())
            If value IsNot _values Then
                _values = value
            End If
        End Set
    End Property

#End Region

#Region "TCP Properties"
    Private TCPClient As TCPSingleClient
    Public Shared ID As Integer
    Private Structure Frame
        Public DeviceID As String
        Public ActionID As String
        Public Data As String
    End Structure
#End Region

    Public Sub StartWrite()
        Select Case Protocol
            Case "PC"
                If Not Directory.Exists("C:\QMSoftware\") Then Directory.CreateDirectory("C:\QMSoftware\")
                PCTmrWrite.Start()
            Case "TCP"
                UpdateNames()
                TCPTmrWrite.Start()
        End Select
        Active = True
        RaiseEvent GraphStart()
    End Sub

    Public Sub StopWrite()
        Select Case Protocol
            Case "PC"
                PCTmrWrite.Stop()
            Case "TCP"
                TCPTmrWrite.Stop()
                StopConection()
        End Select
        Active = False
    End Sub

#Region "PC Protocol"

    Private Sub FileWrite() Handles PCTmrWrite.Tick
        Dim FWT As New Task(AddressOf FileWriteTask)
        FWT.Start()
    End Sub

    Private Sub FileWriteTask()
        Try
            If Values IsNot Nothing Then
                Dim Obj As New List(Of String)
                For i = 0 To DataNombre.Length - 1
                    Obj.Add(DataNombre(i) + ";" + Values(i))
                Next
                File.WriteAllLines("C:\QMSoftware\GraphData.graph", Obj)
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

#End Region

#Region "TCP Protocol"
    ''Funcion que devuelve true si nos conectamos al servidor
    Public Function TCPConnect(ByVal Host As String, ByVal Port As Integer) As Boolean
        Try
            TCPClient = New TCPSingleClient(Host, Port)
            Return True
        Catch ex As Exception
            LogError(ex)
            Return False
        End Try
    End Function
    ''Sub para desconectar la conexion
    Public Sub TCPDisconnect()
        If TCPClient IsNot Nothing Then TCPClient.Disconnect()
    End Sub
    ''Timer de envio de datos
    ''Action ID identifica el tipo de accion
    ''Action ID = 0 -> envio de datos
    ''Action ID = 1 -> envio de nombres
    ''Action ID = 2 -> finalizacion de conexion -> borrado de variables
    ''El orden de los datos tiene que respetar los nombres
    Private Sub TCPTmrWrite_Tick() Handles TCPTmrWrite.Tick
        Try
            SendData(Values, ID, 0)
        Catch ex As Exception
            TCPTmrWrite.Stop()
            LogError(ex)
        End Try
    End Sub
    ''Sub para enviar los nuevos nombres
    Private Sub UpdateNames()
        Try
            SendData(DataNombre, ID, 1)
        Catch ex As Exception
            TCPTmrWrite.Stop()
            LogError(ex)
        End Try
    End Sub

    ''Sub para finalizar envio de datos
    Public Sub StopConection()
        Try
            SendData({""}, ID, 2)
        Catch ex As Exception
            TCPTmrWrite.Stop()
            LogError(ex)
        End Try
    End Sub
    ''Sub para enviar datos
    Public Sub SendData(ByVal Data() As String, ByVal DeviceID As Integer, ByVal ActionID As Integer)
        Dim NewFrame As New Frame With {
            .DeviceID = DeviceID.ToString() + ";",
            .ActionID = ActionID.ToString() + ";",
            .Data = Join(Data, ";")
        }
        TCPClient.SendAsync(StructToString(NewFrame))
    End Sub

    Private Function StructToString(obj As Object) As String
        Return String.Join("", obj.GetType().GetFields().Select(Function(field) field.GetValue(obj)))
    End Function
#End Region

End Class
