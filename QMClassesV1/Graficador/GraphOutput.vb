﻿Public Class GraphOutput

    Private IsWriting As Boolean = False

    Public UsedValues As New List(Of String) From {"", "", "", "", ""}
    Private ReadOnly SPList As New List(Of SerialPort) From {New SerialPort, New SerialPort, New SerialPort, New SerialPort, New SerialPort}
    Public SerialPortName As New List(Of String) From {"", "", "", "", ""}                                                  ''Nombre del puerto serie
    Public BaudRate As New List(Of Integer) From {9600, 9600, 9600, 9600, 9600}                                             ''BaudRate del puerto serie
    Public Parity As New List(Of Parity) From {0, 0, 0, 0, 0}                                                               ''Paridad del puerto serie
    Public DataBits As New List(Of Integer) From {8, 8, 8, 8, 8}                                                            ''DataBits del puerto serie
    Public StopBits As New List(Of StopBits) From {1, 1, 1, 1, 1}                                                           ''StopBits del puerto serie
    Public ProtocolList As New List(Of String) From {"", "", "", "", ""}                                                    ''Protocolo por el cual se envia
    Public SPSeparador As New List(Of String) From {";", "", "", "", ""}
    Public ActiveList As New List(Of String) From {False, False, False, False, False}
    Public EntryDataIndex As New List(Of Integer) From {0, 0, 0, 0, 0}

    Public SPTmrOutput As New List(Of Timer) From {New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}, New Timer() With {.Interval = 1000, .Enabled = False},
                                                    New Timer() With {.Interval = 1000, .Enabled = False}}                  ''Timer SP

    Public UsedWITSIDs As New List(Of List(Of Integer)) From {New List(Of Integer), New List(Of Integer), New List(Of Integer), New List(Of Integer), New List(Of Integer)}
    Public UsedWITSIndexes As New List(Of List(Of Integer)) From {New List(Of Integer), New List(Of Integer), New List(Of Integer), New List(Of Integer), New List(Of Integer)}

    Private Shared GraphOutput
    Public Shared GraphConnector As RGraphConnect

    Private StringConvert As New Converter(Of Double, String)(AddressOf StringConverter)

    Public Terminator As New List(Of String) From {"", "", "", "", ""}

    ''Devuelve una instancia unica de GraphOutput - Funciona como Singleton
    Public Shared Function GetInstancia()
        If GraphOutput Is Nothing Then
            GraphOutput = New GraphOutput()
        End If
        Return GraphOutput
    End Function

    ''Delegado para poder utilizar multiples puertos serie y un mismo SUB
    Dim Handler() As EventHandler = {Sub() SerialPortTimer(0), Sub() SerialPortTimer(1), Sub() SerialPortTimer(2), Sub() SerialPortTimer(3), Sub() SerialPortTimer(4)}

    ''Iniciamos el puerto serie con todos los parametros pasados por el usuario
    Private Sub InitializeSP(ByVal DataIndex As Integer)
        Try
            If Not SPList.Item(DataIndex).IsOpen Then
                RemoveHandler SPTmrOutput.Item(DataIndex).Tick, Handler(DataIndex)
                With SPList.Item(DataIndex)
                    .PortName = SerialPortName.Item(DataIndex)
                    .BaudRate = BaudRate.Item(DataIndex)
                    .Parity = Parity.Item(DataIndex)
                    .DataBits = DataBits.Item(DataIndex)
                    .StopBits = StopBits.Item(DataIndex)
                    .RtsEnable = True
                    .DtrEnable = True
                    .WriteTimeout = 100
                    .Open()
                    .DiscardInBuffer()
                End With
                CreateTimerDelegate(DataIndex)
                SPTmrOutput.Item(DataIndex).Start()
            End If
        Catch ex As Exception
            ActiveList.Item(DataIndex) = False
            LogError(ex)
        End Try
    End Sub
    ''Creamos el sub SerialPortTimer para el puerto serie en DataIndex
    Private Sub CreateTimerDelegate(DataIndex As Integer)
        AddHandler SPTmrOutput.Item(DataIndex).Tick, Handler(DataIndex)
    End Sub

    ''Sub que recibe la informacion y la transforma en UsedValues
    Private Sub SerialPortTimer(ByVal DataIndex As Integer)
        Dim SPTimerTask As New Task(Sub() SerialPortTimerTask(DataIndex))
        SPTimerTask.Start()
    End Sub

    Private Sub SerialPortTimerTask(ByVal DataIndex As Integer)
        Try
            If IsWriting Then
                Select Case ProtocolList.Item(DataIndex)
                    Case "Puerto Serie"
                        UsedValues.Item(DataIndex) = Join(Array.ConvertAll(GraphConnector.UsedValues.Item(EntryDataIndex.Item(DataIndex)).ToArray, StringConvert), SPSeparador.Item(DataIndex))
                        SPList.Item(DataIndex).WriteLine(UsedValues.Item(DataIndex))
                    Case "WITS"
                        SPList.Item(DataIndex).Write("&&" + Terminator(DataIndex))
                        For Item = 0 To UsedWITSIDs.Item(DataIndex).Count - 1
                            Dim ID = UsedWITSIDs.Item(DataIndex).Item(Item)
                            Dim Value = GraphConnector.DetectedValue.Item(EntryDataIndex(DataIndex)).Item(UsedWITSIndexes.Item(DataIndex).Item(Item))
                            SPList.Item(DataIndex).Write(ID)
                            SPList.Item(DataIndex).Write(Value.ToString() + Terminator(DataIndex))
                        Next
                        SPList.Item(DataIndex).Write("!!" + Terminator(DataIndex))
                End Select
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Function StringConverter(ByVal dbl As Double) As String
        Dim value As String = Convert.ToString(dbl)
        Return value
    End Function

    Public Sub StartWrite()
        IsWriting = True
        For Item = 0 To ActiveList.Count - 1
            If ActiveList(Item) Then InitializeSP(Item)
        Next
    End Sub

    Public Sub StopWrite()
        Dim CurrentIndex As Integer = 0
        For Each SP As SerialPort In SPList
            SP.Close()
            RemoveHandler SPTmrOutput.Item(CurrentIndex).Tick, Handler(CurrentIndex)
            CurrentIndex += 1
        Next
        IsWriting = False
    End Sub
End Class
