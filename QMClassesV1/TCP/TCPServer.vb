﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Public Class TCPServer

    Public Shared Event MessageReceived(Data As String)
    Public Shared Event NewConnection(sender As Integer)
    Public Shared Event ClientDisconnected(sender As Integer)
    ' SERVER CONFIG
    Public Server As TcpListener

    Public IsListening As Boolean = True

    ' CLIENTS
    Private Client(0) As TcpClient
    Private ClientData(0) As StreamReader
    Private CurrentClient As Integer = 0

    Public Sub New(ByVal IP As String, ByVal ServerPort As Integer)
        Server = New TcpListener(IPAddress.Parse(IP), ServerPort)
        Server.Start()
        Dim NewClientTask As New Task(AddressOf NewClientListener)
        NewClientTask.Start()
    End Sub

    Public Sub Close()
        Server.Stop()
        IsListening = False
    End Sub

    Private Async Sub NewClientListener()
        Do Until IsListening = False
            If Server.Pending = True Then
                Client(CurrentClient) = Server.AcceptTcpClient
                RaiseEvent NewConnection(CurrentClient)
                Dim NewDataTask As New Task(Sub() NewDataListener(CurrentClient))
                NewDataTask.Start()
            End If
            Await Task.Delay(100)
        Loop
    End Sub

    Private Async Sub NewDataListener(ByVal ThisClient As Integer)
        Dim ThisClientLocal As Integer = ThisClient
        CurrentClient += 1
        ReDim Preserve Client(CurrentClient)
        ReDim Preserve ClientData(CurrentClient)
        While (1)
            If Client(ThisClientLocal).Available > 0 Then
                ClientData(ThisClientLocal) = New StreamReader(Client(ThisClientLocal).GetStream)
                RaiseEvent MessageReceived(ClientData(ThisClientLocal).ReadLine)
            End If
            Await Task.Delay(1)
        End While
    End Sub

End Class
