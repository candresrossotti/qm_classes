﻿
Imports System.Net.Sockets
Imports System.IO
Public Class TCPSingleClient

    Public Client As TcpClient
    Public DataStream As StreamWriter

    Public Sub New(ByVal Host As String, ByVal Port As Integer)
        Client = New TcpClient(Host, Port)
        DataStream = New StreamWriter(Client.GetStream)
    End Sub

    Public Sub SendAsync(ByVal Data As String)
        Dim SendTask As New Task(Sub() Send(Data))
        SendTask.Start()
    End Sub

    Private Sub Send(ByVal Data As String)
        Try
            DataStream.Write(Data & vbCrLf)
            DataStream.Flush()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub Disconnect()
        Client.Close()
    End Sub
End Class
