﻿Imports EasyModbus
Public Class IBlenderSystem

    Inherits MoxaHandle

#Region "STOP/RESET FUNCTIONS"
    Public Sub StopAll()

        ''Detenemos los LAS
        StopLAS(1)
        StopLAS(2)
        StopLAS(3)
        StopLAS(4)

        ''Detenemos los DAS
        StopDAS(1)
        StopDAS(2)

        ''Detenemos los tornillos
        StopTornillo(1)
        StopTornillo(2)

        ''Detenemos las centrifugas
        StopCentrifuga("Entrada")
        StopCentrifuga("Salida")

        ''Enviamos señal de reset al blender para que no se pueda usar hasta resetear
        ''MoxaSend("ResetBlender",1)

    End Sub

    Public Sub StopLAS(ByVal currentLAS As Integer)
        ''Setpoint a cero
    End Sub

    Public Sub StopDAS(ByVal currentDAS As Integer)
        ''Setpoint a cero
    End Sub

    Public Sub StopTornillo(ByVal currentTornillo As Integer)
        ''Setpoint a cero
    End Sub

    Public Sub StopCentrifuga(ByVal currentCentrifuga As String)
        ''Setpoint a cero
    End Sub

    Public Sub ResetBlender()
        ''MoxaSend("ResetBlender",0)
    End Sub

#End Region

End Class
