﻿Public Class CAN

    Public Shared m_devtype As Integer
    Public Shared m_connect As Byte
    Public Shared m_devind As Integer
    Public Shared m_cannum As Integer
    Public Shared cannum As Integer
    Public Shared code As Object
    Public Shared mask As Integer
    Private Shared InitConfig As VCI_INIT_CONFIG
    Private Shared status1 As VCI_CAN_STATUS
    Public Shared Mode As Byte
    Public Timing0, Timing1, filtertype As Object
    Public Shared DataDictionary As New Dictionary(Of String, Double) From {
        {"Dato1", 0},
        {"Dato2", 0},
        {"Dato3", 0},
        {"Dato4", 0},
        {"Dato5", 0},
        {"Dato6", 0},
        {"Dato7", 0},
        {"Dato8", 0},
        {"Dato9", 0},
        {"Dato10", 0},
        {"Dato11", 0},
        {"Dato12", 0},
        {"Dato13", 0},
        {"Dato14", 0}
    }
    Private Shared DataOut As VCI_CAN_OBJ

    Private WithEvents TmrRead As New Timer() With {.Interval = 1000, .Enabled = False}

    Public Function ConnectCAN() As Boolean
        Try
            'CONFIGURACION CAN
            m_devtype = 3 'con esto elijo que el protocolo sea el 1.0 si pongo cuatro elijo el 2.0 
            cannum = 0 'con este parametro decido si enviar por por el can1 para el cual hay q poner 0 o el can2 para el cual hay q elegir el 1
            filtertype = 1 '1 recibe todas las tramas, 2 recibe tramas standar, 3 recibe tramas extendidas 

            code = Val("&H" + "00000000")
            mask = Val("&H" + "ffffffff")
            Timing0 = Val("&H" + "01")
            Timing1 = Val("&H" + "1c") ' velocidad 250k

            InitConfig.AccCode = code
            InitConfig.AccMask = mask
            InitConfig.Filter_Renamed = filtertype
            InitConfig.Mode = Mode
            InitConfig.Timing0 = Timing0
            InitConfig.Timing1 = Timing1
            If VCI_OpenDevice(m_devtype, 0, 0) <> 1 Then
                Return False
            Else
                If VCI_InitCAN(m_devtype, 0, cannum, InitConfig) = 1 Then
                    m_connect = 1
                    m_devind = 0
                    m_cannum = cannum
                Else
                    Return False
                End If
            End If
            'desde aca comienza el strart de can
            If VCI_StartCAN(m_devtype, m_devind, m_cannum) <> 1 Then

                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            LogError(ex)
            Return False
        End Try
        Return False
    End Function

    Public Sub DisconnectCAN()
        Try
            VCI_CloseDevice(m_devtype, 0)
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub
    Public Sub StartRead()
        TmrRead.Start()
    End Sub

    Public Sub StopRead()
        TmrRead.Stop()
    End Sub
    Private Sub ReadCAN() Handles TmrRead.Tick
        Try
            If VCI_ReadCANStatus(m_devtype, m_devind, m_cannum, status1) <> 0 Then

                Dim ErrInfo(0) As VCI_ERR_INFO
                Dim data(7) As Byte
                Dim length As Long
                Dim SendFrame(49) As VCI_CAN_OBJ

                length = VCI_Receive(m_devtype, m_devind, m_cannum, SendFrame(0), 50, 10)


                For j = 0 To length - 1
                    Select Case (SendFrame(j).ID)
                        Case 1000
                            DataDictionary("Dato1") = SendFrame(j).data1 * 256 + SendFrame(j).data0
                            DataDictionary("Dato2") = SendFrame(j).data3 * 256 + SendFrame(j).data2
                            DataDictionary("Dato3") = SendFrame(j).data5 * 256 + SendFrame(j).data4
                            DataDictionary("Dato4") = SendFrame(j).data7 * 256 + SendFrame(j).data6
                        Case 1001
                            DataDictionary("Dato5") = SendFrame(j).data1 * 256 + SendFrame(j).data0
                            DataDictionary("Dato6") = SendFrame(j).data3 * 256 + SendFrame(j).data2
                            DataDictionary("Dato7") = SendFrame(j).data5 * 256 + SendFrame(j).data4
                            DataDictionary("Dato8") = SendFrame(j).data7 * 256 + SendFrame(j).data6
                        Case 1002
                            DataDictionary("Dato9") = SendFrame(j).data1 * 256 + SendFrame(j).data0
                            DataDictionary("Dato10") = SendFrame(j).data3 * 256 + SendFrame(j).data2
                            DataDictionary("Dato11") = SendFrame(j).data5 * 256 + SendFrame(j).data4
                            DataDictionary("Dato12") = SendFrame(j).data7 * 256 + SendFrame(j).data6
                        Case 1003
                            DataDictionary("Dato13") = SendFrame(j).data1 * 256 + SendFrame(j).data0
                            DataDictionary("Dato14") = SendFrame(j).data3 * 256 + SendFrame(j).data2
                    End Select
                Next
                VCI_ClearBuffer(m_devtype, m_devind, m_cannum)
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub SendCAN(value As Byte(), ID As Integer)
        Try
            If VCI_ReadCANStatus(m_devtype, m_devind, m_cannum, status1) <> 0 Then

                Array.Resize(value, 8)
                DataOut.ID = ID
                DataOut.DataLen = 8
                DataOut.RemoteFlag = 0
                DataOut.ExternFlag = 0
                DataOut.data0 = value(0)
                DataOut.data1 = value(1)
                DataOut.data2 = value(2)
                DataOut.data3 = value(3)
                DataOut.data4 = value(4)
                DataOut.data5 = value(5)
                DataOut.data6 = value(6)
                DataOut.data7 = value(7)

                VCI_Transmit(m_devtype, m_devind, m_cannum, DataOut, 1)
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

End Class
