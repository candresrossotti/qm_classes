﻿Imports System.IO
Imports System.Timers
Imports CsvHelper
Imports CsvHelper.Configuration
Imports XL = Microsoft.Office.Interop.Excel

Public Class CSV

    Private ReadOnly CSV_STREAMWRITER As StreamWriter
    Private ReadOnly CSV_CONFIGURATION As New CsvConfiguration(Globalization.CultureInfo.InvariantCulture)
    Private ReadOnly CSV_WRITER As CsvWriter
    Private ReadOnly OPPath As String
    Private ReadOnly GFracName As String
    Private ReadOnly CreateExcel As Boolean

    ' Private WithEvents FlushTimer As New Timer

    Public GenerateRPMGraph As Boolean
    Public GenerateCaudalGraph As Boolean
    Public GeneratePresionGraph As Boolean
    Public GeneratePresCaudGraph As Boolean
    Public GenerateEngTransGraph As Boolean
    Public GeneratePumpGraph As Boolean

    Public Sub New()

    End Sub

    Public Sub New(ByVal FracName As String, ByVal Path As String, ByVal GenerateExcel As Boolean, ByVal TimerInterval As Integer)
        Try
            'FlushTimer.Interval = TimerInterval
            CreateExcel = GenerateExcel
            GFracName = FracName
            OPPath = Path + Date.Today.Year.ToString() + "-" + Date.Today.Month.ToString().PadLeft(2, "0") + "-" + Date.Today.Day.ToString().PadLeft(2, "0") + "\" + "Iniciado " + TimeOfDay.Hour.ToString().PadLeft(2, "0") + "." + TimeOfDay.Minute.ToString().PadLeft(2, "0")
            Directory.CreateDirectory(OPPath)
            If File.Exists(OPPath + "\Fractura " + FracName + ".txt") Then
                File.Delete(OPPath + "\Fractura " + FracName + ".txt")
            End If
            CSV_STREAMWRITER = New StreamWriter(OPPath + "\Fractura " + FracName + ".txt")
            CSV_WRITER = New CsvWriter(CSV_STREAMWRITER, CSV_CONFIGURATION)
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub


    Public Sub WriteFields(ByVal Fields As ICollection(Of String))
        Try
            If CSV_WRITER Is Nothing Then Return
            CSV_WRITER.WriteField(Fields)
            CSV_WRITER.NextRecord()
            ' FlushTimer.Start()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub WriteRecord(ByVal Record As ICollection(Of String))
        Try
            If CSV_WRITER Is Nothing Then Return
            CSV_WRITER.WriteField(Record)
            CSV_WRITER.NextRecord()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub WriteRecord(ByVal Record As ICollection(Of Object))
        Try
            If CSV_WRITER Is Nothing Then Return
            For CurrentRecord = 0 To Record.Count - 1
                CSV_WRITER.WriteField(Record(CurrentRecord))
            Next
            CSV_WRITER.NextRecord()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Public Sub CloseCSV()
        Try
            '  FlushTimer.Stop()
            If CSV_STREAMWRITER IsNot Nothing Then CSV_STREAMWRITER.Close()
            If CreateExcel Then
                Dim GenExcel As New Task(AddressOf GenerateExcel)
                GenExcel.Start()
            End If
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    'Private Async Sub FlushData() Handles FlushTimer.Elapsed
    '    'Await CSV_WRITER.FlushAsync()
    'End Sub

    Public Function Path() As String
        Return OPPath + "\Fractura " + GFracName + ".txt"
    End Function
    Public Sub GenerateExcel(Optional Path = "")
        Dim Exl As New XL.Application()
        Dim GeneralPath As String
        Try
            Dim wb1 As Workbook
            If Path <> "" Then
                GeneralPath = Path
                wb1 = Exl.Workbooks.Open(Path, Format:=2)
            Else
                GeneralPath = OPPath + "\Fractura " + GFracName + ".txt"
                wb1 = Exl.Workbooks.Open(GeneralPath, Format:=2)
            End If

            Dim r As Range
            With wb1.ActiveSheet
                .Name = "General"
                .UsedRange.Columns.AutoFit
                r = .Range("A1").CurrentRegion
                .ListObjects.Add(XlListObjectSourceType.xlSrcRange, r, , XlYesNoGuess.xlYes).Name = "Tabla1"
            End With

            If GenerateRPMGraph = True Then
                AddGraph(wb1, "RPM", "RPM", "EngineRPM")
            End If
            If GeneratePresionGraph = True Then
                AddGraph(wb1, "Presion", "PSI", "Presion")
            End If
            If GenerateCaudalGraph = True Then
                AddGraph(wb1, "Caudal", "BBL/MIN", "Caudal")
            End If
            If GeneratePresCaudGraph = True Then
                Add2Graph(wb1, "Presión - Caudal", "Presión", "PSI", "Presion", "Caudal", "BBL/MIN", "Caudal")
            End If
            If GenerateEngTransGraph = True Then
                AddEngTransGraph(wb1)
            End If
            If GeneratePumpGraph = True Then
                AddPumpGraph(wb1)
            End If


            wb1.SaveAs(Replace(GeneralPath, ".txt", ".xlsx"), FileFormat:=XlFileFormat.xlOpenXMLWorkbook)
            wb1.Close()
            Exl.Quit()
        Catch ex As Exception
            LogError(ex)
            Exl.DisplayAlerts = False
            Exl.Quit()
            For Each Proccess In Process.GetProcessesByName("EXCEL")
                Proccess.Kill()
            Next
        End Try
    End Sub

    Private Sub AddGraph(ByVal Wb As Workbook, ByVal Title As String, ByVal Unit As String, ByVal Data As String)
        Wb.Sheets.Add()
        Wb.ActiveSheet.Name = Title
        Wb.ActiveSheet.Shapes.AddChart2(240, XlChartType.xlXYScatterSmoothNoMarkers).Select
        Wb.Application.CutCopyMode = False
        Wb.ActiveChart.ChartTitle.Text = Title
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Text = Unit
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelSpacing = 10
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(1).Name = Data
        Wb.ActiveChart.FullSeriesCollection(1).Values = "Tabla1[" + Data + "]"
        Wb.ActiveChart.ChartArea.Select()
        Wb.ActiveChart.FullSeriesCollection(1).XValues = "Tabla1[Hora]"
        Wb.ActiveChart.ChartArea.Left = Wb.Worksheets(Title).Cells(1).Left
        Wb.ActiveChart.ChartArea.Top = Wb.Worksheets(Title).Cells(1).Top
        Wb.ActiveChart.SetElement(334)
        Wb.ActiveChart.ChartArea.Width = 1400
        Wb.ActiveChart.ChartArea.Height = 535
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow
        Wb.ActiveChart.SetElement(Microsoft.Office.Core.MsoChartElementType.msoElementLegendRight)
    End Sub
    Private Sub Add2Graph(ByVal Wb As Workbook, ByVal Title As String, ByVal Title1 As String, ByVal Unit1 As String, ByVal Data1 As String, ByVal Title2 As String, ByVal Unit2 As String, ByVal Data2 As String)
        Wb.Sheets.Add()
        Wb.ActiveSheet.Name = Title
        Wb.ActiveSheet.Shapes.AddChart2(240, XlChartType.xlXYScatterSmoothNoMarkers).Select
        Wb.Application.CutCopyMode = False
        Wb.ActiveChart.ChartTitle.Text = Title
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Text = Unit1
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelSpacing = 10
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(1).Name = Title1
        Wb.ActiveChart.FullSeriesCollection(1).Values = "=Tabla1[" + Data1 + "]"
        Wb.ActiveChart.FullSeriesCollection(1).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(2).Name = Title2
        Wb.ActiveChart.FullSeriesCollection(2).Values = "=Tabla1[" + Data2 + "]"
        Wb.ActiveChart.FullSeriesCollection(2).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(2).AxisGroup = XlAxisGroup.xlSecondary
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Text = Unit2
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.ChartArea.Left = Wb.Worksheets(Title).Cells(1).Left
        Wb.ActiveChart.ChartArea.Top = Wb.Worksheets(Title).Cells(1).Top
        Wb.ActiveChart.SetElement(334)
        Wb.ActiveChart.ChartArea.Width = 1400
        Wb.ActiveChart.ChartArea.Height = 535
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow
        Wb.ActiveChart.SetElement(Microsoft.Office.Core.MsoChartElementType.msoElementLegendRight)
    End Sub

    Private Sub AddEngTransGraph(ByVal Wb As Workbook)
        Wb.Sheets.Add()
        Wb.ActiveSheet.Name = "Motor - Transmision"
        Wb.ActiveSheet.Shapes.AddChart2(240, XlChartType.xlXYScatterSmoothNoMarkers).Select
        Wb.Application.CutCopyMode = False
        Wb.ActiveChart.ChartTitle.Text = "Motor - Transmision"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Text = "°C"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelSpacing = 10
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(1).Name = "Temp. Agua Motor"
        Wb.ActiveChart.FullSeriesCollection(1).Values = "=Tabla1[EngCoolantTemp]"
        Wb.ActiveChart.FullSeriesCollection(1).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(2).Name = "Temp. Aceite Motor"
        Wb.ActiveChart.FullSeriesCollection(2).Values = "=Tabla1[EngOilTemp]"
        Wb.ActiveChart.FullSeriesCollection(2).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(2).AxisGroup = XlAxisGroup.xlPrimary
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(3).Name = "Temp. Transmision"
        Wb.ActiveChart.FullSeriesCollection(3).Values = "=Tabla1[TransOilTemp]"
        Wb.ActiveChart.FullSeriesCollection(3).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(3).AxisGroup = XlAxisGroup.xlPrimary


        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(4).Name = "Presion Aceite Motor"
        Wb.ActiveChart.FullSeriesCollection(4).Values = "=Tabla1[EngOilPres]"
        Wb.ActiveChart.FullSeriesCollection(4).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(4).AxisGroup = XlAxisGroup.xlSecondary
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(5).Name = "Presion Trans."
        Wb.ActiveChart.FullSeriesCollection(5).Values = "=Tabla1[TransOilPres]"
        Wb.ActiveChart.FullSeriesCollection(5).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(5).AxisGroup = XlAxisGroup.xlSecondary
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Text = "PSI"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12

        Wb.ActiveChart.ChartArea.Left = Wb.Worksheets("Motor - Transmision").Cells(1).Left
        Wb.ActiveChart.ChartArea.Top = Wb.Worksheets("Motor - Transmision").Cells(1).Top
        Wb.ActiveChart.SetElement(334)
        Wb.ActiveChart.ChartArea.Width = 1400
        Wb.ActiveChart.ChartArea.Height = 535
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow
        Wb.ActiveChart.SetElement(Microsoft.Office.Core.MsoChartElementType.msoElementLegendRight)
    End Sub

    Private Sub AddPumpGraph(ByVal Wb As Workbook)
        Wb.Sheets.Add()
        Wb.ActiveSheet.Name = "Bomba"
        Wb.ActiveSheet.Shapes.AddChart2(240, XlChartType.xlXYScatterSmoothNoMarkers).Select
        Wb.Application.CutCopyMode = False
        Wb.ActiveChart.ChartTitle.Text = "Bomba"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Text = "°C"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlPrimary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelSpacing = 10
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(1).Name = "Temp. Lubricacion"
        Wb.ActiveChart.FullSeriesCollection(1).Values = "=Tabla1[LubTemp]"
        Wb.ActiveChart.FullSeriesCollection(1).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(2).Name = "Presion Succion"
        Wb.ActiveChart.FullSeriesCollection(2).Values = "=Tabla1[SuccPres]"
        Wb.ActiveChart.FullSeriesCollection(2).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(2).AxisGroup = XlAxisGroup.xlSecondary
        Wb.ActiveChart.SeriesCollection.NewSeries
        Wb.ActiveChart.FullSeriesCollection(3).Name = "Presion Lubricacion"
        Wb.ActiveChart.FullSeriesCollection(3).Values = "=Tabla1[LubPres]"
        Wb.ActiveChart.FullSeriesCollection(3).XValues = "=Tabla1[Hora]"
        Wb.ActiveChart.FullSeriesCollection(3).AxisGroup = XlAxisGroup.xlSecondary
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).HasTitle = True
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Text = "PSI"
        Wb.ActiveChart.Axes(XlAxisType.xlValue, XlAxisGroup.xlSecondary).AxisTitle.Format.TextFrame2.TextRange.Font.Size = 12
        Wb.ActiveChart.ChartArea.Left = Wb.Worksheets("Bomba").Cells(1).Left
        Wb.ActiveChart.ChartArea.Top = Wb.Worksheets("Bomba").Cells(1).Top
        Wb.ActiveChart.SetElement(334)
        Wb.ActiveChart.ChartArea.Width = 1400
        Wb.ActiveChart.ChartArea.Height = 535
        Wb.ActiveChart.Axes(XlAxisType.xlCategory).TickLabelPosition = XlTickLabelPosition.xlTickLabelPositionLow
        Wb.ActiveChart.SetElement(Microsoft.Office.Core.MsoChartElementType.msoElementLegendRight)
    End Sub

End Class
