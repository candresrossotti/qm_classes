﻿Imports System.IO
Imports iText.IO.Image
Imports iText.Kernel.Geom
Imports iText.Kernel.Pdf
Imports iText.Layout
Imports iText.Layout.Element

Imports ChartDirector

Imports CsvHelper
Imports CsvHelper.Configuration
Imports iText.Forms
Imports System.Drawing

Public Class PDF

    ''Colores para los chart
    Private ReadOnly ColorBg40 As Integer = ColorFrom0BGR(Color.White.ToArgb())
    Private ReadOnly ColorBg60 As Integer = ColorFrom0BGR(Color.FromArgb(60, 60, 60).ToArgb())
    Private ReadOnly ColorBg192 As Integer = ColorFrom0BGR(Color.Black.ToArgb())

    Private ReadOnly CSV_CONFIGURATION As New CsvConfiguration(Globalization.CultureInfo.InvariantCulture)


    Private Shared _equipo() As String = {""}
    Public Shared Property Equipo() As String()
        Get
            Return _equipo
        End Get
        Set(value As String())
            If value IsNot _equipo Then
                _equipo = value
            End If
        End Set
    End Property
    Private Shared _yacimiento As String = "-"
    Public Shared Property Yacimiento() As String
        Get
            Return _yacimiento
        End Get
        Set(value As String)
            If value <> _yacimiento Then
                _yacimiento = value
            End If
        End Set
    End Property
    Private Shared _etapa As Integer = 0
    Public Shared Property Etapa() As Integer
        Get
            Return _etapa
        End Get
        Set(value As Integer)
            If value <> _etapa Then
                _etapa = value
            End If
        End Set
    End Property
    Private Shared _locacion As String = "-"
    Public Shared Property Locacion() As String
        Get
            Return _locacion
        End Get
        Set(value As String)
            If value <> _locacion Then
                _locacion = value
            End If
        End Set
    End Property
    Private Shared _tipopozo As String = "-"
    Public Shared Property TipoPozo() As String
        Get
            Return _tipopozo
        End Get
        Set(value As String)
            If value <> _tipopozo Then
                _tipopozo = value
            End If
        End Set
    End Property
    Private Shared _distrito As String = "-"
    Public Shared Property Distrito() As String
        Get
            Return _distrito
        End Get
        Set(value As String)
            If value <> _distrito Then
                _distrito = value
            End If
        End Set
    End Property
    Private Shared _provincia As String = "-"
    Public Shared Property Provincia() As String
        Get
            Return _provincia
        End Get
        Set(value As String)
            If value <> _provincia Then
                _provincia = value
            End If
        End Set
    End Property
    Private Shared _cliente As String = "-"
    Public Shared Property Cliente() As String
        Get
            Return _cliente
        End Get
        Set(value As String)
            If value <> _cliente Then
                _cliente = value
            End If
        End Set
    End Property
    Private Shared _csvpath As String = ""
    Public Shared Property CSVPath() As String
        Get
            Return _csvpath
        End Get
        Set(value As String)
            If value <> _csvpath Then
                _csvpath = value
            End If
        End Set
    End Property


    Public Sub SavePDF(ByVal Path As String, ByVal TINICIO As Date, ByVal TFIN As Date, ByVal MaximumPressure As Integer,
                       ByVal TotalFuel As Double, ByVal CaudalTotal As Double, ByVal BarrilesTotales As Double, ByVal NFrac As Integer, ByVal iCaudal() As Double, ByVal iBarriles() As Double)
        Try
            Dim StampingProp As New StampingProperties
            Dim TemplatePath As String = Directory.GetCurrentDirectory() + "\Template.pdf"

            If File.Exists(Path) Then
                File.Delete(Path)
            End If

            Dim pdfDoc As New PdfDocument(New PdfReader(TemplatePath), New PdfWriter(Path), StampingProp)
            Dim form As PdfAcroForm = PdfAcroForm.GetAcroForm(pdfDoc, True)
            Dim Doc As New Document(pdfDoc)

            Doc = AddGraph(Doc, pdfDoc)

            form.GetField("FECHA").SetValue(Today.ToShortDateString())

            form.GetField("BARRILESTOTALES").SetValue(BarrilesTotales.ToString("F1"))
            form.GetField("CAUDALTOTAL").SetValue(CaudalTotal.ToString("F1"))
            form.GetField("COMBUSTIBLETOTAL").SetValue(TotalFuel.ToString("F1"))
            form.GetField("PRESIONMAXIMA").SetValue(MaximumPressure)

            form.GetField("YACIMIENTO").SetValue(Yacimiento)
            form.GetField("ETAPA").SetValue(Etapa)
            form.GetField("LOCACION").SetValue(Locacion)
            form.GetField("TIPOPOZO").SetValue(TipoPozo)
            form.GetField("DISTRITO").SetValue(Distrito)
            form.GetField("PROVINCIA").SetValue(Provincia)
            form.GetField("CLIENTE").SetValue(Cliente)
            form.GetField("TINICIO").SetValue(TINICIO.ToLongTimeString)
            form.GetField("TFIN").SetValue(TFIN.ToLongTimeString())
            form.GetField("TOP").SetValue(TFIN.Subtract(TINICIO).ToString("hh\:mm\:ss"))

            For i = 1 To NFrac
                form.GetField("EQUIPO" + i.ToString()).SetValue(Equipo(i - 1))
                form.GetField("CAUDAL" + i.ToString()).SetValue(iCaudal(i - 1).ToString("F1"))
                form.GetField("BARRILES" + i.ToString()).SetValue(iBarriles(i - 1).ToString("F1"))
            Next

            form.FlattenFields()
            Doc.Close()
            pdfDoc.Close()
        Catch ex As Exception
            LogError(ex)
        End Try
    End Sub

    Private Function AddGraph(ByVal Doc As Document, ByVal pdfDoc As PdfDocument) As Document
        Dim NewDoc As Document = Doc
        Dim NewPdfDoc As PdfDocument = pdfDoc
        Dim ChartImg As Element.Image

        Dim CSV_STREAMREADER As New StreamReader(CSVPath)
        Dim CSV_READER As New CsvReader(CSV_STREAMREADER, CSV_CONFIGURATION)
        CSV_READER.Read()

        Dim Hora = ""
        Dim CaudalTotal = ""
        Dim CombTotal = ""
        Dim Presion = ""
        Dim Barriles = ""

        Dim AllRecords = New With {Key Hora, Key CaudalTotal, Key CombTotal, Key Presion, Key Barriles}

        Dim HoraL As New List(Of Date)
        Dim CaudalL As New List(Of Double)
        Dim CombL As New List(Of Double)
        Dim PresionL As New List(Of Double)
        Dim BarrilesL As New List(Of Double)
        Dim KeepReading As Boolean = True
        Do Until KeepReading = False
            Dim Records = CSV_READER.GetRecord(AllRecords)
            If Records Is Nothing Then Return NewDoc
            KeepReading = CSV_READER.Read()
            HoraL.Add(Date.Parse(Records.Hora))
            CaudalL.Add(GetDouble(Records.CaudalTotal))
            CombL.Add(GetDouble(Records.CombTotal))
            PresionL.Add(GetDouble(Records.Presion))
            BarrilesL.Add(GetDouble(Records.Barriles))
        Loop


        ''Definimos 3 ejes secundarios
        Dim LeftAxis As Axis
        Dim LeftAxis1 As Axis
        Dim LeftAxis2 As Axis
        Dim LeftAxis3 As Axis

        ''Definicion de las lineas
        Dim Layer(3) As SplineLayer

        ''Agregamos el CHART
        Dim c As New XYChart(1920, 1040, ColorBg40, ColorBg40, 0)

        c.setBorder(ColorBg60)
        '================================================================================
        'Se setea el inicio y tamaño del area a graficar, junto con sus colores
        '================================================================================
        c.setPlotArea(180, 40, 1560, 960, -1, -1, ColorBg192, ColorBg60, ColorBg60)
        '================================================================================
        'Set clipping para la informacion que cae fuera del grafico
        '================================================================================
        c.setClipping()
        '================================================================================
        'Titulo del graficador
        '================================================================================
        Dim title As TextBox = c.addTitle(8, "Fractura", "Montserrat", 22, ColorBg192)
        '================================================================================
        'Seteamos estilo al eje X
        '================================================================================
        Dim AxisWidth As Integer = 2

        c.xAxis().setLabelStyle("Montserrat", 16)
        c.xAxis().setTickDensity(75, 15)
        c.xAxis().setWidth(AxisWidth)
        c.xAxis().setTickWidth(AxisWidth)
        c.xAxis.setColors(ColorBg192, ColorBg192, ColorBg192, ColorBg192)
        '================================================================================
        'Agregamos tres ejes ademas del primero
        '================================================================================
        LeftAxis = c.addAxis(Chart.Left, 0)
        Dim yAxis As TextBox = LeftAxis.setTitle("Presión [PSI]")
        yAxis.setAlignment(Chart.Left)
        yAxis.setFontStyle("Montserrat")
        yAxis.setFontSize(14)
        LeftAxis.setWidth(AxisWidth)
        LeftAxis.setTickWidth(AxisWidth)
        LeftAxis.setLabelStyle("Montserrat", 14, ColorBg192)
        LeftAxis.setColors(ColorFrom0BGR(Color.Red.ToArgb), ColorFrom0BGR(Color.Red.ToArgb), ColorFrom0BGR(Color.Red.ToArgb), ColorFrom0BGR(Color.Red.ToArgb))
        LeftAxis.setTitlePos(Chart.Left, 0)
        LeftAxis.setTickDensity(30)

        LeftAxis1 = c.addAxis(Chart.Left, 95)
        Dim yAxis1 As TextBox = LeftAxis1.setTitle("Caudal [BBL/MIN]")
        yAxis1.setAlignment(Chart.Left)
        yAxis1.setFontStyle("Montserrat")
        yAxis1.setFontSize(14)
        LeftAxis1.setWidth(AxisWidth)
        LeftAxis1.setTickWidth(AxisWidth)
        LeftAxis1.setLabelStyle("Montserrat", 14, ColorBg192)
        LeftAxis1.setColors(ColorFrom0BGR(Color.Green.ToArgb), ColorFrom0BGR(Color.Green.ToArgb), ColorFrom0BGR(Color.Green.ToArgb), ColorFrom0BGR(Color.Green.ToArgb))
        LeftAxis1.setTitlePos(Chart.Left, 0)
        LeftAxis1.setTickDensity(30)

        LeftAxis2 = c.addAxis(Chart.Right, 0)
        Dim yAxis2 As TextBox = LeftAxis2.setTitle("Combustible [GAL]")
        yAxis2.setAlignment(Chart.Left)
        yAxis2.setFontStyle("Montserrat")
        yAxis2.setFontSize(14)
        LeftAxis2.setWidth(AxisWidth)
        LeftAxis2.setTickWidth(AxisWidth)
        LeftAxis2.setLabelStyle("Montserrat", 14, ColorBg192)
        LeftAxis2.setColors(ColorFrom0BGR(Color.Blue.ToArgb), ColorFrom0BGR(Color.Blue.ToArgb), ColorFrom0BGR(Color.Blue.ToArgb), ColorFrom0BGR(Color.Blue.ToArgb))
        LeftAxis2.setTitlePos(Chart.Right, 0)
        LeftAxis2.setTickDensity(30)
        LeftAxis2.setLinearScale(0, CombL.Max * 1.3)

        LeftAxis3 = c.addAxis(Chart.Right, 95)
        Dim yAxis3 As TextBox = LeftAxis3.setTitle("Barriles [BBL]")
        yAxis3.setAlignment(Chart.Left)
        yAxis3.setFontStyle("Montserrat")
        yAxis3.setFontSize(14)
        LeftAxis3.setWidth(AxisWidth)
        LeftAxis3.setTickWidth(AxisWidth)
        LeftAxis3.setLabelStyle("Montserrat", 14, ColorBg192)
        LeftAxis3.setColors(ColorFrom0BGR(Color.Fuchsia.ToArgb), ColorFrom0BGR(Color.Fuchsia.ToArgb), ColorFrom0BGR(Color.Fuchsia.ToArgb), ColorFrom0BGR(Color.Fuchsia.ToArgb))
        LeftAxis3.setTitlePos(Chart.Right, 0)
        LeftAxis3.setTickDensity(30)
        LeftAxis3.setLinearScale(0, BarrilesL.Max * 1.5)
        '================================================================================
        'El eje principal Y lo dejamos transparente
        '================================================================================
        c.yAxis().setColors(Chart.Transparent, Chart.Transparent, Chart.Transparent, Chart.Transparent)

        '================================================================================
        'Agregamos todas las lineas a dibujar. Las asociamos al eje que queremos, y seteamos su ancho.
        '================================================================================

        Layer(0) = c.addSplineLayer()
        Layer(0).setUseYAxis(LeftAxis)
        Layer(0).setLineWidth(AxisWidth)
        Layer(0).setFastLineMode()
        Layer(0).setXData(HoraL.ToArray())
        Layer(0).addDataSet(PresionL.ToArray(), ColorFrom0BGR(Color.Red.ToArgb), "Presion")

        Layer(1) = c.addSplineLayer()
        Layer(1).setUseYAxis(LeftAxis1)
        Layer(1).setLineWidth(AxisWidth)
        Layer(1).setFastLineMode()
        Layer(1).setXData(HoraL.ToArray())
        Layer(1).addDataSet(CaudalL.ToArray(), ColorFrom0BGR(Color.Green.ToArgb), "Caudal")

        Layer(2) = c.addSplineLayer()
        Layer(2).setUseYAxis(LeftAxis2)
        Layer(2).setLineWidth(AxisWidth)
        Layer(2).setFastLineMode()
        Layer(2).setXData(HoraL.ToArray())
        Layer(2).addDataSet(CombL.ToArray(), ColorFrom0BGR(Color.Blue.ToArgb), "Combustible")

        Layer(3) = c.addSplineLayer()
        Layer(3).setUseYAxis(LeftAxis3)
        Layer(3).setLineWidth(AxisWidth)
        Layer(3).setFastLineMode()
        Layer(3).setXData(HoraL.ToArray())
        Layer(3).addDataSet(BarrilesL.ToArray(), ColorFrom0BGR(Color.Fuchsia.ToArgb), "Barriles")

        ''================================================================================
        '' Configuracion de la escala de X y las leyendas
        ''================================================================================
        c.xAxis.setDateScale(HoraL(0), HoraL(HoraL.Count - 1))

        c.xAxis().setFormatCondition("align", 360 * 86400)
        c.xAxis().setLabelFormat("{value|yyyy}")

        c.xAxis().setFormatCondition("align", 30 * 86400)
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(), "<*font=bold*>{value|mmm yyyy}",
            Chart.AllPassFilter(), "{value|mmm}")

        c.xAxis().setFormatCondition("align", 86400)
        c.xAxis().setMultiFormat(Chart.StartOfYearFilter(),
            "<*block,halign=left*><*font=bold*>{value|mmm dd<*br*>yyyy}", Chart.StartOfMonthFilter(),
            "<*font=bold*>{value|mmm dd}")
        c.xAxis().setMultiFormat2(Chart.AllPassFilter(), "{value|dd}")

        c.xAxis().setFormatCondition("align", 3600)
        c.xAxis().setMultiFormat(Chart.StartOfDayFilter(), "<*font=bold*>{value|hh:nn<*br*>mmm dd}",
            Chart.AllPassFilter(), "{value|hh:nn}")

        c.xAxis().setFormatCondition("align", 60)
        c.xAxis().setLabelFormat("{value|hh:nn}")

        c.xAxis().setFormatCondition("else")
        c.xAxis().setLabelFormat("{value|hh:nn:ss}")

        c.xAxis().setMinTickInc(2)

        ''================================================================================
        '' Configuracion de leyendas
        ''================================================================================
        ' Clear the current dynamic layer and get the DrawArea object to draw on it.
        Dim d As DrawArea = c.initDynamicLayer()
        ' The plot area object
        Dim plotArea As PlotArea = c.getPlotArea()


        Dim legendEntries As ArrayList = New ArrayList()

        ' Iterate through all layers to build the legend array
        For i As Integer = 0 To c.getLayerCount() - 1
            Dim layer1 As Layer = c.getLayerByZ(i)


            ' Iterate through all the data sets in the layer
            For j As Integer = 0 To layer1.getDataSetCount() - 1
                Dim dataSet As ChartDirector.DataSet = layer1.getDataSetByZ(j)

                ' We are only interested in visible data sets with names
                Dim dataName As String = dataSet.getDataName()
                Dim color As Integer = dataSet.getDataColor()
                If (Not String.IsNullOrEmpty(dataName)) And (color <> Chart.Transparent) Then
                    ' Build the legend entry, consist of the legend icon, name and data value.
                    legendEntries.Add("<*block*>" & dataSet.getLegendIcon() & " " & dataName)
                End If
            Next
        Next

        ' Create the legend by joining the legend entries
        legendEntries.Reverse()
        Dim legendText As String = "<*block,maxWidth=" & plotArea.getWidth() &
            "*><*block*><*font=Montserrat,Color=" + Hex(ColorBg192) + "*>" & Join(CType(legendEntries.ToArray(GetType(String)), String()), "        ") &
            "<*/*>"

        ' Display the legend on the top of the plot area
        Dim y As TTFText = d.text(legendText, "Montserrat", 12)
        y.draw(plotArea.getLeftX() + 5, plotArea.getTopY() - 3, (ColorBg192), Chart.BottomLeft)

        '================================================================================
        ' Agregamos el chart al pdf
        '================================================================================
        Dim Im = c.makeImage()

        Dim BitmapIm As New Bitmap(Im)

        ''Esto es para sacar la franja que dice que no compramos el software
        For i = 0 To 1920 - 1
            For j = 1040 - 10 To 1040 - 1
                If (j = 1040 - 1) Or (i = 0) Or (i = 1920 - 1) Then
                    BitmapIm.SetPixel(i, j, Color.Black)
                Else
                    BitmapIm.SetPixel(i, j, Color.White)
                End If
            Next
        Next

        ChartImg = New Element.Image(ImageDataFactory.Create(BitmapIm, Color.Transparent))
        ChartImg = ChartImg.Scale(0.42, 0.42).SetFixedPosition(4, 25, 18).SetRotationAngle(Math.PI / 2)
        NewDoc.Add(ChartImg)
        Return NewDoc
    End Function
End Class