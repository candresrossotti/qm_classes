﻿Public Class MoxaHandle

    Private Shared ReadOnly devicePORT As Integer = 502
    Public Shared MSocket As ModbusClient = Nothing
    Private Enable As Boolean = True


    ''' <summary>
    ''' Funcion para escanear una IP
    ''' </summary>
    Public Function AutoScan(ByVal deviceIP As String) As Boolean
        Try
            If My.Computer.Network.IsAvailable Then
                If My.Computer.Network.Ping(deviceIP, 10) Then
                    Return True
                End If
            Else
                avisoStr = "No se conecto a ninguna red"
                avisoTitulo = "Error"
                Dim AG As New AvisoGeneral
                AG.ShowDialog()
            End If
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = "Ocurrio un error en la deteccion del equipo"
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            'Return False
        End Try
        Return False
    End Function

    ''' <summary>
    ''' Funcion para rehabilitar todo
    ''' </summary>
    Public Sub Release()
        Enable = True

    End Sub


    ''' <summary>
    ''' Funcion para conectarse a un fracturador.
    ''' </summary>
    ''' <param name="IP">IP del equipo.</param>
    '''  <remarks></remarks>
    Public Function Connect(ByVal IP As String) As Boolean
        Dim state As Boolean
        Try
            MSocket = New ModbusClient With {
                .ConnectionTimeout = 5000,
                .IPAddress = IP,
                .Port = devicePORT,
                .NumberOfRetries = 3
            }
            MSocket.Connect()
            Return MSocket.Connected
        Catch ex As Exception
            LogError(ex)
            avisoTitulo = "Error"
            avisoStr = "Se produjo un error en la conexión."
            Dim AG As New AvisoGeneral
            AG.ShowDialog()
            state = False
            Return state
        End Try
    End Function


    ''' <summary>
    ''' Funcion para leer datos del moxa.
    ''' </summary>
    ''' <param name="startAddressRX">Address inicial donde se van a leer los datos.</param>
    ''' <param name="numberOfPoints">Cantidad de datos a leer.</param>
    '''  <remarks></remarks>
    Public Function MoxaRead(startAddressRX As Integer, numberOfPoints As Integer) As Integer()
        Try
            If Enable Then
                Return MSocket.ReadHoldingRegisters(startAddressRX, numberOfPoints)
            End If
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'AG.ShowDialog()
            ' Enable = False
        End Try
        Return {}
    End Function



    ''' <summary>
    ''' Funcion para enviar datos al moxa.
    ''' </summary>
    ''' <param name="startAddressTX">Address inicial donde se van a escribir los datos.</param>
    ''' <param name="data">Datos a escribir.</param>
    '''  <remarks></remarks>
    Public Function MoxaSend(startAddressTX As Integer, data As Integer) As Boolean
        Try
            If Enable Then
                MSocket.WriteSingleRegister(startAddressTX, data)
                Return True
            End If
        Catch ex As Exception
            LogError(ex)
            'avisoTitulo = "Error"
            'avisoStr = ex.Message
            'Dim AG As New AvisoGeneral
            'Enable = False
            'AG.ShowDialog()
        End Try
        Return False
    End Function


    Public Function SocketAvailable() As Boolean
        Return MSocket.Available(2000)
    End Function
End Class
