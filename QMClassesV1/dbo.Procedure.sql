﻿CREATE PROCEDURE [dbo].[Save]
      @saveSett FracDB READONLY
AS
BEGIN
      SET NOCOUNT ON;
     
      INSERT INTO FracDB(Id, FracName,FracID,FracID,FracEngine,FracTransmission,FracPump)
      SELECT Id, FracName,FracID,FracID,FracEngine,FracTransmission,FracPump FROM @saveSett
END
